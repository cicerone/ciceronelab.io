---
layout: page
title: Postagens por tags
permalink: /tags/
---

Esta é uma lista de todas as postagens feitas na _Biblioteca do Cicerone_ separadas por tags. Para listar as postagens cronologicamente, [clique aqui](https://cicerone.gitlab.io).

1. Índice
{:toc}

{% for tag in site.tags %}
  {% assign t = tag | first %}
  {% assign posts = tag | last %}

<h4><a name="{{t | downcase | replace:" ","-" }}"></a>{{ t | replace:"-"," " }}</h4>

<ul>
{% for post in posts %}
  {% if post.tags contains t %}
  <li>
    <a href="{{ post.url }}">{{ post.title }}</a>
    <span class="date">{{ post.date | date: "%d/%m/%Y, às %T"  }}</span>
  </li>
  {% endif %}
{% endfor %}
</ul>

<hr>

{% endfor %}
