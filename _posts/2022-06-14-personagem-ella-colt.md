---
layout: post
title: "Ella Colt (ela/dela), um personagem para Fate of Umdaar"
lang: pt-BR
date: 2022-06-14 09:30:00 -0300
tags: RPG Fate Evil-Hat Masters-of-Umdaar Fate-of-Umdaar-playtest Fate-Condensado Personagens Exemplo
---

Cansei de escrever RPGs minimalistas por um tempo, até porque as pessoas precisam de tempo para os lerem. Então decidi voltar à criação de personagens para o playtest de _Fate of Umdaar_. Lembrando: as regras usadas são as disponíveis no material distribuído pela Evil Hat para os participantes do playtest, e que este personagem pode se tornar incompatível com a versão final.

## Ella Colt (ela/dela)

Ella nasceu em Porto Rascamar, uma cidade portuária famosa por ser um antro de criminosos e autoridades corruptas e dispostas a olhar para outro lado em troca de algumas moedas. Ela não é governada nem faz parte do domínio de nenhum dos Mestres de Umdaar, pois existe um acordo coletivo entre eles, embora jamais firmado explicitamente, de que todos os Mestres da região poderiam usar a cidade portuária para conduzir seus negócios sem interferências. Não é de se estranhar, então, que a criminalidade seja altíssima, e a lei e a ordem, subservientes aos barões do crime locais.

Visando dar uma vida melhor para a própria família, a família de Ella fugiram de Porto Rascamar com a roupa do corpo apenas. Temendo sofrer represálias, eles nunca ficaram muito tempo no mesmo lugar durante dois anos, até se estabelecerem em Altamar, uma outra cidade portuária porém capital de uma das _Terras Livres_ Lá, Ella ficou impressionada com o valor que a lei, a ordem e a justiça recebiam, e decidiu se tornar uma _xerfie_[^marshall] a fim de levar a lei e a ordem para todos aqueles que precisassem dela.

Ella é uma mulher de cabelos negros curtos e olhos verdes que parecem estar semicerrados. Sua voz é rouca, denunciando alguma doença respiratória do passado que, felizmente, já foi curada. Suas roupas são adequadas ao ambiente em que ela se encontra: ela sabe igualmente se vestir para um baile de alta sociedade quanto para investigar um covil de criminosos sujos e carniceiros. Porém, não importa que roupa ela esteja usando, ela sempre terá consigo a _Pacificadora_, sua arma pessoal.


**Nome**: Ella Colt (ela/dela)  
**Recarga**: 3

| **Aspectos** | |
| ---: | :--- |
| ***Conceito***| Xerife[^marshall] humana com olhar clínico para política |
| ***Motivação*** | Eu devo proteger os fracos e oprimidos usando a lei |
| ***Origens*** | Um ícone de justiça em um antro de criminosos |
| ***Relacionamento*** | |
| ***Aspecto Livre*** | |

| **Perícias** | | | | |
| ---: | :--- | :--- | :--- | :--- |
| ***Ótimo (+4)*** | Investigação | | | |
| ***Bom (+3)*** | Comunicação | Contatos | | |
| ***Razoável (+2)*** | Atirar | Atletismo | Percepção | |
| ***Regular (+1)*** | Vigor | Empatia | Vontade | Furtividade |

| **Façanhas** | |
| ---: | :--- |
| **Vamos fazer um acordo** | Ella recebe +2 em _Comunicação_ quando estiver tentando obter informações de outras pessoas enquanto honestamente oferecer alguma coisa que aquela pessoa queira em troca da cooperação. |
| **Diga _oi_ à minha amiguinha!** | Quando Ella ameaçar alguém com uma arma carregada, ela pode usar _Atirar_ ao invés de _Provocar_ para intimidar alguém. |
| **Servir e Proteger** | Quando um ataque físico atingir uma pessoa que esteja perto de Ella, ela pode pular na frente do ataque e marcar uma de suas próprias caixas de estresse para absorver as tensões de dano provocadas pelo ataque. |


| **Estresses e Consequências** | |
| ---: | :--- |
| ***Estresse Físico*** | [ 1 ] [ 1 ] [ 1 ] [ 1 ] |
| ***Estresse Mental*** | [ 1 ] [ 1 ] [ 1 ] [ 1 ] |
| ***Consequência Suave (2)*** | |
| ***Consequência Moderada (4)*** | |
| ***Consequência Severa (6)*** | |

---

[^marshall]: Tradução livre para o termo _marshall_.
