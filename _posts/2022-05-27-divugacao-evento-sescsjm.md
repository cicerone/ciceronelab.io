---
layout: post
title: "Divulgação: primeiro evento aberto do Workshop Formando RPGistas"
lang: pt-BR
date: 2022-05-27 10:30:00 -0300
tags: RPG Divulgação Workshop Eventos RPGWorld Lampião-Game-Studio SESC SESC-São-João-de-Meriti Fate-of-Umdaar-playtest
---

Moradores do Grande Rio, atenção: dia 28/05/2022 vai ter o segundo encontro do [_Workshop Formando RPGistas_](https://cicerone.gitlab.io/2022/05/25/workshop-formando-rpgistas.html), uma parceiria entre o SESC São João de Meriti, o RPGWorld e o Lampião Game Studio para divulgar o RPG na Baixada Fluminense.

![flyer-workshop](/assets/workshop-formando-rpgistas/divulgacao.jpeg)

Este encontro será uma _oficina prática_, isto é, um evento RPG com mesas e narradores e espaço para as pessoas jogarem e se divertirem com o hobby. Confira abaixo as mesas confirmadas para o dia 28/05/2022:

| **Sistema** | **Narrador** | **Horário** |
| :---: | :---: | :---: |
| Tormenta T20 | Guilherme | 10-13h |
| Laser e Sentimentos | Cesar | 10-13h |
| Old Dragon | Cesar | 14-17h |
| Fate of Umdaar | Lu Cavalheiro | 10-13h |
| Icons | Eduardo | 10-13h |
| Jogos narrativos (cartas e dados) | Jorge Valpaços | 10-17h |
| Boardgames e mestre reserva | Rafael Wernek | 10-17h |

Quem estiver disposto e disponível, apareça! Vai ser um prazer imenso vê-los por lá.

---

