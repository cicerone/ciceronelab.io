---
layout: post
title: "Corrun Falt: um personagem para Fate Condensado"
lang: pt-BR
date: 2022-06-22 22:20:00 -0300
tags: RPG Fate Evil-Hat Fate-Condensado Personagens Exemplo CC-BY-SA-4.0-Internacional
---

Sejam bem-vindos a mais uma postagem aqui no site! Hoje eu dei uma pausa no processo de criação de jogos e aventuras para postar um personagem para Fate Condensado, a fim de que vocês possam usá-los à vontade.

Diferente das outras paradas postadas aqui no site, este personagem está licenciado sob **Licença Creative Commons Atribuição-CompartilhaIgual 4.0 Internacional**, cujo resumo em português para leigos pode ser lido em <https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR>


## Corrun Falt (ele/dele)

Corrun era membro de uma força paramilitar de um ricaço até ser pego saindo com a esposa do chefe. Porém, ele não era um soldado, mas um especialista em inteligência -- um nome bonito para dizer que ele é especialista em invasão de sistemas de segurança. Corrun se gaba constantemente de que não existe sistema de segurança ou de computadores que ele não consiga invadir com um pé nas costas e o outro na testa.

Ele é um homem careca com uma tatuagem tribal cobrindo a metade direita do rosto. Ele não é muito musculoso, mas gosta de usar roupas apertadas para parecer mais forte do que ele é. Seus olhos são negros, e seu cavanhaque castanho dá a ele um certo ar malandro e malicioso que combina muito bem com sua personalidade. Corrun não é avesso a obter vantagem de quem quer que seja, e apenas as pessoas em quem ele aprendeu a confiar estão seguras dos golpes que ele aplicaria sem pensar nem duas vezes nas demais. Ele é capaz de vender a própria mãe, não a entregar e ainda processar o reclamante por propaganda enganosa.

| **Aspectos** | | 
| ---: | :--- |
| ***Conceito*** | Hacker ex-paramilitar metido a malandro | 
| ***Dificuldade*** | Acha que quase todo mundo é otário | 
| ***Relacionamento*** |  | 
| ***Aspecto Livre***  |  | 
| ***Aspecto Livre***  |  | 

| **Perícias**
| ---: | :--- | :--- | :--- | :--- |
| ***Ótimo (+4)*** | Roubo | | | |
| ***Bom (+3)*** | Enganar | Percepção | | |
| ***Razoável (+2)*** | Provocar | Investigar | Contatos | |
| ***Regular (+1)*** | Recursos | Atirar | Vontade | Comunicação |

**Façanhas**

- ***Rosto Confiável***: Se Corrun for bem sucedido em usar *Enganar* contra alguém, ele pode usar *Enganar* no lugar de *Empatia* para discernir segredos e mentiras desse alguém, contanto que ele continue falando com Corrun e não perceba as mentiras que lhe foram contadas.
- ***Mãos ágeis***: Corrun têm mãos tão ágeis que são capazes de bloquear ataques. Ele pode usar *Roubo* ao invés de *Atletismo* ou *Lutar* para bloquear ataques feitos com *Lutar* (mas não com *Atirar*).
- ***Destruir é mais fácil sempre***: Corrun recebe +2 em **Ações de Superar** por *Roubo* se ele pretender destruir um dispositivo que ele possa acessar remotamente com seu tecnopad.

**Estresses e Consequências**

- ***Estresse Físico***: [ 1 ] [ 1 ] [ 1 ]
- ***Estresse Mental***: [ 1 ] [ 1 ] [ 1 ] [ 1 ]

**Consequências**

- ***Suave (2)***
- ***Moderada (4)***
- ***Severa (6)***

---

