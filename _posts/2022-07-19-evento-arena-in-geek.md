---
layout: post
title: "Divulgação: dia 24/07/2022 tem o Arena in Geek na Lona Cultural de Anchieta!"
lang: pt-BR
date: 2022-07-19 07:10:00 -0300
tags: RPG Divulgação Eventos RPGWorld Arena-in-Geek Fate Fate-Condensado
---

Moradores do Grande Rio, atenção: dia 24/07/2022 terá o **Arena in Geek** na Lona Cultural de Anchieta (Estrada Marechal Alencastro, s/n, Anchieta, Rio de Janeiro/RJ). Vai ser um evento com um monte de coisa maneira acontecendo, como você pode ver na imagem abaixo:

![flyer-evento](/assets/eventos/arena-in-geek/2022-07-24.jpg)

Irei com o pessoal do [RPGWorld](https://www.facebook.com/rpgworldsite), e serei eu a narrar a mesa de **Fate Condensado** por lá. A aventura é autoral, e será baseada em um conto que escrevi há alguns anos, _A Dama de Mil Faces_, que estou em processo de republicar e disponibilizar para vocês por meio da minha lojinha, <https://lucavalheiro.itch.io>. 

**Atenção**: não é um evento gratuito! O ingresso é R$ 10,00 antecipado ou R$ 15,00 na hora. Entre na [página do evento no Facebook](https://www.facebook.com/events/1176924039758781/) para saber como conseguir o ingresso antecipado.

Espero vocês lá!

---

