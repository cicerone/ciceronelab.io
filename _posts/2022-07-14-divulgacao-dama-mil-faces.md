---
layout: post
title: "Em breve: o conto 'A Dama de Mil Faces' (versão publicada pela Andross Editora) no itch.io"
lang: pt-BR
date: 2022-07-14 12:10:00 -0300
tags: Geral Divulgação itch.io Lojinhas Literatura Contos Fantasia
---

Não falta muito para meu conto _A Dama das Mil Faces_ (a versão publicada pela Andross Editora) estar disponível para aquisição em <https://lucavalheiro.itch.io> por um precinho bem módico! Acompanhe minhas publicações para o tão esperado anúncio!

Para vocês ficarem com gostinho na boca, eis a capa, arte de [Sandy de Oliveira](https://www.instagram.com/s_artteee/)

![capa-dama-mil-faces](/assets/literatura/contos/dama-mil-faces/dama-mil-faces.jpg)

---

