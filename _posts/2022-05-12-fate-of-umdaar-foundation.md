---
layout: post
title: "Fate of Umdaar: Fundação, um modelo de sessão zero"
lang: pt-BR
date: 2022-05-13 11:20:00 -0300
tags: RPG Fate Evil-Hat Masters-of-Umdaar Fate-of-Umdaar-playtest
---

Na postagem [Fate of Umdaar: primeiras impressões sobre o playtest](https://cicerone.gitlab.io/2022/05/12/fate-of-umdaar-um-playtest.html), apresentei uma visão geral sobre o _Fate of Umdaar_, que no momento da escrita deste texto encontra-se em playtest. Sinto, porém, que uma visão geral não esgotou tudo que há para ser dito sobre o jogo, então decidi escrever mais algumas coisas sobre os pontos específicos que mais me chamaram a atenção no _Fate of Umdaar_.

Para quem me conhece do _podcast_ [Fate Masters](https://fatemasters.gitlab.io), sabe que uma das minhas bandeiras favoritas é a defesa e a realização da _sessão zero_. Resumidamente, a sessão zero é o momento em que o grupo de jogadores senta para discutir e combinar questões pertinentes ao jogo, desde a regulação das relações interpessoais entre eles (o famoso _contrato social_) até a criação conjunta de elementos de cenário. Isso ajuda a reduzir ruídos entre os jogadores, dá diretrizes sobre como lidar com situações desconfortáveis, e garante que todos estejam na mesma página com relação às expectativas para o jogo.

Em _Fate of Umdaar_, a sessão zero é chamada de _Fundação_[^foundation], e ela é considerada um passo essencial. Nas palavras do autor:

[^foundation]: Tradução livre do termo _Fundação_. Não relacionado à série de contos e aos romances de Isaac Asimov.

> Any game of Fate of Umdaar starts the same way: by laying out the Foundation.  
> [Tradução livre] _Todo jogo de Fate of Umdaar começa da mesma maneira: estabelecendo a Fundação._

Como toda sessão zero, a Fundação em _Fate of Umdaar_ visa estabelecer elementos de segurança para os jogadores, regras de convívio social e criar conjuntamente o jogo com o qual todos pretendem se divertir por algumas sessões de jogo. O _Fate of Umdaar_ apresenta a Fundação bem estruturada, com cada conjunto de discussões pertinentes divididas em _fases_, e falarei um pouco sobre cada uma delas a seguir.

## Fase 1: Limites

Nesta fase são estabelecidos os limites, tanto entre os jogadores, como para o mundo de jogo. Esses limites são agrupados em _ferramentas de segurança_, _temas e tons_, _ajustes_ e _aspectos de mundo_.

### Ferramentas de segurança

Ferramentas de segurança são recursos que visam garantir que todos na mesa sintam-se seguros e confortáveis, tanto uns com os outros, quanto com o jogo que estão jogando. São elas que permitem a um jogador estabelecer quais temas são desconfortáveis ou mesmo traumáticos para ele e, portanto, não devem ser abordados em jogo, e são essas mesmas ferramentas que permitem ao jogador expor-se sem temer ridicularização ou outras formas de julgamento. É claro, como é impossível compilar uma lista com todos os problemas que poderão surgir, as ferramentas também preveem meios para lidar com questões inesperadas que surjam ao longo do jogo.

O _Fate of Umdaar_ apresenta quatro ferramentas de segurança famosas e já estabelecidas entre os adeptos do jogo seguro: a _X-Card_, de John Stavropoulos e disponível em <http://tinyurl.com/x-card-rpg>; o _Script Change RPG Toolbox_, de Brie Beau Sheldon e disponível em <http://tinyurl.com/nphed7m>; os _Limites e Véus_, discutidos em <https://rpg.stackexchange.com/questions/30906/what-do-the-terms-lines-and-veils-mean>; e a _Verificação de Temperatura_, explicada com mais detalhes no _Fate of Umdaar_, mas que pode ser resumido como uma conversa regular e franca entre os envolvidos sobre se as coisas estão confortáveis para todos.

### Temas e tons

O grupo deve discutir quais serão os temas e tons adotados para suas sessões de jogo. Alguns grupos vão preferir algo mais adulto, outros vão preferir um ritmo mais desenho animado dos anos 80. Desde que todos estejam na mesma página, tudo está certo e nada está errado.

### Ajustes

_Fate of Umdaar_ permite uma série de ajustes próprios, além daqueles que podem ser feitos diretamente no sistema de regras. Para quem não é exatamente familiar com o conceito, _ajustes_ são regulagens feitas nas regras, seja por alteração, inclusão ou exclusão, que permitem adequar o jogo aos gostos do grupo de jogadores. Em alguns casos, podem ser a adoção de regras opcionais ao jogo que, por terem ampla repercussão no sistema como um todo, demandam consenso de mesa.

### Aspectos de mundo

Em _Aspectos do mundo_, discute-se não apenas os aspectos do mundo propostos pelo _Fate of Umdaar_ para o jogo, como os jogadores são estimulados a pensar em quais aspectos de mundo eles gostariam de criar e usar. Uma discussão especialmente importante para reforçar uma visão descolonizada entre os participantes.

## Fase 2: Estrutura

A _Estrutura_ do jogo diz respeito ao modelo das sessões. As escolhas aqui são _Independente_[^standalone], a famosa _one-shot_; ou _Serializada_[^long_form], com vários arcos narrativos de alguma forma interligados entre si. No caso de criação de um jogo _serializado_, apresenta-se também algumas discussões úteis na hora de elaborar a estrutura geral da série.

[^standalone]: Tradução livre do termo _standalone_.

[^long_form]: Tradução livre do termo _long form_.

## Fase 3: Foco

Como discutido no [artigo anterior sobre o playtest de _Fate of Umdaar_](https://cicerone.gitlab.io/2022/05/12/fate-of-umdaar-um-playtest.html), existem três focos possíveis: _Dignatários_, _Rebeldes_ e _Arqueonautas_. Apesar de não ser obrigatório adotar um foco, seja por misturar elementos dos três, seja por querer explorar algo menos formulaico, adotar um ajuda muito a manter o foco do planejamento do jogo, e por isso é altamente recomendado fazer uma escolha aqui.

_Dignatários_ são os diplomatas, emissários, nobres e cavaleiros que pretendem unir e proteger as _Freelands_. Trata-se de um jogo mais voltado para personagens sociais e para jogadores que gostam de jogos mais focados nas interações entre os personagens e menos focados nas interações entre as armas deles.

_Rebeldes_ são insurgentes que vivem nos _Dread Domains_ e estão dispostos a fazer o necessário para derrubar os Mestres e libertar a região de dentro para fora. É um foco mais centrado em combates, e ao o escolher o grupo dá essa direção geral ao jogo como um todo.

_Arqueonautas_ são exploradores, aventureiros que cruzam os _Wildernexues_ para encontrar, catalogar e restaurar ruínas ou artefatos do Demiurgo, e ocasionalmente estabelecer os primeiros contatos com as comunidades isoladas encontradas nesses locais. Este foco é mais recomendado para grupos cujo maior prazer é explorar e desvendar os mistérios do mundo ao redor deles, e não raramente os jogadores terão grande impacto na criação do cenário como um todo.

## Fase 4: Locais

Nesta fase, serão criados os locais que serão importantes para a narrativa. Isso não apenas dá aos jogadores a chance para moldarem o mundo de _Fate of Umdaar_ de acordo com seus gostos, como cria as comunidades com as quais os personagens possuirão alguma ligação. Com isso, os personagens evitam em duas frentes o arquétipo do _Grande Salvador Branco_, pois eles deixam de ser retratados tanto como forasteiros sábios que vieram civilizar os locais, quanto como assassinos e saqueadores errantes cujo único propósito na vida é matar, pilhar, destruir e votar no Bolsonaro.

Narrativas _independentes_ vão precisar de menos locais do que narrativas _serializadas_, mas ainda assim é bom que existam pelo menos uma comunidade relacionada ao problema. Um bom exemplo disso vem de, acreditem se quiser, _Indiana Jones e o Templo da Perdição_, onde, embora ele ainda seja um _Grande Salvador Branco_, Indy cai de para-quedas (quase que literalmente) em uma comunidade local, que pede para ele resgatar um artefato importantíssimo que foi roubado por Mola Ram e seus seguidores de Kali Ma. No filme temos o Tibet (onde ele conhece a _Indy Lady_ do dia), a comunidade que teve seu artefato roubado, o palácio do Marajá (com a icônica cena de degustação de cérebros de macacos), a mina e o templo de Mola Ram. Depois de um bom banho para limpar os arquétipos e estereótipos colonialistas, esse filme bem serve como modelo para uma narrativa _independente_ focada em arqueonautas!

Narrativas _serializadas_, entretanto, demandam mais trabalho. Afinal de contas, a história será mais longa, e não seria muito interessante manter tudo acontecendo em um só lugar. Além disso, narrativas _serializadas_ permitem (eu diria _pedem_) maior exploração do mundo ao redor do personagem, e não só da comunidade ao redor deles. Assim, pode ser interessante criar mais de uma comunidade, e o _Fate of Umdaar_ dá orientações sobre como criar comunidades de diversos tipos (_Freelands_, _Dread Domains_ e _Wildernexuses_), bem como definir quais as interações entre essas comunidades, e extrapola isso para como criar continentes inteiros! Para fãs de jogos estilo _West Marches_, isso é um prato cheio.

Ainda sobre as comunidades, o _Fate of Umdaar_ traz várias discussões sobre como criá-las, bem como quais arquétipos e estereótipos evitar. Em especial, há uma discussão sobre quais _bioformas_[^bioforms] adicionar a uma cidade, e porque certos arquétipos, como a cidade de uma raça só, não funcionam bem em um jogo descolonizado. As regras de criação de comunidades ainda prevêem como os _Valores_ de uma comunidade, conceitos abstratos mas tidos como importantes por ela, como honra, sabedoria, justiça, a afetam dentro do mundo de jogo. Os _Valores_ são apresentados em pares dualistas, mas sendo complementares e não necessariamente excludentes entre si.

[^bioforms]: Tradução livre para o termo _bioform_. _Fate of Umdaar_ evita o termo _raça_ (_race_) por várias razões, mas para uma discussão mais ampla do tema, recomendo ler tanto o _Masters of Umdaar_ quanto o próprio _Fate of Umdaar_.

## Fase 5: Questões

_Questões_ são um termo bonito para se referir aos _aspectos de campanha_, tal como se pensa este conceito desde o _Fate Básico_. Só que ao invés de uma _questão iminente_ e uma _questão presente_, em _Fate of Umdaar_ se pensa em um _aspecto de aventura_ e uma _questão de série_, para as narrativas _serializadas_, ou dois _aspectos de aventura_, para as narrativas _independentes_.

Para as narrativas _serializadas_, a _questão de série_ é um aspecto que descreve uma questão importante que estará presente ao longo da série. Normalmente relacionada ao _foco_ da narrativa, a _questão de série_ é um problema imenso e difícil de consertar (isto é, se for possível consertá-lo, para começo de conversa).  Ao lado da _questão de série_, o _Fate of Umdaar_ sugere adotar um aspecto para descrever a _questão iminente_, o que acontecerá ao mundo caso os inimigos consigam atingir seus objetivos ou o que acontece caso um fator previamente desconhecido seja revelado.

Já o _aspecto de aventura_ funciona como a _questão presente_. Trata-se de um aspecto que descreve um problema que está acontecendo aqui e agora, e que pode ser corrigido pelos personagens mediante algum esforço e empenho. Resolver o _aspecto de aventura_ marca o fim de uma narrativa _independente_, mas pode ser apenas um dos arcos de uma narrativa _serializada_.

## Fase 6: Vilões

Hora de criar os vilões da narrativa.  Obviamente, é um péssimo negócio imaginar um inimigo que seja uma pulga alienígena vinda do espaço, sem razão ou motivo para estar ali além de ser um vilão. Entre as que eu conheço, apenas uma narrativa com uma pulga alienígena funciona – _Chrono Trigger_ –, e mesmo assim apenas porque a referida pulga – _Lavos_ – tem razão para estar ali na história.

Para evitar isso, o _Fate of Umdaar_ traz algumas discussões sobre como criar os vilões da narrativa. Normalmente, eles serão os Mestres de Umdaar, mas em dados contextos, outras escolhas podem servir muito bem, como desastres naturais ou mesmo líderes de _Freelands_ discordando sobre algum tema político. É uma boa prática, também, vincular as _Questões_ criadas na fase anterior aos vilões a serem criados nesta – nem que seja apenas para evitar a pulga alienígena vinda do espaço.

## Fase 7: Campeões

A última etapa da Fundação é a criação dos personagens dos jogadores, que em _Fate of Umdaar_ são chamados de _campeões_. Trata-se de um bom momento não apenas para preencher as fichas de personagem, mas para discutir como o grupo de personagens funciona, com o que eles costumam lidar, qual é a história pregressa deles como grupo, essas coisas. Entretanto, pretendo, se o tempo deixar, criar um artigo específico sobre a criação de personagens em _Fate of Umdaar_, de modo que não vou me demorar muito neste tópico.

## Considerações finais

Assim funciona a Fundação em _Fate of Umdaar_. Nota-se, mesmo ao se ler apenas uma apresentação superficial como esta, que a Fundação é um modelo robusto de sessão zero, um que pode ser adotado em diversos jogos, diversos sistemas, com um mínimo de adaptações. Espero que isso inspire mais jogadores ainda a tratar a sessão zero com o devido respeito e importância que ela tem.

E esta é minha deixa. Espero que após esta leitura vocês estejam melhor preparados para, de um modo seguro para todos, derrubar... **OS MESTRES DE UMDAAR**!

---

