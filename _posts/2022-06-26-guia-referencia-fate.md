---
layout: post
title: "Atualização do Guia de Referência para Fate: um PDF que todos deveriam ter"
lang: pt-BR
date: 2022-06-26 11:30:00 -0300
tags: RPG Fate Geral Guias Fate-Masters
---

Queridos leitores, [em uma postagem recente](https://cicerone.gitlab.io/2022/06/09/guia-referencia-fate.html), eu publiquei aqui um trabalho recente de Fábio Costa, mais conhecido como o _Mr. Mickey_ do [Fate Masters](https://fatemasters.gitlab.io), um _Guia de Referência para Fate_, com lembretes e explicações rápidas para todas as regras do sistema. A pedido dele, eu rediagramei o texto de modo a caber em uma única página, em três colunas.

Só que eu ainda não estava muito satisfeito com a diagramação que eu fiz originalmente, então peguei um tempinho livre para mexer e melhorar a aparência do negócio. Se você tiver interesse em baixar o pdf atualizado (e por que caralhos você não teria?), [clique aqui](/assets/pdfs/guia-tres-colunas-atualizado.pdf) e seja feliz.

---

