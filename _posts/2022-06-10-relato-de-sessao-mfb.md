---
layout: post
title: "Relato de sessão: A Necrópole Amaldiçoada, uma aventura para Fate of Umdaar, no servidor Movimento Fate Brasil"
lang: pt-BR
date: 2022-06-10 22:15:00 -0300
tags: RPG Divulgação Fate-of-Umdaar-playtest Relato-de-sessão Oneshot Fate Movimento-Fate-Brasil Discord
---

Camaradas, hoje eu narrei novamente a aventura _A Necrópole Amaldiçoada_, desta vez no servidor do [_Movimento Fate Brasil_ no Discord](https://discord.gg/hyAbuThFQd). Quero compartilhar com vocês minhas experiências com esse grupo.

## Jogadores

Contei com três jogadores: Tatiana, Estevam e João. Todos eles já possuem experiência em Fate, bem como já conheciam _Masters of Umdaar_, de modo que não precisei entrar em detalhes sobre as regras ou o cenário.

## A narrativa

Os jogadores escolheram personagens prontos e seguiram para a aventura. Os quatro mostraram grande imersão e interesse pela história, mantendo-se nos personagens e usando liberalmente seus aspectos e pontos de destino para afetar tanto mecânica quanto narrativamente a aventura. Eles seguiram por um caminho que eu não havia previsto, mas com isso descobriram as informações necessárias para resolver o problema com muito mais facilidade do que meu planejamento inicial previra.

As cenas seguiram sem dificuldade. Os jogadores se mantiveram em seus personagens e respeitaram o princípio da _ficção primeiro_ característico de Fate. Ao final da aventura, eles resolveram o mistério de um jeito que certamente eu não esperava, mas que foi muito mais divertido do que o planejamento original. Com isso, eles salvaram mais uma comunidade dos... **MESTRES DE UMDAAR**!

---

