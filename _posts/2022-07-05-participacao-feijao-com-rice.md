---
layout: post
title: "A visão estética e moral de Louis em 'A entrevista com o vampiro', de Anne Rice: participação minha no podcast 'Feijão com Rice'"
date: 2022-07-05 20:10:00 -0300
lang: pt-BR
tags: Geral Divulgação Podcast Feijão-com-Rice Crônicas-Vampirescas Anne-Rice Entrevista-com-o-Vampiro Filosofia Estética Moral
---

Caros leitores, quero aproveitar aqui para fazer uma divulgação de um podcast, [Feijão com Rice](https://anchor.fm/feijao-com-rice), no qual Priscila Marques e Tay Teister dedicado à leitura, debate e celebração das _Crônicas Vampirescas_, de Anne Rice. Elas estão começando agora (na data presente elas estão no episódio piloto e mais dois episódios), nos quais elas leem e debatem trechos do livro.

Para o terceiro episódio, elas querem debater o momento em que Louis apresenta ao repórter Daniel sua visão de que estética e moral são a mesma coisa. Para isso, elas chamaram uma convidada muito especial, graduada em Licenciatura Plena em Filosofia pela Universidade do Estado do Rio de Janeiro: esta que vos fala! Enfim, não vou dar _spoilers_ sobre o episódio, só vou falar que a gravação vai ser agora, dia 08/07/2022, às 16h.

Gente, eu parei pra ouvir o podcast delas, e só tenho uma coisa a dizer: elas são muito boas e sabem o que estão fazendo. Então parem de perder tempo e acessem o site do podcast delas (dá pra ouvir no Spotify também, se você preferir) e passem a acompanhá-las!

---

