---
layout: post
title: "Oficina de classificação de RPGs do Workshop Formando RPGistas"
lang: pt-BR
date: 2022-06-07 18:30:00 -0300
tags: RPG Divulgação Workshop Eventos RPGWorld Lampião-Game-Studio SESC SESC-São-João-de-Meriti
---

Moradores do Grande Rio, atenção: começou dia 21 de maio de 2022 o Workshop Formando RPGistas, uma parceiria entre o SESC de São João de Meriti, o RPGWorld e o Lampião Game Studio. A proposta do workshop, cujo cronograma está disponível na imagem a seguir, é realizar uma série de encontros para discutir o que é RPG e introduzir novas pessoas ao hobby em uma linguagem clara e acessível, bem como discutir e abordar conceitos mais familiares a jogadores mais experientes.

![flyer-divulgacao](/assets/workshop-formando-rpgistas/divulgacao.jpeg)

No encontro desde dia 11/06/2022, o tema será _Classificação de RPGs_, mais uma vez contanto com Rafael Wernek do RPGWorld e Jorge Valpaços do Lampião Game Studios. Se você vive em alguma dimensão paralela e não conhece esses caras, na [primeira postagem que fiz divulgando o evento](https://cicerone.gitlab.io/2022/05/25/workshop-formando-rpgistas.html) eu os apresento para você.

Enfim, se você estiver pela região e disponível, vá para esse workshop. Eu, que sou RPGista há mais de vinte anos, aprendi muita coisa legal e interessante no primeiro encontro, e a linguagem clara e acessível garante que mesmo que você nunca tenha ouvido falar em RPG na vida vai conseguir entender tudo que será explicado.

Espero vocês lá!

---

