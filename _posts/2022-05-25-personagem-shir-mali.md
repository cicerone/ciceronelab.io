---
layout: post
title: "Shir Mali (ela/dela): um personagem para Fate of Umdaar"
lang: pt-BR
date: 2022-05-25 12:00:00 -0300
tags: RPG Fate Evil-Hat Masters-of-Umdaar Fate-of-Umdaar-playtest Fate-Condensado Personagens Exemplo
---

E está na hora de meter a mão na massa! Ainda na esteira do playtest do _Fate of Umdaar_, decidi criar alguns personagens, a fim de que vocês possam saborear como o jogo está. Lembrando que as regras usadas são as disponíveis no material distribuído pela Evil Hat para os participantes do playtest, e que este personagem pode se tornar incompatível com a versão final.

## Shir Mali (ela/dela)

Shir é uma das Dervixes dos Artshoks, um clã nômade que habita os Desertos Ardentes no coração do continente. Seu espírito livre e coração curioso, porém, a levaram a abandonar as terras sagradas de seu clã a fim de explorar os mistérios e segredos de Umdaar. Sua perícia com a espada veio bem a calhar quando ela entendeu que o mundo fora do Deserto estava sob a ameaça dos Mestres de Umdaar, um grupo de tiranos terríveis que reduziriam o mundo a ruínas caso não sejam detidos. Disposta a impedir essa desgraça, Shir enfrenta os Mestres e vai para onde seus talentos são solicitados.

Ela é uma humana de cabelos castanhos presos em uma trança longa o bastante para atingir a linha da cintura. Sua pele morena carrega ainda um forte bronzeado dos anos vividos sob o sol escaldante dos Desertos Ardentes, e seus olhos, da cor de mel, são vívidos e curiosos. Ela usa roupas leves e flexíveis, de modo a não atrapalhar seus movimentos – em hipótese alguma ela usará armadura. Vermelho e dourado são suas cores favoritas.

**Nome**: Shir Mali (ela/dela)  
**Recarga**: 3

| **Aspectos** | |
| ---: | :--- |
| ***Conceito***| Humana dervixe[^dervixe] dos Desertos Ardentes |
| ***Motivação*** | Eu desejo conhecer e explorar Umdaar |
| ***Origens*** | Fugitiva do isolacionismo dos Artshoks |
| ***Relacionamento*** | |
| ***Aspecto Livre*** | |

[^dervixe]: Não existe este arquétipo no material do playtest do _Fate of Umdaar_. Aproveitarei para criá-lo em um próximo artigo para mostrar como funciona a criação de arquétipos, e o artigo resultante pode ser lido [aqui](https://cicerone.gitlab.io/2022/05/25/fou-arquetipo-dervixe.html).

| **Perícias** | | | | |
| ---: | :--- | :--- | :--- | :--- |
| ***Ótimo (+4)*** | Atletismo | | | |
| ***Bom (+3)*** | Lutar | Percepção | | |
| ***Razoável (+2)*** | Enganar | Provocar | Vigor | |
| ***Regular (+1)*** | Vontade | Empatia | Contatos | Saberes |

| **Façanhas** | |
| ---: | :--- |
| ***Cimitarra do Sol Ardente*** | A arma característica dos Dervixes de Artshok, essa cimitarra é uma lâmina levemente curva, desenhada para ser usada em uma mão, e cujo aço emite um brilho dourado característico. Uma vez por cena, Shir pode usar a invocação clássica dos Dervixes de Artshok, _Pelo poder do Sol Ardente!_, para receber um bônus de +2 em uma _Ação de Atacar_ por _Lutar_. |
| ***Dançarina de batalha*** | A arte secreta dos Dervixes parece um misto de dança e luta. Sempre que Shir realizar uma _Ação de Criar Vantagem_ por _Atletismo_ a fim de criar vantagens relacionadas a movimento, receba um bônus de +2 na rolagem. |
| ***Contra-golpe*** | Sempre que Shir obtiver um sucesso com estilo em uma _Ação de Defender_ por _Atletismo_ contra um ataque corpo-a-corpo, ela pode causar duas tensões de dano físico em seu atacante ao invés de receber um impulso. O atacante não terá direito a uma _Ação de Defender_ contra essas tensões de dano. |


| **Estresses e Consequências** | |
| ---: | :--- |
| ***Estresse Físico*** | [ 1 ] [ 1 ] [ 1 ] [ 1 ] |
| ***Estresse Mental*** | [ 1 ] [ 1 ] [ 1 ] [ 1 ] |
| ***Consequência Suave (2)*** | |
| ***Consequência Moderada (4)*** | |
| ***Consequência Severa (6)*** | |

---

