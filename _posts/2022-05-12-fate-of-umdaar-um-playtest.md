---
layout: post
title: "Fate of Umdaar: primeiras impressões sobre o playtest"
lang: pt-BR
date: 2022-05-12 18:00:00 -0300
tags: RPG Fate Evil-Hat Masters-of-Umdaar Fate-of-Umdaar-playtest
---

Uma das novidades no mundo Fate é o _Fate of Umdaar_. Escrito por Dave Joria, _Fate of Umdaar_ é uma expansão de um dos mais estimados _Worlds of Adventure_, _Masters of Umdaar_[^traducao], do mesmo autor, usando o _Feite Moça_[^feite_moca] ao invés do _Fate Acelerado_. Como tal, _Fate of Umdaar_ não é traz apenas elaborações adicionais sobre o cenário, mas toda uma restruturação de várias mecânicas originais para encaixar-se naquela variante do sistema.

[^feite_moca]: O apelido carinhoso dado pela comunidade ao _Fate Condensado_.

[^traducao]: Traduzido como _Mestres de Umdaar_ por Fábio Silva e publicado em português pela Pluma Press. Entretanto, vou usar os termos em inglês mesmo porque, por alguma razão, eu não tenho acesso à tradução.

Atualmente em fase de playtest (que se encerrará em 16/07/2022), o que já se tem sobre o _Fate of Umdaar_ mostra um jogo sólido e muito interessante, capaz de agradar qualquer pessoa minimamente interessada no gênero de _Romance Planetário_. O playtest é aberto ao público, mas, como está em inglês, essa pode ser uma barreira para alguns jogadores brasileiros. Para participar, acesse [https://www.evilhat.com/home/fate-of-umdaar/](https://www.evilhat.com/home/fate-of-umdaar/).

Depois de ler o material, eu gostaria de compartilhar com vocês minhas impressões. Tenha em mente que elas não representam em nenhum momento nenhuma outra opinião que não a minha, e que essas impressões podem mudar caso haja mudanças no _Fate of Umdaar_ após o playtest.

## O cenário

O cenário continua bem similar àquele apresentado em _Masters of Umdaar_, mas com adições que certamente tornaram as coisas mais interessantes. Antes, havia apenas os _Dread Domains_, os Domínios dos Mestres de Umdaar, e as _Freelands_, os territórios livres nos quais aventureiros heroicos e valorosos se congregam para partir em expedições para, de alguma forma, deter os planos dos Mestres. Agora existem os _Wildernexuses_, biomas inexplorados e selvagens que surgem ao redor das ruínas do Demiurgo[^demiurgo] e que podem ou não conter alguma forma de civilização, muitas vezes descendentes de algum legado do Demiurgo enquanto ele ainda estava no planeta.

[^demiurgo]: Mantendo a tônica do texto do _Fate of Umdaar_, que não especifica nem o gênero, nem a quantidade de seres que um dia puderam ser chamados de Demiurgos, vou adotar a forma neutra ao me referir ao Demiurgo.

A adição de comunidades nos _Wildernexuses_ marca a introdução da existência da neutralidade no cenário. Ainda que os personagens dos jogadores sejam heróis combatendo os Mestres de alguma maneira, existem comunidades que buscam se manter neutras na luta entre os Mestres e as _Freelands_. É possível ser um personagem inicialmente neutro, mas ao fazê-lo perde-se um pouco da graça do jogo.

Outra possibilidade é que foram introduzidos dois novos modos de jogo, chamados _focos_. _Masters of Umdaar_ trouxe um deles, o jogo de _arqueonautas_: exploradores que às vezes combatem os Mestres, mas são principalmente desbravadores de ruínas, aventureiros e resgatadores de artefatos. Os outros focos são o jogo de _dignatários_, diplomatas que fazem da política e das palavras armas mais perigosas do que machados e exércitos; e _rebeldes_, heróis e guerrilheiros situados nos _Dread Domains_ e tentando derrubar o Mestre do Domínio de dentro para fora.

Como houve maior detalhamento desses elementos de cenário, o _Fate of Umdaar_ traz também tabelas para a criação de elementos geográficos, como terrenos, comunidades e até continentes, a fim de facilitar a vida daqueles cuja criatividade não estiver colaborando. Porém, com relação às tabelas, vou falar sobre mais à frente.

Entretanto, acredito que a maior adição ao cenário são as dicas de descolonização do jogo. O gênero do romance planetário é marcado por muitos estereótipos e arquétipos que são francamente euro- ou estadunidense-cêntricos. Não seria difícil encaixar Alan Quartermain, Indiana Jones ou outros exploradores que encarnam e exsudam o mito do _Grande Explorador Branco_ em um romance planetário. Muitas vezes, isso significa que um aventureiro vindo de uma cultura que foi determinada como superior aproxima-se de uma comunidade cuja cultura foi determinada como inferior, resolve problemas dos nativos que eles poderiam ter resolvido por conta própria se não fosse, normalmente, por conta de algo apresentado como mera inércia cultural, e muitas vezes acaba se casando com uma jovem nativa que possui imensa curiosidade com todos os modos de vida que não sejam os da tribo dela. Familiar esse resumo? De _Pocahontas_ à _Stargate_, você vai achar o _Grande Explorador Branco_ em muitos lugares.

Dave Joria tomou especial cuidado para não apenas evitar esses estereótipos e arquétipos, como para ativamente desconstruí-los. Ao longo do livro, existem caixas como _A New Narrative_ e _A Note on Decolonization_ em que são discutidos pontos e apresentadas reflexões sobre como conduzir uma narrativa no gênero de romance planetário sem incorrer nos vícios euro- ou estadunidense-cêntricos comumente repetidos pelas diversas mídias. Ele, inclusive, reconheceu erros que ele cometeu em _Masters of Umdaar_, o que não foi apenas um _mea culpa_, mas a demonstração de um amadurecimento e reconhecimento de que essas são questões importantes que não podem ser ignoradas. Não vou, porém, me estender muito nessa discussão para não perder o foco, basta dizer que essas considerações são interessantes e importantes para quaisquer narrativas, em quaisquer gêneros.

## As regras

A migração de _Fate Acelerado_ para _Feite Moça_ provocou muitas mudanças nas regras. Ao invés de escrever alguns parágrafos apenas, vou organizar em subtítulos.

### Aspectos

_Fate of Umdaar_ usa os aspectos _High Concept_, _Motivation_, _Background_, _Relationship_ e dois _Free Aspects_. Essa é uma mudança com relação ao _Masters of Umdaar_, que usa _High Concept_, _Motivation_, _Personal Aspect_ e _Shared Aspect_, mas não é tão grande quanto parece.

_High Concept_ é o famoso aspecto _Conceito_: ele define de maneira sucinta quem o personagem é. Porém, tanto em _Masters of Umdaar_ quanto _Fate of Umdaar_, o _High Concept_ é escrito de maneira bem definida: o _bioform_ do personagem, o _archetype_ do personagem, e mais alguma informação relevante. O _High Concept_ é escrito dessa forma de modo a permitir invocações e compulsões em cima das propriedades do _bioform_ do personagem ou de seu _archetype_ sem precisar atravancar tudo com uma montanha de aspectos redundantes.

_Motivation_ substitui o mais clássico aspecto _Dificuldade_. A razão para essa mudança é, nas palavras do autor:

> The Motivation is a proactive version of the Trouble aspect; With this in mind, we recommend picking a motivation that is likely to force your character to do what they want/need to do, even if it results in bigger consequences later.  
> [Tradução livre] A Motivação é uma versão proativa do aspecto Dificuldade; com isso em mente, recomendamos escolher uma motivação que possa vir a forçar seu personagem a fazer o que ele quer/precisa fazer, mesmo que isso resulte em grandes consequências depois.

O aspecto _Background_ é diferente do _Personal Aspect_ usado em _Masters of Umdaar_ no sentido em que ele é algo mais focado. Enquanto o _Personal Aspect_ podia ser usado para criar um aspecto qualquer relacionado ao personagem, e por isso é praticamente idêntico a um _Free Aspect_, o _Background_ deve ser usado para criar um aspecto relacionando o personagem à sua comunidade de origem, seja esta uma vila, uma cidade, ou mesmo um grupo ou uma organização.

_Relationship_ e _Shared Aspect_ rezam sobre a mesma coisa: ambos são um aspecto que estabelece quais são os vínculos que existem entre os personagens dos jogadores. As mesmas orientações são válidas para ambos: esse vínculo não precisa ser necessariamente simétrico ou positivo.

Por fim, o _Free Aspect_ cumpre o papel do _Personal Aspect_ e vai mais além. Como o próprio nome indica, são dois aspectos que o personagem pode usar para o que julgar mais interessante.

### Perícias

Como o _Fate of Umdaar_ usa o _Feite Moça_, ele deu adeus às Abordagens do _Fate Acelerado_ e olá para as Perícias. O _Feite Moça_ usa 19 perícias, ao invés das 18 do _Fate Básico_, dividindo _Conhecimento_ (_Lore_) em _Conhecimentos_ (_Academics_) e _Saberes_ (_Lore_). Entretanto, _Fate of Umdaar_ desfaz essa divisão, reincorporando a maior parte de _Academics_ em _Lore_ e deixando coisas como política e eventos atuais para _Contacts_.

Além disso, houve outras duas mudanças nas perícias. _Drive_ inclui também operar quaisquer máquina com partes móveis, incluindo coisas como guindastes, que tecnicamente não vão de um lado para outro mas possuem um elemento móvel que pode ser operado. Já _Rapport_ passou a poder ser usada em ações de atacar para causar dano mental, o que é explicado no subtítulo _A New Way To Think About Attacks_, mas pode ser resumido na ideia de que se uma ação contra um oponente visa encerrar um conflito, então ela pode ser tratada como um ataque, mesmo que seja uma sessão de puxa-saquismo com o propósito de convencer um outro personagem a concordar com os pontos defendidos pelo seu personagem.

### Archetypes

Um _archetype_ define o treinamento, a ocupação, a experiência e o nicho que um dado personagem vai ocupar na narrativa. Leitores mais atentos vão notar que isso se assemelha ao conceito de _classe de personagem_ que outros jogos usam, mas que Dave Joria deliberadamente evitou "por várias razões", segundo o próprio – mas vale notar que em _Masters of Umdaar_ ele usa o termo _class_ para o mesmo conceito. Basicamente, um _archetype_ é, mais ou menos, a profissão do personagem: é como ele chuta a bunda dos Mestres de Umdaar.

No _Masters of Umdaar_, uma classe era uma distribuição pré-determinada das Abordagens do _Fate Acelerado_. Entretanto, como o _Fate of Umdaar_ baseia-se no _Feite Moça_ e este usa Perícias ao invés de Abordagens, se tornou necessário rever como os _archetypes_ funcionariam. O _archetype_ do personagem determina não o _lead approach_ (já que Abordagens são coisas do _Fate Acelerado_), mas a _lead skill_ do personagem, isto é, aquela que figurará no topo da Pirâmide de Perícias (por padrão, aquela com valor _Ótimo (+4)_). Como o _Fate of Umdaar_ usa 18 Perícias, o livro oferece, logo de entrada, 18 _archetypes_, e ainda por cima traz orientações sobre como construir um, caso nenhum dos prontos agrade! Além da _lead skill_, cada _archetype_ traz uma lista de perícias com forte sinergia com aquele _archetype_, o que auxilia ainda mais na construção de um personagem relacionado com o _archetype_ em questão.

Além disso, cada um dos _archetypes_ descritos no livro tem uma lista de Façanhas pré-definidas e características daquele _archetype_. Isso, juntamente com a questão das perícias de cada _archetype_, ajuda a criar a sensação de que cada personagem possui capacidades próprias que os ajudam a se diferenciar entre si e a os tornar únicos e especiais.

### Façanhas

Talvez a parte das regras que menos mudou, já que as façanhas pré-existentes precisaram ser adaptadas de Abordagens para Perícias. O acréscimo aqui é que agora existem façanhas específicas para os _archetypes_, paras os _bioforms_ e até mesmo para os _focos_ de narrativa. Muitas façanhas já disponíveis de entrada no livro!

### As tabelas e geradores de coisas

Um dos atrativos de _Masters of Umdaar_ eram as tabelas e geradores de personagens. Isso permitia a um grupo com menos tempo disponível para criar toda uma narrativa do zero sentar e jogar, simplesmente rolando alguns dados e seguindo as decisões que as tabelas e geradores ofereciam.

Esse conceito foi expandido no _Fate of Umdaar_. De um personagem a todo o continente a ser explorado, é possível gerar aleatoriamente praticamente tudo, bastando para isso rolar alguns dados e consultar a tabela apropriada. Houve um especial cuidado para integrar as decisões das tabelas umas às outras e a outras decisões que um jogador possa querer fazer por conta própria, sem confiar tanto nos dados.

## Regras alternativas

O _Fate of Umdaar_ traz algumas regras alternativas para aqueles que não estiverem exatamente satisfeitos com o jeito padrão de jogar o jogo. Algumas delas são reminiscências de outros jogos, como a regra alternativa para construção de relacionamentos, que nada mais é do que a boa e velha Fase Trio do _Fate Básico_ ou a regra de substituir _Consequências_ por _Condições_, que foi vista pela primeira vez no _Ferramentas do Sistema_. Outras são mais recentes, como a discussão sobre listas de perícias alternativas ou formato da lista de perícias, ambas vistas em larga escala pela primeira vez no _Feite Moça_.

Vamos às duas regras alternativas próprias do _Fate of Umdaar_. A primeira não tomará muito tempo, pois são diretrizes para converter o _Fate of Umdaar_ de _Feite Moça_ para _Fate Acelerado_. A segunda, entretanto, é mais interessante: trata-se do _Fate ABC_, essencialmente um modo de criação de personagem que usa tanto as perícias do _Feite Moça_ quanto as _Abordagens_ do _Fate Acelerado_. A metodologia por trás disso não é nenhuma novidade, entretanto: ela é derivada das regras de _esferas de perícia_ descritas no _Ferramentas do Sistema_. As _Abordagens_ são usadas como as _esferas_ no método, e o modo de atribuição de perícias é diferente daquele descrito no _Ferramentas do Sistema_, mas o resultado é um personagem cuja _Pirâmide de Perícias_ é idêntica à _Pirâmide_ de um personagem criado normalmente. Entretanto, ele ainda possui valores em _Abordagens_, que podem ser usados para _Façanhas_ ou outras coisinhas. É um novo método de criação de personagem, e um bem interessante.

## Considerações finais

Tal como seu predecessor, _Fate of Umdaar_ é um jogo interessantíssimo, um que não decepciona e que, tão logo seja lançado, será uma adição e tanto à qualquer acervo. Se o inglês não for uma barreira, eu recomendo enormemente participar oficialmente do playtest (por meio do link indicado no início). Caso a língua não seja o seu forte, aguarde, pois eu devo organizar em breve algumas sessões de jogo no [servidor do Movimento Fate Brasil no Discord](https://discord.gg/hyAbuThFQd).

Junte-se aos heróis para deter... **OS MESTRES DE UMDAAR**!

---

