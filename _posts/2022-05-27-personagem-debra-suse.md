---
layout: post
title: "Debra Suse (ela/dela): um personagem para Fate of Umdaar"
lang: pt-BR
date: 2022-05-27 10:00:00 -0300
tags: RPG Fate Evil-Hat Masters-of-Umdaar Fate-of-Umdaar-playtest Fate-Condensado Personagens Exemplo
---

E está na hora de meter a mão na massa! Ainda na esteira do playtest do _Fate of Umdaar_, decidi criar alguns personagens, a fim de que vocês possam saborear como o jogo está. Lembrando que as regras usadas são as disponíveis no material distribuído pela Evil Hat para os participantes do playtest, e que este personagem pode se tornar incompatível com a versão final.

## Debra Suse (ela/dela)

Debra não é seu verdadeiro nome. Ela já foi Fedora Slack, a impiedosa franco-atiradora a serviço do Barão Jel'Thazzar, um dos mais perigosos Mestres de Umdaar das Taigas Profundas. Sob as ordens dele, ela já eliminou vários opositores ao Barão, incluindo campeões famosos. Certa vez, ela foi resgatada de uma situação de quase morte por um grupo de campeões, que a ajudaram apesar de terem reconhecido a assassina. Isso, e as vivências que ela teve depois, a convenceram a mudar de lado, e hoje ela é uma das campeãs mais ativas na luta contra os Mestres. Para tirar os Mestres de seu rastro, ela adotou um novo nome, Debra Suse.

Ela é uma humana sem muitos traços físicos distintos. Ela tem uma cicatriz de queimadura química cobrindo metade do rosto, além de sofrer de nevralgia constante por conta de vários ferimentos mal curados do passado. Seus cabelos e olhos são verde-musgo, consequência da exposição prolongada a substâncias perigosas nas Taigas Profundas. Tirando isso, porém, ela é melhor descrita pelo o que ela não é: ela não é alta nem baixa, ela não é feia nem bonita, ela não é gorda nem magra, ela não é branca nem negra. Ela tem a aparência mais comum que sua queimadura e seus cabelos e olhos permitem a uma pessoa ter.

**Nome**: Debra Suse (ela/dela)  
**Recarga**: 3

| **Aspectos** | |
| ---: | :--- |
| ***Conceito***| Humana franco-atiradora[^sharpshooter] afetada por cicatrizes e nevralgia constante |
| ***Motivação*** | Preciso provar que mudei de lado de verdade |
| ***Origens*** | Já fui a melhor atiradora do Barão Jel'Thazzar |
| ***Relacionamento*** | |
| ***Aspecto Livre*** | |

[^sharpshooter]: Tradução livre para o termo _sharpshooter_.

| **Perícias** | | | | |
| ---: | :--- | :--- | :--- | :--- |
| ***Ótimo (+4)*** | Atirar | | | |
| ***Bom (+3)*** | Furtividade | Atletismo | | |
| ***Razoável (+2)*** | Percepção | Contatos | Vigor | |
| ***Regular (+1)*** | Provocar | Vontade | Condução | Enganar |

| **Façanhas** | |
| ---: | :--- |
| ***Contra-argumento persuasivo*** | Sempre que um oponente usar _Provocar_ contra Debra e ela tiver uma arma de projéteis carregada, em mãos e com linha de tiro contra o dito oponente, ela pode usar _Atirar_ como se fosse _Vontade_ para se defender. |
| ***Franco-atiradora*** | Se Debra sofreu estresse ou consequências nesta cena, ela recebe +2 em _Furtividade_ para se defender contra pessoas procurando por ela. |
| ***Truques com armas*** | Uma vez por cena, use _Atirar_ no lugar de qualquer outra perícia para realizar uma _Ação de Criar Vantagem_, contanto que seja possível descrever como disparar uma arma pode criar a dita vantagem. |

| **Estresses e Consequências** | |
| ---: | :--- |
| ***Estresse Físico*** | [ 1 ] [ 1 ] [ 1 ] [ 1 ] |
| ***Estresse Mental*** | [ 1 ] [ 1 ] [ 1 ] [ 1 ] |
| ***Consequência Suave (2)*** | |
| ***Consequência Moderada (4)*** | |
| ***Consequência Severa (6)*** | |

---

