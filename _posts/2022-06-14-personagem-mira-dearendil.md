---
layout: post
title: "Mira Dearendil de Altena (ela/dela), um personagem para Fate of Umdaar"
lang: pt-BR
date: 2022-06-14 12:00:00 -0300
tags: RPG Fate Evil-Hat Masters-of-Umdaar Fate-of-Umdaar-playtest Fate-Condensado Personagens Exemplo
---

E para finalizar o surto criativo de hoje, mais uma personagem para _Fate of Umdaar_. Lembrando que as regras usadas são as disponíveis no material distribuído pela Evil Hat para os participantes do playtest, e que este personagem pode se tornar incompatível com a versão final.

## Mira Dearendil de Altena (ela/dela)

Mira nasceu em Altena, e é uma princesa de nascimento. Filha do Rei Mark Dearendil de Altena, ele fez de tudo para que Mira se tornasse uma perfeita dama e donzela da corte, só que esse tiro saiu totalmente pela culatra. Aventureira, intrépida e causadora de confusão, a única coisa que o Rei Mark conseguiu fazer para se ver livre da filha causadora de problemas foi expulsá-la da corte.

Isso fez da princesa uma aventureira. Com o tempo, ela descobriu que seu pai não apenas era aliado a um Mestre de Umdaar como a havia prometido em casamento para o dito Mestre! Isso deu à Mira motivação adicional para se tornar uma _campeã_, pois assim como ela, muitas princesas devem ter sido dadas em casamentos dessa forma. Pro inferno com todos os Mestres de Umdaar, Mira vai chutar a bunda deles com tanta força que até os demônios mais adormecidos acordarão!

Mira é uma mulher de estatura mediana, cabelos pretos bem curtos e olhos com as íris brancas, um traço genético de sua família. Ela é esguia e atlética, e sempre se veste com roupas escuras e práticas. Porém, ela tem sempre à sua disposição algo apropriado para recepções na corte e coisas do tipo. Nunca se sabe, não é?

**Nome**: Mira Dearendil de Altena (ela/dela)  
**Recarga**: 3

| **Aspectos** | |
| ---: | :--- |
| ***Conceito***| Princesa humana que jamais será uma donzela em perigo |
| ***Motivação*** | Chutar a bunda dos Mestres de Umdaar para bem longe do planeta! |
| ***Origens*** | Eu era rebelde demais para a corte em Altena |
| ***Relacionamento*** | |
| ***Aspecto Livre*** | |

| **Perícias** | | | | |
| ---: | :--- | :--- | :--- | :--- |
| ***Ótimo (+4)*** | Recursos | | | |
| ***Bom (+3)*** | Atletismo | Lutar | | |
| ***Razoável (+2)*** | Contatos | Comunicação | Vigor | |
| ***Regular (+1)*** | Provocar | Vontade | Percepção | Saberes |

| **Façanhas** | |
| ---: | :--- |
| **Amigos em Todos os Lugares** | Mira não é uma princesa que cresceu enclausurada em um castelo. Sempre que ela estiver em um lugar novo e usar _Contatos_ para descrever alguém que ela conhece, em caso de sucesso ela também ganha um pequeno bônus sob a forma de um pequeno grupo de pessoas que tem boas lembranças dela (como _Os ruidosos mas amáveis frequentadores da Taverna do Capitão Caolho_ |
| **Habilidade com Rapieiras** | Mira usa uma rapieira, uma arma estilosa de corte. No início de qualquer conflito, como uma ação livre e antes que qualquer outra pessoa possa agir, ela pode tentar realizar uma _Ação de Criar Vantagem_ com _Lutar_ para mostrar o quão impressionante ela é como uma lutadora. |
| **Não poupei despesas** | Uma vez por sessão, Mira pode tirar um dispositivo caro de sua bolsa e descrever como aquele dispositivo pode ajudá-la com uma ação e uma perícia (por exemplo, uma luva-taser que a permite _Lutar_). Ela pode usar _Recursos_ no lugar daquela perícia pelo resto da cena. |


| **Estresses e Consequências** | |
| ---: | :--- |
| ***Estresse Físico*** | [ 1 ] [ 1 ] [ 1 ] [ 1 ] |
| ***Estresse Mental*** | [ 1 ] [ 1 ] [ 1 ] [ 1 ] |
| ***Consequência Suave (2)*** | |
| ***Consequência Moderada (4)*** | |
| ***Consequência Severa (6)*** | |

---

