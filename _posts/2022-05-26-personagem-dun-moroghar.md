---
layout: post
title: "Dun Moroghar (ele/dele): um personagem para Fate of Umdaar"
lang: pt-BR
date: 2022-05-26 22:00:00 -0300
tags: RPG Fate Evil-Hat Masters-of-Umdaar Fate-of-Umdaar-playtest Fate-Condensado Personagens Exemplo
---

E está na hora de meter a mão na massa! Ainda na esteira do playtest do _Fate of Umdaar_, decidi criar alguns personagens, a fim de que vocês possam saborear como o jogo está. Lembrando que as regras usadas são as disponíveis no material distribuído pela Evil Hat para os participantes do playtest, e que este personagem pode se tornar incompatível com a versão final.

## Dun Moroghar (ele/dele)

Dun nasceu na Borda da Mata, uma comunidade localizada às margens da Mata Tenebrosa famosa por seus exploradores e mateiros. Para ele, ser um mateiro era uma profissão de família: seu pai era um, bem como seu avô, bisavô, e demais membros da linhagem. Entretanto, ele sentiu que explorar a floresta era pouco, pois havia um mundo imenso lá fora sofrendo graças às maldades dos Mestres de Umdaar. Decidido a fazer algo de bom pelo mundo, Dun pegou suas coisas e partiu de casa com essa missão auto-imposta de derrubar os Mestres de Umdaar.

Dun é um Anano, uma bioforma humanoide com altura média equivalente a três quartos da altura média de um humano, embora possuam ombros e membros mais largos do que os de um humano. Ananos são famosos pela resistência física, tolerância a longas jornadas de trabalho e privações, e por um amor ancestral às matas e florestas, o que os torna excelentes mateiros. São excelentes trabalhando com madeira e fibras naturais, bem como possuem uma longa tradição como oleiros. Dun tem uma estatura mediana para sua bioforma, e seus cabelos e barbas castanhas são mantidos aparados para não se embolarem nas matas. Ele usa roupas camufladas mesmo quando está em cidades, um ambiente que lhe deixa desconfortável.


**Nome**: Dun Moroghar (ele/dele)  
**Recarga**: 3

| **Aspectos** | |
| ---: | :--- |
| ***Conceito***| Anano mateiro[^explorer] altruísta e nada urbano|
| ***Motivação*** | Eu devo eliminar os Mestres de Umdaar |
| ***Origens*** | Lidar com o mundo natural é negócio de família |
| ***Relacionamento*** | |
| ***Aspecto Livre*** | |

[^explorer]: Nome alternativo para o arquétipo _explorador_ (_explorer_, no original), do _Fate of Umdaar_.

| **Perícias** | | | | |
| ---: | :--- | :--- | :--- | :--- |
| ***Ótimo (+4)*** | Percepção | | | |
| ***Bom (+3)*** | Atletismo | Atirar | | |
| ***Razoável (+2)*** | Roubo | Furtividade | Vigor | |
| ***Regular (+1)*** | Ofícios | Condução | Vontade | Provocar |

| **Façanhas** | |
| ---: | :--- |
| ***Tem algo errado aqui*** | Dun recebe +2 em Percepção quando tentando notar algo ou alguém em um Caosnexus[^wildernexuses] ou ruína que não era para estar ali, como um soldado de um Mestre escondido em um arbusto, uma espécie invasiva, etc. |
| ***Tiro furtivo*** | Dun recebe +2 em _Ações de Atacar_ por _Atirar_, desde que tenha sobre si algum aspecto indicando que seu alvo não sabe onde Dun está (como, por exemplo, _Camuflado_ ou _Bem escondido_). |
| ***Cuidado!*** | Sempre que Dun tiver sucesso com estilo em uma _Ação de Defender_ por _Percepção_ contra uma armadilha ou um ataque oculto, ele pode escolher que outra pessoa que possa sofrer esse mesmo ataque tenha sucesso em sua _Ação de Defender_ (mesmo se esta já tiver sido rolada) ao invés do impulso. |

[^wildernexuses]: Tradução livre para o termo _wildernexuses_.

| **Estresses e Consequências** | |
| ---: | :--- |
| ***Estresse Físico*** | [ 1 ] [ 1 ] [ 1 ] [ 1 ] |
| ***Estresse Mental*** | [ 1 ] [ 1 ] [ 1 ] [ 1 ] |
| ***Consequência Suave (2)*** | |
| ***Consequência Moderada (4)*** | |
| ***Consequência Severa (6)*** | |

---

