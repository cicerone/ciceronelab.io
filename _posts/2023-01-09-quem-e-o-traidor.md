---
layout: post
title: 'Quem é o Traidor? – Um jogo sobre descobrir qual ministro é apoiador dos terroristas'
date: 2023-01-09 11:30:00 -0300
lang: pt-BR
tags: Geral RPG Jogo-autoral RPG-histórico Crítica-Social
---

E é hora de anunciar mais um joguinho de minha autoria!

![quem-e-o-traidor](/assets/Publicações/QeoT-1.jpg)

Em **Quem é o Traidor?**, atos terroristas incitados pelo candidato à presidência derrotado nas urnas começam a acontecer em todo o País após a posse do Presidente eleito. Ele precisará se reunir com seus Ministros para resolver os problemas, só que o Presidente não sabe que entre eles existe em segredo um apoiador do candidato derrotado, que fará tudo para minar os esforços de combate ao terrorismo.

Jogo inspirado nos atos de terrorismo bolsonarista acontecidos em 08/01/2023 em Brasília/DF.

Caso você goste do jogo e queira contribuir com o meu trabalho, pode me mandar um Pix.

<details>
  <summary markdown="span">Clique aqui para ver minha chave Pix</summary>
  
  lu.cicerone.cavalheiro@gmail.com
</details>

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/Quem%20%C3%A9%20o%20traidor%3F/Quem-%C3%A9-o-traidor.pdf?inline=false)

---

