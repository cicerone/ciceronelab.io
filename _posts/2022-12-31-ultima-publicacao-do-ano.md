---
layout: post
title: 'Memorial Poético 2022: uma antologia poética de poemas avulsos escritos em 2022'
date: 2022-12-31 13:20:00 -0300
lang: pt-BR
tags: Geral Literatura Poesia Antologia-Poética 2022
---

E eis minha última publicação no ano de 2022!

![memorial-poetico](/assets/Publicações/MP2022.jpg)

_Memorial Poético 2022: Uma antologia poética de poemas avulsos escritos em 2022_ reúne diversos poemas que publiquei ao longo do ano de 2022 no Instagram em umúnico livro. Os temas são variados, pois foram poemas escritos de acordo com o sabor do momento, da inspiração recebida da hora (Erato é uma musa bem caprichosa, como os gregos antigos diriam para qualquer um), e do contexto social do meu entorno na época da escrita. Ao lado de cada poema, haverá junto a imagem com a qual a obra foi publicada no Instagram, visto que as artes escolhidas para cada poema se integram (e, às vezes, até complementam) a mensagem que pretendo transmitir com os versos. As imagens serão redimensionadas para as dimensões da página, o que pode gerar algumas distorções – não me julguem por isso.

Coletânea não indicada para menores de 14 anos.

Você pode adquirir gratuitamente o PDF da obra no link abaixo. Caso você queira e possa contribuir com meu cafezinho, aceito Pix.

<details>
  <summary markdown="span">Clique aqui para ver minha chave Pix</summary>
  
  lu.cicerone.cavalheiro@gmail.com
</details>

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/Literatura/Poesia/Projetos%20Prontos/Memorial%20Po%C3%A9tico%202022/Memorial-Po%C3%A9tico-2022.pdf?inline=false)

---

