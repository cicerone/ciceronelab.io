---
layout: post
title: "Casinha RPG: um RPG minimalista de brincar de casinha"
lang: pt-BR
date: 2022-06-09 14:30:00 -0300
tags: RPG Folheto Minimalista Sistema Autoral Casinha-RPG
---

Em **Casinha RPG**, seus personagens serão crianças
brincando de casinha. Como somos todos adultos (espero!), não haverá a
necessidade de um Narrador para jogar **Casinha RPG**.

**IMPORTANTE**: este jogo pode tocar em tema sensíveis para algumas
pessoas, como abuso ou abandono parental, divórcio, violência doméstica,
entre outros. Sejamos adultos responsáveis e discutamos esses pontos
antes de sentar para jogar. Afinal de contas, RPG é para se divertir,
não para causar traumas nos coleguinhas.

## Criando seu personagem

Escolha uma *Personalidade* para seu personagem:
**Birrento**, **Teimoso**, **Prestativo**, **Gentil**, **Submisso**,
**Grosseiro** ou **Mandão**.

Escolha qual é o *Vínculo Familiar* do seu personagem:
**Papai/Mamãe**, **Filhinho**, **Tio do Pavê**, **Primo Chato**, **Tio
Solteirão** ou **Avô Bonachão**. É importante frisar que vocês podem
repetir os *Vínculos Familiares* de seus personagens, e
você não precisa se ater aos gêneros usados nas descrições. Se você
quiser ser um **Papai Transgênero**, por exemplo, vá fundo!

Escolha um *Número* entre 2 e 5. Quanto mais baixo o
número, melhor você é fazendo coisas **Infantis** (brincar, ser imaturo,
mentir, fazer traquinagens). Quanto mais alto o número, melhor você é
fazendo coisas **Adultas** (trabalhar, ser maduro, apaziguar conflitos,
consertar coisas).

## Jogando *Casinha RPG*

Brinque de casinha interpretando os personagens que vocês criaram. Se
porventura você quiser fazer uma ação cujo resultado seja incerto ou
duvidoso, role **1d6**. Se sua *Personalidade* for útil na
ação, role **+1d6**. Se seu *Vínculo Familiar* for útil na
ação, role **+1d6**. Os demais jogadores sempre darão a última palavra
sobre quantos dados você rolará.

Se você estiver tentando fazer algo **Infantil**, você contará como
sucessos dados cujos valores sejam *maiores ou iguais* ao seu
*Número*. Se você estiver tentando fazer algo **Adulto**,
você contará como sucessos dados cujos valores sejam *menores ou iguais*
ao seu *Número*. Compare o número de sucessos com a tabela
abaixo:

| **Sucessos** | **Resultado** |
| :---: | :---: |
| 0 | Você não apenas não conseguiu fazer o que queria, como surgiu uma *Complicação* que piorou a situação. Os outros jogadores determinam qual foi a *Complicação* |
| 1 | Você conseguiu realizar a ação mal e porcamente. Você conseguiu fazer o que queria, mas os demais jogadores devem adicionar um porém, algo que diminuiu ou mitigou a ação que você estava fazendo |
| 2 | Você realizou a ação como desejado |
| 3 | Você realizou a ação muito melhor do que o esperado. Os demais jogadores devem descrever como sua ação foi acima da média, ou como ela conferiu ao seu personagem algum bônus, vantagem ou benefício inesperados |

Se algum outro personagem quiser se opor à ação do seu personagem, ele
também rolará os dados. Os jogadores não envolvidos na disputa
arbitrarão quantos dados cada um dos envolvidos irá rolar. O vencedor da
disputa será o personagem com maior número de sucessos, porém os
sucessos do perdedor da disputa subtrairão sucessos do vencedor --
afinal de contas, tinha alguém atrapalhando!

## Tema do jogo

Para cada jogo de *Casinha RPG*, role 1d6 e escolha um
tema:

| **Dado** | **Tema** |
| :---: | :---:  |
| 1 | Chá de bonecas |
| 2 | Atraso para o almoço |
| 3 | Festa de aniversário |
| 4 | Passeio no parque |
| 5 | Visitar os parentes no interior |
| 6 | Alguém está doente |

Conversem entre os jogadores como vocês jogarão esse tema, quais os
limites que devem ser obedecidos e respeitados por todos. Depois de
conversarem isso tudo, juntem-se e contem uma história maravilhosa,
brincando de casinha com os personagens que vocês criaram!

Quando você sentirem que já exploraram tudo que havia para ser feito no
tema sorteado, discutam entre vocês se algum dos seus personagens passou
por alguma mudança significativa de *Personalidade*. Em
caso positivo, faça a mudança de acordo. Sorteiem um novo tema e joguem
*Casinha RPG* com seus personagens!

## Créditos, Licença e Parcerias

**Casinha RPG** é baseado em *Lasers & Feelings*, de John
Harper.

**Casinha RPG** é licenciado por Lu *Cicerone* Cavalheiro
sob **Licença Creative Commons CC-BY 4.0 Internacional** --
https://creativecommons.org/licenses/by/4.0/deed.pt\_BR.

---

