---
layout: post
title: 'Não apertem o botão vermelho! – um jogo solo sobre evitar uma guerra nuclear'
date: 2022-12-24 05:10:00 -0300
lang: pt-BR
tags: RPG RPG-Brasileiro RPG-Autoral Jogo-Solo Jogo-Indie
---

É com prazer que eu anuncio mais um jogo de minha autoria!

![nao-apertem-esse-botao](/assets/Publicações/NABV.jpg)

Em **Não apertem o botão vermelho!**, simula-se uma hipotética situação na qual os presidentes dos Estados Unidos da América e da Rússia decidem iniciar uma guerra nuclear entre seus países, situação que certamente destruiria o planeta Terra tal como o conhecemos. Seu personagem é um diplomata apontado pela Organização das Nações Unidas como o último recurso para evitar a catástrofe.

Caso o jogo lhe agrade e esteja dentro de suas capacidades contribuir para meu cafezinho, eu aceito Pix.

<details>
  <summary markdown="span">Clique aqui para ver minha chave Pix</summary>
  
  lu.cicerone.cavalheiro@gmail.com
</details>

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/Jogos%20Solo/N%C3%A3o%20apertem%20o%20bot%C3%A3o%20vermelho/N%C3%A3o-apertem-o-bot%C3%A3o-vermelho.pdf?inline=false)

