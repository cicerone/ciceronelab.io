---
layout: post
title: 'Mês do RPG Brasileiro 2022 – Dia 22: conspiração'
date: 2022-12-22 23:00:00 -0300
lang: pt-BR
tags: Mês-do-RPG-Brasileiro-2022 Tagmar Jorge-Valpaços Lampião-Game-Studio Arquivos-Paranormais RPG RPG-Brasileiro RPG-Indie Editora-CHA Jotun-Raivoso Dungeonist Coisinha-Verde Bazar-Verde
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/Fc0G0GJHc0I" title="YouTube video player" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Hoje, 22/12/2022, é a vigésima-segunda atividade do Mês do RPG Brasileiro 2022, um momento em que comemoramos a publicação do primeiro RPG brasileiro, Tagmar, publicado em 21/12/1991.

![arquivos-paranormais](/assets/Publicações/APRPG.jpg)

A temática de hoje é conspiração, e aproveitei para falar do **Arquivos Paranormais**, do grande [Jorge Valpaços](https://www.instagram.com/valpacosjorge). Em **Arquivos**, os personagens interpretam _Agentes_ de uma _Agência_ que investiga fenômenos paranormais e as conspirações a estes associadas. O jogo permite uma ampla variedade de possibilidades, desde a _Agência_ ser protetora da conspiração quanto a tentar a expor, quanto pode ser um jogo que pode ir da galhofa ao horror cósmico. Caso a proposta do jogo tenha lhe interessado, acesse [a página da Lampião Game Studio](https://lampiaogamestudio.wordpress.com) para não apenas o adquirir, como vários materiais de suporte gratuitos.

Este vídeo está disponível em <https://youtu.be/Fc0G0GJHc0I>, e a playlist do Mês do RPG Brasileiro em <https://www.youtube.com/playlist?list=PL44VOrX_-JdPfNHOfEIm0AS7ikOlpp9xN>. Espero que vocês tenham gostado do vídeo, e amanhã teremos mais!

Música de fundo:

"Investigations" by Kevin MacLeod (<https://incompetech.com>)    
Licensed under Creative Commons: By Attribution 4.0 License    
<http://creativecommons.org/licenses/by/4.0/>    

---

