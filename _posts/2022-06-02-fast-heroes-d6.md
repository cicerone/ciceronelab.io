---
layout: post
title: "Fast Heroes d6: um sistema de RPG minimalista para super-heróis"
lang: pt-BR
date: 2022-06-02 19:30:00 -0300
tags: RPG Folheto Minimalista Sistema Autoral Fast-Heroes-d6
---

Meus caros leitores, hoje quero aproveitar e divulgar para vocês um RPG minimalista de minha autoria, o **Fast Heroes d6**. Escrito originalmente para caber em um panfleto, ele é bem mínimo, voltado para jogos rápidos e divertidos nos quais os personagens serão super-heróis.

Enfim, divirtam-se com a leitura. Em breve, o jogo será publicado, e aí eu passo para vocês o link para obtenção da versão diagramada.


## Fast Heroes d6

Lu *Cicerone* Cavalheiro

Maína *Palomita* de Lima

Em *Fast Heroes d6*, seu personagem é um super-herói igualzinho ao dos
quadrinhos ou TV, pronto para salvar o mundo de supervilões e demais
ameaças!

Junte um grupo de amigos para jogar. Um de vocês será o *Narrador*, a
pessoa que contará as histórias e aventuras, e os demais serão os
*Jogadores*, que criarão os personagens centrais, os heróis da aventura.

## Criando seu Herói

Se você é um Jogador, você precisa criar um personagem de jogador, o seu
*Herói*:

-   Defina uma *Profissão*, uma ou duas palavras dizendo o que seu Herói
    faz da vida quando ele não está sendo um super-herói, tipo *Médico*,
    *Jornalista*, *Gari*, *Escritor*, etc;

-   Defina o *Superpoder* do seu Herói em uma ou duas palavras, como
    *Raio de gelo*, *Superforça*, *Psiquismo*, etc.;

-   Anote seus *Pontos de Ação* (PA) máximos iniciais, 5;

-   Crie o *Nome* do seu herói, e o nome que ele usa enquanto não está
    sendo um herói.

## Equipamento

Seu Herói tem tudo aquilo que ele precisa para exercer a *Profissão*
dele, bem como quaisquer equipamentos comuns que forem razoáveis ele
ter.

Você pode reduzir seus *PA* máximos em 1 para ter um *Superequipamento*,
um equipamento que só um herói teria. Defina esse *Superequipamento* em
uma ou duas palavras, igual você fez com seu *Superpoder*.

## Personagens do Narrador (PDNs)

Todos os personagens que não são dos jogadores são PDNs. O Narrador deve
criar os PDNs igual aos Heróis, mas com mais ou menos *Pontos de Ação*:
capangas comuns terão 1 ou 2 PA; capangas mais poderosos terão 3 ou 4
PA; e superseres terão 5 ou mais PA.

Narrador, lembre-se: somente superseres têm superpoderes!

## Missões

Uma *missão* é o objetivo que os Heróis devem cumprir durante a
aventura. O Narrador deve criar uma Missão contendo um *objetivo*, ou
ele poderá usar a tabela abaixo para gerar um aleatoriamente:

| **Dado** | **Objetivo** |
| :---: | :---: |
| 1 ou 2 | Salvar ou resgatar alguém |
| 3 ou 4 | Impedir um supervilão |
| 5 ou 6 | Prevenir uma catástrofe natural |

## Testes

Sempre que seu Herói decidir fazer algo mais complicado, o Narrador
pedirá um *teste*. Se sua *Profissão*, *Superpoder* ou
*Superequipamento* forem úteis ou relevantes no momento, você estará com
*Vantagem*.

O Narrador não deve pedir testes para ações cujo resultado seja claro.
Se o Herói quiser atravessar a rua quando o sinal fechar, ele consegue.
Se ele tentar levantar um prédio com as mãos nuas mas não tiver nenhum
tipo de superforça, ele não consegue. Os testes servem para adicionar
suspense e arbitrar situações que não sejam obviamente possíveis ou
obviamente impossíveis.

Role um dado de seis lados (1d6) e compare na tabela abaixo:

| **Dado** | **Sem Vantagem** | **Com Vantagem** |
| :---: | :---: | :---: |
| 5 ou 6 | Sucesso pleno | Sucesso crítico |
| 3 ou 4 | Sucesso parcial | Sucesso pleno |
| 1 ou 2 | Falha | Sucesso parcial |

-   **Falha**: o Herói não consegue realizar a ação, e o Narrador
    introduz uma *Complicação*, um problema que surge por causa da ação
    tentada;

-   **Sucesso parcial**: o Herói consegue realizar a ação como o
    esperado, mas o Narrador introduz uma *Complicação*;

-   **Sucesso pleno**: o Herói consegue realizar a ação como o esperado;

-   **Sucesso crítico**: o Herói consegue realizar a ação melhor do que
    o esperado. O Narrador deve introduzir algum benefício para o Herói,
    ou conceder a *Vantagem* em uma rolagem futura apropriada.

Uma *Complicação* é um problema ou dificuldade que surge por causa da
falha, não uma desculpa para punir os Heróis por tentarem agir.
Narradores, usem o bom senso.

## Teste oposto

Se algum Herói ou PDN quiser se opor à ação do seu Herói, eles farão um
*teste oposto*. Seu Herói e seu opositor verificarão quem tem mais
*Vantagens* para aquela rolagem: quem tiver mais, terá a *Vantagem* no
*teste oposto*; em caso de empate, ninguém terá a *Vantagem*.

A seguir, ambos rolam 1d6. O *vencedor* será aquele que obtiver o maior
valor no dado, mas o valor que ele usará para consultar a tabela será a
*diferença entre os dados*, não o valor que ele rolou. Afinal de contas,
havia alguém atrapalhando a ação!

## Combate

Se seu Herói precisar cair no pau com outros Heróis ou com PDNs,
determine quem age primeiro. Os Heróis sempre agem antes dos PDNs.
Jogadores, determinem amistosamente a ordem de ação. A seguir, o
Narrador determinará em qual ordem os PDNs agirão.

O primeiro a agir escolherá um alvo para seu ataque. Ele descreverá
então seu ataque e realizará um *teste oposto* contra o alvo.

-   **Falha**: o *Vencedor* atacou tão mal que na verdade foi ele que
    apanhou. O *Vencedor* sofre 1 PA de dano;

-   **Sucesso parcial**: o *Vencedor* atacou mal, mas nem ele, nem o
    alvo sofrem dano;

-   **Sucesso pleno**: o ataque do *Vencedor* conecta, e o alvo sofre 1
    PA de dano;

-   **Sucesso crítico**: o ataque do *Vencedor* foi perfeito, e ele
    escolhe: o alvo sofre 2 PA de dano, ou o alvo sofre 1 PA de dano e o
    *Vencedor* tem uma *Vantagem* em seu próximo teste.

Após resolver o ataque, se alguém teve seus PA reduzidos a 0, ele é
*nocauteado*. Passe a vez ao próximo a agir.

Continue até todos os oponentes ou todos os Heróis sejam nocauteados. Se
todos os oponentes forem nocauteados, os Heróis vencem o combate. Eles
recuperam seus PA até o máximo, mesmo os que foram nocauteados, e seguem
para a próxima cena.

Se todos os Heróis foram nocauteados, os oponentes vencem. O Narrador
introduz alguma *Complicação de Derrota* na história, como, por exemplo,
os Heróis foram capturados e agora devem fugir. Os Heróis recuperarão
apenas metade de seus PA, arredondado para cima.

Narradores, evitem matar seus Heróis! A morte deve ser algo dramático,
não algo trivial. Além disso, sempre há destinos piores do que a
morte\...

## Fim de Jogo e Evolução

Ao final da sessão de jogo, cada Herói recupera seus PA até o máximo.

Ao final da sessão de jogo cada Herói ganha 1 *Ponto de Evolução* (PE).

Se nenhuma *Complicação de Derrota* foi introduzida na história, eles
marcam mais 1 PE cada.

Se eles cumpriram o objetivo da *Missão*, eles marcam mais 1 PE cada.

Ao final da sessão de jogo, o Herói pode gastar 5 PE acumulados para:

-   Aumentar em 1 os PA máximos do Herói;

-   Modificar ou a *Profissão*, ou o *Superpoder*, ou um
    *Superequipamento* que o Herói já possua;

-   Adquirir um novo *Superpoder* ou *Superequipamento*.

Se a *Missão* foi cumprida, o Narrador cria ou gera aleatoriamente uma
nova missão, e as aventuras continuam!

**Regra alternativa: pedra-papel-tesoura ao invés do d6**

Se por alguma razão você não possuir um d6, você pode usar o famoso
pedra-papel-tesoura para substituir o dado.

Em um teste comum, o jogador faz um pedra-papel-tesoura contra o
Narrador. Trate uma vitória do jogador como se ele tivesse rolado 5 ou 6
no dado, um empate como se fosse um 3 ou 4, e uma vitória do Narrador
como se fosse um 1 ou 2.

Em um teste oposto, as coisas mudam um pouquinho. Quem tem a *Vantagem*
é calculado normalmente, mas se a *Vantagem* for do opositor, ela será
ignorada. Então o Herói realiza a jogada de pedra-papel-tesoura contra
seu opositor como se estivesse jogando contra o Narrador, conforme
descrito no parágrafo anterior.

---
