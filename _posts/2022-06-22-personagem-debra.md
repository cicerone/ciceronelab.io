---
layout: post
title: "Debra Ubuntu: um personagem para Fate Condensado"
lang: pt-BR
date: 2022-06-22 22:20:00 -0300
tags: RPG Fate Evil-Hat Fate-Condensado Personagens Exemplo CC-BY-SA-4.0-Internacional
---

Sejam bem-vindos a mais uma postagem aqui no site! Hoje eu dei uma pausa no processo de criação de jogos e aventuras para postar um personagem para Fate Condensado, a fim de que vocês possam usá-los à vontade.

Diferente das outras paradas postadas aqui no site, este personagem está licenciado sob **Licença Creative Commons Atribuição-CompartilhaIgual 4.0 Internacional**, cujo resumo em português para leigos pode ser lido em <https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR>


## Debra Ubuntu (ela/dela)

Debra é uma mulher que fez da palavra sua ferramenta e da negociação, uma arte. Ela fez fortunas como parte neutra em negociações complicadas entre figurões e chefões do crime igualmente, e sua riqueza a permitia circular entre os grupos mais influentes de pessoas na **Cidade**.  Infelizmente, um de seus clientes decidiu traí-la. Julgando que seria mais barato entregá-la para os incontáveis inimigos que ela fez durante sua ascensão, ele a atraiu para uma armadilha e a deu de bandeja para pessoas que tentaram a matar com o melhor de seu talento. Eles falharam, e Debra conseguiu fugir. Agora, sem muitas escolhas, ela é uma *Mercenária* negociadora, e seu maior propósito de vida atualmente é vingar-se do imbecil que achou que poderia vendê-la dessa forma.

Ela é uma mulher esguia, com traços delicados e talvez mais magra do que deveria. Ela costuma usar os longos cabelos ruivos presos em um rabo de cavalo, e seus óculos imitam o formato de olhos de gato. Ela sempre trajará a roupa mais elegante possível para a situação, embora ela saiba perfeitamente que é uma péssima ideia usar um vestido de gala para descer aos esgotos da **Cidade**.

| **Aspectos** | 
| ---: | :--- | 
| ***Conceito*** | Negociadora elegante com a língua rápida no gatilho | 
| ***Dificuldade*** | Minha vingança vem em primeiro lugar | 
| ***Relacionamento*** |    | 
| ***Aspecto Livre***  |   | 
| ***Aspecto Livre***  |   | 

| **Perícias** | 
| ---: | :--- | :--- | :--- | :--- | 
| ***Ótimo (+4)*** | Comunicação | 
| ***Bom (+3)*** | Enganar | Empatia | 
| ***Razoável (+2)*** | Provocar | Recursos | Contatos | 
| ***Regular (+1)*** | Atirar | Vigor | Vontade | Furtividade | 

**Façanhas**

- **Vou fazer uma proposta que ele não poderá recusar**: Uma vez por cena, se Debra tentar intimidar alguém apresentando uma contrapartida razoável, ela pode somar o valor dela em *Comunicação* como bônus na rolagem de *Provocar*
- **Gato escaldado tem medo de água fria**: Se Debra tiver razões para suspeitar que seu interlocutor a está enganando (ou seja, se ela souber de algum **aspecto** que justifique essa suspeita), ela recebe **+2** em todos os testes de **Empatia** feitos contra esse interlocutor.
- **Eu sei que sou bonita**: Debra recebe **+2** em **Ações de Superar** usando *Comunicação* contra uma pessoa se ativamente estiver se valendo de sua aparência para tal.

**Estresses e Consequências**

- ***Estresse Físico***: [ 1 ] [ 1 ] [ 1 ] [ 1 ]
- ***Estresse Mental***: [ 1 ] [ 1 ] [ 1 ] [ 1 ]

**Consequências**

- ***Suave (2)***
- ***Moderada (4)***
- ***Severa (6)***

---

