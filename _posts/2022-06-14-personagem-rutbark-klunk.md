---
layout: post
title: "Rutbark Klunk (ele/dele), um personagem para Fate of Umdaar"
lang: pt-BR
date: 2022-06-14 11:40:00 -0300
tags: RPG Fate Evil-Hat Masters-of-Umdaar Fate-of-Umdaar-playtest Fate-Condensado Personagens Exemplo
---

E entrego a vocês mais um personagem para _Fate of Umdaar_! Lembrando que as regras usadas são as disponíveis no material distribuído pela Evil Hat para os participantes do playtest, e que este personagem pode se tornar incompatível com a versão final.

## Rutbark Klunk (ele/dele)

Rutbark nasceu na estrada, filho de dois trasgos[^troll] membros da _Companhia Circense Tamanho é Documento_. Seus pais queriam que ele fosse um acrobata, mas notaram que o filho era desajeitado demais para a função e o redirecionaram para outra coisa. Como Rutbark tem o cérebro afiado para fazer piadas provocativas e irritar pessoas, eles decidiram que ele seria um excelente bufão. E assim Rutbark viajou por toda a Umdaar, conhecendo muitos lugares e pessoas.

Quando a _Companhia_ faliu, ele notou que teria que fazer algo da vida para pagar as contas. Como ele já havia visto como os _Domínios Sombrios_[^dreadlands] eram lugares bem zoados, ele decidiu se tornar um campeão, acreditando que sua habilidade de fazer as pessoas rirem poderia ser uma arma poderosíssima contra os Mestres de Umdaar.

[^dreadlands]: Tradução livre para o termo _dreadlands_

Rutbark é um trasgo[^troll]. Sua bioforma tem a pele verde-musgo cheia de verrugas e brotoejas escuras, ninguém tem cabelo mas os nascidos no sexo masculino possuem uma barba verde que mais parece tufos de grama mal aparada, e todos eles andam meio curvados a despeito de sua altura média ser aproximadamente 150 cm – isso é um hábito derivado da origem da bioforma, cavernas profundas e estreitas. Rutbark, em específico, veste roupas espalhafatosas, que enfatizam o fato de ele ser um bufão[^jester], e nunca é visto sem um saco de couro cru amarrado no seu cinto.

**Nome**: Rutbark Klunk (ele/dele)  
**Recarga**: 3

| **Aspectos** | |
| ---: | :--- |
| ***Conceito***| Trasgo[^troll] bufão[^jester] bem-humorado mas desengonçado com o próprio tamanho |
| ***Motivação*** | Rir é a maior arma contra os Mestres de Umdaar |
| ***Origens*** | Vi o mundo em minha caravana artística |
| ***Relacionamento*** | |
| ***Aspecto Livre*** | |

[^troll]: Para quem não conhece, trasgo é a tradução da palavra _troll_.

[^jester]: Tradução livre do termo _Jester_.

| **Perícias** | | | | |
| ---: | :--- | :--- | :--- | :--- |
| ***Ótimo (+4)*** | Provocar | | | |
| ***Bom (+3)*** | Enganar | Saberes | | |
| ***Razoável (+2)*** | Recursos | Comunicação | Vigor | |
| ***Regular (+1)*** | Atletismo | Percepção | Empatia | Vontade |

| **Façanhas** | |
| ---: | :--- |
| **Saco de piadas** | Uma vez por turno, como uma ação livre, Rutbark pode criar um aspecto sem invocações gratuitas para representar um item comum – e potencialmente engraçado – que ele puxa de seu saco de piadas. Ele pode fazer isso um número de vezes por sessão igual ao seu nível de Recursos. |
| **Rosto confiável** | Se Rutbark for bem sucedido em usar _Enganar_ contra alguém, ele pode usar _Enganar_ no lugar de _Empatia_ para discernir segredos e mentiras desse alguém, contanto que ele continue falando com Rutbark e não perceba as mentiras que lhe foram contadas. |
| **Quero ver o circo pegar fogo!** | Uma vez por cena, se Rutbark estiver na mesma cena em que alguém esteja se defendendo contra uma _Ação de Atacar_ por _Provocar_ ou _Comunicação_, esse alguém pode adicionar o valor de _Provocar_ de Rutbark em sua rolagem de defesa. |

| **Estresses e Consequências** | |
| ---: | :--- |
| ***Estresse Físico*** | [ 1 ] [ 1 ] [ 1 ] [ 1 ] |
| ***Estresse Mental*** | [ 1 ] [ 1 ] [ 1 ] [ 1 ] |
| ***Consequência Suave (2)*** | |
| ***Consequência Moderada (4)*** | |
| ***Consequência Severa (6)*** | |

---

