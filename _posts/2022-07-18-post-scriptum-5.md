---
layout: post
title: "Post Scriptum 5: um RPG solo minimalista sobre a própria morte"
lang: pt-BR
date: 2022-07-18 15:50:20 -0300
tags: Geral Divulgação itch.io Lojinhas RPG Folheto Minimalista Sistema Autoral Post-Scriptum-5 RPGWorld
---

Queridíssimos leitores, quero anunciar mais um RPG minimalista que escrevi. _Post Scriptum 5_ é um RPG solo minimalista no qual você, jogador, é um espírito recém-falecido que não lembra quase nada de sua vida e precisa resolver essa questão antes de encarar o que o Juízo lhe reserva: o _Paraíso_, o _Limbo_ ou o _Inferno_. Ao longo de cinco cenas bem intimistas, você vai recuperando suas memórias até recompor uma pequena sinopse de quem você era. Mas cuidado: a morte potencializou uma emoção negativa sua, e ela fará de tudo para corromper suas memórias e até mesmo quem você foi em vida.

_Post Scriptum 5_ é mais um jogo escrito como resultado da parceria com o [RPGWorld](https://www.facebook.com/rpgworldsite), e vai ser incluso na coletânea _Ideias Cavalheiras_, a coletânea na lojinha deles (<https://https://rpgworldsite.itch.io/>) com os meus jogos. É claro, você pode adquirir o jogo também na minha lojinha, no endereço <https://lucavalheiro.itch.io/post-scriptum-5-pt>.

---

