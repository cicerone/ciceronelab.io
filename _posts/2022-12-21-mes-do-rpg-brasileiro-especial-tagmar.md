---
layout: post
title: 'Mês do RPG Brasileiro 2022 – Dia 21: especial Tagmar'
date: 2022-12-21 21:00:00 -0300
lang: pt-BR
tags: Geral RPG Jogo-autoral RPG-solo RPG-histórico Tagmar Vídeo
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/dQUbVB6K4jA" title="YouTube video player" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Hoje é 21/12/2022, um dia especial para a comunidade do RPG brasileiro: há exatos 31 anos, Tagmar, o primeiro RPG brasileiro, foi publicado!

Para comemorar o aniversário do nosso pioneiro, hoje gravei um vídeo especial, em nome da nostalgia pura: a criação de um personagem em Tagmar! Ainda estou tendo problemas de framerate do vídeo com meu setup, mas o áudio está sem problemas. O importante é que prestei minha humilde homenagem ao jogo que abriu as portas para todos os autores de RPG brasileiros.

Este vídeo pode ser assistido no YouTube em <https://youtu.be/dQUbVB6K4jA>, e a playlist completa está em <https://www.youtube.com/playlist?list=PL44VOrX_-JdPfNHOfEIm0AS7ikOlpp9xN>. Espero que vocês tenham gostado, e amanhã teremos mais atividades!

Música de fundo:
"Noble Race" Kevin MacLeod (<https://incompetech.com>)  
Licensed under Creative Commons: By Attribution 4.0 License  
<http://creativecommons.org/licenses/by/4.0/>

---
