---
layout: post
title: 'Mês do RPG Brasileiro 2022 – Dia 24: horror'
date: 2022-12-24 18:00:00 -0300
lang: pt-BR
tags: RPG RPG-Brasileiro RPG-Autoral Aventura Fate Fate-Condensado Mês-do-RPG-Brasileiro-2022
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/gmwh69i3INQ" title="YouTube video player" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Hoje é 24/12/2022, véspera de Natal e dia da vigésima-quarta atividade do Mês do RPG Brasileiro 2022, momento em que a comunidade comemora a publicação do primeiro RPG brasileiro, Tagmar, lançado em 21/12/2022.

A temática deste dia é horror. Como não poderia deixar de ser, decidi abordar esse tema ao meu modo. Muitos RPGs de horror dão tratamento desrespeitoso às condições associadas ao gênero, como neuroatipicidade, condições psiquiátricas adquiridas em virtude de traumas, etc. Por esta razão eu gosto tanto de Fate: por política da Evil Hat, a editora gringa responsável pelo sistema, se um assunto não puder ser tratado de maneira respeitosa, ele não será abordado.

![o-portao-do-empc](/assets/Publicações/OpEMPC.jpg)

Com isso em mente, decidi escrever hoje uma aventura para [Fate Condensado](https://fatesrdbrasil.gitlab.io/fate-srd-brasil/fate-condensado/). Em **O portão da Escola Municipal Pandiá Calógeras**, que é uma aventura baseada em um conto de minha autoria, [**As Crianças do Pandiá Calógeras**](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/Literatura/Prosa/Andross/As%20crian%C3%A7as%20do%20Pandi%C3%A1%20Cal%C3%B3geras/As-crian%C3%A7as-do-Pandi%C3%A1-Cal%C3%B3geras.pdf?inline=false), os jogadores interpretarão alunos calouros da escola, que em virtude do sistema de ensino do Estado de São Paulo, abriga apenas o Ensino Fundamental I (1º ao 5ª anos), que são forçados a participar de um _ritual de iniciação_ para serem vistos como _caras legais_ pelos demais alunos. Sendo uma aventura de horror cósmico, imagina como essa história termina...

[Assista a playlist do Mês do RPG Brasileiro 2022 clicando aqui](https://www.youtube.com/playlist?list=PL44VOrX_-JdPfNHOfEIm0AS7ikOlpp9xN)

O PDF da aventura está disponível para download gratuito, basta clicar no botão abaixo. Se você quiser e puder contribuir com o meu cafezinho, eu aceito Pix.

<details>
  <summary markdown="span">Clique aqui para ver minha chave Pix</summary>
  
  lu.cicerone.cavalheiro@gmail.com
</details>

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/Aventuras%20do%20Cicerone/Volume%201/O%20port%C3%A3o%20da%20Escola%20Municipal%20Pandi%C3%A1%20Cal%C3%B3geras/O-port%C3%A3o-da-Escola-Municipal-Pandi%C3%A1-Cal%C3%B3geras.pdf?inline=false)

Música de fundo:    
"Irregular" by Kevin MacLeod (<https://incompetech.com>)    
Licensed under Creative Commons: By Attribution 4.0 License    
<http://creativecommons.org/licenses/by/4.0/>    

---

