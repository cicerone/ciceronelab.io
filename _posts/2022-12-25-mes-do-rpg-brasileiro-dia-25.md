---
layout: post
title: 'Mês do RPG Brasileiro 2022 – Dia 25: Natal'
date: 2022-12-25 21:55:00 -0300
lang: pt-BR
tags: RPG RPG-Brasileiro RPG-Autoral RPG-Solo Jogo-Solo Mês-do-RPG-Brasileiro-2022
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/YtIOOFLN8C4" title="YouTube video player" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

E é hora de anunciar mais um joguinho solo meu, escrito no modo _quem sabe faz ao vivo_, como pode ser visto no vídeo acima.

![voce-foi-uma-boa-crianca](/assets/Publicações/VFuBCeA.jpg)

**Você foi uma boa criança este ano?** é um jogo solo no qual você interpretará uma criança sendo confrontada pelo Papai Noel para saber se ela é merecedora do presente que pediu ou não. Para tanto, você precisará de um baralho comum de 54 cartas (as 52 de jogo mais os dois coringas) e a tabela ao final deste texto para servir de oráculo.

Caso você queira contribuir com o meu cafezinho, eu aceito Pix.

<details>
  <summary markdown="span">Clique aqui para ver minha chave Pix</summary>
  
  lu.cicerone.cavalheiro@gmail.com
</details>

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/Jogos%20Solo/Voc%C3%AA%20foi%20uma%20boa%20crian%C3%A7a%20este%20ano%3F/Voc%C3%AA-foi-uma-boa-crian%C3%A7a-este-ano.pdf?inline=false)

---

