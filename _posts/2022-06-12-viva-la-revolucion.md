---
layout: post
title: "Viva la Revolución: um RPG minimalista de rebeldes cyberpunk contra um ditador de uma república das bananas"
lang: pt-BR
date: 2022-06-12 15:00:00 -0300
tags: RPG Folheto Minimalista Sistema Autoral Viva-la-Revolución
---

Em **Viva la Revolución**, seus personagens serão **Rebeldes** contra o ditador de um país cyberpunk. Entretanto, o país é uma piada, uma clássica *república das bananas*, com toda a corrupção e estética esperadas de um estado policialesco e militarizado porém estereotipadamente ineficiente e caricato.

**ATENÇÃO**: este jogo envolve temas políticos e ideológicos que podem ser sensíveis para algumas pessoas. Antes de jogar, sente com seu grupo de jogo e discuta se vocês estão de acordo com os temas e quais seus limites nas discussões e cenas que possam vir a surgir em um jogo sobre **Rebeldes** tentando derrubar uma república das bananas.

## Jogadores: criando seus **Rebeldes**

Crie uma **Descrição**, um resumo de quem seu personagem é, se possível em duas ou três palavras. Exemplos: *Cidadão descontente*, *Anarquista incendiário*, _Traidor dos **Rebeldes**_.

Crie um **Motivo**, uma razão pela qual seu personagem se uniu aos **Rebeldes**, se possível em duas ou três palavras. Exemplos: *Vingança contra a morte dos meus pais*, *Derrubar o ditador incompetente e assumir o poder*, *Trair o movimento para ganhar favores do governo*.

Crie uma razão pela qual você é **Procurado pelo Governo**, se possível em duas ou três palavras. Exemplos: *Explodi bancos*, *Publiquei textos subversivos*, _Para melhor enganar os **Rebeldes**_.

Anote os **Pontos de Vida** (PVs) máximos iniciais do seu personagem: 5.

## **Narrador**: criando a missão dos **Rebeldes**

Cabe ao **Narrador** criar qual é a missão que os **Rebeldes** vão tentar cumprir para desestabilizar ou até mesmo derrubar o governo. Role 1d6 e escolha na tabela abaixo o **Objetivo Principal** da missão:

| **Dado** | **Objetivo Principal** |
| :---: | :---: |
| 1 | Roubar recursos do governo |
| 2 | Sabotar instalações do governo |
| 3 | Sequestrar uma pessoa importante do governo |
| 4 | Incitar uma revolta popular contra o governo |
| 5 | Contactar amistosamente outro grupo **Rebelde** |
| 6 | Salvar um preso político importante |

Após definir o **Objetivo Principal** da missão, o **Narrador** vai criar três **Objetivos Secundários** para a missão, que serão três **Descrições** que aquela missão possuem e são eventos que podem ou não serem cumpridos pelos **Rebeldes**.

## Testes

Quando o **Rebelde** descrever que ação ele pretende fazer, cabe ao **Narrador** avaliar se a ação é exequível. Se por alguma razão a **Descrição** ou o **Motivo** do **Rebelde** deixarem claro que ele é capaz de fazer aquilo, como um *Especialista em parkour* querendo pular entre prédios, ou *Vendedor de carros usados* tentando consertar um carro, o **Narrador** simplesmente determina que o **Rebelde** foi bem sucedido em sua ação.

Entretanto, se por alguma razão pela qual o **Rebelde** é **Procurado pelo Governo** puder interferir na cena, ele simplesmente não consegue realizar a ação desejada. Afinal, como um *Matador conhecido de policiais* vai conseguir convencer um policial de que ele é inocente de uma tentativa de assassinato que acabou de acontecer?

Porém, se não houver certeza se o **Rebelde** é ou não capaz de realizar a ação pretendida, será preciso realizar um **Teste**. Se não houver ninguém se opondo à ação que o **Rebelde** está tentando fazer, como pular um muro ou arrombar uma janela, ele deve rolar 1d6. Caso sua **Descrição** ou **Motivo** possam ajudar na ação pretendia, some +1 por cada fator que esteja ajudando. Porém, se a razão pela qual o **Rebelde** é **Procurado pelo Governo** puder interferir na ação, diminua em -2 o valor do dado. Depois de rolar o dado e fazer os somatórios apropriados, veja abaixo qual foi seu resultado:

- **1 ou menos**: **Falha catastrófica**! Você não apenas não conseguiu realizar a ação, mas a coisa saiu tão errado que o **Narrador** deve inserir um **Problema**, que mecanicamente funciona igual à razão pela qual você é **Procurado pelo Governo**, para tornar a situação ainda mais problemática para o seu **Rebelde**, mas existirá apenas durante aquela cena;
- **2 ou 3**: **Falha comum**! Você não conseguiu realizar sua ação.  Dependendo do que estava acontecendo, o **Narrador** pode querer adicionar alguma Complicação para o seu **Rebelde**;
- **4 ou 5**: **Sucesso**! Seu **Rebelde** conseguiu realizar a ação tal como ele desejava;
- **6 ou mais**: **Sucesso extraordinário**! Seu **Rebelde** conseguiu realizar a ação melhor do que como ele desejava! Cabe ao **Narrador** introduzir uma **Vantagem** para seu **Rebelde**, que mecanicamente funcionará como se fosse uma **Descrição** adicional válida apenas naquela cena.

Um **Rebelde** pode tentar criar deliberadamente uma **Vantagem** a seu favor ou um **Problema** contra outro personagem. Ele deve fazer o **Teste** normalmente, porém a seguinte adição. Se o **Rebelde** tentar criar uma **Vantagem** mas tiver uma **Falha Catastrófica**, além do **Problema** normalmente criado, a **Vantagem** que o **Rebelde** tentou criar vai virar um **Problema** adicional para o **Rebelde**.

Se houver alguém tentando se opor à ação que o **Rebelde** está tentando fazer, como tentar se esconder de um guarda, será preciso realizar um **Teste Oposto**. O **Rebelde** rola 1d6 e soma ou subtrai todos os fatores relevantes, como em um teste comum. Entretanto, seu opositor faz a mesma coisa. O valor final da rolagem do **Rebelde** será reduzido pelo valor final da rolagem de seu opositor, e esse será o resultado do teste do **Rebelde**.

## Conflitos

Quando a cena descamba para a violência, é preciso realizar um **Conflito**. Um dos **Rebeldes** sempre começa primeiro, decidam entre os jogadores qual. Ele declara então sua ação, e resolve sua ação normalmente.

Entretanto, se a ação declarada for atacar, o atacante escolhe um alvo e realiza um **Teste Oposto** contra seu alvo. Se o resultado do teste oposto for um valor positivo, o alvo sofre aquela quantidade de dano em seus PVs. Caso seus PVs sejam reduzidos a 0 ou menos dessa, forma, ele estará **Incapacitado**. Se até o final da cena ninguém tentar prestar auxílio ao personagem **Incapacitado**, ele morrerá.

Após concluir sua ação, o personagem que acabou de agir declara quem será o próximo a agir. O próximo personagem a agir declara e resolve sua ação, conforme explicado anteriormente, e após resolvê-la declara quem será o próximo a agir. Siga essa diretriz até que todos os inimigos dos **Rebeldes** estejam **Incapacitados**, situação na qual os **Rebeldes** venceram o conflito; ou todos os **Rebeldes** sejam **Incapacitados**, situação na qual eles serão deixados para morrer.

## Concluindo a missão

Caso os **Rebeldes** concluam o **Objetivo Principal** da missão, dê 1 **Ponto de Evolução** (PE) para cada um dos sobreviventes. Dê 1 PE adicional para os sobreviventes para cada uma dos **Objetivos Secundários** cumpridos. Se os **Rebeldes** matarem alguém desnecessariamente (isto é, uma morte que não faça parte nem do **Objetivo Principal**, nem dos **Objetivos Secundários** da missão) ao longo da missão, retire 1 PE por morte desnecessária de cada um dos sobreviventes -- eles são rebeldes, não assassinos.

Ao final de cada missão, todos os **Rebeldes** sobreviventes recuperam todos seus PVs até ao máximo.

Ao final de cada missão, um **Rebelde** pode gastar 5 PE para:

- Aumentar em 1 seus PVs máximos;
- Alterar sua **Descrição** ou **Motivação**;
- Adicionar uma **Descrição** ou **Motivação**;
- Reduzir a penalidade que a razão pela qual ele é **Procurado pelo Governo** aplica em testes de -2 para -1. Ao contrário das demais opções, esta opção só pode ser escolhida uma vez.

---

