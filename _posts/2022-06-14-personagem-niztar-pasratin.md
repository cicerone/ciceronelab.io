---
layout: post
title: "Niztar Pasratin (ele/dele), um personagem para Fate of Umdaar"
lang: pt-BR
date: 2022-06-14 10:00:00 -0300
tags: RPG Fate Evil-Hat Masters-of-Umdaar Fate-of-Umdaar-playtest Fate-Condensado Personagens Exemplo
---

E aproveitando o impulso de criatividade, segue para vocês mais um personagem de _Fate of Umdaar_. Lembrando que as regras usadas são as disponíveis no material distribuído pela Evil Hat para os participantes do playtest, e que este personagem pode se tornar incompatível com a versão final.

## Niztar Pasratin (ele/dele)

Niztar nasceu entre os Sheendar, um povo de homens-lagartos bastante conhecido por seu senso de lealdade e honra. Eles eram mais comuns nas _Areias do Tempo_, um _caosnexus_[^wildernexuses] famoso por simplesmente misteriosamente fazer aparecer objetos que obviamente pertencem a algum passado ou futuro desconhecido em Umdaar. Visando esse poder para si, Runmar, o Poderoso, um dos Mestres de Umdaar da região, juntou seus homens e invadiu as _Areias do Tempo_. Os Sheendar lutaram com todas as suas forças, mas a sua bravura foi insuficiente para resistir à crueldade e à brutalidade dos homens de Runmar. Os poucos Sheendar sobreviventes fugiram para todos os lados do continente, levando o amargor da derrota consigo.

Niztar se estabeleceu em _Caliópolis_, uma cidade localizada próxima à _Borda do Mato_, uma _Terra Livre_[^freeland] localizada próxima à Mata Tenebrosa. Entre os ananos, Niztar aprendeu como espreitar e observar suas presas sem ser visto. Entretanto, ele não quis se tornar um caçador ou um explorador. Seu senso de propósito lhe dizia que essas habilidades poderiam ser mais úteis para espionar os domínios dos Mestres e revelar os segredos deles, na esperança de que algo que ele descubra possa ser útil para alguém.

[^freeland]: Tradução livre para o termo _freeland_.

Ele é um homem-lagarto alto, com escamas foscas cuja cor é algo que fica entre o verde e o preto. Seus olhos são amarelos com pupilas em fenda, e ele não tem pelos corporais ou faciais. Suas mãos e pés tem quatro dedos cada, mas com microventosas que permite a Niztar manter-se preso a uma superfície vertical por algum tempo, desde que a superfície seja lisa e seca. Seu rabo tem a ponta cortada, uma cicatriz da batalha contra os homens de Runmar, o Poderoso.

**Nome**: Niztar Pasratin (ele/dele)  
**Recarga**: 3

| **Aspectos** | |
| ---: | :--- |
| ***Conceito*** | Espião[^spy] homem-lagarto amargurado mas leal e honrado |
| ***Motivação*** | Eu devo revelar os segredos dos Mestres de Umdaar |
| ***Origens*** | Sobrevivente de uma cidade conquistada pelos Mestres |
| ***Relacionamento*** | |
| ***Aspecto Livre*** | |

| **Perícias** | | | | |
| ---: | :--- | :--- | :--- | :--- |
| ***Ótimo (+4)*** | Enganar | | | |
| ***Bom (+3)*** | Roubo | Furtividade | | |
| ***Razoável (+2)*** | Investigar | Atletismo | Percepção | |
| ***Regular (+1)*** | Comunicação | Contatos | Lutar | Provocar |

| **Façanhas** | |
| ---: | :--- |
| **Mãos ágeis** | Niztar têm mãos tão ágeis que são capazes de bloquear ataques. Ele pode usar _Roubo_ ao invés de _Atletismo_ ou _Lutar_ para bloquear ataques feitos com _Lutar_ (mas não com _Atirar_). |
| **Eles estão comigo** | Se Niztar for pego em algum lugar em que ele não deveria estar mas obtiver um _sucesso com estilo_ para convencer as pessoas de que ele poderia estar ali, ele pode abrir mão do impulso para permitir que outro colega de equipe passe automaticamente no teste e ganhar o aspecto _Identidade Falsa_ com uma invocação gratuita. |
| **Paredes têm ouvidos** | Se Niztar estiver sobre si algum aspecto que indique que ele não está sendo percebido na cena, como _Oculto nas sombras_, ele recebe +2 em _Percepção_ para ouvir conversas alheias. |

| **Estresses e Consequências** | |
| ---: | :--- |
| ***Estresse Físico*** | [ 1 ] [ 1 ] [ 1 ] |
| ***Estresse Mental*** | [ 1 ] [ 1 ] [ 1 ] |
| ***Consequência Suave (2)*** | |
| ***Consequência Moderada (4)*** | |
| ***Consequência Severa (6)*** | |

---

[^spy]: Tradução livre do termo _spy_.

[^wildernexuses]: Tradução livre do termo _wildernexuses_.

