---
layout: post
title: 'Mês do RPG Brasileiro 2022 – Dia 27: sistemas'
date: 2022-12-27 11:27:00 -0300
lang: pt-BR
tags: RPG RPG-Brasileiro RPG-Autoral RPG-Solo Jogo-Solo Mês-do-RPG-Brasileiro-2022
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/NCgSbwUnvsI" title="YouTube video player" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Hoje é dia 27/12/2022, vigésimo-sétimo dia de atividades do Mês do RPG Brasileiro 2022! A comunidade comemora o RPG Brasileiro em dezembro em virtude da publicação do primeiro RPG Brasileiro, Tagmar, ocorrida em 21/12/1991.

A temática de hoje é sistemas. Um sistema de RPG é o que modela as "leis da realidade" da narrativa: ele diz o que pode ou não pode ser feito, e o como. Hoje, decidi falar de um sistema de minha autoria, o **Porrinha RPG – um RPG de Porrinha!**, criado em virtude do bordão de Maína Paloma, _eu jogo até RPG de porrinha_. Como o nome sugere, o sistema não usa dados ou baralhos para arbitrar os testes, mas o popular jogo de boteco conhecido como Porrinha no RJ e em outros lugares do Brasil. Aproveitei para falar do suplemento, **Porrinha RPG – versão solo**, que permite o uso do sistema para jogos solo. Ambos os materiais podem ser encontrados em <https://cicerone.gitlab.io/publicacoes>.

Assista este vídeo em <https://youtu.be/NCgSbwUnvsI>, bem como as demais atividades que eu gravei para o Mês do RPG Brasileiro 2022 na playlist <https://www.youtube.com/playlist?list=PL44VOrX_-JdPfNHOfEIm0AS7ikOlpp9xN>. Espero que você tenha gostado do vídeo, e até amanhã!

Caso você aprecie meu trabalho, você pode contribuir com o meu cafezinho me mandando um Pix.

<details>
  <summary markdown="span">Clique aqui para ver minha chave Pix</summary>
  
  lu.cicerone.cavalheiro@gmail.com
</details>

Música de fundo:    
"Perspectives" by Kevin MacLeod (<https://incompetech.com>)         
Licensed under Creative Commons: By Attribution 4.0 License     
<http://creativecommons.org/licenses/by/4.0/>       

---


