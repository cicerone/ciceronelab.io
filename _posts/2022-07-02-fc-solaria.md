---
layout: post
title: "Lançamento do Financiamento Coletivo do Solaria RPG pela IndieVisível Press"
lang: pt-BR
date: 2022-07-02 19:35:00 -0300
tags: RPG Fate Divulgação Fate-Condensado IndieVisivel Solaria-RPG
---

Caros leitores, a [IndieVisivel Press](https://indievisivelpress.com.br/loja/) iniciou hoje, dia 02/07/2022, o financiamento coletivo do Solaria RPG! Para quem não conhece, escute o [Fate Masters Episódio 65 - Apresenta Solaria](https://fatemasters.gitlab.io/podcast/FateMasters65-ApresentaSolaria/), mas resumidamente, trata-se de um RPG de mesa nacional com a temática solarpunk sobre a luta por um futuro melhor através da coletividade que usa uma versão modificada do Feite Moça como base para suas regras!

Se você curte jogos indies, jogos nacionais, a temática solarpunk ou está cansado de jogos que são na lógica "eu rolo, eu bato", Solaria RPG pode ser uma grande escolha. As artes internas já liberadas são lindas, e o jogo parece ser de uma leveza que estamos precisando em dias tão complicados como os atuais. Solarpunk é sobre esperança, utopia, e, essencialmente, na superação da lógica capitalista e sua substituição por um cooperativismo que fica entre o anarcocoletivismo clássico e os kibutzim israelenses.

O financiamento coletivo do Solaria RPG estará disponível até 06/08/2022, mas para quem apoiar durante as primeiras 24h haverá metas com preços especiais. Corra, ainda dá tempo! Acesse <https://www.catarse.me/Solaria> e coce esse cartão de crédito aí que a gente sabe que ainda não foi pago mas precisa ser usado mesmo assim!

---

