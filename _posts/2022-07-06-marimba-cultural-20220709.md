---
layout: post
title: "Dia 09/07/2022, dia de Marimba Literária em Jardim Primavera, Duque de Caxias"
date: 2022-07-06 09:40:00 -0300
lang: pt-BR
tags: Geral Divulgação Marimba-Literária Literatura Evento Baixada-Fluminense Duque-de-Caxias
---

Meus caros leitores, quero aproveitar aqui a oportunidade para divulgar o coletivo **Marimba Cultural**, cujo Instagram é <https://www.instagram.com/marimbaliteraria/>. Trata-se de um coletivo de autores e autoras da Baixada Fluminense que promove vários eventos culturais no esforço de fomentar cultura nessa região que parece viver esquecida pelas autoridades e pela grande mídia como um todo.

Dia 09/07/2022 eles realizarão um evento em Jardim Primavera, Duque de Caxias, que irá das 10h às 17h, e a primeira imagem da divulgação pode ser vista abaixo:

![marimba-cultural](/assets/Marimba Cultural/marimba-cultural-2022-07-09.jpeg)

Para mais detalhes do evento, como o cronograma de atividades, acessem o Instagram do coletivo.

## Como chegar

Jardim Primavera não é o lugar mais próximo do planeta se você não mora em Duque de Caxias ou Magé. Eu mesma não sei quais linhas de ônibus passam pelo endereço do evento (Avenida Primavera, 761, Jardim Primavera, Duque de Caxias), mas o local fica a uns 15 minutos de caminhada da estação de trem de Jardim Primavera. Se a SuperVia não me sacanear, eu devo conseguir chegar no horário. Para quem quiser ir de ônibus, recomendo entrar em contato com a [Transturismo Rei (TREL)](https://www.facebook.com/Trel-transturismo-Rei-LTDA-658320080963844/) para saber se existe um ônibus deles. Se eles me responderem, farei um edit nesta postagem para repassar as informações que eles me derem.

Vejo vocês lá!

---

