---
layout: post
title: "O assalto ao Banco Central: uma aventura para MicroFATE"
lang: pt-BR
date: 2022-06-21 20:20:00 -0300
tags: RPG Folheto Minimalista Sistema Simplificação MicroFATE Fate Aventura-pronta RPGWorld
---
Meus caros leitores, eu estou realmente pegando fogo! Acabei de escrever uma aventura para o [MicroFATE](https://cicerone.gitlab.io/2022/06/11/microfate.html), _O assalto ao Banco Central_. A versão diagramada dela será publicada em algum momento graças à minha parceria com o [RPGWorld](https://rpgworldsite.wordpress.com/), mas quem quiser se aventurar e seguir sem a versão diagramada mesmo, ei-la. Boa diversão!

## Introdução: a ***Cidade***

A ***Cidade*** é uma grande, bela e próspera metrópole. Ela tem seus problemas, como qualquer cidade tem, mas o maior deles é **Lord Shadowdread**, um supervilão cujos esquemas quase sempre envolvem conquistar a ***Cidade*** de alguma forma. Ele é a maior ameaça à ***Cidade***, e a mais frequente fonte de problemas que os **Heróis** precisam resolver.

Desta vez, os capangas de **Lord Shadowdread** estão assaltando um banco. A polícia foi chamada, mas ela está sendo incapaz de resolver o problema sozinha, e por isso os **Heróis**, mais uma vez, são necessários para salvar o dia.

## Criação de Personagens

***O assalto ao Banco Central*** é uma aventura escrita para ser jogada usando as regras ligeiramente modificadas do **MicroFATE**, uma simplificação do sistema **Fate RPG** escrita por Lu *Cicerone* Cavalheiro e disponível gratuitamente na compilação *Ideias Cavalheiras -- RPGs simples para o dia a dia*, disponível tanto no **Dungeonist** quanto no **DriveThruRPG**. Veja as modificações na sessão abaixo, .

Entretanto, nada impede que você adapte a aventura para seu sistema favorito e compartilhe o resultado. Caso o faça, lembre-se que esta aventura está licenciada sob **Licença Creative Commons Atribuição-CompartilhaIgual CC-BY-SA 4.0 Internacional**, o que significa que você não apenas precisa realizar a atribuição de autoria original (o texto para tal estará no final da aventura), como deve compartilhá-la usando a mesma licença. O texto para leigos da licença em português pode ser lido em <https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR>.

### Com relação ao MicroFATE, o que mudou?

Como já deve ter ficado óbvio, é uma aventura para super-heróis. Aí fica a questão: como construir super-heróis em **MicroFATE**?

Em primeiro lugar, os personagens serão chamados de **Heróis**. Para as demais mudanças, siga a lista abaixo para ver quais são as mudanças necessárias no sistema:

- O **Aspecto Conceito** precisa conter uma descrição sucinta **Superpoder** do **Herói**, como **O homem mais forte do universo** ou **Nenhuma inteligência se compara à minha**;
- Se seu **Herói** tem alguma fraqueza (como a de um certo alienígena superforte que não tolera a presença de um certo mineral verde), você pode aproveitar seu **Aspecto Dificuldade** para defini-la;
- Às vezes um **Herói** tem um ajudante. Se for o seu caso, use o **Aspecto Livre** para descrevê-lo sucintamente;
- Use sua **Façanha** para criar e descrever o efeito do seu superpoder. Lembre-se de que ele precisa estar limitado ao que uma **Façanha** pode fazer, mas dentro disso apenas sua imaginação é o limite. Lembre-se também que se seu **Aspecto Conceito** inclui um superpoder como **A mais forte mulher voadora do universo**, você não precisa de uma **Façanha** para dizer que você é capaz de voar, seu **Conceito** já diz isso.

### *In Media Res*: O que vocês estavam fazendo antes da aventura começar?

Após criarem seus **Heróis**, conversem um pouco sobre quais foram suas aventuras passadas? Como eles se conheceram? Eles formam um supergrupo?  Caso sim, qual é a história desse supergrupo? Qual sua reputação, quais casos ele resolveu, em quais aventuras o supergrupo não deu conta da missão e precisou de auxílio externo, se houve alguma vez em que eles foram derrotados por um supervilão.

Discutam também se existe mais de um supervilão na ***Cidade*** além de **Lord Shadowdread**. Eles trabalham juntos? Existe algum tipo de fraternidade ou liga de supervilões, planejando juntos como conquistar a ***Cidade***? É possível que os **Heróis** se aliem a um deles para enfrentar o outro?

Se vocês estiverem usando as regras de **MicroFATE**, crie um aspecto chamado **Aspecto de Supergrupo** caso seus **Heróis** formem ou façam parte de algum grupo de super-heróis. Se você estiver usando outro sistema para narrar esta aventura e ele possuir regras similares aos aspectos de Fate, faça a adaptação de acordo.

## A aventura começa

Após criarem seus **Heróis**, estabelecer como é a dinâmica entre eles e decidir se eles fazem parte ou não de algum grupo de super-heróis, está na hora de começar a aventura.

### Primeira cena: o assalto ao banco

No momento em que os **Heróis** chegarem ao banco, eles verão que o local está completamente cercado por carros e oficiais da polícia, todos tentando conter o incidente e tentando negociar em vão com os ladrões.  Os policiais receberão os **Heróis** e os porão a par da situação: alguns capangas de **Lord Shadowdread** invadiram o banco, anunciaram o assalto e estão mantendo as pessoas como reféns, e eles simplesmente nem querem falar com a polícia. Os policiais então pedirão aos **Heróis** para que eles ponham um fim ao assalto e salvem os reféns, sem nenhum morto no processo se possível.

Hora para os **Heróis** discutirem seu plano. Se algum deles sugerir alguma abordagem mais sutil ou furtiva ao problema, um dos policiais revelará que existe um duto de ventilação de ar que os **Heróis** podem usar para entrar no banco em uma sala atrás do salão principal, que é onde os reféns estão sendo mantidos. Se os **Heróis** tentarem negociar, os capangas de **Lord Shadowdread** atirarão contra a porta e berrarão que ninguém ali vai negociar nada.

Usar o duto de ventilação permitirá aos **Heróis** tentar usar uma **Ação de Criar Vantagem** sob a forma de uma *Entrada Silenciosa*.  Enquanto esse aspecto estiver em cena, será possível tentar eliminar os capangas do **Lord Shadowdread** (sempre dois capangas por **Herói**) furtivamente. Caso um dos **Heróis** tente eliminar um capanga silenciosamente, peça uma **Ação de Superar** contra dificuldade **+2**. Narrador, oriente-se pela lista abaixo a fim de descrever o resultado da
rolagem do **Herói**:

- **Sucesso com estilo**: o capanga é eliminado e nenhum dos outros nota o que está acontecendo. Adicione um uso gratuito ao aspecto *Entrada Silenciosa*;
- **Sucesso**: o capanga é eliminado e nenhum dos outros nota o que está acontecendo;
- **Empate**: o capanga é eliminado, mas os restantes notam que tem algo errado acontecendo. Cria-se o aspecto *Capangas Desconfiados*, que o Narrador pode usar para aumentar a dificuldade de futuras tentativas de eliminações silenciosas de capangas nesta cena;
- **Falha**: o capanga é eliminado, mas os restantes perceberam a presença dos **Heróis** no salão. Mesmo que ele ainda tenha usos gratuitos, o aspecto *Entrada Silenciosa* deixa de existir e a situação descamba para um conflito.

Caso os **Heróis** prefiram uma abordagem mais direta e saiam entrando pela porta da frente ou consigam fazer o aspecto *Entrada Silenciosa* deixar de existir, acontecerá um **Conflito**. Narradores, recomenda-se que em caso de **Falha** no ataque de um **Herói** um refém seja morto, enquanto em caso de **Empate** seja causado algum dano estrutural ao banco. Seja criativo na hora de descrever as cenas!

**Capangas de Lord Shadowdread**: ***Aspectos***: _Capangas de **Lord Shadowdread**, Inteligência não é meu forte, Sou mais que competente em criar o caos._; ***Estresse***: 1.

Caso os capangas vençam o conflito, os **Heróis** serão nocauteados e capturados. Por alguma razão, os capangas capturam e arrastam os **Heróis** e conseguem fugir do banco. Siga direto para o ponto indicado na terceira cena para saber o que acontece.

Mas se os **Heróis** vencerem, os capangas de **Lord Shadowdread** serão derrotados e finalmente a polícia entrará no banco para os prender. Um deles, provavelmente mais estúpido do que o normal, começará a contar vantagem sobre como aquele assalto era na verdade uma cortina de fumaça para encobrir os verdadeiros planos de **Lord Shadowdread**: a construção de uma máquina de dominação mental em massa em seu **Armazém Secreto**, e que ninguém conseguirá impedir **Lord Shadowdread** de ter sucesso nesse seu novo plano. Esse capanga é tão estúpido ao ponto de dar a localização e o endereço do tal armazém sem nenhuma necessidade de ser pressionado para tal.

Com isso, os **Heróis** têm a direção de seu próximo passo. Narrador, caso eles não percebam isso por conta própria, faça com que um dos policiais peça para que os **Heróis** vão até o tal armazém para investigar, pois **Lord Shadowdread** é famoso por seus truques letais e apenas os **Heróis** são capazes de lidar com eles.

### Segunda cena: os arredores do _Armazém Secreto de **Lord Shadowdread**_

O armazém é um prédio sem identificação ou sinais claros localizado no distrito industrial da ***Cidade***. Os **Heróis** não terão nenhuma dificuldade em localizá-lo usando o endereço dado pelo capanga na cena anterior, mas só de chegarem perto notarão que há um boa quantidade de seguranças ao redor do prédio (Narrador, coloque dois agentes de segurança por **Herói**), bem como um bom sistema de segurança vigiando o prédio, representado pelo aspecto *Sistemas automatizados de vigilância*.

Deixe os **Heróis** discutirem um plano de ação. Caso eles tentem uma abordagem furtiva para entrar no armazém, peça um teste de **Ação de Superar** a eles, lembrando sempre de considerar que o aspecto *Sistemas automatizados de vigilância* está em cena e, portanto, está constantemente contribuindo negativamente para o teste.

Consulte a lista abaixo para saber o que pode acontecer caso os
**Heróis** tentem a abordagem furtiva:

- **Sucesso com estilo**: vocês conseguem contornar os sistemas de vigilância tão bem que vocês não só entram no armazém sem serem notados, como criam o aspecto *Despercebidos*, válido para quando vocês estiverem dentro do armazém;
- **Sucesso**: vocês conseguem contornar os sistemas de vigilância do armazém e conseguem entrar sem serem notados;
- **Empate**: a sua movimentação não é percebida pelos sistemas de vigilância, mas haverá dois agentes de segurança na entrada lateral, que vocês pretendiam usar furtivamente, esperando por vocês e prontos para um **Conflito**;
- **Falha**: os sistemas de vigilância os detectara, mandando toda e qualquer possibilidade de furtividade para o espaço, incluindo os outros **Heróis**!

Caso a abordagem furtiva falhe ou os **Heróis** se decidam pela ação direta, será preciso realizar um **Conflito** entre os agentes de segurança (dois por **Herói** caso eles tenham tentado a abordagem direta, mas apenas dois caso eles tenham **Empatado** enquanto tentavam a abordagem furtiva).

**Agentes de Segurança do Armazém de Lord Shadowdread**: _**Aspectos**: Capangas de Lord Shadowdread; Atirar primeiro, perguntar jamais; Meus olhos nunca me falharam._ ***Estresse***: 3

Se os agentes de segurança vencerem o **Conflito**, os **Heróis** serão nocauteados e arrastados para dentro do armazém. Continue a aventura a partir do ponto indicado na terceira cena. Se os **Heróis** vencerem o conflito, eles nocauteiam os seguranças e conseguem entrar no **Armazém Secreto do *Lord Shadowdread***.

### Terceira cena: dentro do Armazém Secreto do **Lord Shadowdread**

Caso em algum momento os **Heróis** tenham sido nocauteados e capturados pelos capangas ou seguranças de **Lord Shadowdread**, eles acordarão em uma gaiola cujo aspecto é *Prisão anti-superseres* e da qual eles deverão escapar antes que eles possam fazer qualquer coisa. **Lord Shadowdread** estará sozinho dentro do armazém, trabalhando em uma máquina bizarra que parece uma grande antena improvisada.

Caso os **Heróis** tenham entrado no armazém sem conseguirem o aspecto *Despercebidos*, eles serão recebidos por **Lord Shadowdread** sozinho dentro do armazém e trabalhando na máquina bizarra. Ele começará a se vangloriar sobre seu *Mega Dispositivo de Controle Mental em Massa*, sobre como ele usará sua nova máquina para dominar as mentes de todos as pessoas da ***Cidade***, e sobre como os **Heróis** não poderão impedi-lo desta vez. Ele puxará uma arma de raios e iniciará um **Conflito** contra os **Heróis**.

Se os **Heróis** tenham conseguido o aspecto *Despercebidos* antes de entrar no armazém, eles verão **Lord Shadowdread** sozinho trabalhando na tal máquina enquanto assobia uma cantiga infantil qualquer. Não será possível nocauteá-lo furtivamente nesta cena, mas caso algum **Herói** tente isso, um campo de eletricidade estática emanará do cinto do **Lord Shadowdread**, repelindo qualquer **Herói** que esteja por perto sem causar dano nem ao **Herói**, nem em **Lord Shadowdread**. Ele fará o mesmo discurso descrito no parágrafo anterior, puxará a arma de raios e partirá para o **Conflito** contra os **Heróis**.

**Lord Shadowdread**: _**Aspectos**: Supercientista supervilão; Excesso de autoconfiança; Meu objetivo é dominar a **Cidade**._ _**Superequipamento**: Arma de raios letais._ _**Estresse**: 5_

### Conclusão

Se **Lord Shadowdread** vencer o conflito, ele testará seu *Mega Dispositivo de Controle Mental em Massa* nos **Heróis**, e, não importa o quão poderosa seja a vontade deles, eles serão mentalmente controlados por **Lord Shadowdread**, que começará a rir loucamente de seu sucesso e começará a se vangloriar como será fácil dominar a ***Cidade*** agora que os **Heróis** estão sob seu comando. Narrador, acrescente à ficha dos **Heróis** o aspecto _Mentalmente controlado por **Lord Shadowdread**_, e pense junto com o grupo sobre como eles poderão ser salvos desse controle mental em uma próxima aventura. Dê aos personagens um **Marco Menor** por seus esforços.

Se os **Heróis** vencerem, eles capturarão **Lord Shadowdread** e o enviarão para uma prisão especial de segurança máxima para superseres, da qual todos nós sabemos que em breve ele escapará e com isso iniciará uma nova aventura. O *Mega Dispositivo de Controle Mental em Massa* ficará a disposição dos **Heróis**, que devem decidir o que fazer com ele. Dê um **Marco Maior** aos personagens.

De qualquer forma, a ***Cidade*** estará salva por um tempo graças aos **Heróis**. Viva!

---

