---
layout: post
title: "Electra das Plêiades (ela/dela), uma personagem para Fate of Umdaar"
lang: pt-BR
date: 2022-06-14 10:30:00 -0300
tags: RPG Fate Evil-Hat Masters-of-Umdaar Fate-of-Umdaar-playtest Fate-Condensado Personagens Exemplo
---

E aproveitando a onda de criatividade, segue mais um personagem para _Fate of Umdaar_. O mesmo lembrete de antes de aplica: as regras usadas são as disponíveis no material distribuído pela Evil Hat para os participantes do playtest, e que este personagem pode se tornar incompatível com a versão final.

## Electra das Plêiades (ela/dela)

Electra nasceu entre as ninfas da Mata Tenebrosa, uma _caosnexus_[^caosnexus] razoavelmente calmo e inexplorado, apesar de haver várias comunidades estabelecidas ao redor da floresta, como Caliópolis e Borda da Mata. Segundo a tradição de seu povo, ela recebeu o nome da estrela que parecia brilhar diretamente sobre ela em sua primeira noite de vida. Entretanto, como Electra é uma estrela extremamente variável (ela pode ficar oculta por anos nos céus), isso foi visto com um mau sinal. Isso não impediu que Electra recebesse toda a educação de seu povo e que ela fosse tratada com carinho e amor por todos.

[^caosnexus]: Tradução livre para o termo _wildernexuses_.

Porém, ela ansiava por mais. A Mata Tenebrosa era grande, mas poucas pessoas moravam nela. Ela havia aprendido belíssimas canções, mas não tinha com quem as compartilhar. Ela desejava amar, mas o fato de ter nascido sob Electra fazia com que seus concidadãos a temessem e a evitassem como uma possível arauta de algum problema. Então ela decidiu deixar a Mata Tenebrosa para trás. Essa não foi uma decisão fácil, e não foi sem dor no coração que ela decidiu se aventurar por Umdaar em busca de pessoas com as quais ela pudesse se envolver e para as quais ela pudesse cantar suas canções.

Foi assim que ela foi parar em Caliópolis. Ela se tornou muito popular entre os homens e mulheres da cidade, tanto por ser uma cancioneira extremamente talentosa como por ser uma romântica paqueradora incurável. Porém, a estadia em Caliópolis foi mais valiosa para ela do que isso, pois a mostrou como Umdaar era grande e cheia de coisas lindas que mereciam ser amadas e cantadas, e isso é o que a motiva a ser uma _campeã_.

Electra é uma ninfa da Mata Tenebrosa. Seu corpo tem formato idêntico ao humano, incluindo as zonas erógenas. Sua pele é verde escuro como folhas de árvores, e seus olhos são azulados como pétalas de orquídeas. Seus cabelos, da mesma cor dos olhos, não são fios, mas são como vinhas e gavinhas que crescem da cabeça e às vezes até brota uma ou outra folhinha azul no meio delas. Sua cultura não tem o conceito de usar roupas, por isso ela se sente mais confortável andando nua por aí. Entretanto, ela aprendeu que outras bioformas não lidam com isso muito bem, e ela usa a habilidade de sua bioforma de criar uma cobertura de musgo sobre a pele a fim de criar para si trajes apropriados para o local em que ela esteja.

**Nome**: Electra das Plêiades (ela/dela)  
**Recarga**: 3

| **Aspectos** | |
| ---: | :--- |
| ***Conceito***| Ninfa cancioneira[^songspinner] paqueradora e leviana |
| ***Motivação*** | Impedir que a beleza do mundo seja destruída pelos Mestres |
| ***Origens*** | A Mata Tenebrosa tinha pouca gente para eu amar, embora eu tenha saudades de casa |
| ***Relacionamento*** | |
| ***Aspecto Livre*** | |

[^songspinner]: Tradução livre para o termo _songspinner_.

| **Perícias** | | | | |
| ---: | :--- | :--- | :--- | :--- |
| ***Ótimo (+4)*** | Comunicação | | | |
| ***Bom (+3)*** | Empatia | Saberes | | |
| ***Razoável (+2)*** | Contatos | Atletismo | Roubo | |
| ***Regular (+1)*** | Enganar | Percepção | Investigação | Vontade |

| **Façanhas** | |
| ---: | :--- |
| **Regeneração** | Electra recebe +2 para iniciar o processo de recuperação de consequências físicas (com ou sem ajuda). |
| **Embaixadora** | Quando Electra e um aliado estiverem trabalhando como representantes ou enviados para uma figura importante, ambos ganham +1 em Comunicação para _Ações de Criar Vantagem_. |
| **Meias Verdades** | Electra pode usar _Comunicação_ no lugar de _Enganar_ para se defender de tentativas de descobrir seus motivos ou emoções usando _Empatia_. |

| **Estresses e Consequências** | |
| ---: | :--- |
| ***Estresse Físico*** | [ 1 ] [ 1 ] [ 1 ] |
| ***Estresse Mental*** | [ 1 ] [ 1 ] [ 1 ] [ 1 ] |
| ***Consequência Suave (2)*** | |
| ***Consequência Moderada (4)*** | |
| ***Consequência Severa (6)*** | |

**PS**: ok, podem zoar dizendo que Electra é minha Hera Venenosa de pobre...

---

