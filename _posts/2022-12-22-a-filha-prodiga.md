---
layout: post
title: 'A Longa Noite – Parte I: A Filha Pródiga'
date: 2022-12-22 00:00:00 -0300
lang: pt-BR
tags: Geral Literatura Fantasia-sombria Fantasia-urbana vampiros Maína-Paloma
---

![a-filha-prodiga](/assets/Publicações/AFP-01.jpg)

Winter sempre se sentiu desassociada da _Noite_, o mundo onde sua irmã gêmea, Summer, se encaixava melhor do que qualquer um; o mundo de seus pais; o mundo dos _Filhos da Noite_, como os vampiros se chamam. Ao voltar para casa de seus pais, atendendo a um pedido da irmã para participar da recepção de seus Tios vindos de outra Corte, Winter acaba decidindo lidar com seus problemas e consertar as suas relações familiares.

No entanto, ela acaba descobrindo mais semelhanças com a irmã do que sabia que tinha, e se vê em conflito tentando escapar de seu destino...

**A Filha Pródiga** é o primeiro livro da série **A Longa Noite**, uma coletânea de horror pessoal passado em um mundo de fantasia urbana sombria perturbadoramente similar ao nosso.

Se você quiser contribuir com o trabalho de Maína Paloma, você pode mandar um Pix para ela.

<details>
  <summary markdown="span">Clique aqui para ver a chave Pix de Maína Paloma</summary>
  
  maina-paloma@hotmail.com
</details>

[![botao-download.jpg](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/Encomendas/Ma%C3%ADna/A%20Longa%20Noite/A%20Filha%20Pr%C3%B3diga/A-Filha-Pr%C3%B3diga.pdf?inline=false)

---
