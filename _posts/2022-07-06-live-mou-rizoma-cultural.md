---
layout: post
title: "As Lâminas Estelares de Su'ul: uma aventura para Masters of Umdaar transmitida pelo Youtube"
date: 2022-07-06 20:10:00 -0300
lang: pt-BR
tags: Geral Divulgação RPG Live Youtube Fate Masters-of-Umdaar Rizoma-Cultural Fate-Acelerado
---

Prezados leitores, nesta sexta-feira, dia 08/07/2022, participarei de uma sessão de _Masters of Umdaar_ narrada por Cesar Milman, do [Rizoma Cultural](https://linktr.ee/RizomaCulturaRPG), que será transmitida no canal do Youtube dele, o [Rizoma Cultura RPG](https://www.youtube.com/channel/UCqM1FX1xkiEMZ2vIWQg-6bQ).

![divulgacao-live-cesar](/assets/Rizoma Cultural/20220708-live-masters-of-umdaar.jpeg)

Nesta mesa, jogarei com [Lady Eudora Dilantis de Altena](https://cicerone.gitlab.io/2022/06/14/personagem-eudora-dilantis.html), uma personagem que postei aqui no site recentemente. Ela foi criada usando as regras padrão do _Masters of Umdaar_, portanto na hora em que o Cesar estiver narrando ela poderá sofrer algumas alterações. Vou entrar na sessão 3 da campanha, então não sei o que me espera ;-)

Não prometo, mas vou tentar manter um diário das aventuras de Lady Eudora aqui no site. Assistam à live se inscrevendo no canal do Youtube, e vejam com seus próprios olhos o que Lady Eudora e os demais personagens vão aprontar.

---

