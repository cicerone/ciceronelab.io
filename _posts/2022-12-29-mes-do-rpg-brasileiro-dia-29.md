---
layout: post
title: 'Mês do RPG Brasileiro 2022 – Dia 29: romance'
date: 2022-12-29 01:05:00 -0300
lang: pt-BR
tags: RPG RPG-Brasileiro RPG-Autoral RPG-Solo Jogo-Solo Mês-do-RPG-Brasileiro-2022
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/Zeu-4wrlhB8" title="YouTube video player" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Hoje é 29/12/2022, vigésimo-nono dia de atividades do Mês do RPG Brasileiro 2022! Para quem não sabe (?), a comunidade faz essa comemoração em homenagem a Tagmar, publicado em 21/12/1991, o primeiro RPG brasileiro.

O tema de hoje é romance, e, para não cometer nenhuma injustiça com os inúmeros autores que já abordaram o tema em seus jogos, decidi falar de um que escrevi na atividade do dia 18/12/2022, "Até que a morte nos una". Baseado na tradição dos casamentos fantasmas chineses, este é um jogo solo no qual você descobre no pós-vida que sua família realizou um casamento fantasma em seu nome, e os Poderes Superiores decidem que seu cônjuge (tão desnorteado quanto você por conta do mesmo motivo) e você devem ver se são compatíveis. Após um tempo de convivência, os Poderes Superiores voltam para avaliar se o casamento fantasma foi bem sucedido ou não, e agirão de acordo.

Caso a proposta do jogo tenha interessado, baixe o PDF gratuitamente em <https://cicerone.gitlab.io/publicacoes/#at%C3%A9-que-a-morte-nos-una>. Se você puder contribuir com o meu cafezinho, você pode me mandar um Pix.

<details>
  <summary markdown="span">Clique aqui para ver minha chave Pix</summary>
  
  lu.cicerone.cavalheiro@gmail.com
</details>

Este vídeo pode ser assistido em <https://youtu.be/Zeu-4wrlhB8>, e a playlist com minhas atividades durante o Mês do RPG Brasileiro 2022 pode ser conferida em <https://www.youtube.com/playlist?list=PL44VOrX_-JdPfNHOfEIm0AS7ikOlpp9xN>. Espero que vocês tenham gostado, e até amanhã!

Música de fundo:     
"Inner Light" Kevin MacLeod (<https://incompetech.com>)      
Licensed under Creative Commons: By Attribution 4.0 License     
<http://creativecommons.org/licenses/by/4.0/>      

---

