---
layout: post
title: "Lady Eudora Dilantis de Altena, uma personagem para Masters of Umdaar"
lang: pt-BR
date: 2022-06-14 13:15:00 -0300
tags: RPG Fate Evil-Hat Masters-of-Umdaar Fate-Acelerado Personagens Exemplo Rizoma-Cultural
---

Meus caros leitores, vou dar uma pausa na criação de personagens para _Fate of Umdaar_ para postar aqui uma criação minha para _Masters of Umdaar_. Fui convidado por César Milman, do [Rizoma Cultural](https://linktr.ee/RizomaCulturaRPG), para jogar uma mesa de _Masters of Umdaar_. Obviamente, já criei minha personagem, e vou compartilhá-la aqui com vocês.

## Lady Eudora Dilantis de Altena

![image](/assets/personagens/eudora.jpg)

Lady Eudora nasceu filha do Barão Karl Dilantis de Altena, um dos mais temidos Mestres de Umdaar dos *Altiplanos Cinzentos*. Ela cresceu na corte, ignorante das crueldades e opressões que seu pai fazia com o povo. Certa vez, aconteceu de ela ver um servo sendo chicoteado gratuitamente. Ela tomou o chicote das mãos do capataz e o chicoteou, estarrecida com a crueldade do homem. Entretanto, seu próprio pai, quando soube da história, mandou prender Eudora em uma masmorra. Ela conseguiu fugir, e desde então dedica sua vida a combater os Mestres de Umdaar, em especial seu próprio pai.

Audaciosa, cativante e imprudente, Eudora transpira a imagem de alguém que vive pelo próprio talento e inventividade. Sua linguagem corporal e sua aparência revelam uma pessoa pragmática mas nada discreta, escondendo o charme e a compaixão de uma pessoa genuinamente compromissada com o bem do mundo ao seu redor. Seu olhar oblíquo traem sua personalidade gentil ao mesmo tempo que adicionam uma quase impenetrável aura de mistério sobre ela. Sua voz é clara e bem entonada, facilmente se distinguindo em meio a uma multidão.

### Aspectos

**Conceito**: Nobre esgrimista humana caçada por homens de seu próprio
pai

**Motivação**: Eu devo proteger os inocentes dos Mestres de Umdaar

**Aspecto Pessoal**: Não sei ser uma pessoa discreta

**Aspecto Compartilhado**: a definir

### Abordagens

| **Abordagem** | **Valor** | **Abordagem** | **Valor** |
| :---: | :---: | :---: | :---: |
| **Ágil** | +1 | **Estiloso** | +3 |
| **Cuidadoso** | +2 | **Poderoso** | +2 |
| **Esperto** | +1 | **Sorrateiro** | +0 |

### Façanhas

**Rapieira (Estiloso)**: Eudora usa uma rapieira, uma arma estilosa de corte. No início de qualquer conflito, como uma ação livre e antes que qualquer outra pessoa possa agir, ela pode tentar realizar uma *Ação de Criar Vantagem Estilosa* para mostrar o quão impressionante ela é como uma lutadora.

**Os arredores são uma arma (Estiloso)**: Eudora é acostumada a realizar manobras chamativas e estilosas como parte de seu estilo de combate. Sempre que ela tentar realizar uma *Ação de Criar Vantagem Estilosa* usando alguma coisa que esteja na mesma zona que ela (um candelabro, um corrimão, uma pessoa, etc.), ela recebe +2 na rolagem.

### Estresses e Consequências

| **Estresses e Consequências** | |
| ---: | :--- |
| ***Estresse*** | [ 1 ] [ 2 ] [ 3 ] |
| ***Consequência Suave (2)*** | |
| ***Consequência Moderada (4)*** | |
| ***Consequência Severa (6)*** | |

---

