---
layout: post
title: "Meus RPGs minimalistas foram publicados no Dungeonist e no DriveThruRPG!"
lang: pt-BR
date: 2022-06-13 19:30:00 -0300
tags: RPG Folheto Minimalista Sistema Autoral Publicados RPGWorld Dungeonist DriveThruRPG
---

Falaê, camaradas! Recentemente, vocês devem ter notado o meu surto de criação de RPGs minimalistas. Seis jogos criados em cinco dias. Puta que me pariu!

Isso aconteceu graças ao apoio de três figuras importantes no cenário cultural indie brasileiro:

+ [Cristiano Ludgerio](https://www.instagram.com/cristianoludgerio/), quadrinhista e ilustrador de Duque de Caxias e membro do [Capa Comics](https://www.instagram.com/capacomics/), que foi quem me convenceu que eu deveria começar a escrever;
+ [Jorge Valpaços](https://www.instagram.com/valpacosjorge), um dos maiores autores de RPG indies nacional da atualidade, membro do [Lampião Game Studio](https://lampiaogamestudio.wordpress.com/) e uma pessoa com quem tenho ótimas conversas e aprendo um bocado;
+ E por último, mas não menos importante, [Rafael Wernek](https://chat.whatsapp.com/LgozG0nVNaH2K7ialcehlN) e um dos fundadores do [RPGWorld](https://rpgworldsite.wordpress.com/), o mesmo do _Workshop Formando RPGistas_ que está acontecendo no SESC de São João de Meriti.

Com o apoio e fomento do RPGWorld, eu consegui publicar esses jogos tanto no [Dungeonist](https://dungeonist.com/) quanto no [DriveThruRPG](https://www.drivethrurpg.com/)! Os jogos estão no modelo _pague o quanto quiser_, então não tem nem a desculpa de que o cartão não virou para adquirir as versões diagramadas dos joguinhos que eu criei recentemente.

Por questão de conveniência, os jogos estão divididos em dois grupos:

+ _Ideias Cavalheiras – RPGs simples para o dia a dia_, que contêm os jogos [Coin-Op RPG](https://cicerone.gitlab.io/2022/06/02/coin-op-rpg.html), [Zombocalipse](https://cicerone.gitlab.io/2022/06/02/zombocalipse.html), [Devaneios](https://cicerone.gitlab.io/2022/06/07/devaneios-rpg-solo.html), [Dungeon Explorers](https://cicerone.gitlab.io/2022/06/07/dungeon-explorers.html), o [Casinha RPG](https://cicerone.gitlab.io/2022/06/09/casinha-rpg.html), e o [Private Eyes](https://cicerone.gitlab.io/2022/06/10/private-eyes.html). [Clique aqui para adquirir o _Ideias Cavalheiras_ no Dungeonist](https://dungeonist.com/ideias-cavalheiras-rpgs-simples-para-o-dia-a-dia/), ou [clique aqui para adquirir o _Ideias Cavalheiras_ no DriveThruRPG](https://www.drivethrurpg.com/product/348545/Ideias-Cavalheiras--RPGs-simples-para-o-dia-a-dia);
+ [_Fast Heroes d6_](https://cicerone.gitlab.io/2022/06/02/fast-heroes-d6.html), um RPG minimalista de super-heróis que escrevi em co-autoria com Maína _Palomita_ de Lima, minha atual esposa. Por enquanto, ele não está no DriveThruRPG (burocracia do site, deve entrar em alguns dias tanto a versão em pt\_BR quanto a versão em en\_US), mas [clique aqui para adquirir o _Fast Heroes d6_ no Dungeonist](https://dungeonist.com/fast-heroes-d6/). Eu aviso quando o _Fast Heroes d6_ entrar no DriveThruRPG, podem ficar tranquilos!

Se algum desses jogos te interessou e você gostaria de possuir um PDF já prontinho para impressão (todos eles cabem em uma página A4, menos o _Fast Heroes d6_ que requer uma página A4 frente e verso, e o _Jardineiros e Goblins_ que cabe em uma página A5), vai no Dungeonist ou no DriveThruRPG, adquira esses jogos e manda brasa! Eu adoraria quaisquer opiniões, comentários ou mesmo críticas que vocês tenham a fazer sobre meus jogos, podem mandar por e-mail mesmo (aquele que está no pé da página) ou pelo Telegram (aquele botãozinho também no pé da página, mais pro meio).

Espero que vocês se divirtam, e apreciem muito os jogos que escrevi!

---

