---
layout: post
title: 'O Gatilho mais Rápido do Oeste – um jogo de duelos e tiroteios na temática faroeste'
date: 2022-12-23 14:40:00 -0300
lang: pt-BR
tags: RPG RPG-Brasileiro RPG-Autoral Jogo-Minimalista Faroeste Bazar-Verde Mês-do-RPG-Brasileiro-2022
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/8Iy_pWgWCXY" title="YouTube video player" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Prezados leitores, é com muito prazer que trago a vocês este mais novo RPG de minha autoria, escrito enquanto eu gravava a atividade de hoje do _Mês do RPG Brasileiro 2022_!

![o-gatilho-mais-rapido-do-oeste](/assets/Publicações/OGmRO.jpg)

Em **O Gatilho mais Rápido do Oeste**, os jogadores, mediados por um **Árbitro**, farão o papel de **Pistoleiros** enfrentando uns aos outros ou a **Pistoleiros não Jogadores** em uma ambientação de faroeste. Assista o vídeo para entender porque eu decidi escrever um jogo com temática tão restrita.

Caso você goste da proposta de **O Gatilho mais Rápido do Oeste** e queira contribuir com o meu cafezinho, você poderá me mandar um Pix. Eu aceito e não vou ficar nem um pouco chateada com isso.

<details>
  <summary markdown="span">Clique aqui para ver minha chave Pix</summary>
  
  lu.cicerone.cavalheiro@gmail.com
</details>

Baixe o jogo em formato PDF gratuitamente:

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/O%20Gatilho%20mais%20R%C3%A1pido%20do%20Oeste/O-Gatilho-mais-R%C3%A1pido-do-Oeste.pdf?inline=false)

---

