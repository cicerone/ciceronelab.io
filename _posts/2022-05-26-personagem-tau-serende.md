---
layout: post
title: "Tau Serende (ela/dela): um personagem para Fate of Umdaar"
lang: pt-BR
date: 2022-05-26 22:00:00 -0300
tags: RPG Fate Evil-Hat Masters-of-Umdaar Fate-of-Umdaar-playtest Fate-Condensado Personagens Exemplo
---

E está na hora de meter a mão na massa! Ainda na esteira do playtest do _Fate of Umdaar_, decidi criar alguns personagens, a fim de que vocês possam saborear como o jogo está. Lembrando que as regras usadas são as disponíveis no material distribuído pela Evil Hat para os participantes do playtest, e que este personagem pode se tornar incompatível com a versão final.

## Tau Serende (ela/dela)

Tau nunca conheceu seus pais, dois arqueonautas mortos em conflito contra os Mestres de Umdaar. Ela foi criada dentro da Sagrada Livraria de Finestra, um templo dedicado aos mistérios dos Demiurgos mantido pelos Guardarrunas[^runekeeper], pelo Hierofante Pal Ajuntere. Pal foi uma excelente e carinhosa figura paterna, o que influenciou Tau a se tornar uma Guardarrunas, mas sua criação foi um tanto reclusa, dedicada ao aprendizado dos mistérios, ciências e demais segredos do passado. Na idade certa, Pal estimulou sua filha adotiva a se tornar uma campeã contra os Mestres, e Tau partiu de Finestra em busca de comunidades precisando de auxílio e conhecimentos precisando de resgate.

[^runekeeper]: Tradução livre para o termo _runekeeper_.

Tau é uma mulher-gato. Seu pelo é curto e cinza-claro com listras cinza-escuras. Suas orelhas são pontudas, e seu rosto é mais humano do que felino, apesar de ela ter bigodes e o nariz ser laranja, como o de um gato. Seus olhos são verde-claros e as pupilas são felinas. Contrariando o estereótipo, ela prefere calças e jaquetas confortáveis a robes desengonçados. Ela é uma campeã, não uma bibliotecária reclusa, afinal.

**Nome**: Tau Serende (ela/dela)  
**Recarga**: 3

| **Aspectos** | |
| ---: | :--- |
| ***Conceito***| Mulher-gato guardarrunas tímida e aventureira |
| ***Motivação*** | Entender o passado é entender o presente |
| ***Origens*** | Livrarias e bibliotecas são como minha primeira casa |
| ***Relacionamento*** | |
| ***Aspecto Livre*** | |

| **Perícias** | | | | |
| ---: | :--- | :--- | :--- | :--- |
| ***Ótimo (+4)*** | Saberes | | | |
| ***Bom (+3)*** | Investigação | Ofícios | | |
| ***Razoável (+2)*** | Atletismo | Percepção | Furtividade | |
| ***Regular (+1)*** | Empatia | Contatos | Comunicação | Vontade |

| **Façanhas** | |
| ---: | :--- |
| ***Livro de Piromancia Ofensiva*** | Tau possui um livro com magias ofensivas de fogo. Por conta disso, ela pode usar _Saberes_ no lugar de _Atirar_ para atacar com magias desse tipo, contanto que ela esteja de posse do livro e tenha condições de o ler. |
| ***Restauradora de Ruínas*** | Em uma ruína não funcional, Tau recebe +2 em _Ofícios_ para tentar consertar um dispositivo menor que não esteja funcionando adequadamente, como um painel quebrado, uma porta ou tranca quebrada, ou uma mensagem corrompida. Se o dispositivo estiver além de qualquer possibilidade de reparo, Tau consegue dizer o que é preciso para o consertar. |
| ***Time de Pesquisa*** | Quando um aliado e Tau estiverem usando ao mesmo tempo _Ações de Criar Vantagem_ para encontrar pistas ou informações, ambos recebem um bônus de +1. |

| **Estresses e Consequências** | |
| ---: | :--- |
| ***Estresse Físico*** | [ 1 ] [ 1 ] [ 1 ] |
| ***Estresse Mental*** | [ 1 ] [ 1 ] [ 1 ] [ 1 ] |
| ***Consequência Suave (2)*** | |
| ***Consequência Moderada (4)*** | |
| ***Consequência Severa (6)*** | |

---

