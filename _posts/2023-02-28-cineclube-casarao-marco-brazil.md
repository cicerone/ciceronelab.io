---
layout: post
title: 'Cineclube Casarão – Brazil, o Filme (Terry Gilliam, 1985)'
date: 2023-02-28 20:30:00 -0300
lang: pt-BR
tags: Casarão-de-Cultura-de-Duque-de-Caxias Cineclube-Casarão Brazil-o-Filme Terry-Gilliam Evento-Gratuito Debate Evento-Cultural
---

Caros leitores, dia 05/03/2023 acontecerá a edição de março do Cineclube Casarão! Desta vez, exibir-se-á e debater-se-á o filme "Brazil" (Terry Gilliam, 1985). A entrada é franca, e estão todos convidados.

## Sinopse do filme

![poster-brazil-filme](/assets/Publicações/2023-02-28-cineclube-casarao-2023-03.jpg)

Sam Lowfry é um funcionário do baixo escalão de um governo distópico ultraburocratizado contente com sua posição na hierarquia, apesar dos esforços de sua mãe, bem conectada com o alto escalão, para conseguir uma promoção para ele. Em seus sonhos, porém, ele se vê como um cavaleiro alado de armadura brilhante que resgata uma princesa aprisionada. Sua vida muda quando ele encontra na vida real uma mulher idêntica à princesa do sonho, e ele passa a fazer várias coisas, inclusive enfrentar o governo, para se aproximar dela.

## Sobre o Cineclube Casarão

O Cineclube Casarão é um evento cultural que acontece todo primeiro domingo do mês no [Casarão Cultural de Duque de Caxias](https://www.instagram.com/casaraoculturaldc), localizado na Avenida Primavera, 761, Jardim Primavera, Duque de Caxias/RJ, um polo de cultura independente e de resistência mantido totalmente por doações e trabalho voluntário.

## Como chegar

Do centro de Duque de Caxias, é possível chegar de ônibus (linhas Parque Independência, da Trel, ou Bom Retiro, da Viação União) ou de trem (desembarque na estação Jardim Primavera, seguido por uma caminhada de 10 a 20 minutos, dependendo do seu condicionamento físico). O ponto de referência é a Igreja de São Judas Tadeu.

---

