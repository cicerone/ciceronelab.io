---
layout: post
title: 'A Máquina Windrush: um conto de investigação em uma Inglaterra vitoriana steampunk'
date: 2022-12-26 13:57:00 -0300
lang: pt-BR
tags: Literatura Contos Conto-Autoral Steampunk
---

E é com muito prazer que eu venho anunciar mais uma publicação de minha autoria!

![capa-maquina-windrush](/assets/Publicações/AMW.jpg)

Victoria Lovegrove é uma detetive particular em plena Londres vitoriana. Seu ofício não lhe rendia muito – seu escritório fica na rua logo atrás de Baker Streeet 222B –, mas era o suficiente para pagar as contas e manter sua vida interessante – especialmente quando seu vizinho famoso estava prestando alguma consultoria para a Scotland Yard.

Certo dia, ela estava prestes a encerrar o expediente quando um homem misterioso entrou em seu escritório. Mal sabia ela que se envolveria em uma investigação muito interessante...

**A Máquina Windrush: um conto de investigação em uma Inglaterra vitoriana steampunk** foi publicado originalmente na coletânea **Engrenagens: contos com temática steampunk**, organizada por Paola Giometti e publicada pela Andross Editora em 2017.

Caso você queira contribuir com o meu cafezinho, eu aceito Pix.

<details>
  <summary markdown="span">Clique aqui para ver minha chave Pix</summary>
  
  lu.cicerone.cavalheiro@gmail.com
</details>

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/Literatura/Prosa/Andross/A%20M%C3%A1quina%20Windrush/A-maquina-Windrush.pdf?inline=false)

---

