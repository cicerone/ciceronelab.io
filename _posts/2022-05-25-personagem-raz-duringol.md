---
layout: post
title: "Raz Duringol (ele/dele): um personagem para Fate of Umdaar"
lang: pt-BR
date: 2022-05-25 12:00:00 -0300
tags: RPG Fate Evil-Hat Masters-of-Umdaar Fate-of-Umdaar-playtest Fate-Condensado Personagens Exemplo
---

E está na hora de meter a mão na massa! Ainda na esteira do playtest do _Fate of Umdaar_, decidi criar alguns personagens, a fim de que vocês possam saborear como o jogo está. Lembrando que as regras usadas são as disponíveis no material distribuído pela Evil Hat para os participantes do playtest, e que este personagem pode se tornar incompatível com a versão final.

## Raz Duringol (ele/dele)

Raz Duringol é um cavaleiro errante, um valoroso guerreiro cujo juramento o leva a combater os Mestres de Umdaar e prestar auxílio aos necessitados. Afiliado à _Ordem da Torre Cintilante_, sediada em Murambar, Raz carrega consigo a bandeira da paz e da liberdade para onde quer que vá. A guerra, em sua visão, é um mal necessário, que deve ser evitado sempre que possível.

Ele é um sátiro, cujo torso é humano e as pernas são de touro. Seu torso é coberto por uma camada de pelo cor de creme bem baixo e curto, similar ao dos grandes felinos, e as pernas apresentam pelos marrons mais grossos e volumosos, e os pés terminam em cascos. Seus cabelos são negros, os olhos, castanhos, e seus caninos inferiores são um tanto protuberantes, lembrando pequenas presas um tanto incongruentes com sua herança sátira. Quando não está usando sua armadura, Raz prefere roupas confortáveis e elegantes, porém que não transmitam a ideia de superioridade ou pedantismo. Ele nunca usa calçados.

**Nome**: Raz Duringol (ele/dele)  
**Recarga**: 3

| **Aspectos** | |
| ---: | :--- |
| ***Conceito***| Sátiro Cavaleiro[^ruffian] adepto da paz |
| ***Motivação*** | Eu devo defender Umdaar das maquinações dos Mestres |
| ***Origens*** | De soldado a um exemplo da _Ordem da Torre Cintilante_ |
| ***Relacionamento*** | |
| ***Aspecto Livre*** | |

[^ruffian]: Nome alternativo para o arquétipo _Rufião_ do _Fate of Umdaar_.

| **Perícias** | | | | |
| ---: | :--- | :--- | :--- | :--- |
| ***Ótimo (+4)*** | Vigor | | | |
| ***Bom (+3)*** | Lutar | Vontade | | |
| ***Razoável (+2)*** | Saberes | Provocar | Condução | |
| ***Regular (+1)*** | Percepção | Atletismo | Empatia | Comunicação |

| **Façanhas** | |
| ---: | :--- |
| ***Armadura da Ordem da Torre Cintilante*** | Como cavaleiro da Ordem da Torre Cintilante, Raz possui uma armadura pesada capaz de protegê-lo de praticamente quaisquer ataques que ele sofra. A _armadura_ concede a Raz Armadura:2, mas enquanto estiver usando a Armadura, ele terá sobre si o aspecto _Desconfortável_. |
| ***Forte como um touro*** | Uma vez por cena, dobre o valor de _Vigor_ de Raz quando ele usar a perícia em uma _Ação de Superar_ para superar um obstáculo físico. Em caso de sucesso, coloque em cena o aspecto situacional _Destroços_ sem invocações gratuitas nele. |
| ***Mato no peito!*** | Raz tem uma _Consequência Suave (2)_ adicional, que pode ser usada apenas para absorver danos físicos. |


| **Estresses e Consequências** | |
| ---: | :--- |
| ***Estresse Físico*** | [ 1 ] [ 1 ] [ 1 ] [ 1 ] [ 1 ] [ 1 ] |
| ***Estresse Mental*** | [ 1 ] [ 1 ] [ 1 ] [ 1 ] [ 1 ] [ 1 ] |
| ***Consequência Suave (2)*** | |
| ***Consequência Suave para dano físico apenas (2)*** | |
| ***Consequência Moderada (4)*** | |
| ***Consequência Severa (6)*** | |

---
