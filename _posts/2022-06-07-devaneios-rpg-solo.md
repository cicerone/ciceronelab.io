---
layout: post
title: "Devaneios: Um RPG solo sobre sonhar acordado"
lang: pt-BR
date: 2022-06-07 11:30:00 -0300
tags: RPG Folheto Minimalista Sistema Autoral Solo Devaneios
---

*Devaneios* é um jogo sobre sonhar acordado e deixar a mente fluir
livremente no mundo ao redor dela. Você não precisa de muitas coisas
para jogar, apenas de um cronômetro, um lápis, um papel e um baralho
comum de 52 cartas.

Segue aqui a versão não diagramada. Em breve, eu divulgarei o link para a versão diagramada do jogo.

## Estabelecendo o *Olho da Mente*

O *Olho da Mente* é o seu personagem em Devaneios. Para
criá-lo, puxe quatro cartas do baralho com as faces viradas para baixo,
devolva uma e então revele as três cartas restantes. No final deste
panfleto, tem uma tabela com uma palavra associada a cada carta. Crie
uma descrição para o *Olho da Mente* com base nas palavras reveladas
pelas cartas, e escreva essa descrição no papel.

Agora estabeleça uma duração para o jogo. Pode ser um minuto, dez
minutos, duas horas, o que você achar mais confortável. Não existe uma
quantidade de tempo certa para jogar Devaneios.

## Jogando *Devaneios*

Embaralhe as cartas. Olhe ao seu redor e escolha um objeto qualquer.
Pode ser um sofá, um quadro, um relógio, uma caneta, enfim, qualquer
coisa. Puxe quatro cartas do baralho com as faces viradas para baixo,
devolva uma e então revele as três cartas restantes. Consulte a tabela
ao final do panfleto para ver quais as palavras associadas a essas
cartas. Interpretando o *Olho da Mente* que você criou anteriormente,
crie um devaneio sobre o objeto escolhido usando as três palavras que as
cartas escolheram. Se você conseguir criar um devaneio, anote no papel,
embaralhe as cartas, escolha um outro objeto e repita.

## Fim do jogo

O jogo acaba quando o tempo estabelecido se esgota, ou caso você não
consiga criar um devaneio usando as palavras definidas pelas cartas
puxadas. Em ambos os casos, tente criar uma historinha usando os
devaneios criados como se fosse o *Olho da Mente* escrevendo.

## Significado das cartas

| **Paus** | |
| :---: | :---: |
| Ás | Fogo |
| 2 | Domínio |
| 3 | Virtude |
| 4 | Complitude |
| 5 | Contenda |
| 6 | Vitória |
| 7 | Valor |
| 8 | Rapidez |
| 9 | Força |
| 10 | Opressão |
| J | Ajuda |
| Q | Traição |
| K | Poder |

| **Copas** | |
| :---: | :---: |
| Ás | Água |
| 2 | Amor |
| 3 | Abundância |
| 4 | Luxúria |
| 5 | Desapontamento |
| 6 | Prazer |
| 7 | Deboche |
| 8 | Indolência |
| 9 | Felicidade |
| 10 | Saciedade |
| J | Romance |
| Q | Exploração |
| K | Abuso |

| **Espadas** | |
| :---: | :---: |
| Ás | Ar |
| 2 | Paz |
| 3 | Tristeza |
| 4 | Trégua |
| 5 | Derrota |
| 6 | Ciência |
| 7 | Futilidade |
| 8 | Interferência |
| 9 | Crueldade |
| 10 | Ruína |
| J | Tirania |
| Q | Diplomacia |
| K | Autoridade |

| **Ouros** |  |
| :---: | :---: |
| Ás | Terra |
| 2 | Mudança |
| 3 | Trabalho |
| 4 | Poder |
| 5 | Preocupação |
| 6 | Sucesso |
| 7 | Falha |
| 8 | Prudência |
| 9 | Ganho |
| 10 | Riqueza |
| J | Herança |
| Q | Amante |
| K | Patrono |

## Créditos e licenças

O significado das cartas foi baseado no *Tarot de Thoth*, criado por
Aleister Crowley e pintado por Lady Frida Harris.

*Devaneios* está licenciado por Lu *Cicerone* Cavalheiro
sob licença Creative Commons CC-BY 4.0 Internacional.

---

