---
layout: post
title: "Fate of Umdaar: como criar um Campeão"
lang: pt-BR
date: 2022-05-15 18:40:00 -0300
tags: RPG Fate Evil-Hat Masters-of-Umdaar Fate-of-Umdaar-playtest
---

_Campeões_ é o nome dado no _Fate of Umdaar_ para os personagens dos jogadores. Um _campeão_ é definido como alguém que luta por uma Umdaar melhor, se opõe aos tirânicos Mestres, explora o desconhecido e fortalece os vínculos existentes em sua comunidade. Além disso, por ser um jogo usando o sistema Fate, espera-se que um _campeão_ seja um personagem proativo, capaz[^sobre_capacidade], e disposto a ter uma vida dramática.

[^sobre_capacidade]: Seguindo uma tradição que remonta de antes mesmo do _Feite Moça_, em Fate _ser capaz_ **não** significa _não possuir deficiências_. Tal como no mundo real, pessoas com deficiências visíveis ou invisíveis são comuns em Umdaar, e essas pessoas não são definidas por estas, mas por suas ações e compaixão para com o mundo e as pessoas ao seu redor.

Não se trata, portanto, de um personagem qualquer, mas de um tipo bem específico. Antes mesmo de descrever as regras de criação de personagens, o _Fate of Umdaar_ se permite apresentar quais são os traços definitórios de um _campeão_:

+ Um _campeão_ sempre faz a coisa certa.
    Umdaar é um mundo com uma dose bem alta de problemas. Os _campeões_ estão aí para lutar o bom combate porque é preciso que alguém o lute, não para acumular riquezas ou artefatos poderosos.
+ Um _campeão_ salva o dia.
    Não importa o tamanho da dificuldade, um _campeão_ sempre vence no final. Umdaar não é um cenário cinzento ou mesmo niilista para que as ações deles sejam invalidadas ou torcidas ao ponto de só haver vitórias parciais e condicionais difíceis de aceitar.
+ O poder da amizade e da comunidade.
    Um _campeão_ não luta somente por si, mas por um mundo melhor, e é desse mundo que ele extrai a força e a coragem necessária para enfrentar o que precisa ser enfrentado.
+ A família expandida.
    _Família_ vai muito além de vínculos sanguíneos ou do conceito de família nuclear (pai, mãe e filhos). Por menos normativa que a família de um _campeão_ seja, ela ainda é uma família, e em muitos aspectos espera-se que o grupo de personagens se torne uma família em seus próprios termos.
+ Um _campeão_ vai para onde ele é solicitado.
    Um _campeão_ ajuda os necessitados, mas quando solicitado a isso. O real motivo por trás desta cláusula é evitar as narrativas do _Grande Salvador Branco_, e ao mesmo tempo empoderar as comunidades que serão ajudadas pelos personagens: elas não pedem ajuda por fraqueza moral ou por inércia cultural (duas das principais razões pelas quais um _Grande Salvador Branco_ se mete na vida de comunidades alheias), mas por reconhecer no _campeão_ alguém capacitado para ajudá-las com a questão em mãos.

Dito isso, o _Fate of Umdaar_ discorre sobre os passos necessários para a criação de um _campeão_.

## Aspectos

Talvez o elemento mais característico de Fate, um _aspecto_ é uma frase curta que descreve seu personagem ou algo importante relacionado a ele. Como sempre, _aspectos são sempre verdadeiros_, e a partir daí eles podem ser usados para introdução de elementos na narrativa ou para alteração de elementos mecânicos do jogo, como de costume.

Em _Fate of Umdaar_ usa-se os aspectos _Conceito_[^high_concept], _Motivação_[^motivation], _Origens_[^background], _Relacionamento_[^relationship] e _Aspecto Livre_[^free_aspect]. De todos estes, o aspecto _Conceito_ é o mais complexo, pois deve sumarizar tanto a _bioforma_ quanto o _arquétipo_ do personagem. Isso não quer dizer que os outros aspectos não sejam importantes ou relevantes, apenas que eles não são rigidamente definidos por uma fórmula.

[^high_concept]: Tradução livre de _high concept_.

[^motivation]: Tradução livre de _motivation_.

[^background]: Tradução livre de _background_.

[^relationship]: Tradução livre de _relationship_.

[^free_aspect]: Tradução livre de _free aspect_.

### Aspecto _conceito_

O aspecto _Conceito_ é responsável por definir e apresentar sucintamente quem o personagem é. Traçando um paralelo, criar um aspecto gera uma frase bem parecida com aquela que você diz ao oferecer uma descrição bem simplificada de seu super-herói favorito.

Em _Fate of Umdaar_, porém, o aspecto _Conceito_ precisa incluir duas coisas: sua _bioforma_, e seu _arquétipo_. Isso é importante para que você possa invocar (e compelir!) características tanto de uma quanto da outra sem precisar de aspectos adicionais. É claro, o _Conceito_ não deve se restringir a isso, do contrário todo _Homem-cobra Paladino_ seria a mesma coisa, e não campeões distintos e únicos. Lembre-se de adicionar outras coisas ao seu _Conceito_ quando estiver criando seu campeão, para não acabar com algo do tipo "meu personagem é um guerreiro de tantos níveis e mago de tanto níveis que tem esse e aquele talentos e itens mágicos".

#### Arquétipo

O _arquétipo_ de um campeão representa seu treinamento, proficiência, experiência e nicho no mundo. Ele pode ser um guerreiro, um xerife[^marshall], um paladino, entre muitas outras opções. O _Fate of Umdaar_ traz 18 arquétipos prontos, bem como as instruções sobre construir um arquétipo personalizado caso nenhum desses lhe interesse.

[^marshall]: Tradução livre para o termo _marshall_.

Trata-se de uma escolha importante. Cada _arquétipo_ tem uma lista exclusiva de _façanhas_, bem como determina qual será a perícia principal[^lead_skill] do campeão, isto é, a perícia que estará no topo da pirâmide de perícias. Por exemplo, se o arquétipo de um dado campeão for _xerife_, sabemos que o treinamento e a experiência dele residem em atuar conforme a lei e manter a paz. Por conta disso, nada mais natural que sua perícia principal seja _Investigação_ e que ele tenha acesso a façanhas como _**Modus Operandi**: Ao analisar os planos ou local de trabalho de uma pessoa, mesmo se ela não estiver presente, uma vez por cena o campeão pode usar Investigar no lugar de Empatia para criar ou descobrir um aspecto da personalidade dessa pessoa_[^modus_operandi].

[^lead_skill]: Tradução livre do termo _lead skill_.

[^modus_operandi]: Tradução livre do texto original:  
    **Modus Operandi**: When analyzing a person's workplace or plans, even if not present, I may use Investigate as if it were Empathy to create or discover an aspect of their personality once per scene.

Além da perícia principal e de façanhas exclusivas, um arquétipo apresenta perícias com alta sinergia com aquele arquétipo, bem como situações em que o aspecto _Conceito_ pode ser invocado ou forçado em virtude do arquétipo. Um _xerife_ pode invocar seu aspecto _Conceito_ para receber +2 em uma rolagem de Investigação na qual ele esteja agindo em nome da lei, bem como ele pode ser forçado ou mesmo receber uma invocação hostil com base em seu _Conceito_ na hora de lidar com foras-da-lei. Seja criativo!

#### Bioforma

A bioforma de um personagem é o conjunto de características que ele possui em virtude de seu genoma. Em outros jogos, isso costuma ser chamado de _raça_, mas por razões próprias _Fate of Umdaar_ adotou o termo _bioforma_ desde _Masters of Umdaar_. Tal como os arquétipos, o livro já traz 18 bioformas prontas, cada uma com algumas façanhas exclusivas, e instruções sobre como criar mais bioformas caso nenhuma delas o agrade.

Assim como o arquétipo, a bioforma de um personagem é fonte para invocações e compulsões do aspecto _Conceito_. Por exemplo, se a bioforma do personagem possuir asas, nada mais natural que ele possa invocar o aspecto _Conceito_ dele para receber +2 em uma rolagem de _Atletismo_ na qual as asas possam o ajudar. Seja criativo!

### Motivação

O aspecto _Motivação_ define por quais razões seu personagem é um campeão de Umdaar. Ao criar este aspecto, pense em algo que o mantenha indo para a frente, sem perder o foco e sem querer abandonar a luta e a causa. Usando palavras do próprio livro, em caso de dúvida, pense em algo começando com "Eu devo..."[^zeca].

[^zeca]: Desde que não seja a dona Maria da quitanda, você pode dever o que quiser.

### Origens

O aspecto _Origens_ descreve a ligação do campeão com sua comunidade de origem. Ele pode ser um membro leal e ufanista, pode ser um questionador ou mesmo um rebelde, mas existe algum lugar no mundo de Umdaar ao qual o personagem é ligado. Pense sobre isso, afinal, esse aspecto ajuda a definir as origens do seu personagem.

### Relacionamento

O aspecto _Relacionamento_ descreve a natureza do vínculo que você possui com outro campeão do grupo. Pode ser um vínculo positivo ou negativo, e esse vínculo pode ser síncrono (vocês têm o vínculo na mesma conta) ou assíncrono (um dos lados valoriza mais o vínculo do que o outro), mas ainda assim será um vínculo.

### Aspectos livres

Por fim, defina um ou dois _Aspectos Livres_ para o campeão. Podem ser sobre lugares, eventos, pessoas, coisas ou mesmo conceitos importantes para o personagem.

## Perícias

Hora de escolher as perícias do seu campeão. Por padrão, um campeão tem dez perícias organizadas em pirâmide, com uma perícia em _Ótimo (+4)_, duas em _Bom (+3)_, três em _Razoável (+2)_ e quatro em _Regular (+1)_. A única restrição é que a perícia principal do campeão (definida por seu arquétipo) precisa estar no topo da pirâmide – o que, por definição, significa que ela deve ser alocada em _Ótimo (+4)_.

Em _Fate of Umdaar_ usa-se 18 perícias, como já foi dito anteriormente em [Fate of Umdaar: primeiras impressões sobre o playtest](https://cicerone.gitlab.io/2022/05/12/fate-of-umdaar-um-playtest.html), subtítulo _Perícias_. Tirando a restrição mencionada anteriormente, e a recomendação de atribuir bons valores às demais perícias do seu arquétipo, você é livre para definir quais perícias seu campeão terá.

Entretanto, existem duas regras alternativas para atribuição de perícias, e acredito que este seja um bom momento para falar delas.

### Usando Abordagens ao invés de Perícias

_Masters of Umdaar_ usava as Abordagens padrão do _Fate Acelerado_ no lugar das perícias do _Fate Básico_. Se por alguma razão, você acredita que Abordagens funcionam melhor do que Perícias, você pode usá-las no _Fate of Umdaar_ tranquilamente!

No _Apêndice C: Regras Opcionais_, encontramos o subtítulo _Fate Acelerado_, que contém exatamente as regras de conversão do _Fate of Umdaar_ de _Feite Moça_ para _Fate Acelerado_. Basta segui-las (e fazer as adaptações indicadas, é claro!) que você jogará _Fate of Umdaar_ com as regras do _Fate Acelerado_ sem nenhuma dificuldade.

### Fate ABC: usando Abordagens e Perícias ao mesmo tempo

Após o subtítulo _Fate Acelerado_ no Apêndice C, encontramos o subtítulo _Fate ABC: Accelerated Bundled Core_[^no_export], um jeito de usar as Abordagens do _Fate Acelerado_ e as perícias do _Fate of Umdaar_ ao mesmo tempo. Parece complexo, mas isso é só aparência.

[^no_export]: Eu não vou nem tentar traduzir isso de um jeito sucinto. Se um dia houver alguém que vá traduzir isso oficialmente para português do Brasil, ele que se vire.

O Fate ABC usa a regra de _Esferas de perícia_, descrita [neste trecho do _Ferramentas do Sistema_](https://fatesrdbrasil.gitlab.io/fate-srd-brasil/ferramentas-de-sistema/esferas-de-pericias/). São definidas seis Esferas, uma para cada Abordagem, e cada Esfera encompassando três Perícias. O jogador deve classificar as Abordagens de A a F, e então seguir as instruções do subtítulo para gerar uma pirâmide de perícias com base nessa classificação recém-feita. As Abordagens continuam ali, e podem ser usadas para situações em que Perícias pareçam não se encaixar bem, ou para criar Façanhas que usem o valor da Abordagem ao invés da Perícia encompassada por ela. Mais detalhes em um artigo futuro, prometo!

## Recarga

Por padrão, a Recarga em _Fate of Umdaar_ é 3.

## Façanhas

Por padrão, o campeão tem direito a três _façanhas_ gratuitas, e pode comprar mais reduzindo a Recarga, conforme as regras padrão do _Feite Moça_. Você é livre para criar suas próprias Façanhas, mas o livro traz uma quantidade bem grande de façanhas interessantes já prontas para você pegar e usar. Divirta-se!

## Estresse e Consequências

Como no _Feite Moça_. _Vigor_ aumenta o estresse físico, Vontade aumenta o mental. Você começa com três caixas de 1-estresse em cada uma das trilhas, e valores altos de Vigor ou Vontade aumentam a trilha correspondente. Consequências como o usual: uma _Suave_, uma _Moderada_ e uma _Severa_.

## Considerações finais

Agora que demos uma olhadinha no processo de criação de campeões para _Fate of Umdaar_, dá pra ver que ele não é tão diferente assim do processo de criação de personagens do _Feite Moça_, que por sua vez não diverge tanto assim da criação de personagens no _Fate Básico_. As regras de Bioforma e Arquétipos são bem interessantes até mesmo para outros cenários, caso existam diferentes bioformas nele ou você queira se valer do conceito de personagens com diferentes treinamentos específicos. O _Fate ABC_ é uma adição realmente empolgante, mas que merece um texto só para ela a fim de podermos a explorar com o carinho que a criação de Dave Joria merece.

Bom, sobre a criação de campeões, é isso... Podemos dizer que você já está a um passo de enfrentar... **OS MESTRES DE UMDAAR**!

---

