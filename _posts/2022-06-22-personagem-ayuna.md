---
layout: post
title: "Ayuna Pathmos: um personagem para Fate Condensado"
lang: pt-BR
date: 2022-06-22 22:20:00 -0300
tags: RPG Fate Evil-Hat Fate-Condensado Personagens Exemplo CC-BY-SA-4.0-Internacional
---

Sejam bem-vindos a mais uma postagem aqui no site! Hoje eu dei uma pausa no processo de criação de jogos e aventuras para postar um personagem para Fate Condensado, a fim de que vocês possam usá-los à vontade.

Diferente das outras paradas postadas aqui no site, este personagem está licenciado sob **Licença Creative Commons Atribuição-CompartilhaIgual 4.0 Internacional**, cujo resumo em português para leigos pode ser lido em <https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR>


## Ayuna Pathmos (ela/dela)

Ayuna Pathmos nasceu em Kwonlong, um dos distritos mais perigosos da **Cidade**. Quando criança, seu rosto foi desfigurado por ácido atirado nela por um ladrão que tentava roubar as compras que ela fizera. Esse incidente modificou a vida dela, que decidiu virar uma *Mercenária* a fim de não apenas ter dinheiro para pagar suas contas como para evitar que esse tipo de incidente acontecesse com outras pessoas.

Ayuna é uma mulher alta e atlética. Seus cabelos e olhos são negros, mas o lado esquerdo do rosto é todo repuxado por conta da queimadura química e o olho esquerdo dela é cego. Ela é um tanto cônscia demais de como a queimadura afeta sua aparência, e sua vaidade lhe leva a não apenas usar um tapa-olho como a usar cortes de cabelo que permitam esconder o lado desfigurado da face. Ela é, em suas próprias palavras, uma *escopeteira*, preferindo usar armas de fogo mas mantendo a distância curta. Ela gosta do risco e da emoção de ser uma *Mercenária*, e isso para ela é mais valioso do que o dinheiro.

**Nome**: Ayuna Pathmos (ela/dela)  
**Recarga**: 3

| **Aspectos** | |
| ---: | :--- |
| ***Conceito*** | *Mercenária viciada em adrenalina com parte do rosto desfigurada* |
| ***Dificuldade*** | *Excessivamente vaidosa* |
| ***Relacionamento*** | |
| ***Aspecto Livre*** | | 
| ***Aspecto Livre*** | | 

| **Perícias** | | | | |
| ---: | :--- | :--- | :--- | :---- |
| ***Ótimo (+4)*** | Atirar |  |  |  |
| ***Bom (+3)*** | Atletismo |  Vigor |  |  |
| ***Razoável (+2)*** | Provocar | Investigar | Percepção | |
| ***Regular (+1)*** | Recursos | Roubo | Vontade |  Contatos |

**Façanhas**

- ***Diga oi para minha amiguinha!***: Quando Ayuna ameaçar alguém com uma arma carregada, ela pode usar *Atirar* ao invés de *Provocar* para intimidar alguém.
- ***Autópsia instantânea***: Quando Ayuna obtiver um **sucesso crítico** em uma **Ação de Atacar** usando uma espingarda, ela reduzir em um as tensões de dano causadas com o ataque para forçar o defensor a absorver as tensões de dano apenas com **Consequências**. Se ele não conseguir absorver todas as tensões de dano apenas com as **Consequências**, ele será **nocauteado**.
- **Atira na fechadura!**: Ayuna pode usar *Atirar* ao invés de *Roubo* para arrombar portas, janelas, etc., contanto que haja uma fechadura na qual ela possa atirar.

**Estresses e Consequências**

- ***Estresse Físico***: [ 1 ] [ 1 ] [ 1 ] [ 1 ] [ 1 ] [ 1 ]
- ***Estresse Mental***: [ 1 ] [ 1 ] [ 1 ] [ 1 ]

**Consequências**

- ***Suave (2)***
- ***Moderada (4)***
- ***Severa (6)***

---

