---
layout: post
title: "Abdul Hussein: um personagem para Fate Condensado"
lang: pt-BR
date: 2022-06-22 22:20:00 -0300
tags: RPG Fate Evil-Hat Fate-Condensado Personagens Exemplo CC-BY-SA-4.0-Internacional
---

Sejam bem-vindos a mais uma postagem aqui no site! Hoje eu dei uma pausa no processo de criação de jogos e aventuras para postar um personagem para Fate Condensado, a fim de que vocês possam usá-los à vontade.

Diferente das outras paradas postadas aqui no site, este personagem está licenciado sob **Licença Creative Commons Atribuição-CompartilhaIgual 4.0 Internacional**, cujo resumo em português para leigos pode ser lido em <https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR>


## Abdul Hussein (ele/dele)

Abdul Hussein é um dos agentes melhor treinados em espionagem em sua turma na academia da **Polícia do Mercado**. Sua habilidade de entrar e sair dos locais sem ser visto é quase lendária, e sua capacidade de fazer contatos e enganar pessoas é tamanha que muitos consideram um alívio ele ter se juntado à corporação ao invés de uma vida de crime.  Nascido em família pobre, ele decidiu que não deixaria sua origem determinar seu futuro, e se tem uma coisa na qual ele não acredita é destino, fado, e esse tipo de coisa.

Ele é um homem de estatura mediana, de cabelos e olhos pretos e pele levemente dourada por conta da constante exposição ao sol. Ele não usa brincos nem tem tatuagens -- nada que facilite ou permita sua identificação. Suas roupas são sempre casuais, e é difícil vê-lo usando o uniforme da **Polícia do Mercado** mesmo em situações formais. Abdul sabe que é um espião, um homem invisível, e sabe a importância de não chamar a atenção.

**Nome**: Abdul Hussein (ele/dele)  
**Recarga**: 3

| Aspectos | |
| ---: | :--- :
| ***Conceito*** | Espião promissor em luta contra o destino |
| ***Dificuldade*** | Minha vida é ser invisível |
| ***Relacionamento*** | |
| ***Aspecto Livre***  | |
| ***Aspecto Livre***  | |

| **Perícias** | | | | |
| ---: | | | | |
| ***Ótimo (+4)*** | Furtividade | | | |
| ***Bom (+3)*** | Contatos | Enganar | | |
| ***Razoável (+2)*** | Investigação | Roubo | Percepção | |
| ***Regular (+1)***  | Vigor |  Lutar |  Vontade |  Provocar |

**Façanhas**

- ***Ataque furtivo***: Se Abdul estiver sob o efeito de algum aspecto indicando que seu alvo não sabe sua localização, como **Escondido**, ele pode realizar uma **Ação de Atacar** por *Furtividade* ao invés de *Lutar*. A não ser que Abdul tenha um **Sucesso com estilo** no ataque, ele revela sua posição ao alvo, e o aspecto que indicava sua furtividade deixa de existir.
- ***Conheço pessoas***: Quando Abdul realizar uma **Ação de Criar Vantagem** por *Contatos* tentando contactar pessoas envolvidas em atividades duvidosas ou ilícitas, ele recebe **+2** em sua rolagem.
- ***Rosto confiável***: Se Abdul estiver conversando com uma pessoa que não tenha razões para duvidar ou não acreditar nele, ele pode usar *Enganar* no lugar de *Comunicação*.

**Estresses e Consequências**

- ***Estresse Físico***: [ 1 ] [ 1 ] [ 1 ] [ 1 ]
- ***Estresse Mental***: [ 1 ] [ 1 ] [ 1 ] [ 1 ]

**Consequências**

- ***Suave (2)***
- ***Moderada (4)***
- ***Severa (6)***

---

