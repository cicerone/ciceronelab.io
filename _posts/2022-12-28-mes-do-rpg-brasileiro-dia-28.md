---
layout: post
title: 'Mês do RPG Brasileiro 2022 – Dia 28: supers'
date: 2022-12-28 11:23:00 -0300
lang: pt-BR
tags: RPG RPG-Brasileiro RPG-Autoral Mês-do-RPG-Brasileiro-2022
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/NIA3CFPJniY" title="YouTube video player" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Hoje é 28/12/2022, vigésimo-oitavo dia de atividades do Mês do RPG Brasileiro 2022! Dezembro é o mês no qual a comunidade comemora a publicação do primeiro RPG brasileiro, Tagmar, lançado em 21/12/1991.

A temática de hoje é super-heróis. Como não poderia deixar de ser, aproveitei para falar do primeiro RPG que eu escrevi, o Fast Heroes D6, um jogo minimalista cuja proposta é emular quadrinhos e desenhos animados do gênero de um modo acessível para crianças de 4 a 6 anos. Se você quiser introduzir o hobby para uma criança, ele é uma ótima recomendação. Obtenha o PDF gratuitamente em <https://cicerone.gitlab.io/publicacoes>, e se você gostar do jogo e puder e quiser, eu aceito uma contribuição para o meu cafezinho via Pix.

<details>
  <summary markdown="span">Clique aqui para ver minha chave Pix</summary>
  
  lu.cicerone.cavalheiro@gmail.com
</details>

Este vídeo pode ser assistido no YouTube em <https://youtu.be/NIA3CFPJniY>, e a playlist das minhas atividades no Mês do RPG Brasileiro 2022 pode ser conferida em <https://www.youtube.com/playlist?list=PL44VOrX_-JdPfNHOfEIm0AS7ikOlpp9xN>. Espero que você tenha gostado, e até amanhã com mais uma atividade!

Música de Fundo:    
"Crusade - Heavy Industry" by Kevin MacLeod (<https://incompetech.com>)    
Licensed under Creative Commons: By Attribution 4.0 License    
<http://creativecommons.org/licenses/by/4.0/>

---

