---
layout: post
title: 'O portão da Escola Municipal Pandiá Calógeras – uma aventura de horror cósmico para Fate Condensado'
date: 2022-12-24 15:50:00 -0300
lang: pt-BR
tags: RPG RPG-Brasileiro RPG-Autoral Aventura Fate Fate-Condensado
---

É com prazer que eu anuncio mais um jogo de minha autoria!

![o-portao-da-empc](/assets/Publicações/OpEMPC.jpg)

Os alunos da _Escola Municipal Pandiá Calógeras_, em São Paulo, tem o hábito de realizar um trote no mínimo curioso com os calouros. Sempre que um aluno novo chega na escola, o grupo dos “descolados” o leva para um portão nos fundos do pátio cuja pintura amarela está descascando, as marcas de ferrugem são visíveis e existe uma marca de tinta vermelha em forma de mão. Ninguém sabe o que há atrás do portão, pois nem mesmo o diretor da escola sabe onde a chave está e nunca houve o interesse por o arrombar para resolver o mistério.

Nesta aventura não será diferente. Os jogadores interpretarão alunos calouros da _Escola Municipal Pandiá Calógeras_, e serão levados a realizar o trote, mesmo que seja contra suas vontades. Entretanto, existem certas coisas que nunca deveriam ser mexidas, ou _invocadas_...

A aventura **O portão da Escola Municipal Pandiá Calógeras: uma aventura de horror cósmico para Fate Condensado** usa as regras do [Fate Condensado](https://fatesrdbrasil.gitlab.io/fate-srd-brasil/fate-condensado/).

Quem quiser contribuir com o meu cafezinho, eu aceito Pix, certo?

<details>
  <summary markdown="span">Clique aqui para ver minha chave Pix</summary>
  
  lu.cicerone.cavalheiro@gmail.com
</details>

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/Aventuras%20do%20Cicerone/Volume%201/O%20port%C3%A3o%20da%20Escola%20Municipal%20Pandi%C3%A1%20Cal%C3%B3geras/O-port%C3%A3o-da-Escola-Municipal-Pandi%C3%A1-Cal%C3%B3geras.pdf?inline=false)

---

