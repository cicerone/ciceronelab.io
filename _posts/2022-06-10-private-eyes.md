---
layout: post
title: "Private Eyes: um RPG minimalista de investigação na Londres vitoriana"
lang: pt-BR
date: 2022-06-10 12:20:00 -0300
tags: RPG Folheto Minimalista Sistema Autoral Private-Eyes
---

Em **Private Eyes**, vocês fazem parte de uma agência de detetives particulares na Londres vitoriana.

## Jogadores: Criando seus *Detetives*

- Crie uma ***Descrição*** para o seu Detetive em uma ou duas palavras.  
    Exemplos: **Ex-policial cínico**, **Perito em contabilidade**, **Gênio taciturno**, **O Estressadinho**...  
- Crie uma ***Vantagem*** para o seu Detetive, algo em que ele se destaca.  
    Exemplos: **Pontaria impecável**, **O Sombra**, **Olho de águia**, **Deduções impecáveis**...  
- Crie uma ***Desvantagem***, um problema, uma limitação ou algum defeito que seu Detetive tenha.  
    Exemplos **Dedo frouxo no gatilho**, **Movido por vingança**, **Problemas com figuras autoridade**... **ATENÇÃO**: uma ***Desvantagem*** não é uma desculpa para você ser um babaca!  
- Crie um ***Nome*** para o seu personagem. Pode ser um apelido, se você preferir.
- Seu detetive tem 4 ***Pontos de Vida***.

## Narrador: crie o caso a ser investigado

O Narrador é quem conduzirá a história e adicionará elementos interessantes e relevantes para os detetives descobrirem e, esperançosamente, resolverem o caso. Mas para isso, ele precisa de um caso para vocês investigarem. Primeiramente, o Narrador vai jogar duas moedas duas vezes na tabela a seguir:

| **Moedas** | **Contratante** | **Objetivo do caso** |
| :---: | :---: | :---: |
| Duas Coroas | Scotland Yard | Assassinato ou Desaparecimento |
| Uma Cara, Uma Coroa | Um Nobre | Crime de corrupção ou colarinho branco |
| Duas Caras | Uma pessoa comum | Roubo ou chantagem |

Com base nessas informações, o Narrador irá criar o **Caso**, a investigação que a **Agência** terá que resolver.

## Testes e Conflitos

Uma hora algum dos detetives vai tentar realizar uma ação cujo resultado será incerto ou duvidoso. Atravessar uma rua deserta não é algo incerto, mas pular de um prédio para outro é. Nessas horas, o Narrador vai pedir um **Teste** para os detetives envolvidos.

Para realizar um **Teste**, jogue duas moedas. Se a **Descrição** ou **Vantagem** do detetive forem relevante no teste, jogue mais uma moeda para cada e fique com o melhor resultado possível. Se a **Desvantagem** for relevante, jogue mais uma moeda e fique com o pior resultado possível. Se tanto a **Desvantagem** quanto a **Descrição** ou o **Vantagem** forem relevantes, jogue o número apropriado de moedas e fique com o pior resultado possível. Além disso, se o Narrador julgar que a descrição da ação é particularmente boa ou ruim, ele poderá aumentar ou diminuir o número de moedas que jogador usará em seu teste.  Entretanto, não importa quantas moedas seu detetive perca, ele nunca jogará menos do que duas moedas.

**Conflitos** são tipos especiais de **Testes** nos quais alguém está tentando ferir ou mesmo matar outro alguém. Em um conflito, o Narrador não joga moedas: ele descreve a ação do agressor e pergunta ao detetive alvo da ação como ele irá se defender. Basicamente, um **Conflito** funciona como um **Teste**. O Narrador calcula quantas moedas seu detetive vai jogar, e então você consulta os resultados abaixo:

- **Duas Coroas**: **Sucesso Total**  
    Em um sucesso total, o detetive consegue realizar a ação que ele pretendia de um jeito melhor do que o esperado. Cabe ao Narrador descrever como o detetive realizou sua ação melhor do que o esperado. Em um conflito, o detetive conseguiu causar 1 PV de dano em seu agressor. Além disso, escolha uma opção entre:
    - Causar 1 PV de dano adicional;
    - Fazer uma pergunta sobre a cena para o Narrador, que deverá responder clara e honestamente;
    - Obter um **Vantagem** temporária que ele possa usar na cena;
    - Ter mais uma moeda em um próximo teste.
- **Uma Cara, uma Coroa**: **Sucesso Parcial**  
    Em um sucesso parcial, o detetive consegue realizar a ação que ele pretendia do modo que ele esperava. Em um conflito, tanto o detetive quanto o agressor sofreram 1 PV de dano cada. Além disso, escolha uma opção entre:
    - Fazer uma pergunta sobre a cena para o Narrador, que deverá responder de maneira vaga porém honestamente;
    - Notar algo na cena que ele não havia notado antes.
- **Duas Caras**: **Falha**  
    Em uma falha, seu detetive não conseguiu realizar a ação desejada. Em um conflito, apenas o detetive sofreu o ataque, e por isso somente ele sofreu 1 PV de dano. Além diso, escolha uma opção entre:
    - Deixar de notar algo importante ou relevante na cena, como uma armadilha ou uma pista para o caso, o que passará a contar como uma **Desvantagem** para o detetive;
    - Sofrer algum tipo de acidente e perder 1 PV, ou perder 1 PV adicional se ele estiver em um conflito;
    - Sofrer uma **Falha Dramática** e ganhar um **Ponto Dramático**.

Uma **Falha Dramática** significa não apenas que seu detetive falhou na ação que ele tentou fazer. Simplesmente o pior possível aconteceu. Se ele estava se infiltrando em um lugar, você não apenas chamou atenção: você tem agora seguranças com armas apontadas para você querendo saber o que você está fazendo ali. Se você estava tentando arrombar uma fechadura, você não apenas falhou: você enjambrou o mecanismo e agora aquela fechadura não pode ser aberta nem pela chave dela. O Narrador deve ser criativo na hora de descrever uma **Falha Dramática**. A orientação aqui é simples: qualquer coisa que não mate o detetive, mas o deixe em uma situação extremamente desagradável é adequada.

Cabe ao Narrador determinar quantos PVs os agressores têm. Capangas comuns, como músculos contratados ou membros de gangue de baixo escalão, devem ter 1 ou 2 PVs apenas. Um agressor melhor treinado, como um soldado ou policial veterano, ou um boxeador fazendo bico de segurança, pode ter 3 ou 4 PVs. Grandes vilões, caso eles desejem enfrentar os detetives fisicamente, podem ter 5 ou até mesmo mais PVs.

Um agressor que tenha seus PVs reduzidos a 0 é nocauteado, e cabe aos detetives decidirem o que farão com eles. Da mesma forma, se todos os detetives presentes no conflito forem nocauteados, eles terão seus destinos decididos pelos agressores. Narradores, lembrem-se: há destinos **muito piores** do que a morte...

## Conclusão do Caso

Caso os detetives consigam concluir o caso, o Narrador deve dar a cada um deles um **Ponto Dramático**.

Ao final de cada caso, todos os detetives recuperam seus PVs. Além disso, um detetive pode usar 3 **Pontos Dramáticos** para:

- Alterar sua ***Desvantagem***. Um detetive nunca pode se livrar de ter uma ***Desvantagem***, não importa o que aconteça;
- Alterar uma ***Vantagem***;
- Adquirir uma nova ***Vantagem***.

---

