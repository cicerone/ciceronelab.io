---
layout: post
title: "Dervixe, um arquétipo para Fate of Umdaar (incidental: Como criar um arquétipo para Fate of Umdaar)"
lang: pt-BR
date: 2022-05-25 13:00:00 -0300
tags: RPG Fate Evil-Hat Masters-of-Umdaar Fate-of-Umdaar-playtest Fate-Condensado Arquétipos-para-Fate-of-Umdaar Exemplo
---

E está na hora de meter a mão na massa! Ainda na esteira do playtest do _Fate of Umdaar_, decidi criar alguns personagens, a fim de que vocês possam saborear como o jogo está. Lembrando que as regras usadas são as disponíveis no material distribuído pela Evil Hat para os participantes do playtest, e que este personagem pode se tornar incompatível com a versão final.

Entretanto, ao criar a [Shir Mali](https://cicerone.gitlab.io/2022/05/25/personagem-shir-mali.html), eu notei que precisaria criar um arquétipo para ela! Sendo assim vou aproveitar a situação para falar um pouco sobre a criação de arquétipos para _Fate of Umdaar_. Vamos nós?

## Criando um arquétipo

Citando o _Fate of Umdaar_, um arquétipo define o treinamento, a ocupação, a experiência e o nicho ocupado por um campeão. Sendo assim, torna-se preciso definir claramente qual é o treinamento e o nicho ocupado pelo arquétipo a ser criado. Idealmente, essa definição é concisa o suficiente para não se tornar, ela própria, um livro, mas clara o bastante para ninguém ter dúvidas sobre o que aquele arquétipo é.

A seguir, é preciso definir quais são as perícias com maior sinergia com aquele arquétipo. Escolha quatro perícias, sendo que uma delas sera a _perícia principal_[^lead-skill] do arquétipo, ou seja, aquela que é a mais característica e, por isso, deverá ir para o topo da pirâmide de perícias.

[^lead-skill]: Tradução livre para o termo _lead skill_.

Opcionalmente, você pode querer definir possíveis variações para aquele arquétipo. As variações são pequenas mudanças de foco ou treinamento, representadas pela inclusão ou alteração da lista de perícias com maior sinergia com aquele arquétipo. Por exemplo, o arquétipo de [Raz Duringol](https://cicerone.gitlab.io/2022/05/25/personagem-raz-duringol.html), Cavaleiro, é uma variação do arquétipo Rufião que acrescenta _Vontade_ à lista de perícias com maior sinergia com o arquétipo.

A seguir você deve definir seis _Façanhas_ características para o arquétipo. Três delas podem ser escolhidas livremente por qualquer campeão que adote aquele arquétipo, mas as outras três devem estar associadas a um dos três tipos de locais disponíveis em _Fate of Umdaar_: as Terras Livres[^freelands], os Domínios Sombrios[^dread-domains] e os Caosnexus[^wildernexuses]. Estas três _façanhas_ podem ser escolhidas de acordo com o foco do personagem ou do jogo: em um jogo de Dignatários, as façanhas associadas às Terras Livres são mais adequadas; em um jogo de Rebeldes, as dos Domínios Sombrios; e em um jogo de Arqueonautas, as de Caosnexus.

[^freelands]: Tradução livre para _Freelands_.

[^dread-domains]: Tradução livre para _Dread Domains_.

[^wildernexuses]: Tradução livre para _Wildernexuses_.

E com isso, parabéns, seu arquétipo está criado! Após essa discussão breve, vamos à criação do arquétipo _Dervixe_.

## Dervixe – Atletismo

Dervixes são guerreiros que combinam dança e habilidade com espadas em uma arte bela, porém letal. Suas origens podem variar, desde guerreiros sagrados até artistas circenses que aprenderam a se defender das várias depredações e agressões a que estão sujeitos. Porém, todos os dervixes têm em comum a fluidez de seus movimentos, o fato de nunca ficarem parados enquanto lutam, e o desprezo por armas ou armaduras que inibam seus movimentos.

+ Perícias: Atletismo, Lutar, Enganar, Vigor

**Dançarina de batalha**: A técnica de um Dervixe parece um misto de dança e luta, com grande ênfase em não ficar parado e movimentos fluidos. Sempre que o campeão realizar uma _Ação de Criar Vantagem_ por _Atletismo_ a fim de criar vantagens relacionadas a movimento, receba um bônus de +2 na rolagem.

**Movimentação defensiva**: Um dervixe sempre está em movimento, e pode usar essa movimentação para não apenas evitar um ataque como melhor se posicionar em um conflito. Uma vez por cena, se o campeão obtiver sucesso em uma _Ação de Defender_, ele pode se deslocar para uma zona adjacente como parte de sua defesa, considerando que não haja impedimentos para que ele se desloque para a zona desejada.

**Resistência aeróbica**: Os anos de treinamento como dançarino melhoraram muito a resistência física do dervixe. O campeão recebe duas caixas de consequências físicas adicionais.

**Terras Livres – Minha reputação me precede**: Um dervixe pode usar sua reputação como dançarino e campeão para fazer amigos e se conectar com as pessoas. Uma vez por sessão, você pode capitalizar em cima de sua reputação para usar _Atletismo_ no lugar de _Contatos_ ou _Comunicação_ em uma rolagem. Fazer isso tornará sua presença pública no local, automaticamente removendo quaisquer aspectos relacionados à furtividade ou ocultação de sua identidade que o campeão porventura tenha.

**Domínios Sombrios – Aparentemente inofensivo**: Um dervixe pode capitalizar em cima de sua aparência e técnica de dançarino para evitar chamar a atenção. Receba +2 em _Ações de Superar_ por _Enganar_ quando estiver lidando com oponentes que não saibam que o campeão é um dervixe.

**Caosnexus – Contra-golpe**: A técnica de um Dervixe o permite realizar movimentos nem sempre fáceis de serem previstos por um oponente, e essa movimentação fluida pode ser usada para revidar um ataque. Sempre que o campeão obtiver um sucesso com estilo em uma _Ação de Defender_ por _Atletismo_ contra um ataque corpo-a-corpo, ele pode causar duas tensões de dano físico em seu atacante ao invés de receber um impulso. O atacante não terá direito a uma _Ação de Defender_ contra essas tensões de dano.

---

