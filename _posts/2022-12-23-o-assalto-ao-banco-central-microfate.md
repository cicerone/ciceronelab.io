---
layout: post
title: 'O Assalto ao Banco Central – uma aventura para MicroFATE'
date: 2022-12-23 12:10:00 -0300
lang: pt-BR
tags: RPG RPG-Brasileiro RPG-Autoral MicroFATE Aventuras
---

![o-assalto-ao-banco-central](/assets/Publicações/MF-ABC.jpg)

Quero apresentar a vocês uma aventura que há muito escrevi, mas só agora lembrei de publicar. **O Assalto ao Banco Central** é uma aventura para **MicroFATE**, que você pode baixar gratuitamente [aqui](https://cicerone.gitlab.io/publicacoes/#microfate). Como de praxe, o download é gratuito, mas se você quiser contribuir para o meu cafezinho, você pode me mandar um Pix que eu vou ficar muito feliz ;-)

<details>
  <summary markdown="span">Clique aqui para ver minha chave Pix</summary>
  
  lu.cicerone.cavalheiro@gmail.com
</details>

## Sobre a aventura

A **Cidade** é uma grande, bela e próspera metrópole. Ela tem seus problemas, como qualquer cidade tem, mas o maior deles é **Lord Shadowdread**, um supervilão cujos esquemas quase sempre envolvem conquistar a **Cidade** de alguma forma. Ele é a maior ameaça à **Cidade**, e a mais frequente fonte de problemas que os **Heróis** precisam resolver.

Desta vez, os capangas de **Lord Shadowdread** estão assaltando o Banco Central da **Cidade**. A polícia foi chamada, mas ela está sendo incapaz de resolver o problema sozinha, e por isso os **Heróis**, mais uma vez, são necessários para salvar o dia.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/MicroFATE/Portugu%C3%AAs/Aventuras/O%20Assalto%20ao%20Banco%20Central/microfate-o-assalto-ao-banco-central.pdf?inline=false)

---


