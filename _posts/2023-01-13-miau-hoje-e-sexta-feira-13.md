---
layout: post
title: 'Miau! Hoje é Sexta-Feira 13! – um jogo solo sobre os riscos que gatos pretos correm nestes dias'
date: 2023-01-13 11:09:00 -0300
lang: pt-BR
tags: Geral RPG Jogo-autoral RPG-histórico Crítica-Social RPG-Solo Jogo-Solo
---

E é hora de anunciar mais um joguinho meu!

![miau](/assets/Publicações/MHSF13.jpg)

Em **Miau! Hoje é Sexta-Feira 13!**, você é um gato preto que tem liberdade para vaguear pelas ruas. Gatos são animais muito inteligentes, e você não é uma exceção. Você sabe que hoje é um dia perigoso: sexta-feira 13. Humanos têm crenças estapafúrdias sobre esse dia, e você sabe que parte dessas crenças pode colocar você em risco. Durante o dia de hoje, você passará por alguns eventos, e terá que contar com sua sorte e habilidade para evitar que o pior aconteça.

**Miau! Hoje é Sexta-Feira 13!** é um jogo solo baseado nas superstições relacionadas a gatos pretos e sextas-feiras 13, bem como o risco que esses animaizinhos inocentes correm nesses dias. Em nenhum sentido este jogo é um endosso a tais crenças ou práticas – muito pelo contrário: é uma crítica e um alerta. Se você possui um gatinho preto e o permite passear na rua, tome bastante cuidado nesses dias, pois pode ser que seu filhote peludo não volte.

Caso você goste do meu trabalho e queira contribuir com o meu cafezinho, eu aceito Pix.

<details>
  <summary markdown="span">Clique aqui para ver minha chave Pix</summary>
  
  lu.cicerone.cavalheiro@gmail.com
</details>

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/Jogos%20Solo/Miau!%20Hoje%20%C3%A9%20Sexta-Feira%2013!/Miau-hoje-e-sexta-feira-13.pdf?inline=false)

---

