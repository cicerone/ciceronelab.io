---
layout: post
title: 'A fala de MC Pipokinha sobre professores: por que estão todos se ofendendo?'
date: 2023-03-13 23:10:00 -0300
lang: pt-BR
tags: MC-Pipokinha Artigo-de-opinião Verdades-incômodas Hipocrisia
---

Muito se fala na tal MC Pipokinha, cujo nome eu nunca tinha ouvido antes. Mas fica a pergunta: ela falou alguma mentira? Ela ganha em uma hora de show mais do que muitos professores ganham em um ano, isso é verdade. O professor trabalha muito mais do que ela e é pago para aturar desaforo de filho dos outros, de direções, coordenações e governos, e isso é verdade. Tudo que ela faz é dançar e cantar pra ter uma fortuna, enquanto um professor se desdobra em três, quatro escolas para ter dignidade, isso é verdade.

Faço, pois, a pergunta: a quem a verdade ofende?

Quem fala a verdade não se sente ofendido por ela, pois sabe não estar falando da coisa o que ela não é, ou não estar falando da coisa o que ela é. Quem conhece a verdade não deveria se ofender com quem fala a verdade, pela mesma razão. Oras, se a verdade não ofende a quem a fala nem a quem a conhece, a quem ela ofende então?

Resposta: ao hipócrita, aqui definido como "a pessoa que conhece a verdade mas a nega, substituindo-a por um fingimento pessoal ou coletivo".

Ofender-se com as declarações da tal MC, que – reitero – não conhecia antes desse episódio, é pura hipocrisia. Todo e qualquer cidadão brasileiro sabe o quão precária é a carreira docente – incluindo as crianças, cujos sonhos para o próprio futuro são se tornarem jogadores de futebol, MCs, ou outras carreiras que rendem muito dinheiro – ao mesmo tempo que incentivam seus jovens a não a procurarem – inclusive muitos professores, e eu me coloco nessa lista, pois eu sei o que é ser um jovem periférico julgado pela sociedade por conta de sua aparência e suas posses e nada mais. Não é segredo algum para a sociedade o quão massacrado o exercício da docência é pela sociedade – basta correr os olhos pela lista de pacientes de qualquer psiquiatra para ver o número de professores que passam pelas mãos dele.

Mas porque foi uma MC que falou isso, e não uma pessoa branca ocupando uma posição central na sociedade, ela é execrada pela sociedade. O opróbrio que se derrama sobre a artista é ainda de maior hipocrisia quando se lembra que ela, intencionalmente ou não, levantou, ao modo dela, a mesma tese de Darcy Ribeiro sobre o sucateamento da educação brasileira. Vamos execrar Darcy Ribeiro também? Que tal apedrejarmos Paulo Freire também, que defendia tese sobre o ensino brasileiro bem similar às declarações da cantora? "Ah, mas eles são educadores e ela, uma funkeira!", os hipócritas dirão. Eis minha resposta: se teóricos da educação e uma artista para mim desconhecida dizem a mesma coisa, é um claro sinal de que a educação brasileira é um trem descarrilado misturado com colisão frontal entre dois caminhões-tanque.

Antes que minha postagem descarrile como a hipocrisia da sociedade brasileira, cito Mustafa Mond, personagem de Aldous Huxley em Admirável Mundo Novo: "entre conforto e liberdade, as pessoas escolherão o conforto". A MC tem o que se considera conforto por nossa sociedade – muitas posses materiais –, enquanto o professor não tem. Isso explica porque crianças querem ser MCs, mas não querem ser professores. Quem não gostaria de ganhar R$ 140.000,00 por hora trabalhada? Certamente, eu adoraria. Certamente, qualquer um adoraria. Pois bem, a MC ganha isso por hora trabalhada, porém um professor não faz isso nem em cinco anos de trabalho duro.

Criticar a tal MC (cujo nome nem lembro mais, de tanto que a desconheço) não passa de hipocrisia. Retornando à definição de hipócrita apresentada anteriormente, são pessoas que conhecem a verdade – a precarização e o sucateamento da docência no Brasil – mas a substituem por um fingimento individual ou coletivo – a romantização do exercício da docência.

Então, meu caro hipócrita, vá tomar bem no meio do olho do seu cu, por favor.

Lu Cavalheiro, 12/03/2023
Licenciatura Plena em Filosofia pela UERJ (porque se é pra cagar título, eu também tenho o meu)

---

