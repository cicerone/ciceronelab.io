---
layout: post
title: "Relato de sessão: A Necrópole Amaldiçoada, uma aventura para Fate of Umdaar"
lang: pt-BR
date: 2022-06-03 08:20:00 -0300
tags: RPG Divulgação Workshop Eventos RPGWorld Lampião-Game-Studio SESC SESC-São-João-de-Meriti Fate-of-Umdaar-playtest Relato-de-sessão Oneshot
---

Moradores do Grande Rio e adjacências, dia 28/05/2022 foi o primeiro evento aberto do _Workshop Formando RPGistas_, uma parceria entre o SESC de São João de Meriti, o RPGWorld e o Lampião Game Studio.

![flyer-workshop](/assets/workshop-formando-rpgistas/divulgacao.jpeg)

Nesse dia, recebi o convite para narrar uma mesa, e, obviamente, levei uma aventura minha para _Fate of Umdaar_, _A Necrópole Amaldiçoada_. Confira abaixo o relato da sessão:

## Jogadores

Contei com quatro jogadores: Tatiana, Lucas, Nicoli e Fernanda. Tatiana já havia jogado Fate comigo em outros eventos promovidos pelo RPGWorld, mas a única experiência de Lucas, Nicoli e Fernanda com RPG tinha sido com D&D.

## Apresentação das regras e do cenário

Os quatro jogadores ouviram atentamente a explicação que dei sobre as regras do Feite Moça e o _Fate of Umdaar_, e se mostraram extremamente atentos e interessados. Outras pessoas no mesmo evento cronometraram o tempo que gastei nessa apresentação: uma hora e trinta minutos!

## A narrativa

Os jogadores escolheram personagens prontos e seguiram para a aventura. Os quatro mostraram grande imersão e interesse pela história, mantendo-se nos personagens e usando liberalmente seus aspectos e pontos de destino para afetar tanto mecânica quanto narrativamente a aventura. Neste ponto, especial mérito precisa ser dado à Fernanda, que dos jogadores novatos em Fate foi quem melhor captou a essência do que um aspecto pode fazer.

As cenas seguiram sem dificuldade. Os jogadores se mantiveram em seus personagens e respeitaram o princípio da _ficção primeiro_ característico de Fate. Ao final da aventura, eles resolveram o mistério de um jeito que certamente eu não esperava, mas que foi muito mais divertido do que o planejamento original. Com isso, eles salvaram mais uma comunidade dos... **MESTRES DE UMDAAR**!

---

