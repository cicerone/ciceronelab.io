---
layout: post
title: "Divulgação: atualização no Workshop Formando RPGistas"
lang: pt-BR
date: 2022-06-16 16:15:00 -0300
tags: RPG Divulgação Workshop Eventos RPGWorld Lampião-Game-Studio SESC SESC-São-João-de-Meriti
---

Moradores do Grande Rio, atenção: o **Workshop Formando RPGistas**, uma parceiria entre o SESC de São João de Meriti, o RPGWorld e o Lampião Game Studio, sofreu atualizações em seu cronograma. A proposta do workshop é realizar uma série de encontros para discutir o que é RPG e introduzir novas pessoas ao hobby em uma linguagem clara e acessível, bem como discutir e abordar conceitos mais familiares a jogadores mais experientes.

![flyer-workshop](/assets/workshop-formando-rpgistas/divulgacao_v2.jpeg)

As atividades do workshop são bem interessantes e práticas (o [_Viva la Revolución_](https://cicerone.gitlab.io/2022/06/12/viva-la-revolucion.html) foi escrito durante o encontro do dia 11/06/2022), mas os organizadores estão com vontade de levar a coisa ainda mais para a prática. Por isso, nos dias indicados na imagem acima, durante a parte da manhã haverá o workshop, com Rafael Wernek e Jorge Valpaços compartilhando conteúdo conosco, e durante a tarde haverá mesas de RPG! Se você não tem interesse em aprender sobre a parte teórica do nosso hobby, pelo menos vá lá para jogar! Aguardamos sua presença!

---

