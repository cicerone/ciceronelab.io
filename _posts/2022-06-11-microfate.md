---
layout: post
title: "MicroFATE: um Fate ainda mais minimalista"
lang: pt-BR
date: 2022-06-11 20:30:00 -0300
tags: RPG Folheto Minimalista Sistema Simplificação MicroFATE Fate
---

**MicroFATE** é uma simplificação que eu fiz das regras de Fate RPG cujo propósito é proporcionar aos jogadores um jogo rápido e ágil sem, no entanto, abrir mão dos elementos característicos do sistema.

## Criação de Personagens

Para criar um personagem em **MicroFATE**, é preciso definir três *Aspectos* e uma *Façanha*. Além destes, um personagem tem também um valor de *Recarga*, que por padrão é 3, seus *Pontos de Destino*, e seus *Estresses e Condições*.

### Aspectos

Um *Aspecto* é uma frase que descreve algo único ou notável sobre o personagem. *Aspectos* são sempre verdadeiros, e essa verdade deve ser ambígua, ou seja, deve tanto poder ajudar quanto atrapalhar o personagem.

- Um **Aspecto *Conceito***, que apresenta sucintamente o que o personagem é. Por exemplo, *Arqueólogo aventureiro*, *Milionário excêntrico vigilante contra o crime*, *Herói alienígena com super poderes*, *Chefe da máfia italiana*, *Cozinheiro de dia, gênio do mal à noite*;
- Um **Aspecto *Dificuldade***, que apresenta sucintamente uma falha dramática ou um problema que complica a vida do personagem. Por exemplo, *Medo paralisante de cobras*, *Sociopata e misantropo*, *Sempre faz o bem, não interessa a quem*, *Amolecido pela idade*, *Incapaz de mentir*;
- Um **Aspecto *Livre***, que apresenta sucintamente uma outra característica única ou notável sobre o personagem. Por exemplo, *Agente secreto disfaçado*, *Amigos na alta sociedade*, *Muitas pessoas me devem favores*, *Fará qualquer coisa para proteger sua esposa*.

Além dos três *Aspectos* de cada personagem, *Personagens do Narrador*, objetos, lugares, cenas e até mesmo situações que estejam acontecendo no cenário possuirão um ou mais *Aspectos*. Tudo em *Fate* é definido por pelo menos um *Aspecto*, e um personagem pode fazer uso mecânico de qualquer *Aspecto* que ele saiba que exista.

Em **MicroFATE**, os *Aspectos* servem para:

- Sempre que a característica descrita por um *Aspecto* for contribuir positivamente em um teste, adicione +1 ao resultado. Se o jogador gastar 1 *Ponto de Destino*, adicione +3, e não apenas +1, ao resultado. Somente 1 *Ponto de Destino* pode ser gasto em um dado *Aspecto* dessa forma em um dado teste. Uma vez por sessão, o personagem pode usar seu *Aspecto Conceito* desta forma sem gastar *Pontos de Destino*;
- Se um *Aspecto* estiver contribuindo positivamente em um teste, o jogador pode gastar 1 *Ponto de Destino* para jogar novamente os dados, devendo aceitar o novo resultado mesmo se ele for pior que o anterior. Somente 1 *Ponto de Destino* pode ser gasto em um dado *Aspecto* dessa forma em um dado teste. Uma vez por sessão, o personagem pode usar seu *Aspecto Conceito* desta forma sem gastar *Pontos de Destino*; Esses dois usos de *Aspectos* são mutuamente excludentes: ou ele adiciona +3 ao invés de +1 em um dado teste, ou ele permite rolar novamente os dados;
- Sempre que a característica descrita por um *Aspecto* for contribuir negativamente em um teste, adicione -1 ao resultado, ou -2 se for o *Aspecto Conceito*. Se um *Aspecto* estiver contribuindo negativamente em um teste, o jogador pode escolher falhar automaticamente no teste e receber 1 *Ponto de Destino* ao invés de rolar os dados.

Quando for avaliar se um *Aspecto* está a contribuir positiva ou negativamente em um teste, adote como critério o bom senso narrativo. Via de regra, se for difícil ou exigir muito pensamento para justificar a contribuição de um *Aspecto* em uma ação, muito provavelmente ele não estará contribuindo para ela.

### Façanhas

Uma *Façanha* é uma habilidade especial que o personagem pode usar em um tipo específico de situação. Elas podem ser oriundas de equipamentos especiais, super poderes, capacidades alienígenas, magia, psiquismo, etc. A não ser que a descrição da *Façanha* diga o contrário, usar uma *Façanha* não requer o gasto de *Pontos de Destino*.

Cada personagem tem direito a uma *Façanha*, que deverá ser criada pelo jogador e aprovada pelo narrador. Caso o jogador queira ter mais *Façanhas*, ele poderá reduzir seu valor de *Recarga* em 1 para adquirir uma *Façanha* adicional. Todavia, o valor de *Recarga* não poderá ser reduzido para menos que 1 desta forma.

Toda e qualquer *Façanha* deverá seguir um dos dois modelos abaixo:

- **Nome da *Façanha***: O personagem adiciona +2 ao resultado do teste em \<tipo\_de\_ação\> quando \<situação\_específica\>;
- **Nome da *Façanha***: O personagem pode \<realizar\_uma\_ação\_que\_o\_personagem\_não\_poderia\_fazer\_normalmente\>.

Em que *\<tipo\_de\_ação\>* deve ser substituído por uma das *Quatro Ações* de Fate:

- **Superar Obstáculo**: lidar com desafios impostos pela narrativa por meio das suas Perícias/Abordagens;
- **Criar Vantagem**: Usar sua perícia/abordagem para preparar alguma coisa que lhe ajudará mais tarde (na forma de um Aspecto);
- **Ataque**: Provocar prejuízo de qualquer ordem a outro personagem;
- **Defesa**: Defender-se do resultado das ações anteriores, reação.

Já *\<situação\_específica\>* indica a condição na qual a *Façanha* poderá ser usada, e *\<realizar\_uma\_ação\_que\_o\_personagem\_não\_poderia\_fazer\_normalmente\>* deve ser entendido como uma ação que a lógica narrativa determina ser impossível de ser realizada por aquele personagem.

O narrador poderá adicionar elementos que clarifiquem mecanicamente os benefícios advindos da *Façanha*. Exemplos:

- **Enredar com chicote**: O personagem adiciona +2 ao resultado do teste em *Ações de Criar Vantagem* quando tentar imobilizar alguém usando um chicote;
- **Visão noturna**: O personagem pode enxergar na escuridão total caso esteja usando seu óculos de visão noturna;
- **Voar**: O personagem pode voar. Ele pode se deslocar livremente para as zonas adjacentes à sua em um conflito sem precisar empregar sua ação na rodada para tal;
- **Piromancia**: O personagem pode usar magia para manipular fogo e chamas. Isso o permite realizar *Ações de Atacar* com efeitos mágicos baseados em fogo contra oponentes na mesma zona ou imediatamente adjacentes que ele em um conflito.

Caso a *Façanha* pareça muito poderosa, o Narrador poderá atribuir a ela o custo de 1 *Ponto de Destino*, conforme discutido no *Fate Básico*. Exemplo:

- **Raio de dominação mental**: Ao custo de 1 *Ponto de Destino*, o personagem pode realizar uma *Ação de Criar Vantagem* para por sobre outro personagem um *Aspecto* relacionado a controle mental. Enquanto o *Aspecto* assim criado não for *superado*, o personagem que usou esta *Façanha* pode dar ordens simples (um verbo imperativo e um objeto apenas) para o personagem mentalmente dominado sem a necessidade de testes.

### Recarga e Pontos de Destino

A *Recarga* de um personagem diz com quantos *Pontos de Destino* mínimos o personagem começará a sessão. Caso ao final da sessão anterior o personagem possua mais *Pontos de Destino* do que seu valor de *Recarga*, não reduza os *Pontos de Destino* do personagem. O valor padrão de *Recarga* é 3, mas isso pode ser ajustado pelo Narrador para melhor se adequar à proposta do jogo.

Os *Pontos de Destino* de um personagem representam a capacidade dele de ser uma força no mundo em que ele existe. Além dos usos mencionados em *Aspectos*, o jogador pode gastar 1 *Ponto de Destino* para declarar um fato na cena em que esteja, como a existência de um item ou pessoa em determinado lugar. Por exemplo, um *Líder de revolta camponesa* poderia gastar 1 *Ponto de Destino* para declarar que o guarda real que ele acabou de capturar possui as chaves do castelo do rei.

Os *Pontos de Destino* podem ser gastos depois da rolagem dos dados. Isso permite que um personagem decida de maneira estratégica se sua própria sorte lhe bastou ou se ele precisará de uma intervenção do *Destino* para ser bem sucedido em sua ação.

## Estresses e Condições

*Estresses e Condições* são a medida do dano sofrido pelo personagem em um conflito. *Estresses* representam danos superficiais, cansaços, confusões ou outras formas não permanentes de dano, enquanto *Condições* são um tipo especial de *Aspecto* que representam os danos duradouros e de difícil recuperação, como um membro quebrado ou uma perturbação mental.

Cada personagem possui quatro caixas de *Estresse*. Cada caixa pode mitigar uma tensão de dano, e o jogador pode marcar quantas quiser e achar apropriado, desde que elas estejam previamente desmarcadas.

Além das caixas de *Estresse*, possui três *Condições*: **Afetado**, com valor 2; **Incapacitado**, com valor 4; e **Desabilitado**, com valor 6. Cada uma dessas *Condições* pode mitigar uma quantidade de tensões de dano igual ou menor ao valor delas. Por exemplo, um personagem poderia usar a condição **Desabilitado** para mitigar três tensões de dano, mas não poderia usar a condição **Afetado** para isso. O jogador pode marcar quantas *Condições* ele quiser e achar apropriado, desde que elas estejam previamente desmarcadas.

Ao término do conflito, o personagem desmarcará todas as caixas de *Estresse*, mas as *Condições* demoram mais tempo para serem recuperadas. No momento em que um personagem gastar um *Marco Menor*, ele poderá iniciar o processo de recuperação, devendo escolher um entre: desmarcar a Condição *Afetado*; transformar a Condição *Incapacitado* em *Afetado* caso a Condição *Afetado* não esteja marcada; ou transformar a Condição *Desabilitado* em *Incapacitado* caso a Condição *Incapacitado*
não esteja marcada.

## Rolando os dados

Para determinar o quão bem um personagem desempenha uma ação, role 4dF e some todos os modificadores, positivos ou negativos, advindos dos *Aspectos* do personagem, e compare com o número alvo. Por exemplo, um personagem cujos *Aspectos* sejam *Chefe da Máfia italiana*, *Amolecido pela idade* e *Muitas pessoas me devem favores*, sendo o primeiro seu *Aspecto Conceito*, que tente persuadir um guarda a aceitar suborno rolaria 4dF+2 em sua *Ação de Superar*, pois os *Aspectos* *Chefe da Máfia italiana* quanto *Muitas pessoas me devem favores* contribuem positivamente para a ação. Se porventura o guarda estiver sendo arisco, irredutível ou agressivo, o personagem rolaria apenas 4dF+1, pois o *Aspecto Amolecido pela idade* contribui negativamente para a ação pretendida.

A critério do Narrador, um personagem que ofereça uma descrição criativa, elaborada e condizente com a lógica narrativa da cena pode fazer jus a um bônus de +1 em sua rolagem. Não basta apenas dizer o que se está fazendo, é preciso entremear a ideia na ação. Por exemplo, apenas possuir uma arma de fogo não concederia o bônus, mas ocultar-se nas sombras, esperar o alvo passar pelo personagem e declarar que atirará pelas costas concederia. Narradores, usem este recurso para premiar descrições vívidas e uso criativo dos recursos em cena.

## Evolução de personagem

Em **MicroFATE** os personagens recebem *Marcos* para representar seu aprendizado e crescimento.

### Marco Menor

O personagem recebe um *Marco Menor* ao fim de uma sessão de jogo, para representar os pequenos aprendizados e mudanças que aconteceram durante esse curto intervalo da história sendo narrada.

Ao gastar um *Marco Menor* para aprimorar sua planilha, um personagem tem direito a todos os benefícios a seguir:

- Reescrever seus *Aspecto Dificuldade* ou *Livre*;
- Substituir uma de suas *Façanhas* por outra;
- Reduzir sua *Recarga* em 1 e adicionar uma nova *Façanha*, desde que isso não reduza sua *Recarga* para um valor abaixo de 1;
- Iniciar o processo de recuperação, conforme descrito em ***Estresses e Condições***.

### Marco Maior

O personagem recebe um *Marco Maior* ao final de um arco da história sendo narrada, para representar os grandes aprendizados e mudanças que aconteceram com ele e no mundo ao seu redor após esse intervalo maior da história.

Ao gastar um *Marco Maior* para aprimorar sua planilha, um personagem recebe todos os benefícios de um *Marco Menor*, e deve escolher um benefício dentre os abaixo listados:

- Reescrever seu *Aspecto Conceito*;
- Adicionar uma nova *Façanha* sem precisar reduzir sua *Recarga*;
- Aumentar sua *Recarga* em 1.

## Escala de Resultados

| **Valor** | **Adjetivo** | **Valor** | **Adjetivo** |
| :---: | :---: | :---: | :---:|
| **+8** | Lendário | **+2** | Razoável |
| **+7** | Épico | **+1** | Regular |
| **+6** | Fantástico | **+0** | Medíocre |
| **+5** | Excepcional | **-1** | Ruim |
| **+4** | Ótimo | **-2** | Terrível |
| **+3** | Bom | | |                

## Créditos e licenciamentos

Fate™ is a trademark of Evil Hat Productions, LLC. The Powered by Fate logo is © Evil Hat Productions, LLC and is used with permission.

The Fate Core font is © Evil Hat Productions, LLC and is used with permission. The Four Actions icons were designed by Jeremy Keller.

Texto e diagramação por Lu *Cicerone* Cavalheiro.

**MicroFATE** é licenciado por Lu *Cicerone* Cavalheiro sob **Licença Creative Commons CC-BY 4.0 Internacional** -- https://creativecommons.org/licenses/by/4.0/deed.pt\_BR.

**MicroFATE** foi publicado em parceria e com o fomento do **RPGWorld** -- https://rpgworldsite.wordpress.com/.

Lu *Cicerone* Cavalheiro é membro do podcast **Fate Masters** -- https://fatemasters.gitlab.io -- e autor do site **Biblioteca do Cicerone** -- https://cicerone.gitlab.io.

Baixe o pdf diagramado [aqui](/assets/pdfs/MicroFATE-tres-colunas.pdf)

![image-movidofate](/assets/parcerias/imagens-000.png)

![image-rpgworld](/assets/parcerias/logo-rpgworld.jpeg) 

---

