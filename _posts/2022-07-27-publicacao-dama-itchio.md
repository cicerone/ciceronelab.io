---
layout: post
title: "Publicado: o conto 'A Dama de Mil Faces' (versão publicada pela Andross Editora) no itch.io e na Loja Kindle, da Amazon!"
lang: pt-BR
date: 2022-07-27 12:10:00 -0300
tags: Geral Divulgação itch.io Lojinhas Literatura Contos Fantasia Amazon Loja-Kindle
---

**ATUALIZAÇÃO**: o conto já está disponível na loja Kindle, da Amazon. Acesse <https://www.amazon.com.br/dp/B0B7TFQ34F> para adquiri-lo pela módica quantia de R$ 5,00.

Anúncio importante: o conto _A Dama das Mil Faces_ (a versão publicada pela Andross Editora) já está disponível para aquisição em <https://lucavalheiro.itch.io/a-dama-de-mil-faces-verso-fogo-de-prometeu> por um precinho bem módico: um dólar (não consegui configurar o meu itch.io para listar preços em reais, foi mal, galera). 

Para vocês ficarem com gostinho na boca, eis a capa, arte de [Sandy de Oliveira](https://www.instagram.com/s_artteee/)

![capa-conto](/assets/literatura/contos/dama-mil-faces/dama-mil-faces.jpg)

Em breve, o conto também poderá ser adquirido pela Amazon pela quantia de R$ 5,00, e está disponível também pelo programa Kindle Unlimited!

**Sinopse do conto**

Uma ladra misteriosa tem aterrorizado a Praça do Mercado, em Acheon, e os _Ouvidos do Sultão_, os melhores agentes de Sua Majestade, não conseguem descobrir quem ela é. Será que Rashid e Fátima, dois dos melhores Ouvidos, serão capazes de descobrir quem é **A Dama de Mil Faces?**

Conto não recomendado para menores de 14 anos, por retratar consumo explícito de álcool e tabaco e violência implícita.

---

