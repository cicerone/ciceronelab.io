---
layout: post
title: "Jardineiros e Goblins: um RPG solo sobre proteger jardins de goblins"
lang: pt-BR
date: 2022-06-11 22:45:00 -0300
tags: RPG Folheto Minimalista Sistema Autoral RPG-Solo Jardineiros-e-Goblins
---


Em **Jardineiros e Goblins**, você é um jardineiro que precisa cuidar e proteger suas plantas contra goblins que querem destruir seu jardim. E quem são esses goblins? Podem ser espíritos malévolos, entidades brincalhonas, ou mesmo as crianças perturbadas da vizinhança.

## Criando seu jardim

Você começa o jogo com uma ***Zona*** e 1 ***Ponto de Ação*** (PA) máximo. Uma ***Zona*** representa uma região do seu jardim na qual você plantou o mesmo tipo de plantas, por isso dê a ela uma ***Descrição*** clara e sucinta: *Minhas roseiras premiadas*, *Hortênsias enxertadas*, *Girassóis ucranianos*. Seja criativo, já que o jardim é seu e ninguém melhor do que você para saber o que você plantou nele. Em nenhuma hipótese as plantas atacarão os goblins, portanto nada de *Plantas carnívoras devoradoras de goblins*!

A seguir, vamos discorrer para que servem os ***Pontos de Ação***:

- Você pode **Criar ou rearmar uma armadilha contra goblins** em uma única zona, efetivamente protegendo-a de destruição;
- Você pode **Recuperar uma zona destruída** pelos goblins;
- Você pode **Adicionar uma zona nova em seu jardim**.

A cada dia, você pode gastar seus PA conforme quiser. No primeiro dia, todas as zonas do seu jardim estão maravilhosas e perfeitas, logo você não precisará se preocupar em recuperar uma zona destruída. Você não precisa rolar dados para usar seus PAs. PAs não usados em um dia não se acumulam para o dia seguinte.

Porém, é à noite, enquanto você dorme, que os goblins agem. Role 1d6 para cada zona em seu jardim: se cair 1, 2 ou 3, os goblins não fizeram nada contra ela. Mas se cair 4 ou 5, os goblins atacaram aquela zona do seu jardim. Se havia uma armadilha contra goblins naquela zona, os goblins não conseguiram causar destruição nenhuma, mas a armadilha foi usada e não está mais defendendo aquela zona. Entretanto, se não havia uma armadilha contra goblins naquela zona, ou se o dado deu 6, os goblins conseguiram causar uma boa destruição. Mude a descrição daquela zona para refletir o estado destruído em que os goblins a deixaram.

Ao amanhecer, é sua vez de agir. Como já foi dito anteriormente, você pode usar seus PAs para criar ou rearmar uma armadilha contra goblins; recuperar uma zona que eles destruíram, alterando a descrição da zona no processo; ou você pode adicionar uma nova zona no seu jardim. Após suas ações, anoitece, você vai dormir e é a vez dos goblins agirem.

## Evoluindo seu jardim

A cada sete dias, faça as seguintes avaliações:

- **Se uma zona passou esses sete dias sem ser destruída por goblins (seja porque eles tiveram azar nos dados ou você a protegeu com armadilhas)**, aumente seus PAs máximos em 2;
- **Se uma zona foi destruída por goblins durante esses sete dias mas você a recuperou**, aumente seus PAs máximos em 1. Não importa quantas vezes a zona foi recuperada, ela só renderá 1 PA por semana. Entretanto, diferentes zonas podem render PAs na mesma semana;
- **Se uma zona foi destruída por goblins durante esses dias mas você não a recuperou**, reduza seus PAs máximos em 2. Porém, não importa o que aconteça, você nunca pode ficar com menos do que 1 PA máximo.

## Fim do jogo

**Jardineiros e Goblins** termina quando os goblins conseguem destruir todas as zonas do seu jardim, ou quando você consegue acumular 10 PA máximos.

Boa jardinagem!

---

