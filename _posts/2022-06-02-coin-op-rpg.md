---
layout: post
title: "Coin-Op RPG: um sistema minimalista genérico"
lang: pt-BR
date: 2022-06-02 18:00:00 -0300
tags: RPG Folheto Minimalista Sistema Autoral Coin-Op-RPG
---

Meus caros leitores, hoje quero aproveitar e divulgar para vocês um RPG minimalista de minha autoria, o **Coin-Op RPG**. Escrito originalmente para caber em um panfleto, ele é bem mínimo, voltado para jogos rápidos e divertidos em qualquer cenário. Outra coisa interessante: ao invés de dados, ele usa duas moedas para fazer os testes!

Enfim, divirtam-se com a leitura. Em breve, o jogo será publicado, e aí eu passo para vocês o link para obtenção da versão diagramada.

## Coin-Op RPG

Um sistema minimalista genérico de RPG por Lu *Cicerone* Cavalheiro

*Coin-Op RPG* é um RPG minimalista genérico com a proposta de ser
simples, rápido e permitir a narração de qualquer aventura.

Junte um grupo de amigos. Um de vocês será o *Narrador*, o responsável
por contar a história e guiá-los pelo o que acontece. Os demais serão os
*Jogadores*, responsáveis por interpretar os personagens protagonistas
da história.

## Jogador: crie seu personagem

Crie uma *Descrição* para o seu personagem com uma ou duas
palavras, se possível um substantivo com um adjetivo. Exemplos:
***Soldado Veterano***, ***Explorador de tumbas***.

Crie uma *Vantagem*, algo em que seu personagem se
destaca, seja por meio de treinamento, equipamento ou outra razão, com
uma ou duas palavras. Exemplos: ***Armamento pesado***, ***Artista
Marcial***, ***Cientista maluco***.

Crie uma *Dificuldade*, um problema, limitação ou defeito
que ele tem, com uma ou duas palavras. Exemplos: *Ingênuo demais*,
*Ajuda qualquer pessoa*, *Dívidas impagáveis*.

Crie um *Nome* para seu personagem.

Todos os personagens começam com 4 *Pontos de Vida* (PVs).

### Equipamento inicial

 Uma muda de roupas; qualquer equipamento
simples e não especial que tenha a ver com a *Descrição* do personagem;
escolha entre uma arma de combate corpo-a-corpo ou uma arma de combate à
distância.

## Testes e Conflitos

Sempre que o jogador quiser fazer algo sobre o qual haja dúvidas se ele
será capaz de fazer ou não, descreve como ele pretende realizar sua ação
e faz um teste. Para isso, jogue duas moedas. Não importa o que
aconteça, o personagem nunca joga menos do que duas moedas.

Se a *Descrição* ou o *Arquétipo* do
personagem for relevante no teste, jogue mais uma moeda para cada e
fique com o melhor resultado possível. Se a *Dificuldade*
for relevante, jogue mais uma moeda e fique com o pior resultado
possível. Se tanto a *Dificuldade* quanto a
*Descrição* ou o *Arquétipo* forem
relevantes, jogue o número apropriado de moedas e fique com o pior
resultado possível.

Por fim, se o Narrador julgar que a descrição da ação é particularmente
boa ou ruim, ele poderá aumentar ou diminuir o número de moedas que
jogador usará em seu teste.

1.  **Duas coroas**: *Sucesso total*\
    Em um sucesso total, o personagem consegue realizar a ação
    pretendida. Se ele estiver em um conflito, ele reduz em 1 os PVs de
    seu alvo. Além disso, ele pode escolher um entre:

    -   Ter mais uma moeda no próximo teste;

    -   Reduz em 1 os PVs do alvo, mesmo que ele não esteja em conflito;

    -   Tirar uma moeda do próximo teste do alvo;

    -   Fazer uma pergunta sobre a cena, que o Narrador terá de
        responder honestamente.

2.  **Uma coroa e Uma cara**: *Sucesso parcial*\
    Em um sucesso parcial, o personagem consegue realizar a ação
    pretendida, mas acontece algum imprevisto. Se ele estiver em
    conflito, ele reduz em 1 os PVs do alvo, mas os PVs do personagem
    também serão reduzidos em 1.

3.  **Duas caras**: *Falha*\
    O personagem não consegue realizar a ação pretendida e acontece
    algum imprevisto. Se ele estiver em conflito, seus PVs são reduzidos
    em 1 pelo oponente. Além disso, ele deve escolher um entre:

    -   Ter uma moeda a menos no próximo teste;

    -   Reduzir em 1 seus próprios PVs;.

Se os PVs do personagem forem reduzidos a 0, ele deve fazer um teste. Se
ele falhar, ele morreu. Se ele tiver um sucesso parcial, ele está
estável porém com inconsciente e com 0 PVs. Se ele tiver um sucesso
total, ele está estável e consciente, mas com 0PVs. Caso o personagem
esteja com 0PVs e sofra dano, ele deverá fazer novamente este teste, mas
com uma moeda a menos. A redução de moedas e cumulativa. Se o personagem
tiver zero ou menos moedas para rolar, ele falhará automaticamente.

## Narrador: conduzindo o jogo

Cabe ao Narrador contar a história e arbitrar como o mundo reage aos
personagens e suas ações. Jogue para descobrir o que vai acontecer. Ao
introduzir ameaças, mostre evidências de sua presença. Antes de alguma
ameaça agir contra um personagem, dê indícios do que está para acontecer
e então pergunte qual será a ação do personagem.

Peça rolagens quando os resultados forem incertos. Não planeje
resultados com antecedência: deixe as coisas acontecerem e siga de
acordo. A situação sempre munda quando um teste é pedido, seja para o
bem, seja para o mal.

Lembre-se: o Narrador não faz testes em *Coin-Op RPG*.
Descreva a situação para o personagem, pergunte o que ele vai fazer e
então peça um teste de acordo. Se o personagem for ser atacado, por
exemplo, pergunte o que ele vai fazer para se defender e então peça para
o jogador fazer um teste.

Faça perguntas para os demais jogadores e crie em cima das respostas.
Use essas perguntas para sentir o que os personagens querem ver na
história, bem como para aumentar a imersão dos personagens. Se você não
souber como descrever o resultado de um teste, pergunte a outro jogador
o que ele acha que acontecerá e siga com a resposta dele.

## Licença

*Coin-Op RPG* é licenciado por Lu *Cicerone* Cavalheiro
sob **Licença Creative Commons CC-BY 4.0 Internacional** --
https://creativecommons.org/licenses/by/4.0/deed.pt\_BR.

---
