---
layout: post
title: "Guia de Referência para Fate: um PDF que todos deveriam ter"
lang: pt-BR
date: 2022-06-09 19:30:00 -0300
tags: RPG Fate Geral Guias Fate-Masters
---

Queridos leitores, é com imenso prazer que divulgo um trabalho recente de Fábio Costa, mais conhecido como o _Mr. Mickey_ do [Fate Masters](https://fatemasters.gitlab.io). Ele se deu ao trabalho de diagramar um _Guia de Referência para Fate_, com lembretes e explicações rápidas para todas as regras do sistema. A pedido dele, eu rediagramei o texto de modo a caber em uma única página, em três colunas.

Se você tiver interesse em baixar o pdf (e por que caralhos você não teria?), [clique aqui](/assets/pdfs/guia-tres-colunas.pdf) e seja feliz.

---

