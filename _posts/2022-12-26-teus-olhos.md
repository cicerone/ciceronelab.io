---
layout: post
title: 'Teus olhos – exercício de poesia romântica'
date: 2022-12-26 18:57:00 -0300
lang: pt-BR
tags: Poesia Poesia-Brasileira Poesia-Autoral Poesia-Romântica Ultrarromantismo Exercício-Poético Olhos
---

![teus-olhos](/assets/Publicações/teusolhos.jpg)

Nunca antes eu vi tais olhos em minha vida. Teus segredos, tu os escondes bem, mas não o bastante para quem sabe como olhar. As janelas da alma sempre estão abertas para aqueles que sabem ver – e, ah, minha cara, como meus olhos perfuram quaisquer opacidades!

E o que encontrei aí, nos recessos ocultos de teu ser? Um mar, um oceano, um universo no qual a navegante errante deseja explorar, deseja conhecer, talvez até mesmo aportar. Cansa-me passar pelas tendas à noite e vê-las cheias e felizes, enquanto eu apenas passo, solitária, sem ter uma para me abrigar da lua e das estrelas. Talvez, quem sabe, exista repouso para esta rapsoda cansada em algum recôncavo, algum nicho perdido até de ti mesma, mas que pode ser encontrado se tu te permitires.

Eis a questão: te permitirás?

Crédito pela imagem: Sophia Moran, "Brown eyes in the sunlight after a snowfall and the snow was reflecting light", 2019, Licença CC-BY 4.0 Internacional, <https://commons.wikimedia.org/wiki/File:Brown_eyes_in_the_sunlight.jpg>

Se você quiser contribuir com meu cafezinho, aceito um Pix.


<details>
  <summary markdown="span">Clique aqui para ver minha chave Pix</summary>
  
  lu.cicerone.cavalheiro@gmail.com
</details>

---

Teus Olhos
Lu Cavalheiro (@lu.cicerone.cavalheiro)
Licença CC-BY-SA 4.0 Internacional

Como mergulho em teus olhos densos de castanho vívido
Sem querer sair deles para nada mais em minha existência
Tão profundos, tão densos, reais para além da querência
Desse meu velho espírito por tantas lutas já combalido

Por que escondes tuas potências sob o véu da tristeza?
Por que finges não ser a deidade que nasceste para ser?
Por que temes reclamar para si o que fizeste por merecer?
Por que cerra as portas para mim, amante de tal beleza?

Afundo-me em tua alma, nado na tua essência, me refestelo
Tal navegante a singrar perdido pelos mares da realidade
Tenho as esperanças renovadas ao avistar majestoso castelo

Sorri uma vez só para mim em aquiescência à minha vontade
Abre-te junto a mim para o que há neste mundo de mais belo
E experimenta por um dia que seja usufruir de tua liberdade

---

