---
layout: post
title: "Carrun Falt (ele/dele): um personagem para Fate of Umdaar"
lang: pt-BR
date: 2022-05-26 22:00:00 -0300
tags: RPG Fate Evil-Hat Masters-of-Umdaar Fate-of-Umdaar-playtest Fate-Condensado Personagens Exemplo
---

E está na hora de meter a mão na massa! Ainda na esteira do playtest do _Fate of Umdaar_, decidi criar alguns personagens, a fim de que vocês possam saborear como o jogo está. Lembrando que as regras usadas são as disponíveis no material distribuído pela Evil Hat para os participantes do playtest, e que este personagem pode se tornar incompatível com a versão final.

## Carrun Falt (ele/dele)

Carrun nasceu em uma família de _Guardarrunas_[^runekeeper], mas cedo na vida perdeu a paciência com toda a reverência religiosa que seus pais prestavam aos conhecimentos legados pelos Demiurgos. Assim, ele saiu de casa disposto a explorar e conhecer mais sobre o passado de Umdaar do que seria possível apenas o idolatrando, se tornando um proeminente _Arte-hacker_[^artihacker] no processo. Sua curiosidade não tem os freios da veneração de um _Guardarruna_, e ele busca resgatar e recuperar o máximo possível do passado para que esse conhecimento possa ser útil no presente.

[^runekeeper]: Tradução livre para o termo _runekeeper_.

[^artihacker]: Tradução livre para o termo _artihacker_.

Ele é um humano alto porém magro, e usa óculos cujas lentes facilitam seu trabalho de obter acesso não autorizado aos bancos de dados que ele encontra. Seus cabelos negros são curtos e espetados, e suas roupas são bem desleixadas. Sua pele parece não ter o hábito de tomar sol, apesar de não ser exatamente branca – _desbotada_ seria um termo melhor. Embora não goste de confiar nesse tipo de coisa, sempre tem uma pistola laser ao alcance da mão – nunca se sabe...


**Nome**: Carrun Falt (ele/dele)  
**Recarga**: 3

| **Aspectos** | |
| ---: | :--- |
| ***Conceito***| Humano arte-hacker desleixado e pragmático |
| ***Motivação*** | Recuperar o conhecimento dos Demiurgos |
| ***Origens*** | Irreverente e impaciente com os mistérios dos _guardarrunas_ |
| ***Relacionamento*** | |
| ***Aspecto Livre*** | |

| **Perícias** | | | | |
| ---: | :--- | :--- | :--- | :--- |
| ***Ótimo (+4)*** | Roubo | | | |
| ***Bom (+3)*** | Ofícios | Vontade | | |
| ***Razoável (+2)*** | Condução | Atirar | Enganar | |
| ***Regular (+1)*** | Furtividade | Percepção | Contatos | Comunicação |

| **Façanhas** | |
| ---: | :--- |
| ***Runejambro*** | Se puder usar seu tecnopad, Carrun pode realizar uma _Ação de Criar Vantagem_ por _Roubo_ para interromper qualquer artefato ou dispositivo dos Demiurgos ou Replitech ao alcance da visão. Uma vez por cena, Carrun recebe +2 na primeira tentativa de usar esta façanha. |
| ***Ajudantes Quickbots*** |  Carrun pode realizar uma _Ação de Criar Vantagem_ por _Ofícios_ para construir ajudantes quickbots, dificuldade de acordo com a quantidade de sucata disponível. Esses ajudantes não conseguem fazer nada por eles mesmos, mas até o fim da cena, sempre que um campeão realizar uma ação de _Trabalho em Equpe_, ele concede um bônus de +2 ao invés de +1. Apenas um ajudante pode ajudar em uma dada rolagem. Carrun pode criar apenas um grupo de ajudantes por cena. |
| ***Vou levar todo mundo comigo!*** | Após sofrer dano considerável durante um conflito físico, Carrun pode improvisar uma _solução explosiva_ usando materiais e substâncias disponíveis. Uma vez por cena, e apenas após sofrer uma consequência em um conflito físico, Carrun pode realizar um ataque explosivo contra todos os personagens na mesma zona que ele, aliados ou inimigos, exceto ele próprio. Realize uma _Ação de Atacar_ por _Ofícios_, contra a qual todos os personagens na zona deverão realizar suas defesas. |

| **Estresses e Consequências** | |
| ---: | :--- |
| ***Estresse Físico*** | [ 1 ] [ 1 ] [ 1 ] |
| ***Estresse Mental*** | [ 1 ] [ 1 ] [ 1 ] [ 1 ] [ 1 ] [ 1 ] |
| ***Consequência Suave (2)*** | |
| ***Consequência Moderada (4)*** | |
| ***Consequência Severa (6)*** | |

---

