---
layout: post
title: 'Onde tudo começou: Uma pequena coletânea dos meus contos publicados em antologias físicas'
date: 2022-12-28 19:53:00 -0300
lang: pt-BR
tags: Literatura Literatura-Independente Publicação-Independente Coletânea Andross-Editora Contos
---

E é com prazer que venho anunciar mais uma publicação minha!

![onde-tudo-comecou](/assets/Publicações/OTC.jpg)

**Onde tudo começou: Uma pequena coletânea dos meus contos publicados em antologias físicas** reúne os cinco contos que publiquei pela Andross Editora em um único livro: _A Dama de Mil Faces (versão Fogo de Prometeu)_, _A Máquina Windrush_, _As crianças do Pandiá Calógeras_, _O Monolito Fálico_ e _O último abraço_. Decidi prestar essa homenagem a mim mesma, já que esses foram os primeiros contos que escrevi e publiquei de fato.

Livro não recomendado para menores de 18 anos, em virtude dos temas abordados ao longo dos contos (cortesia de _O último abraço_).

Caso você goste de meu trabalho e queira contribuir com o meu cafezinho, eu aceito Pix.

<details>
  <summary markdown="span">Clique aqui para ver minha chave Pix</summary>
  
  lu.cicerone.cavalheiro@gmail.com
</details>

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/Literatura/Prosa/Andross/Onde%20tudo%20come%C3%A7ou/Onde-tudo-come%C3%A7ou.pdf?inline=false)

---

