---
layout: post
title: "Agora você pode adquirir meus jogos já diagramados em minhas lojinhas virtuais"
lang: pt-BR
date: 2022-07-11 23:20:20 -0300
tags: Geral Divulgação itch.io Lojinhas RPG Fate Folheto Minimalista Sistema Autoral Fast-Heroes-d6 MicroFATE Viva-la-Revolución
---

Queridíssimos leitores, quero anunciar agora que vocês já podem adquirir versões diagramadas de meus jogos em <https://lucavalheiro.itch.io>! Como o processo de diagramar e revisar por mim mesma é lento que dói, por enquanto só temos por lá:

+ Uma versão aprimorada do [Fast Heroes D6](https://cicerone.gitlab.io/2022/06/02/fast-heroes-d6.html), tanto em português quanto em inglês;
+ Uma versão aprimorada do [MicroFATE](https://cicerone.gitlab.io/2022/06/11/microfate.html), tanto em português quanto em inglês;
+ A versão final de [Viva la Revolución!](https://cicerone.gitlab.io/2022/06/12/viva-la-revolucion.html), só em português mesmo.

Por enquanto o que tem por lá é só isso, mas acompanhe minhas lojinhas virtuais no link <https://cicerone.gitlab.io/lojinhas/>, e mantenha-se sempre atualizado sobre minhas publicações!
