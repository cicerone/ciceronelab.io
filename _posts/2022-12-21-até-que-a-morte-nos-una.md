---
layout: post
title: 'Até que a morte nos una: um jogo solo baseado na tradição dos casamentos fantasma chineses'
date: 2022-12-21 00:00:00 -0300
lang: pt-BR
tags: Geral RPG Jogo-autoral RPG-solo RPG-histórico
---

![até-que-a-morte-nos-una](/assets/Publicações/AMNU-1.jpg)

**Até que a morte nos una** é um jogo solo no qual seu personagem nasceu em uma cultura que possui a tradição de casas seus membros solteiros com pessoas mortas a fim de que eles não vivam sozinhos no pós-vida. No seu caso, você morreu solteiro, e sua família – obviamente sem seu consentimento, já que você estava **morto** – realizou seu casamento com uma outra pessoa, também falecida. Ao chegar no pós-vida, você encontra seu cônjuge, também desnorteado com o fato de ter sido casado à revelia. Como vocês dois lidarão com isso?

Caso você goste do jogo, sinta-se livre para contribuir com o meu cafezinho via Pix.

<details>
  <summary markdown="span">Clique aqui para ver minha chave Pix</summary>
  
  lu.cicerone.cavalheiro@gmail.com
</details>

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/Jogos%20Solo/At%C3%A9%20que%20a%20morte%20nos%20una/At%C3%A9-que-a-morte-nos-una.pdf?inline=false)

---

