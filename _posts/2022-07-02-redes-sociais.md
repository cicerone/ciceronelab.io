---
layout: post
title: "Agora o Cicerone tem redes sociais! Siga-me no Facebook, Instagram e Twitter!"
lang: pt-BR
date: 2022-07-02 16:30:00 -0300
tags: Geral RPG Divulgação Redes-sociais
---

Queridos leitores! Finalmente o Cicerone decidiu sair de sua caverna digital e integrar-se ao mundo das redes sociais. Eu ainda estou estudando como isso funciona, então não estranhem se vocês não virem postagem nenhuma ou postagens estranhas nelas.

De qualquer modo, sigam-me os bons:

+ Facebook: <https://www.facebook.com/lu.cicerone.cavalheiro>
+ Instagram: <https://www.instagram.com/lu.cicerone.cavalheiro>
+ Twitter: <https://twitter.com/luRPGcavalheiro>
+ Telegram: <https://t.me/lcavalheiro>
+ E-mail: <lu.cicerone.cavalheiro@gmail.com>

Confesso que não sei usar porra nenhuma dessas coisas, então não estranhem se eu demorar a responder ou a aceitar o seu pedido de amizade (ou seja lá como isso se chame na rede em questão).

---

