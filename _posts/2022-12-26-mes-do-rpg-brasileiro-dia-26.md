---
layout: post
title: 'Mês do RPG Brasileiro 2022 – Dia 26: realismo'
date: 2022-12-26 01:35:00 -0300
lang: pt-BR
tags: RPG RPG-Brasileiro RPG-Autoral RPG-Solo Jogo-Solo Mês-do-RPG-Brasileiro-2022
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/KXkyPxH5dIo" title="YouTube video player" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Hoje é 26/12/2022, dia da vigésima-sexta atividade do Mês do RPG Brasileiro 2022! A comunidade realiza essa comemoração em homenagem a Tagmar, o primeiro RPG brasileiro, publicado em 21/12/1991.

A temática de hoje é realismo. Jogos realistas precisam lidar com a dificuldade de oferecer uma experiência de entretenimento e de catarse porque – sejamos francos – a realidade é uma bosta. Ainda assim, existem jogos realistas, e por isso decidi falar sobre um de minha autoria, [**Ensaio sobre a Dignidade**](https://cicerone.gitlab.io/publicacoes/#ensaio-sobre-a-dignidade), um jogo solo no qual uma pessoa comum avalia eventos do seu dia para descobrir se sua vida tem dignidade ou não. Várias pessoas que o jogaram relataram ter tido experiências catárticas e até mesmo reveladoras com esse jogo.

Este vídeo pode ser assistido no YouTube em <https://youtu.be/KXkyPxH5dIo>, e a playlist dos meus vídeos sobre o Mês do RPG Brasileiro 2022 está em <https://www.youtube.com/playlist?list=PL44VOrX_-JdPfNHOfEIm0AS7ikOlpp9xN>. Espero que você goste do vídeo, e até o próximo!

Música de fundo:    
"Man Down" por Kevin MacLeod (<https://incompetech.com>)    
Licensed under Creative Commons: By Attribution 4.0 License     
<http://creativecommons.org/licenses/by/4.0/>     

---

