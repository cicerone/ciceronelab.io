---
layout: post
title: "A Viúva de Sangue: uma aventura para MicroFate"
lang: pt-BR
date: 2022-06-14 21:15:00 -0300
tags: RPG Folheto Minimalista Sistema Simplificação MicroFATE Fate Aventura-pronta RPGWorld
---

Meus caros leitores, eu estou realmente pegando fogo! Acabei de escrever uma aventura para o [MicroFATE](https://cicerone.gitlab.io/2022/06/11/microfate.html), _A Viúva de Sangue_. A versão diagramada dela será publicada em algum momento graças à minha parceria com o [RPGWorld](https://rpgworldsite.wordpress.com/), mas quem quiser se aventurar e seguir sem a versão diagramada mesmo, ei-la. Boa diversão!

## Introdução

Lucretia Sforzato foi uma das piratas mais temidas dos Mares Ocidentais. Pouco se sabe de sua vida antes do motim que ela, na época imediata do *NSM Intrépido*, e que ela empalou e pendurou na amurada do navio todos os que não foram leais a ela. Como capitã do navio, rebatizado *Viúva de Sangue*, ela e sua tripulação foram forças encarnadas do terror. Eles não deixavam nenhum sobrevivente, mas também não afundavam os navios saqueados. Ao invés disso, eles empalavam os sobreviventes na amurada do navio saqueado e o deixava à deriva, como um recado e uma assinatura dos seus atos. Boatos corriam por todas as tavernas e portos do Império que Lucretia havia feito um pacto com uma força das trevas, e as pessoas que ela deixava para trás eram na verdade sacrifícios para seu patrono.

Seu reinado de terror durou cerca de treze anos, durante os quais as pessoas simplesmente tinham verdadeiro pânico em navegar e o comércio só não desapareceu porque o Império começou a fazer escolta dos navios mercantes. Então, tão subitamente quanto aparecera, Lucretia Sforzato simplesmente sumiu. Por anos especulou-se o porquê de ninguém nunca mais ter ouvido falar em seu nome ou em seu navio, e então os boatos sobre o possível acordo com a entidade maligna aumentaram. Dizia-se que Lucretia serviu a seu mestre por treze anos e então foi simplesmente abduzida por ele para atender a algum propósito nefasto que apenas uma entidade alienígena a este mundo entenderia.

Foi quando os rumores sobre o tesouro acumulado pela pirata sanguinária começaram a surgir. Vagos a princípio, eles foram repetidas tantas vezes que depois de um tempo não houvesse uma pessoa que lidasse com o mar que desconhecesse como a pirata escondeu seu navio em uma ilha desconhecida, sacrificou sua tripulação em honra ao seu mestre sombrio e depois foi levada por ele para um mundo desconhecido. Tripulações de aventureiros cruzaram todos os cantos dos Mares Ocidentais em busca da tal ilha, mas sem sucesso. Especulava-se várias razões pelas quais a ilha não era encontrada, ou por que Lucretia desapareceu sem deixar rastro, mas o Império se limitou a dizer que a pirata deve ter naufragado e que não havia tesouro nenhum para encontrar.

No momento em que todos já começavam a esquecer o nome de Lucretia Sforzato, apareceu em Hypherion um homem misterioso chamado Diophanes Augustus. Ele alegava ter sido imediato de Lucretia Sforzato, que ela havia feito um pacto com entidades das trevas para permanecer eternamente jovem e que sim, o tesouro era real e ele possuía um mapa para a localização da ilha. Muitos desdenharam de suas histórias, a princípio, mas quando ele comprou uma caravela, a *Sereia dos Mares*, com moedas de ouro e passou a recrutar uma tripulação para resgatar o tesouro perdido as pessoas passaram a levá-lo mais a sério. Em Hypherion passou-se a comentar que o imediato de Lucretia estava recrutando uma nova tripulação para o *Viúva de Sangue* e que a pirata sanguinária voltaria à ativa. Outros diziam que o imediato a abandonara e agora iria em busca do tesouro para ele próprio se tornar um pirata poderoso.

Para o bem ou para o mal, vocês são membros da tripulação de Diophanes Augustus. Pouco se sabe sobre o seu capitão, já que ele raramente é visto fora de sua cabine, e as decisões e afazeres cotidianos são deixados ao encargo de sua imediata, Eleanora Treblinka. Porém, muito é prometido a vocês: uma parte justa no butim, grande o bastante para que cada um dos membros sobreviventes da tripulação possa largar tudo e viver como um rei.

## Criação de personagens

_A Viúva de Sangue_ é uma aventura escrita para ser jogada usando as regras do **MicroFATE**, uma simplificação do sistema **Fate RPG** escrita por Lu *Cicerone* Cavalheiro e disponível gratuitamente na compilação *Ideias Cavalheiras -- RPGs simples para o dia a dia*, disponível tanto no **Dungeonist** quanto no **DriveThruRPG**.

Entretanto, nada impede que você adapte a aventura para seu sistema favorito e compartilhe o resultado. Caso o faça, lembre-se que esta aventura está licenciada sob **Licença Creative Commons Atribuição-CompartilhaIgual CC-BY-SA 4.0 Internacional**, o que significa que você não apenas precisa realizar a atribuição de autoria original (o texto para tal estará no final da aventura), como deve compartilhá-la usando a mesma licença. O texto para leigos da licença em português pode ser lido em <https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR>.

### *In media res*: O que vocês estavam fazendo antes da aventura começar?

Após criarem seus personagens, conversem um pouco sobre quais foram as aventuras anteriores da tripulação da *Sereia dos Mares*. Vocês saquearam navios mercantes? Vocês afundaram um navio da Marinha Imperial? Vocês gastaram toda a sua pilhagem em uma taverna com vinho, companhia e *companhia agradável*?

Se vocês estiverem usando as regras do **MicroFATE**, crie um aspecto chamado ***Reputação da tripulação*** com base em suas aventuras passadas. Se você estiver usando outro sistema para narrar esta aventura e ele possuir regras similares aos *aspectos* de Fate, faça a adaptação de acordo.

## A aventura começa

Após criarem os personagens e estabelecerem a reputação do *Sereia dos Mares*, está na hora de começar a aventura.

### Primeira cena: O mapa da imediata

Uma certa noite, após todas as tarefas necessárias terem sido feitas e todos terem jantado, a imediata, Eleanora Treblinka, reunirá toda a tripulação no convés. Ela parecerá mais alegre e expansiva do que o normal, e a razão se torna óbvia quando ela declarar que finalmente um mapa para o mítico tesouro da *Viúva de Sangue* foi encontrado. A tripulação reagirá em êxtase, dando gritos de felicidade e comemorando com intensa alegria.

Pergunte aos jogadores o que seus personagens vão fazer. É possível, caso alguém deseje, notar que há algo errado na postura de Eleanora, mas para isso será preciso realizar uma **Ação de Superar** contra a imediata. Um *Empate* revelará que há algo estranho, mas um *Sucesso* revela o aspecto ***Muito mais fria do que parece***.

Após todos declararem suas ações, anuncie que a imediata está dando ordens imediatas para o navio mudar o rumo em direção ao ponto em que o mapa indica ser o tesouro. Ela explicará, caso alguém pergunte, que se trata da mítica *Isla de los Muertos*, uma ilha que só pode ser encontrada por quem sabe onde ela fica. Se alguém perguntar como ela sabe onde a ilha fica, ela simplesmente mostrará o mapa e dará de ombros, como se a resposta fosse óbvia o bastante. Entretanto, se alguém quiser notar algo a mais nessa atitude dela, será preciso fazer uma **Ação de Superar** contra a imediata. Um *Empate* revelará que ela não está contando algo, mas um *Sucesso* deixará o personagem com a certeza (e a ciência do aspecto) de que ***Eleanora já esteve na *Isla de los Muertos***.

### Segunda cena: desembarque na *Isla de los Muertos*

Após alguns dias de viagem, durante os quais você pode inserir eventos se seus jogadores ou você mesmo acharem que vale a pena, vocês chegam a uma parte do oceano muito estranha. O oceano é negro, de tão escuro, parecendo mais piche do que água, e uma espécie de névoa densa e escura, mais parecida com fumaça, se ergue a mais ou menos um metro da superfície da água. Se alguém for burro ao ponto de provar a água ou aspirar a névoa, descobrirá que adquiriu o aspecto ***Fatalmente envenenado***, que ocupará a ***Condição: Desabilitado*** até que ele possa receber tratamento adequado. Se essa condição já estiver marcada, por alguma razão, marque a pior condição disponível.

Quando o navio chegar nessa parte do oceano, Eleanor irá para a proa do navio e pessoalmente gritará as ordens necessárias para o timoneiro acertar o caminho. Será preciso uma **Ação de Superar** contra dificuldade **+4** para notar que, apesar de estar segurando o mapa, Eleanor não o consulta em momento nenhum.

Depois de algumas ordens, o *Sereia dos Mares* chega nos arredores da *Isla de los Muertos*. A água volta a ter uma cor natural, e a névoa desaparece. Eleanor ordena que toda a tripulação desembarque, e seis marujos aleatórios (nenhum deles personagens dos jogadores) são incumbidos de carregar uma caixa comprida, pesada e completamente fechada. Da praia em que a tripulação desembarcou é possível ver uma trilha entre as árvores, e é por essa trilha que Eleanora decide seguir. Caso o teste mencionado no parágrafo anterior não tenha sido feito, ele pode ser feito agora.

### Terceira cena: o ataque dos *mortos famintos*

A ilha parece sobrenaturalmente silenciosa e vazia. Nem um pio de pássaro, nem um som de riacho. Eleanora parece tranquila, como se estivesse no sofá de casa, mas a tripulação começa a ficar cada vez mais nervosa. Narrador, ocasionamento role 4dF. Enquanto não sair um resultado **-2** ou pior, continue narrando a caminhada, enfatizando cada vez mais nos aspecto antinaturais da ilha.

No momento em que você conseguir essa rolagem, descreva como todos ouvem um som aterrador. Toda a tripulação, menos os personagens e Eleanora, saem correndo para dentro do mato deixando tudo para trás, incluindo a caixa pesada. Os personagens e Eleanora automaticamente são bem sucedidos em resistir a esse medo enlouquecedor, mas toda a calma de Eleanora some, deixando uma tensão palpável no lugar. Ela ordena que os personagens peguem a caixa e sigam em frente.

Não demorará muito para acontecer o que Eleanora temia. Do meio das árvores saem alguns cadáveres decompostos, zumbis que parecem ter ficado tempo demais embaixo d'água. Coloque um zumbi por personagem, mais um para Eleanora. Será preciso derrotá-los em conflito físico para continuar. Uma vez que os personagens derrotem os zumbis, Eleanora vai se mostrar ainda mais tensa, e ordenará rispidamente para que todos apertem o passo. Visivelmente (ou seja, não será preciso realizar nenhuma ação para isso), ***Eleanora está com medo de algo***.

### Quarta cena: a caverna do tesouro

Após dois dias de caminhada forçada, Eleanora e os jogadores finalmente chegam em uma caverna. Ela é iluminada por umas fendas no teto, mas ainda assim ela ordena que tochas sejam acesas. Uma vez que a caverna esteja bem iluminada, será possível ver que ela está recheada com mais tesouros do que qualquer ser humano conseguiria imaginar: ouro, prata, joias, tudo. Também será possível notar um sarcófago aberto no meio da caverna, rodeado pelo tesouro.

Eleanora então ordenará aos personagens colocarem o conteúdo da caixa dentro do sarcófago. A não ser que eles tenham um bom motivo para tal, será praticamente impossível não ficar enojado com o que eles verão: um cadáver semidecomposto de uma mulher idêntica à Eleanora. Peça a todos os personagens uma **Ação de Defender** contra dificuldade **+2**, do contrário atribua a eles a ***Condição Afetado***, ou a condição menos grave disponível caso **Afetado** já esteja marcada.

Então Eleanora revelará seu plano: ela é a própria Lucretia Sforzato, mas sucumbiu a uma doença grave e precisou trocar de corpo para sobreviver. Agora que ela descobriu os rituais apropriados, que incluem a vida de sua própria tripulação, ela poderá reassumir seu antigo corpo e reiniciar seu reino de terror nos mares novamente. Provavelmente os personagens não pretendem sacrificar a própria vida dessa forma, então eles enfrentarão Eleanora Treblinka.

Se eles vencerem, o corpo de Eleanora simplesmente se despedaçará, e o corpo de Lucretia Sforzato apodrecerá imediatamente no caixão. Eles poderão pegar o tesouro da caverna todo, se assim desejarem. Mas, se eles forem derrotados, Eleanora os sacrificará e retornará como Lucretia Sforzato, e um novo período de terror nos mares terá início.

Caso os personagens sobrevivam à aventura, dê a eles um ***Marco Maior***.

## Pontas soltas

Quem era Diophanes Augustus? Isso é deixado para o Narrador decidir. Talvez ele não exista, e tenha sido apenas um nome ou uma marionete que Eleanora usou para recrutar uma tripulação sem chamar atenção para si. Quem sabe, ele pode ser até mesmo um aliado aos personagens contra Eleanora, se eles conseguirem convencê-lo disso.

De onde vieram os zumbis na *Isla de los Muertos*? A não ser que os personagens decidam investigar isso, deixe isso quieto.

## Personagens do narrador importantes

### Eleanora Treblinka

***Aspectos***: *Muito mais fria do que parece*, *Já esteve na Isla de
los Muertos*, *Feiticeira dedicada a reassumir seu corpo anterior*,
*Pessoas são ferramentas*.

### Zumbis da *Isla de los Muertos*

***Aspectos***: *Comedores bestiais e irracionais de carne humana*

---

