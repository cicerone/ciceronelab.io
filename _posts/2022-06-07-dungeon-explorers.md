---
layout: post
title: "Dungeon Explorers: um RPG minimalista de exploração de masmorras"
lang: pt-BR
date: 2022-06-07 14:15:00 -0300
tags: RPG Folheto Minimalista Sistema Autoral Dungeon-Explorers
---

Vocês são um grupo de aventureiros se embrenhando em uma masmorra
ancestral cheia de perigos e monstros em busca do *Lendário
Tesouro*! Um de vocês será o *Mestre da
Masmorra*, ou MM, que dirá aos aventureiros o que eles
encontram e quais perigos eles devem enfrentar.

## Jogadores: criando seus *Aventureiros*

Você deve escolher a *classe* do seu
*Aventureiro*: **Guerreiro**, **Mago**, **Ladrão**,
**Curandeiro**.

Você tem 6 pontos para distribuir entre seus atributos:
**Físico**, que envolve toda e qualquer ação física;
**Sorrateiro**, que envolve toda e qualquer ação furtiva e
social; e **Magia**, que envolve usar magia e itens
mágicos. Você precisa por pelo menos 1 ponto em cada uma delas, mas não
pode por mais do que 3 pontos no mesmo atributo.

Você tem direito a uma arma. Escolha entre uma **Arma concussiva**, uma
**Arma cortante**, uma **Arma perfurante**, uma **Arma de ataque à
distância**, ou uma **Varinha mágica**.

Você começa com 3 *Pontos de Vida* (PV). Some seu valor de
**Físico** a isso.

## Testes

Sempre que você for fazer algo cujo resultado seja incerto, o MM vai
pedir um teste de um de seus atributos. Se sua *Classe*,
arma ou algum equipamento que você tenha conseguido ao longo do jogo
puder ajudar na ação que você quer fazer, some +1 ao seu atributo por
item ou condição que possa ajudar. A seguir, role 1d6. Se você obtiver
no dado um valor igual ou menor do que o do atributo, você teve sucesso
na sua ação. Se você obtiver um valor maior do que o seu atributo, você
falhou em sua ação.

### Sucesso crítico e falha crítica

Se você obtiver 1 no dado, você conseguiu um *Sucesso
crítico*. O MM não vai apenas descrever o sucesso que você
obteve, como vai criar um benefício adicional para sua ação.

Se você obtiver 6 no dado, você conseguiu uma *Falha
crítica*. Você não apenas falhou em sua ação: você fez uma
lambança catastrófica. O MM irá descrever como a situação piorou por
causa da sua falha crítica.

### Conflito

Conflitos acontecem quando monstros querem causar dano em seu
personagem. Durante um conflito, o MM não rola dados: ele descreve o
ataque do monstro e o *Aventureiro* rola o dado para ver
se ele consegue se defender do ataque. Se o resultado for uma falha
crítica, o aventureiro sofre 2 PV de dano. Se o resultado for uma falha,
tanto o monstro quanto o aventureiro sofrem 1 PV de dano cada. Se o
resultado for um sucesso, o monstro sofre 1 PV de dano. Se o resultado
for um sucesso crítico, o monstro sofre 2 PV de dano. Se um aventureiro
ou um monstro chegar a 0 PV, ele morreu.

## Explorando a masmorra

A primeira cena é o grupo entrando na primeira sala da masmorra. O MM
deve rolar 1d6 e consultar o número de portas sala têm além daquela pela
qual os aventureiros entraram:

| **Dado** |  **Portas** |
| :---: | :---: |
| 1 ou 2 |  1 |
| 3 ou 4 |  2 |
| 5 ou 6 |  3 |

A seguir, o MM deve rolar 1d6 para saber o que tem na sala:

|  **Dado** |  **Encontro** |
| :---: | :---: |
|  1 ou 2 | Nada |
|  3 | 1 Arma mágica (causa 1 PV adicional de dano) |
|  4 | 1 Armadura (reduz em 1 PV o dano sofrido pelo aventureiro) |
|  5 ou 6 | Monstros |

Desnecessário dizer, cada aventureiro só pode usar uma arma mágica e uma
armadura mágica.

Se na sala tiver monstros, será preciso rolar 1d6 para saber quantos
monstros há na sala. Monstros têm apenas 1 PV, bem como todos são
capazes de realizar algum tipo de ataque corpo-a-corpo. Role na tabela
abaixo para ver se o monstro tem mais alguma habilidade. Monstros mais
poderosos podem ter mais de uma habilidade adicional, role mais vezes na
tabela se for o caso.

| **Dado** | **Dado** |
| :---: | :---: |
| 1. Monstro comum (o monstro não possui habilidades adicionais) | 4. Encouraçado (ignora 1 PV sofrido por ataque que não seja mágico) |
| 2. Voador (só pode ser atacado por armas de ataque à distância) | 5. Resistente (tem 1 PV adicional) |
| 3. Ataque à distância (o monstro pode realizar ataques à distância) | 6. Perigoso (causa 1 PV de dano adicional) |

## Fim de jogo

O jogo termina basicamente quando os jogadores decidirem que eles já
exploraram essa masmorra o bastante, ou então todos eles morrerem. Se
houver aventureiros vivos na hora em que se decidir encerrar a aventura,
o MM pode descrever como eles conseguiram encontrar o *Lendário
Tesouro*.

## Créditos e Licenças

Eu me inspirei fortemente em Munchkin, da Steve Jackson Games, na hora
de pensar este jogo. Não usei nenhum material ou propriedade intelectual
daquele jogo na hora de escrever o *Dungeon Explorers*.

*Dungeon Explorers* está licenciado por Lu *Cicerone*
Cavalheiro sob licença Creative Commons CC-BY 4.0 Internacional --
https://creativecommons.org/licenses/by/4.0/deed.pt_BR.
