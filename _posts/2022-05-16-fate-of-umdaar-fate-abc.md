---
layout: post
title: "Fate of Umdaar: criando personagens usando Abordagens e Perícias com o Fate ABC"
lang: pt-BR
date: 2022-05-15 23:00:00 -0300
tags: RPG Fate Evil-Hat Masters-of-Umdaar Fate-of-Umdaar-playtest Exemplo Personagens
---

E conforme eu prometi no texto anterior, [Fate of Umdaar: como criar um Campeão](https://cicerone.gitlab.io/2022/05/15/fate-of-umdaar-criacao-de-personagens.html), este texto é dedicado ao _Fate Accelerated Bundle Core_[^fate_abc], ou _Fate ABC_, um método de criação de personagens apresentado no _Fate of Umdaar_ que usa tanto Abordagens quanto Perícias. Apesar de vinculado ao _Fate of Umdaar_, é trivialmente fácil adaptar o _Fate ABC_ para qualquer jogo de _Fate Básico_ ou _Feite Moça_, então se você quer aprender essa técnica nova, continue lendo.o

[^fate_abc]: E não sou eu que vou tentar traduzir essa expressão, como eu mencionei no texto citado.

Primeiramente, quero enfatizar o quanto o _Fate ABC_ me chamou a atenção. Muitas vezes, é difícil atribuir as perícias de um personagem de modo que fique coerente com o que temos em mente para ele. Isso não é tanto um problema em _Fate of Umdaar_ por conta da questão dos _arquétipos_ já indicarem algumas perícias, mas mesmo assim pode ser que alguém se engasgue e acabe com um personagem cuja ficha não represente tão bem a ideia que se teve dele na hora da elaboração. Com o _Fate ABC_, isso não acontece, pois permite um foco ainda maior na atribuição das perícias.

Entretanto, o preço que se paga com isso é uma menor flexibilidade na hora de construção do personagem. Como será visto a seguir, o _Fate ABC_ segue um passo-a-passo bem estruturado, do qual não é possível se desviar sem abrir mão de usar o método. Para jogadores que gostam de ter um controle mais fino e mais flexível na hora da criação do personagem, portanto, o método pode não se adequar bem.

O importante a ser ressaltado aqui é que um personagem criado usando o _Fate ABC_ possui uma pirâmide de Perícias distribuída de modo idêntico ao de um personagem criado tradicionalmente, bem como ele possui valores em Abordagens. Isso facilita ainda mais o processo de usar personagens criados segundo as regras do _Fate Acelerado_ ao lado de personagens criados segundo o _Fate Básico_ ou o _Feite Moça_, além de permitir que uma Abordagem possa ser usada no caso de parecer não haver uma perícia apropriada para a ação que o personagem deseja fazer.

Para completar, após explicar o método, irei criar um personagem usando o _Fate ABC_ para que vocês possam visualizar como ele ficará, e assim ilustrar ainda mais como a coisa funciona.

## Esferas de perícia

_Esferas de perícia_ é uma regra que [aparece pela primeira vez](https://fatesrdbrasil.gitlab.io/fate-srd-brasil/ferramentas-de-sistema/esferas-de-pericias/) no _Ferramentas do Sistema_, no SRD do sistema. Trata-se de um método de atribuição de perícias com base em afinidades existentes entre elas, e funciona muito bem para a criação de personagens mais focados em determinadas funções.

## O _Fate ABC_

Em _Fate of Umdaar_, as _esferas de perícia_ são definidas usando as _Abordagens_ do _Fate Acelerado_, de acordo com a tabela a seguir:

| **Ágil** | **Cuidadoso** | **Esperto** | **Estiloso** | **Poderoso** | **Sorrateiro** |
| :---: | :---: | :---: | :---: | :---: | :---: |
| Atirar | Empatia | Ofícios | Comunicação | Condução | Enganar |
| Atletismo | Investigar | Provocar | Contatos | Lutar | Furtividade |
| Percepção | Vontade | Saberes | Recursos | Vigor | Roubo |

O primeiro passo é priorizar as _Abordagens_, indo de A (a mais prioritária) até F (a menos prioritária). A perícia principal[^lead_skill] do arquétipo deve estar listada na _Abordagem_ definida como A, caso o personagem tenha escolhido um.

[^lead_skill]: Tradução livre de _lead skill_.

O próximo passo é atribuir valores para as _Abordagens_ de acordo com a tabela a seguir:

| **Abordagem** | **Valor** |
| :---: | :---: |
| A | +3 |
| B e C | +2 |
| D e E | +1 |
| F | +0 |

A seguir, você deve escolher a perícia principal do arquétipo (que vai estar sob a _Abordagem_ priorizada como A) e uma perícia da _Abordagem_ priorizada como B e aumentá-las em +1 cada uma delas. Se o personagem não estiver usando um arquétipo, então ele pode escolher qualquer perícia da _Abordagem_ A para isso. As duas perícias aumentadas dessa forma serão as _perícias características_[^signature] do personagem.

[^signature]: Tradução livre para o termo _signature skill_.

O próximo passo é escolher uma perícia em cada uma das _Abordagens_ priorizadas entre A e E e atribuir a elas o valor +0. Essas são as _perícias zeradas_[^zeroed-out] do personagem.

[^zeroed-out]: Tradução livre do termo _zeroed out skill_.

Por fim, o valor das demais _perícias_, aquelas que não sejam nem uma _perícia característica_, nem uma _perícia zerada_, é igual ao valor da _Abordagem_ sob a qual foram agrupadas.

## Exemplo de uso do Fate _ABC_

Márcio deseja criar um personagem para _Fate of Umdaar_ usando o _Fate ABC_. Olhando para a lista de arquétipos, ele se decidiu pelo _Artista Marcial_, cuja perícia principal é _Atletismo_.

O primeiro passo para Márcio é priorizar as Abordagens. Como ele escolheu um arquétipo, a Abordagem que contém a perícia Atletismo deve ser priorizada como A. Ele já adianta as anotações e registra também os valores das Abordagens, ficando como a tabela abaixo:

| **Abordagem** | **Prioridade** | **Valor** |
| :---: | :---: | :---: |
| Ágil | A | +3 |
| Poderoso | B | +2 |
| Cuidadoso | C | +2 |
| Esperto | D | +1 |
| Sorrateiro | E | +1 |
| Estiloso | F | +0 |

Em _Ágil_, temos as perícias _Atirar_, _Atletismo_ e _Percepção_. Como _Atletismo_ é a perícia principal do arquétipo, ele coloca +1 nela. Márcio não vê seu personagem usando ataques à distância, então ele escolher _Atirar_ para ser a _perícia zerada_. Em _Poderoso_, ele escolhe _Lutar_ para receber +1, e _Condução_ para ser a _perícia zerada_ dessa Abordagem. Assim, as _perícias características_ do personagem de Márcio serão _Atletismo_ e _Lutar_. Em _Cuidadoso_, ele escolhe _Empatia_ como a _perícia zerada_; em Esperto, _Provocar_; e em Sorrateiro, _Enganar_.

Depois dessas escolhas, o personagem de Márcio fica com a seguinte pirâmide de perícias:

| **Valor** | | | | |
| ---: | --- | --- | --- | --- |
| Ótimo (+4) | Atletismo | | | |
| Bom (+3) | Percepção | Lutar | | |
| Regular (+2) | Vigor | Investigar | Vontade | |
| Razoável (+1) | Ofícios | Saberes | Furtividade | Roubo |

Por razões óbvias, não registrei as perícias em _Medíocre (+0)_.

### Novas opções para façanhas

Usando o _Fate ABC_, você tem duas novas opções na hora de criar uma _Façanha_:

1. **Deszerar**: Ao usar a perícia [perícia zerada] para [situação específica], use o valor da Abordagem dela ao invés de +0 na hora de rolar os dados.
1. **Vagueza**: Se não for fácil achar uma perícia adequada para uma façanha, use uma Abordagem ao invés de uma perícia.

## Considerações finais

E esse é o _Fate ABC_. Aproveitem esse novo método para criação de personagens, explorem suas opções e estejam prontos para derrubar... **OS MESTRES DE UMDAAR**!

---

