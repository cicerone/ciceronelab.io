---
layout: post
title: "Zombocalipse: um RPG minimalista sobre apocalipse zumbi"
lang: pt-BR
date: 2022-06-02 19:30:00 -0300
tags: RPG Folheto Minimalista Sistema Autoral Zombocalipse Lasers-and-Feelings-hack
---

Meus caros leitores, hoje quero aproveitar e divulgar para vocês um RPG minimalista de minha autoria, o **Zombocapolipse**. Escrito originalmente para caber em um panfleto, ele é um _hack_ de Lasers & Feelings, de John Harper, nos quais os personagens serão sobreviventes de um apocalipse zumbi.

Enfim, divirtam-se com a leitura. Em breve, o jogo será publicado, e aí eu passo para vocês o link para obtenção da versão diagramada.

## Zombocalipse

Vocês são um dos últimos grupos de sobreviventes do apocalipse zumbi que
dizimou a Cidade. Sua missão agora é sobreviver até conseguir escapar
desse caos.

## Jogadores: criando um *Sobrevivente*

Escolha um *Estilo* para o seu
*Sobrevivente*: **Agressivo**, **Cauteloso**, **Curioso**,
**Esperto**, **Estiloso**, **Impulsivo** ou **Sorrateiro**.

Escolha uma *Função* para o seu
*Sobrevivente*: **Agente do governo**, **Cientista**,
**Jornalista**, **Médico**, **Piloto de fuga**, **Policial novato** ou
**Soldado veterano**.

Escolha um *Número* entre 2 e 5. Um número alto significa
que você é bom em **Enfrentar** (enfrentar, confrontar,
lutar, disputar, intimidar, abordar diretamente). Um número baixo
significa que você é bom em **Evadir** (esquivar, fugir,
esconder, dissimular, abordar indiretamente).

Anote suas *Condições* disponíveis: **Abalado**,
**Escoriado**, **Ferido** e **Morrendo**.

Dê ao seu *Sobrevivente* um **nome maneiro de
sobrevivente**. Pode ser um apelido se você quiser ser o
cara misterioso do grupo.

**Seu equipamento inicial**: Uma muda de roupas; dois itens de cura
capazes de curar uma *Condição* cada; escolha entre uma
arma de ataque corpo-a-corpo ou uma arma de ataque à distância com
munição para dez disparos.

**Objetivo do jogador**: Ter seu personagem envolvido em uma história de
sobrevivência a zumbis e fazer o melhor possível.

**Objetivo do personagem**: Escolha um, ou invente o seu próprio:
**Continuar sendo *o cara***, **Descobrir de onde os zumbis vieram**,
**Eliminar os zumbis**, **Fazer dos sobreviventes sua nova família**,
**Pesquisar os zumbis**, **Resgatar sobreviventes**, **Ser o líder dos
sobreviventes**.

## Rolando os dados

Quando você fizer algo arriscado, role **1d6** para descobrir como você
se saiu. Role **+1d6** se você estiver **preparado** e **+1d6** se você
for um **especialista**. Se a *Condição* **Ferido**
estiver marcada, role **-1d6**. Se a *Condição*
**Morrendo** estiver marcada, role **-2d6** ao invés de -1d6 da
*Ferido*. Se você for rolar **0d6** ou menos dados, você
não será capaz de realizar a ação desejada. *O Narrador lhe dirá quantos
dados rolar, de acordo com seu personagem e a situação*.

Role os dados e compare cada resultado com o seu *Número*:
se você estiver usando **Enfrentar**, você vai querer um
valor **menor ou igual ao** seu *Número*; se você estiver
usando **Evadir**, você vai querer um valor **maior ou
igual ao** seu número.

**Se nenhum dos dados for um sucesso**, algo deu errado. O Narrador dirá como as coisas, por alguma razão, pioraram.

**Se apenas um dos dados for um sucesso**, você conseguiu mal e porcamente. O Narrador introduzirá uma complicação, dano ou custo.

**Se dois dados forem sucessos**, você conseguiu fazer o que queria. Bom trabalho!

**Se três dados forem sucessos**, você conseguiu um sucesso crítico! O Narrador descreve um efeito ou benefício adicional que você conseguiu por conta de seu sucesso.

**Se pelo menos um dado cair exatamente o seu *Número***,
você conseguiu um **Enfrentamento evasivo**. Você consegue um
entendimento melhor sobre o que está acontecendo ao seu redor. Faça uma
pergunta ao Narrador, e ele a responderá honestamente. Algumas boas
perguntas:

*O que ele realmente está pensando?*, *Tem algo por trás disso?*, *Como
eu posso fazer com que eles \_\_\_\_?*, *Em que eu deveria estar atento
agora?*, *Qual é o melhor jeito para \_\_\_\_?*, *O que realmente está
acontecendo aqui?*

**Ajudando**: Se você quiser ajudar na rolagem de algum
outro *Sobrevivente*, diga como você vai tentar ajudar e
role os dados. Se você tiver sucesso, dê **+1d6** a ele.

### Enfrentando zumbis

Caso seu *Sobrevivente* ataque ou seja atacado por um
zumbi, descreva seu ataque ou defesa, determine se a ação usará
**Enfrentar** ou **Evadir** e faça uma
rolagem.

**Se nenhum dos dados for um sucesso**, o zumbi conseguiu lhe causar dano. Marque uma **Condição** que esteja desmarcada.

**Se apenas um dos dados for um sucesso**, você e o zumbi causaram dano um no outro. Tanto você quanto o zumbi marcam uma **Condição** que esteja desmarcada.

**Se dois dados forem sucessos**, você causa dano ao zumbi, que marca uma **Condição**.

**Se três dados forem sucessos**, você conseguiu um sucesso crítico! O zumbi marca duas **Condições**.


As *Condições* devem ser marcadas sequencialmente. A
primeira a ser marcada é a **Abalado**, depois é a **Escoriado**, a
seguir é a **Ferido**, e por último é a **Morrendo**. Se o zumbi sofrer
uma *Condição* e não tiver mais nenhuma para marcar, ele
será destruído. Se isso acontecer ao seu *Sobrevivente*,
ele morrerá.

## Narrador: criando Zumbis

Zumbis têm apenas uma *Condição*, **Abalado**, bem como
todos são capazes de realizar algum tipo de ataque corpo-a-corpo. Role
na tabela abaixo para ver se o zumbi tem mais alguma habilidade. Zumbis
mais poderosos podem ter mais de uma habilidade adicional, role mais
vezes na tabela se for o caso.

| Valor no dado | Valor no dado |
| :--- | :--- |
| 1. Zumbi comum (o zumbi não possui habilidades adicionais) | 4. Encouraçado (ignora 1 *Condição* sofrida por ataque) |
| 2. Voador (só pode ser atacado por armas de ataque à distância) | 5. Resistente (tem uma segunda *Condição*, **Escoriado** |
| 3. Ataque à distância (o zumbi pode realizar ataques à distância) | 6. Perigoso (causa uma *Condição* de dano adicional) |

## Narrador: role o jogo

Jogue para descobrir como os *Sobreviventes* vão se virar
no meio do caos. Introduza ameaças mostrando evidências de sua presença.
Antes de uma ameaça fazer algo contra um *Sobrevivente*,
dê indícios do que está para acontecer e então pergunte qual será a
reação dele. *O zumbi inclina a cabeça para trás e se prepara para
cuspir algo em sua direção. O que você vai fazer?*; *Você está
encurralado no topo de um prédio, com outros prédios de alturas
parecidas ao redor. O que você vai fazer?*

Peça por rolagens quando a situação for incerta. Não planeje resultados
com antecedência -- deixe as coisas acontecerem. Use as falhas para
levar a ação adiante. A situação sempre muda quando uma rolagem é
pedida, para o bem ou para o mal.

Faça perguntas e crie em cima das respostas. *Vocês já viram esse tipo
de zumbi antes?*; *Como era este bairro antes do apocalipse zumbi?*;
*Você conhecia esse cara antes de ele ser um zumbi?*.

## Créditos e licença

**Zombocalpyse** é baseado em *Lasers & Feelings*, de John
Harper.

**Zombocalpyse** é licenciado por Lu *Cicerone* Cavalheiro
sob **Licença Creative Commons CC-BY 4.0 Internacional** --
https://creativecommons.org/licenses/by/4.0/deed.pt\_BR.
 
---
