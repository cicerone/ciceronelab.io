---
layout: post
title: "Divulgação: Workshop Formando RPGistas"
lang: pt-BR
date: 2022-05-25 11:00:00 -0300
tags: RPG Divulgação Workshop Eventos RPGWorld Lampião-Game-Studio SESC SESC-São-João-de-Meriti
---

Moradores do Grande Rio, atenção: começou dia 21 de maio de 2022 o **Workshop Formando RPGistas**, uma parceiria entre o SESC de São João de Meriti, o RPGWorld e o Lampião Game Studio. A proposta do workshop, cujo cronograma está disponível na imagem a seguir, é realizar uma série de encontros para discutir o que é RPG e introduzir novas pessoas ao hobby em uma linguagem clara e acessível, bem como discutir e abordar conceitos mais familiares a jogadores mais experientes.

![flyer-workshop](/assets/workshop-formando-rpgistas/divulgacao.jpeg)

Neste dia 21, o tema do encontro foi a discussão _O que é Jogo de Interpretação?_, mediada por ninguém menos do que Rafael Wernek, do RPGWorld, e Jorge Valpaços, do Lampião Game Studio. Foi uma conversa bem agradável, na qual se abordou a questão sobre como conceituar o que é um jogo de interpretação e como esse conceito pode vir a afetar até o próprio design do jogo. A linguagem foi clara e impecável, bastante acessível e sem pedantismos, apesar de direcionada a pessoas que tinham pouco ou nenhum contato com RPG de mesa na vida. Houve atividades práticas que foram bastante lúdicas, o que ajudou enormemente a reforçar as ideias compartilhadas. Àqueles que puderem participar, super recomendo!

Dia 28 agora vai ser um encontro prático, isto é, um evento de RPG, com mesas disponíveis para sentar e jogar. Haverá algumas mesas, todas muito interessantes, umas pela manhã e outras pela tarde, a fim de garantir vagas a todos os interessados. Momento jabá: eu vou narrar, pela parte da manhã, uma aventura para o playtest de _Fate of Umdaar_, então quem quiser jogar precisa chegar cedo e garantir seu lugar na mesa!

## Sobre o RPGWorld

O RPGWorld é um grupo fundado por Rafael Wernek e Andressa Konno cuja missão é trazer diferentes pontos de vista ao RPG e alcançar pessoas e lugares que não conheciam esse jogo tão cheio de qualidades, e cujo prazer é ver jogadores e mestres usando nosso material em suas mesas, receber seu retorno e criar um círculo virtuoso de evolução! Eles não têm a pretensão de estar sempre certos e muito menos de ser a vanguarda do RPG, e sim de ser uma das vozes que soma ao coro diversidade e profundidade, objetivando fomentar a prática do RPG por onde formos, em especial na Baixada Fluminense, fazer dessa macrorregião ser um polo cultural!

Contatos:

+ Blog: <https://rpgworldsite.wordpress.com/>  
+ Discord: <https://discord.gg/AFRz4kJ>  
+ Drivethrurpg: <https://www.drivethrurpg.com/browse/pub/17691/RPGWorld>  
+ Dungeonist: <https://www.dungeonist.com/marketplace/publisher/rpgworld/>  
+ Facebook: <https://www.facebook.com/rpgworldsite/>  
+ Instagram: <https://www.instagram.com/rpgworldsite/>  
+ Linktree: <https://linktr.ee/RPGWorldsite>  
+ Whatsapp: <https://chat.whatsapp.com/LgozG0nVNaH2K7ialcehlN>  

## Sobre o Lampião Game Studio

O Lampião Game Studio é um coletivo criativo que tem por objetivo aquecer e iluminar por meio de nossas narrativas em jogo. Nele, se congregam vários autores de peso, impacto e relevância na produção de RPG indies no nosso país, como Diego Bernard, Jefferson Neves, Jorge Valpaços, Luiz Oliveira, Prissila Oliveira, e Rafão Araújo.

Contatos:

+ Blog: <https://lampiaogamestudio.wordpress.com/>  
+ Facebook: <https://www.facebook.com/lampiaogamestudio/>  
+ Instagram: <https://www.instagram.com/lampiaogamestudio/>  
+ Twitter: <https://twitter.com/LampiaoGS>  
+ Youtube: <https://www.youtube.com/Lampi%C3%A3oGameStudio>  

---
