---
layout: post
title: A Necrópole Amaldiçoada, uma aventura para Fate of Umdaar
lang: pt-BR
locale: pt_BR
date: 2022-05-17 23:00:00 -0300
tags: RPG Fate Evil-Hat Masters-of-Umdaar Fate-of-Umdaar-playtest Exemplos Aventuras
---

E em mais um escrito sobre o playtest de _Fate of Umdaar_, hoje trago a vocês a aventura _A Necrópole Amaldiçoada_. Eu pretendo narrá-la em breve, e assim que eu o tiver feito acrescentarei um subtítulo para agradecer os participantes.

## Introdução

Nostromus era uma pequena porém próspera comunidade localizada nos _Altiplanos Cinzentos_, um imenso planalto no qual a terra possui uma cor e composição muito próximas às da cinza de carne queimada. O clima é de extremos, com verões muito quentes e secos e invernos muito frios e úmidos, com estações intermediárias curtas entre elas. Apesar disso, os nostromenses conseguiam ter algum sucesso com a agricultura, valendo-se dos cereais nativos, cuja resistência é quase a das ervas daninhas, e com a pecuária, graças ao _gnutil_, o ruminante local e animal símbolo oficial da cidade.

Entretanto, a principal fonte de riqueza da cidade é o conhecimento médico. Pessoas de todos os cantos do continente viajam para Nostromus em busca de cura ou remédios para inúmeras mazelas, embora os Doutores, a casta social que detém esse conhecimento, atenda apenas àqueles que possam pagar por seu trabalho. Por essa razão, Nostromus mantém sua neutralidade, atendendo igualmente as _Terras Livres_[^freelands] e os _Domínios Sombrios_[^dread-domains] ao seu redor – ou, nas palavras de um ditado nostromense, _dinheiro é tudo igual, não importa a origem_. Dizem os rumores que todo esse saber foi obtido saqueando ruínas de Demiurgos, mas os Doutores não estão dispostos nem a negar, nem a confirmar os boatos.

[^freelands]: Tradução livre para _Freelands_.

[^dread-domains]: Tradução livre para _Dread Domains_.

Acontece, porém, que os boatos são verdadeiros. No meio dos _Altiplanos Cinzentos_ fica a _Floresta de Metal_, um complexo de Ruínas do Demiurgo formado por vários círculos concêntricos de obeliscos ou minaretes de metal e em cujo centro encontra-se uma torre gigantesca chamada de _A Necrópole_. Soldados nostromenses ficam de guarda o tempo todo, e apenas os Doutores podem entrar em _A Necrópole_. Como que para reforçar a proibição, a lei de Nostromus proíbe remover os cadáveres dos possíveis invasores, conferindo uma aparência sinistra para um lugar com um nome tão convidativo.

Totalmente lisa e sem janelas por fora, _A Necrópole_ possui vinte e dois andares acima do nível da superfície, e pelo menos dez andares, a maioria deles inexplorados, abaixo. Os pesquisadores nostromenses nunca conseguiram decifrar qual era o propósito original da ruína, mas descobriram como recuperar algumas das informações entre as runas do Demiurgo. Além de descobrir que o local era usado para alguma forma de pesquisa médica, os pesquisadores recuperaram uma grande quantidade de conhecimento médico, que é usado pelos Doutores em Nostromus para exercer e desenvolver a medicina – enriquecendo no processo, é claro.

Nos últimos meses, porém, algo muito estranho aconteceu a Nostromus. Os _Altiplanos Cinzentos_ foram dominados por _carniçais_, monstros devoradores de carne que parecem pessoas parcialmente decompostas. A agricultura e a pecuária foram abandonadas, e os camponeses sobreviventes se refugiaram na cidade, tornando-se moradores de rua no processo. O comércio com outras comunidades declinou, pois são poucas as caravanas que têm disposição para atravessar os _Altiplanos Cinzentos_ atualmente. O caos e a pobreza dominaram a outrora Nostromus.

Em um primeiro esforço investigativo, as autoridades de Nostromus descobriram que havia uma grande possibilidade de os tais _carniçais_ estarem vindo de _A Necrópole_. Entretanto, as investigações tiveram de ser abandonadas porque cada vez havia menos pessoas capazes de proteger a cidade contra os números cada vez maiores de _carniçais_ infestando os _Altiplanos Cinzentos_. Desesperadas, as autoridades mandaram mensageiros para várias outras comunidades, tanto nas _Terras Livres_ quanto nos _Domínios Sombrios_.

O primeiro a responder foi o _Barão Surocco de Jel'Thazzar_, um dos mais poderosos Mestres de Umdaar na região. Cobiçando para si os segredos que os nostromenses desencavaram de _A Necrópole_, ele ofereceu ajuda à cidade em troca de transformá-la em um protetorado. Sem aguardar resposta, ele enviou um destacamento de soldados talentosos, comandados por seu mais leal subordinado, _Lorde Guldbaard, o Conquistador_, pronto para eliminar todos os _carniçais_ e conquistar Nostromus para a glória do seu Mestre.

Envoltas com seus próprios problemas, as _Terras Livres_ não puderam enviar um grupamento de soldados para ajudar Nostromus. Entretanto, elas compensaram na qualidade, pois formaram uma equipe com seus melhores _Arqueonautas_ para resolver o mistério dos _carniçais_ e enfrentar os homens de _Lorde Guldbaard, o Conquistador_. Eles terão de cruzar os _Altiplanos Cinzentos_, enfrentar tanto os _carniçais_ quanto os soldados de Guldbaard, e descobrir o que está acontecendo antes que seja tarde demais e Nostromus ou seja devorada pelos _carniçais_, ou seja conquistada por um dos Mestres de Umdaar.

E então, valentes campeões, vocês estão prontos para deter mais uma das maquinações dos... **MESTRES DE UMDAAR**??

### Sobre a aventura

A aventura _A Necrópole Amaldiçoada_ foi escrita tendo em vista os materiais então disponíveis sobre o playtest de _Fate of Umdaar_. Ao longo do seu texto, ela fará muitas menções a esse livro, e é bastante importante ressaltar que ele não está em um estado finalizado ainda. Isso significa que muita coisa pode mudar no _Fate of Umdaar_, e consequentemente esta aventura pode não ser exatamente compatível com a versão final do livro. Recomenda-se cautela ao leitor.

Da mesma maneira, apesar de a aventura ter sido escrita tendo o _Fate of Umdaar_ em mente, não será nada difícil adaptá-la para qualquer outro mundo ou cenário. Sinta-se à vontade para usar entre 0% e 100% deste texto em seu jogo, lembrando-se de me dar os devidos créditos caso seu texto venha a ser publicado em algum lugar. Para maiores orientações sobre como me dar os créditos, leia em [Sobre](https://cicerone.gitlab.io/about).

## A Fundação

Antes de descrever a aventura em si, é preciso oferecer algumas informações sobre os elementos do mundo e do cenário nos quais ela acontecerá. Para tanto, será usada a estrutura estabelecida no capítulo _Fundação_, do _Fate of Umdaar_ – entretanto, não serão realizadas discussões sobre os _Limites_, a fase 1 do processo, por se tratar de algo bem específico para cada grupo.

### Fase 2: Estrutura

_A Necrópole Amaldiçoada_ é uma aventura que pode ser jogada tanto como _independente_ quanto como _serializada_. Essa é uma escolha que precisa ser feita, pois cada uma delas suscita questões que precisarão ser abordadas antes do jogo.

Se _A Necrópole Amaldiçoada_ for uma aventura independente, a maioria das questões que precisam ser feitas serão sobre os campeões. Como o foco da aventura é um jogo de arqueonautas, e não será assumido que todos eles vieram do mesmo lugar ou são de Nostromus, seu grupo de jogo precisará decidir algumas coisas:

1. _Qual é a comunidade de origem dos campeões_?   
    Os campeões não são assassinos errantes sem laços ou ligações com nada nem ninguém. Mesmo que suas comunidades de origem não venham a afetar diretamente a aventura, elas estarão aí, em algum lugar no mundo, e foram essas comunidades que os escolheram para formar a força-tarefa enviada para Nostromus. Sendo assim, é importante pensar na origem dos campeões. Eles são heróis celebrados, ou fizeram alguma besteira no passado e precisam disso para se redimir? Eles vieram do mesmo lugar, ou cada campeão veio de uma comunidade distinta? Algum dos campeões é nativo de Nostromus?  
    Na hora de criar o aspecto _Origens_, pense em algo que possa ser usado mesmo longe da comunidade. Algo como _O Prefeito Joor me deve favores_ pode não ser tão útil quanto _Herói e símbolo de Hocastron_, por exemplo. Tente não acabar com um aspecto pouco usável ao longo da aventura.
1. _Quais são os relacionamentos entre os campeões_?  
    Os campeões foram escolhidos para formar uma força-tarefa para auxiliar Nostromus, e existe a probabilidade de que eles não se conheçam previamente. Ainda assim, eles terão opiniões sobre seus companheiros e afinidades (ou falta delas!) uns com os outros. Como é o relacionamento entre os campeões? Eles são profissionais capazes de por a missão acima de opiniões pessoais, ou discussões e guerras de ego são comuns? Você foi com a cara de seu companheiro de missão, ou você o acha um rematado idiota?  
    Na hora de criar o aspecto _Relacionamento_, pense em algo simples e direto, e deixe as sutilezas por conta das interações, especialmente se vocês não se conheciam previamente. Afinal de contas, é difícil formar opiniões profundas sobre desconhecidos, e quando acontece de se as formar, normalmente elas estão erradas.
1. _Por que o campeão aceitou essa missão_?  
    O campeão aceitou ser enviado para Nostromus por senso de dever, ou foi atraído pela glória de vencer um tenente de um dos Mestres? Sua comunidade ou ele próprio tem interesses em Nostromus? Se sim, quais?  
1. _Qual é a relação entre as demais comunidades e Nostromus_?  
    Nem Nostromus, nem as demais comunidades do mundo existem em um vácuo. Inclusive, se as relações entre elas forem boas e profundas o bastante, vá em frente e as adicione como _Aspectos de Cenário_!

Entretanto, se _A Necrópole Amaldiçoada_ for parte de um jogo _serializado_, será preciso pensar outras questões. Se esta não for a primeira aventura da série, é interessante se perguntar como Nostromus e os demais elementos deverão ser inseridos, e sinta-se à vontade para alterar uma ou outra coisa a fim de fazer com que essa inserção seja mais fácil, ou que o resultado final fique mais ao gosto do seu grupo de jogo. Se esta for a primeira aventura da série, elabore como as demais aventuras se seguirão a ela. Em ambos os casos, não vejo proveito em oferecer uma lista de questões pré-definidas, já que cada série de aventuras possui suas próprias características.

### Fase 3: Foco

O foco da aventura _A Necrópole Amaldiçoada_ é o jogo de _arqueonautas_. Assim sendo, se dará uma ênfase maior à exploração e resolução de problemas em ermos, mais especificamente os _Altiplanos Cinzentos_, e em ruínas, nomeadamente, _A Necrópole_. Haverá algumas possibilidades de conflito físico, seja contra _carniçais_, seja contra os homens de Lorde Guldbaard, mas no âmbito da exploração.

### Fase 4: Locais

#### Os Altiplanos Cinzentos

**Aspectos**: _Cinzento e cinzas até onde a vista alcança_; _Clima extremo e inclemente_; _Infestado por carniçais_; _Patrulhas de Lorde Guldbaard_.

Os _Altiplanos Cinzentos_ são um imenso planalto que domina a região central do continente. Localizado a aproximadamente 1200 metros do nível do mar, não existem variações no relevo mais marcantes do que outeiros ou suaves depressões nos _Altiplanos_, o que causa a sensação de que o lugar se estende para o infinito e além. A vegetação nativa é esparsa, com poucos grupamentos de árvores baixas e retorcias entre as campinas de gramíneas altas. Os _Altiplanos_ são irrigados pela bacia do Rio Jundaassian, o que ameniza a secura do verão mas transforma a região em um grande alagadiço no inverno.

Em termos climáticos, os _Altiplanos_ podem ser definidos como _cerrado_. Entretanto, as estações oscilam entre extremos: os verões são quentes e secos, os invernos, frios e úmidos, e as estações intermediárias são curtas. Além disso, mudanças climáticas podem ocorrer de uma hora para outra, transformando um dia relativamente seco e quente em uma tempestade torrencial. Pesquisadores de Nostramus teorizam que os _Altiplanos_ foram formados quando uma das máquinas de terraformação de Umdaar falhou, mas não existem evidências sustentando essa hipótese.

Porém, a principal característica geográfica dos _Altiplanos_ é seu chão. A terra possui cor e composição próximas às de cinza de carne queimada, embora não tenha o mesmo cheiro. Ao toque, ela parece uma mistura de cinzas com basalto moído e faz a mão coçar, como se exposta a algum produto químico. Apesar disso, ela não é de todo estéril, embora apenas as plantas nativas sejam capazes de nascer e vingar em um solo tão hostil. Os poucos animais e plantas nativos são bem adaptados às condições extremas impostas pelo clima e pela terra.

Nos últimos meses, os _Altiplanos_ foram infestados por _carniçais_, criaturas devoradoras de carne que se parecem com pessoas parcialmente decompostas. _Pessoas_ é a palavra chave aqui, já que não há prevalência de nenhuma bioforma entre os _carniçais_: humanos, centauros, reptiloides, e muitos outras bioformas já foram vistas. Eles são particularmente resistentes a várias formas de ataque, e nada além da destruição total é capaz de parar um _carniçal_.

A chegada dos homens de Lorde Guldbaard não ajudou em nada a situação nos _Altiplanos_. Com a desculpa de eliminar os _carniçais_, ele espalhou seus homens pelos _Altiplanos_, e provavelmente está aguardando o momento certo para atacar Nostromus. Qualquer um andando pelos _Altiplanos_ pode vir a ser atacado por uma patrulha.

**Os Altiplanos Cinzentos**  
***Aspectos***: _Cinzento e cinzas até onde a vista alcança_; _Clima extremo e inclemente_; _Infestado por carniçais_; _Patrulhas de Lorde Guldbaard_.  
***Ótimo (+4) em***: Mudanças climáticas.  
***Clima inclemente***: Uma vez por sessão, é possível invocar gratuitamente o aspecto _Clima extremo e inclemente_. Quem o fizer deverá descrever como uma mudança súbita de clima o beneficiou naquele momento.  

#### Nostromus

**Aspectos**: _Importante centro médico baseado em segredos do Demiurgo_; _Aqui, ou você tem, ou você não é nada_; _Conluio entre os Doutores e o governo_; _Crise de refugiados_; _Sitiada por carniçais e por Lorde Guldbaard_.  
**Valores**: _Altruísmo_ vs. _Individualismo_ (priorizando _Individualismo_).  

Nostromus é uma pequena, porém próspera comunidade situada nos _Altiplanos Cinzentos_. Sua população total, que inclui tanto os camponeses dos assentamentos tributários a Nostromus quanto os moradores da cidade em si, historicamente oscila entre dez e doze mil pessoas, a maioria humanos, mas praticamente toda bioforma minimamente comum pode ser encontrada na cidade. Os principais negócios da cidade são os serviços médicos, e antes da crise com os _carniçais_ vinha gente de toda Umdaar atrás dos tratamentos que só poderiam ser encontrados em Nostromus.

Administrativamente, a cidade é dividida em quatro _Distritos_. A _Cidade Velha_, o distrito mais antigo e centro da cidade, marca a região em que os primeiros moradores de Nostromus decidiram se assentar, e até hoje é tanto um distrito residencial como o centro administrativo e político da cidade. O _Largo dos Doutores_ é o distrito com maior presença de consultórios médicos, clínicas e hospitais, bem como de Doutores e suas famílias. A _Praça do Mercado_ é um distrito majoritariamente comercial, com ênfase na compra e venda de bens e prestação de serviços de hotelaria. A _Cidade Nova_, o mais novo dos distritos, foi construído quando a população de Nostromus começou a aumentar e houve a gentrificação do _Largo dos Doutores_ e da _Praça do Mercado_, expulsando os mais pobres desses dois distritos.

![Mapa dos distritos de Nostromus](/assets/necropole-amaldiçoada/mapa-nostromus.png)

Recentemente, Nostromus está enfrentando uma das maiores crises de sua história. Os _carniçais_ e os soldados de Lorde Guldbaard forçaram os camponeses dos assentamentos tributários a se refugiarem na cidade principal, e simplesmente não havia moradia ou mesmo abrigo para todos. Temendo perder privilégios, os Doutores, a casta social com o direito exclusivo de estudar os segredos da ciência médica, embarreiraram por conta própria os portões de acesso ao _Largo dos Doutores_, e iniciaram uma campanha junto ao governo da cidade a fim de impedir que os refugiados entrem no distrito. O governo, muito ciente do poder dos Doutores, tem cooperado com eles até então, e o _Largo dos Doutores_ é o olho calmo do furacão que assola Nostromus.

Obviamente, a situação está crítica nos demais distritos. Para agradar aos Doutores, o governo deslocou metade, se não mais, das forças militares e policiais para o _Largo dos Doutores_. Com os números das forças responsáveis por manter a lei e a ordem escassos e a crise econômica e social aparentemente sem solução, os moradores da cidade têm recorrido a medidas desesperadas para conseguir coisas básicas como água ou comida, o que piora ainda mais a crise. Os mais audazes recorrem ao banditismo, enquanto os mais sensatos organizam-se em pequenas milícias para proteção mútua. Infelizmente, o espírito geral da cidade está cada vez mais oscilando em direção ao _cada um por si_.

##### Classes sociais em Nostromos

A população nostromense divide-se em três classes sociais: _Patrícios_, _Médios_, e _Proletariado_. Os _Patrícios_ são os ricos e poderosos da cidade, dentre os quais os _Doutores_ se destacam como um dos subgrupos mais poderosos. Por força de leis antigas, apenas os _Patrícios_ podem ocupar cargos no alto escalão do governo, seja entre os funcionários civis, seja entre os militares. Existem alguns outros privilégios legais aos _Patrícios_, como tribunais próprios, maior leniência das leis e da administração pública, prioridade na prestação de certos serviços públicos, poder se tornar um _Doutor_, etc. Para se tornar um _Patrício_, é preciso que o prefeito da cidade promulgue uma lei específica declarando o indivíduo (que necessariamente, por conta das leis, precisa ser um _Médio_) um _Patrício_. Ser um _Patrício_ é uma regalia hereditária.

Os _Médios_ possuem alguma riqueza ou poder na cidade, mas nada capaz de diretamente rivalizar com os _Patrícios_. Os _Médios_ podem ocupar cargos no governo, ainda que estejam restritos ao baixo e médio escalão, e possuem certos privilégios legais. Um deles é poder se candidatar à _Câmara dos Representantes_, um corpo de conselheiros e legisladores que possui certos poderes na cidade e com o qual o Prefeito precisa se consultar antes de tomar decisões que afetem os _Médios_ ou o _Proletariado_. O outro é poder se tornarem _Enfermeiros_, assistentes treinados para os _Doutores_. Para se tornar um _Médio_, é preciso comprar uma _Carta de Cidadania_, acessível apenas aos nascidos em Nostromus ou seus assentamentos tributários e cujo valor nunca é inferior ao rendimento de cinco anos de um camponês mediano. Ao contrário de ser um _Patrício_, ser um _Médio_ não é uma regalia hereditária.

Por fim, o _Proletariado_ são os trabalhadores e a massa da população de Nostromus, além dos descendentes de um _Médio_ que, por alguma razão, não tenham comprado suas próprias _Cartas de Cidadania_. Eles estão proibidos de ocupar cargos no governo, além de serem protelados em favor dos _Médios_ e dos _Patrícios_ quando necessário. Ainda assim, possuem certos direitos básicos, como acesso a segurança e moradia em Nostromus ou seus assentamentos tributários. Apenas indivíduos nascidos em Nostromus_ filhos de _Proletários_ ou _Médios_ são reconhecidos como _Proletários_ pelo governo.

##### Faces em Nostromus

###### Prefeito Gynfreth Moscathi

Governante de Nostromus há mais de vinte anos, o prefeito Gynfreth era um Doutor medíocre até decidir que política era mais interessante do que salvar vidas. Usando sua considerável fortuna pessoal, ele conseguiu ser eleito pela população de Nostromus, e tem usado complexas e intrincadas manobras políticas para permanecer no poder desde então.

Fiel às suas origens, Gynfreth é um dos firmes defensores do _status quo_ na cidade. Foi sua decisão ignorar os primeiros relatos sobre _carniçais_ nos _Altiplanos Cinzentos_, bem como foi sua decisão postergar o pedido de ajuda até quando não fosse mais possível aguentar. À boca miúda, comenta-se que ele só começou a agir quando os _Patrícios_, e em especial os _Doutores_, se viram prejudicados pela situação.

A presença de Lorde Gundbaard nos _Altiplanos_ tem deixado Gynfreth preocupado. Se Nostromus se tornar um poder vassalo aos domínios do Barão Surocco de Jel'Thazzar, os lucros cairão enormemente, pois não será possível comerciar livremente com outras comunidades nem receber pacientes médicos delas. Por outro lado, se os arqueonautas das _Terras Livres_ conseguirem resolver o problema, isso pode servir de incentivo para que a população exija mudanças drásticas na cidade. Ele não sabe o que fazer, e pode acabar tomando alguma decisão muito estúpida por conta disso.

###### Fashav Sorridente

Fashav é uma camponesa proletária por nascimento e uma líder de milícia por necessidade. Nascida em um dos muitos assentamentos tributários de Nostromus, sua família, desde tempos imemoriais, obtém sustento da criação de _gnutis_ e da agricultura de subsistência. Não era uma vida de luxos e riquezas, mas era confortável, e todos estavam felizes com isso.

Tudo mudou quando os _carniçais_ apareceram. Seus pais se recusaram a abandonar as terras da família, mas Fashav e os irmãos conseguiram convencê-los, e eles buscaram refúgio em Nostromus. Na cidade, eles descobriram que não eram os únicos refugiados, e, para piorar, o governo alegava não ter condições de oferecer moradia a todos. Isso significava, no linguajar jurídico, que _Patrícios_ e _Médios_ teriam prioridade, e o _Proletariado_ que contasse com a sorte. Na prática, isso era o mesmo que dizer que o _Proletariado_ estava sem sorte.

De camponeses a moradores de rua na _Cidade Nova_, a família de Fashav tentava não perder as esperanças. A princípio, tentavam viver de bicos, mas logo começou a faltar não apenas esses trabalhos irregulares como víveres para serem comprados. Outros refugiados recorreram ao banditismo para tentar sobreviver. Temendo que essa fosse a desculpa perfeita para o governo impor medidas draconianas contra os refugiados, alguns começaram a formar grupos de defesa mútua, e Fashav se juntou a um deles.

Não demorou para que Fashav se tornasse uma das lideranças do grupo. Sua personalidade compassiva e protetora fez com que seu grupo se tornasse algo maior do que um simples esquema de defesa mútua. Ao proteger outras pessoas, essas pessoas passaram a se juntar ao grupo de Fashav, e logo ela se tornou uma voz que não poderia ser ignorada na cidade. Ela clama por melhores condições para os refugiados, a fim de que todos possam voltar para suas casas com dignidade, e exige que o governo tome providências. A princípio, ela não pensa em derrubar o governo, mas se a crise continuar, bem, situações desesperadas pedem medidas desesperadas...

#### A Floresta de Metal

**Aspectos**: _Intocado pelo clima e pelo tempo_; _Ruínas misteriosas e macabras_; _Carniçais e patrulhas de Lorde Guldbaard para todos os lados_

Nos _Altiplanos Cinzentos_, a alguns dias de viagem de Nostromus, fica a _Floresta de Metal_. Ela tem esse nome por causa dos pilares de metal dispostos em circunferências concêntricas ao redor de _A Necrópole_. Os pilares são feitos de um metal negro altamente reflexivo, e são lisos e brilhantes, como se houvesse sido feitos ontem, sem nenhuma marca ou inscrição ou gravação neles. Os pesquisadores de Nostromus não sabem qual é a utilidade dos pilares. As escavações mais profundas, que atingiram quase seiscentos metros de profundidade, não encontraram nada além dos pilares, que se tornam mais frios a medida que a profundidade aumenta.

Outro fator que marca a fronteira entre a _Floresta de Metal_ e o resto dos _Altiplanos Cinzentos_ é a ausência de alterações climáticas na _Floresta_. Há o dia e a noite e as consequentes alterações na temperatura, mas nada além disso. O ar é imóvel como em uma cripta, não chove, venta ou neva, e a terra, fria, parece não sentir nenhum toque, nenhum efeito climático, por eras.

Apesar disso, a _Floresta de Metal_ sempre foi um lugar muito frequentado. Quando os primeiros nostromenses se aventuraram para fora de sua cidade, eles descobriram _A Necrópole_ no meio da _Floresta_, e desde então existiu um trânsito regular de pessoas entre a cidade e a ruína. Com o tempo, os Doutores conseguiram que o governo proibisse o acesso de pessoas que não fossem um deles à _Necrópole_ e estacionasse soldados com ordens expressas para matar qualquer um que tentasse invadir o lugar – e proibissem que viessem recolher os corpos para os enterrar, o que conferia uma aparência macabra à entrada da _Necrópole_.

Isso mudou quando a crise com os carniçais impediu totalmente o acesso. Em um senso de ironia ou justiça poética, um dos primeiros lugares atacados pelos carniçais foram os arredores da _Necrópole_, forçando os Doutores e os soldados a fugir. Entretanto, como a princípio se julgou que os carniçais não se afastariam da ruína, o governo achou por bem não fazer nada, já que os Doutores asseguraram que poderiam manter os atendimentos mesmo sem acesso à _Necrópole_.

Os carniçais, porém, não se restringiram a assombrar a _Necrópole_. Na verdade, não demorou muito para que eles fossem para além da _Floresta de Metal_ e ocupassem todo os _Altiplanos Cinzentos_, forçando os moradores dos assentamentos tributários a Nostromus a buscarem refúgio dentro dos muros da cidade. Entretanto, o governo só se coçou para resolver o problema quando o fluxo de mercadores e de pacientes para os Doutores começou a ser afetado.

A coisa piorou, pelo menos de acordo com o ponto de vista do governo, quando o Barão Surocco de Jel'Thazzar enviou Lorde Guldbaard para atender aos pedidos de ajuda. Agora, não apenas os carniçais assolam a _Floresta de Metal_, como várias patrulhas de Lorde Guldbaard rondam o local visando abrir caminho até a _Necrópole_.

## Estatísticas para os Personagens do Narrador

### Carniçal

Uma pessoa sem mente devoradora de carne e parcialmente decomposta. Podem se assemelhar a qualquer bioforma, mas não se beneficiam de suas formas anteriores. Atacam ferozmente, sempre em bandos. Sua tática preferida envolve imobilizar uma vítima e então a devorar ainda viva.

**Aspectos**: _Necroide sem mente devorador de carne_; _Regido pela fome_; _Nunca está sozinho_.  
**_Razoável (+2) em_**: Morder, Segurar.  
**_Ruim (-2) em_**: Ações atléticas em geral, Defender.  
***Praticamente invulnerável a dano não táumico[^thaumic]***: Um carniçal recebe apenas 1 estresse quando sofre dano de fontes não táumicas.  
***Muitas mãos ao segurar***: Apesar de não ter mente, um carniçal é capaz de realizar a ação de _Trabalho em Equipe_ a outro carniçal que esteja tentando segurar alguém ou alguma coisa. Se ele o fizer, sua ação de _Trabalho em Equipe_ concede um bônus de +2 ao invés de +1.  
***Segurar e morder***: Uma vez por cena, na primeira vez que morder alguém que esteja sob o efeito de algum aspecto denotando restrição de movimentos, se o carniçal usar uma invocação gratuita ou invocar o aspecto em questão, ele recebe um bônus de +4 ao invés de +2.  
***Estresse***: [ 1 ] [ 1 ] [ 1 ]   

[^thaumic]: Tradução livre para o termo _thaumic_, que é como o autor de _Fate of Umdaar_ chamou as energias místicas do cenário.

### Soldado da Patrulha de Lorde Guldbaard

Estes são os soldados rasos das patrulhas de Lorde Guldbaard espalhadas pelos _Altiplanos Cinzentos_. Originalmente plebeus, camponeses ou simples cidadãos, foram forçados a servir no exército do Barão Surocco de Jel'Thazzar, e apenas a pura intimidação os mantém leais. Podem tentar fugir se tiverem certeza de que não haverá represálias contra eles.

**Aspectos**: _Recruta involuntário ressabiado e nervoso_; _Leal a Guldbaard por medo_.  
***Razoável (+2) em***: Lutar, Percepção.  
***Ruim (-2) em***: Vontade, Furtividade.  
***Estresse***: [ 1 ] [ 1 ]  

### Sargento da Patrulha de Lorde Guldbaard

Os sargentos são comandantes de pequenos destacamentos, como as patrulhas que Lorde Guldbaard espalhou pelos _Altiplanos Sombrios_. Diferentemente dos soldados, os sargentos são pessoas minimamente importantes em Jel'Thazzar que escolheram seguir carreira militar na esperança de atingir o oficialato e conseguirem cair nas graças do Barão Surocco.

**Aspectos**: _Sargento tão sádico quanto o Lorde a quem serve_; _Antes morrer a ser derrotado_.  
**Razoável (+2) em**: Provocar, Atirar.  
**Ruim (+2) em**: Comunicação.  
***Sargentão***: Pode usar _Provocar_ para fornecer bônus de +1 por _Trabalho em Equpe_ para os soldados de sua patrulha, desde que esteja intimidadoramente gritando ordens e comandos para eles.  
***Estresse***: [ 1 ] [ 1 ] [ 1 ]   
***Condição***: [ 1 ] Ferido [ 1 ] Desmoralizado   

### Lorde Guldbaard, o Conquistador

Lorde Guldbaard, o Conquistador é o subordinado mais leal do Barão Surocco de Jel'Thazzar, um dos Mestres de Umdaar da região. Pouco se sabe sobre suas origens, mas especula-se que ele era um gladiador que, por alguma razão, caiu nas boas graças do Barão. Sua ferocidade lhe deram muitas vitórias no campo de batalha, e sua lealdade garantiram-lhe um reconhecimento cada vez maior por parte do Barão. Guldbaard, porém, está satisfeito com sua posição, pois ela lhe permite lutar para sempre, e por isso não pensa em revoltar-se contra seu Mestre.

Fisicamente imponente, Guldbaard é um humano com quase dois metros de altura, extremamente musculoso e igualmente ágil. Seus cabelos grisalhos indicam que ele está pela casa dos quarenta anos. Sua voz é grave, o que, combinado ao seu olhar duro e seco e aos olhos azuis frios como aço, faz com que poucas pessoas se sintam confortáveis em sua presença. Seu corpo é coberto por cicatrizes, evidenciando um passado de lutas e vitórias.

Guldbaard é o portador de Kul'Zahar, o Arco do Demiurgo. Esta relíquia, recuperada de uma das muitas ruínas saqueadas pelo Barão Surocco, é capaz de imobilizar qualquer pessoa atingida por um de seus disparos de energia, e Guldbaard a usa com maestria durante um conflito.

| **Aspectos** | |
| ---: | :--- |
| ***Conceito*** |  _Explorador humano e leal tenente do Barão Surocco de Jel'Thazzar_ |
| ***Motivação*** | _Lutar pelo Barão significa poder lutar para sempre_ |
| **Relacionamento** | _Leal aos meus homens, intolerante com fracassos_ |
| ***Aspecto Livre*** | _Portador de Kul'Zahar, o Arco do Demiurgo_ |
| ***Aspecto Livre*** | _Recuar é sempre melhor que morrer_ |
| ***Aspecto Livre*** | _Presença realmente aterradora_ |

| **Perícias** | | | | | |
| ---: | :--- | :--- | :--- | :--- | :--- |
| ***Excepcional (+5)*** | Percepção | | | | |
| ***Ótimo (+4)*** | Atirar | Provocar | | | |
| ***Bom (+3)*** | Atletismo | Investigar | Furtividade | | |
| ***Razoável (+2)*** | Vigor | Vontade | Recursos | Roubo | |
| ***Regular (+1)*** | Lutar | Saberes | Comunicação | Condução | Contatos |

| **Façanhas** | |
| ---: | :--- |
| ***Kul'Zahar, o Arco do Demiurgo*** | Kul'Zahar é um arco relíquia do Demiurgo que dispara feixes de energia cuja frequência específica pode causar perda de controle muscular em bioformas não mecânicas. Quando um ataque realizado com Kul'Zahar causar pelo menos duas tensões de dano em um alvo cuja bioforma não seja inteiramente mecânica, é possível reduzir o dano causado em um para automaticamente aplicar o aspecto _Imobilizado_ no alvo do ataque. |
| ***Presença aterradora*** | Guldbaard recebe +2 em _Ações de Criar Vantagem_ por _Provocar_ caso ele esteja usando de intimidação no processo. |
| ***Disparo defensivo*** | Uma vez por rodada, Guldbaard pode usar _Atirar_ para realizar _Ações de Defender_, desde que ele ainda não tenha agido nesta rodada e abra mão de sua ação na rodada. Caso a defesa seja bem sucedida (isto é, a _Ação de Atacar_ do oponente tenha resultado em uma falha), o atacante sofre dano igual às tensões pelas quais a defesa foi bem sucedida. |
| ***Modus operandi*** | Ao analisar os planos ou local de trabalho de uma pessoa, mesmo se ela não estiver presente, uma vez por cena Guldbaard pode usar Investigar no lugar de Empatia para criar ou descobrir um aspecto da personalidade dessa pessoa. |

| **Estresses e Consequẽncias** | |
| ---: | :--- |
| ***Estresse físico*** | [ 1 ] [ 1 ] [ 1 ] [ 1 ] |
| ***Estresse mental*** | [ 1 ] [ 1 ] [ 1 ] [ 1 ] |
| ***Consequência suave (2)*** | |
| ***Consequência moderada (4)*** | |
| ***Consequência severa (6)*** | |

### Prefeito Gynfreth Moscathi de Nostromus

Gynfreth, o atual prefeito de Nostromus, é um centauro na casa dos cinquenta e muitos anos de idade. Seu rosto parece cansado e o vigor físico há algum tempo o abandonou, mas seus olhos ainda brilham com uma astúcia e sagacidade ímpares. Ele sempre usa roupas muito elegantes e desenhadas para ressaltar a sua importância política, e ele mantém sua cauda sempre bem limpa e aparada.

| **Aspectos** | |
| ---: | :--- |
| ***Conceito*** | Centauro (parte inferior de cavalo) ex-doutor tornado político defensor do _status quo_ |
| ***Motivação*** | PODER! |
| ***Origens*** | Em Nostromus, existem os poderosos... e o resto |
| ***Relacionamento*** | Entre Lorde Guldbaard e os arqueonautas, preferia escolher nenhum deles |
| ***Aspecto livre*** | Rei das tecnicidades políticas e legais |

| **Perícias** | | | | |
| ---: | :--- | :--- | :--- | :--- |
| ***Ótimo (+4)*** | Contatos | | | |
| ***Bom (+3)*** | Recursos | Enganar | | |
| ***Razoável (+2)*** | Provocar | Saberes | Vontade | |
| ***Regular (+1)*** | Empatia | Comunicação | Investigar | Percepção |

| **Façanhas** | |
| ---: | :--- |
| ***O jogo político*** | Gynfreth recebe +2 ao realizar _Ações de Criar Vantagem_ por _Contatos_ que visem colocar um aspecto de reputação negativa sobre uma pessoa ou um grupo, contanto que ele possa usar sua rede de contatos e informantes para espalhar os boatos. |
| ***Dane-se as regras, eu tenho dinheiro!*** | Gynfreth pode usar _Recursos_ para realizar _Ações de Defesa_ contra ataques que visem causar dano mental, contanto que sua riqueza pessoal possa factualmente ajudá-lo na defesa. |
| ***Você sabe com quem você está falando?*** | Gynfreth recebe +2 ao realizar _Ações de Criar Vantagem_ por _Provocar_, contanto que ele esteja usando seu poder político para intimidar o alvo. |

| **Estresses e Consequẽncias** | |
| ---: | :--- |
| ***Estresse físico*** | [ 1 ] [ 1 ] [ 1 ] |
| ***Estresse mental*** | [ 1 ] [ 1 ] [ 1 ] [ 1 ] |
| ***Consequência suave (2)*** | |
| ***Consequência moderada (4)*** | |
| ***Consequência severa (6)*** | |

### Fashav Sorridente

Fashav é uma sátira cujas partes superior é humana e inferior, gazela. Seus cabelos são verdes e os olhos, castanho-avermelhados, são maiores do que os olhos de um humano comum. Ela tem o corpo definido por anos de trabalho duro no campo, o torso um tanto compacto contrastando com as pernas suavemente curvas. Sua voz é suave, e ela está sempre sorrindo.

| **Aspectos** | |
| ---: | :--- |
| ***Conceito*** | Sátira camponesa tornada líder de milícia |
| ***Motivação*** | Eu quero apenas voltar para casa! |
| ***Origens*** | Eu devo tudo que sou ao trabalho no campo |
| ***Relacionamento*** | Enquanto não houver justiça para todos, não estará bom para ninguém |
| ***Aspecto Livre*** | Um sorriso é a maior arma e o melhor remédio |

| **Perícias** | | | | |
| ---: | :--- | :--- | :--- | :--- |
| ***Ótimo (+4)*** | Vigor | | | |
| ***Bom (+3)*** | Contatos | Atletismo | | |
| ***Razoável (+2)*** | Comunicação | Vontade | Lutar | |
| ***Regular (+1)*** | Ofícios | Empatia | Provocar | Roubo |

| **Façanhas** | |
| ---: | :--- |
| ***Eu conheço um cara que conhece um cara*** | Fashav recebe +2 em _Ações de Criar Vantagem_ por _Contatos_ que visem obter serviços ou favores de alguém. |
| ***Chamado às armas*** | Fashav recebe +2 em _Ações de Criar Vantagem_ por _Contatos_ se ela estiver tentando reunir um grupo de pessoas para algum propósito claramente definido para as pessoas que ela está tentando reunir. |
| ***Chave de braço*** | Sempre que Fashav atacar com Lutar, ela pode também causar dois estresses de dano mental adicionais. Se o oponente for nocauteado dessa forma, trate como se ao invés disso ele tivesse concedido. Este ataque é sempre não letal. |

| **Estresses e Consequẽncias** | |
| ---: | :--- |
| ***Estresse físico*** | [ 1 ] [ 1 ] [ 1 ] [ 1 ] [ 1 ] [ 1 ] |
| ***Estresse mental*** | [ 1 ] [ 1 ] [ 1 ] [ 1 ] |
| ***Consequência suave (2)*** | |
| ***Consequência moderada (4)*** | |
| ***Consequência severa (6)*** | |

