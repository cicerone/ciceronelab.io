---
layout: post
title: A Necrópole Amaldiçoada, uma aventura para Fate of Umdaar
lang: pt-BR
locale: pt_BR
date: TBD
tags: RPG Fate Evil-Hat Masters-of-Umdaar Fate-of-Umdaar-playtest Exemplos Aventuras
---

E em mais um escrito sobre o playtest de _Fate of Umdaar_, hoje trago a vocês a aventura _A Necrópole Amaldiçoada_. Eu pretendo narrá-la em breve, e assim que eu o tiver feito acrescentarei um subtítulo para agradecer os participantes.

## Introdução

Nostromus era uma pequena porém próspera comunidade localizada nos _Altiplanos Cinzentos_, um imenso planalto no qual a terra possui uma cor e composição muito próximas às da cinza de carne queimada. O clima é de extremos, com verões muito quentes e secos e invernos muito frios e úmidos, com estações intermediárias curtas entre elas. Apesar disso, os nostromenses conseguiam ter algum sucesso com a agricultura, valendo-se dos cereais nativos, cuja resistência é quase a das ervas daninhas, e com a pecuária, graças ao _gnutil_, o ruminante local e animal símbolo oficial da cidade.

Entretanto, a principal fonte de riqueza da cidade é o conhecimento médico. Pessoas de todos os cantos do continente viajam para Nostromus em busca de cura ou remédios para inúmeras mazelas, embora os Doutores, a casta social que detém esse conhecimento, atenda apenas àqueles que possam pagar por seu trabalho. Por essa razão, Nostromus mantém sua neutralidade, atendendo igualmente as _Terras Livres_[^freelands] e os _Domínios Sombrios_[^dread-domains] ao seu redor – ou, nas palavras de um ditado nostromense, _dinheiro é tudo igual, não importa a origem_. Dizem os rumores que todo esse saber foi obtido saqueando ruínas de Demiurgos, mas os Doutores não estão dispostos nem a negar, nem a confirmar os boatos.

[^freelands]: Tradução livre para _Freelands_.

[^dread-domains]: Tradução livre para _Dread Domains_.

Acontece, porém, que os boatos são verdadeiros. No meio dos _Altiplanos Cinzentos_ fica a _Floresta de Metal_, um complexo de Ruínas do Demiurgo formado por vários círculos concêntricos de obeliscos ou minaretes de metal e em cujo centro encontra-se uma torre gigantesca chamada de _A Necrópole_. Soldados nostromenses ficam de guarda o tempo todo, e apenas os Doutores podem entrar em _A Necrópole_. Como que para reforçar a proibição, a lei de Nostromus proíbe remover os cadáveres dos possíveis invasores, conferindo uma aparência sinistra para um lugar com um nome tão convidativo.

Totalmente lisa e sem janelas por fora, _A Necrópole_ possui doze andares acima do nível da superfície, e pelo menos cinco andares, a maioria deles inexplorados, abaixo. Os pesquisadores nostromenses nunca conseguiram decifrar qual era o propósito original da ruína, mas descobriram como recuperar algumas das informações entre as runas do Demiurgo. Além de descobrir que o local era usado para alguma forma de pesquisa médica, os pesquisadores recuperaram uma grande quantidade de conhecimento médico, que é usado pelos Doutores em Nostromus para exercer e desenvolver a medicina – enriquecendo no processo, é claro.

Nos últimos meses, porém, algo muito estranho aconteceu a Nostromus. Os _Altiplanos Cinzentos_ foram dominados por _carniçais_, monstros devoradores de carne que parecem pessoas parcialmente decompostas. A agricultura e a pecuária foram abandonadas, e os camponeses sobreviventes se refugiaram na cidade, tornando-se moradores de rua no processo. O comércio com outras comunidades declinou, pois são poucas as caravanas que têm disposição para atravessar os _Altiplanos Cinzentos_ atualmente. O caos e a pobreza dominaram a outrora Nostromus.

Em um primeiro esforço investigativo, as autoridades de Nostromus descobriram que havia uma grande possibilidade de os tais _carniçais_ estarem vindo de _A Necrópole_. Entretanto, as investigações tiveram de ser abandonadas porque cada vez havia menos pessoas capazes de proteger a cidade contra os números cada vez maiores de _carniçais_ infestando os _Altiplanos Cinzentos_. Desesperadas, as autoridades mandaram mensageiros para várias outras comunidades, tanto nas _Terras Livres_ quanto nos _Domínios Sombrios_.

O primeiro a responder foi o _Barão Surocco de Jel'Thazzar_, um dos mais poderosos Mestres de Umdaar na região. Cobiçando para si os segredos que os nostromenses desencavaram de _A Necrópole_, ele ofereceu ajuda à cidade em troca de transformá-la em um protetorado. Sem aguardar resposta, ele enviou um destacamento de soldados talentosos, comandados por seu mais leal subordinado, _Lorde Guldbaard, o Conquistador_, pronto para eliminar todos os _carniçais_ e conquistar Nostromus para a glória do seu Mestre.

Envoltas com seus próprios problemas, as _Terras Livres_ não puderam enviar um grupamento de soldados para ajudar Nostromus. Entretanto, elas compensaram na qualidade, pois formaram uma equipe com seus melhores _Arqueonautas_ para resolver o mistério dos _carniçais_ e enfrentar os homens de _Lorde Guldbaard, o Conquistador_. Eles terão de cruzar os _Altiplanos Cinzentos_, enfrentar tanto os _carniçais_ quanto os soldados de Guldbaard, e descobrir o que está acontecendo antes que seja tarde demais e Nostromus ou seja devorada pelos _carniçais_, ou seja conquistada por um dos Mestres de Umdaar.

E então, valentes campeões, vocês estão prontos para deter mais uma das maquinações dos... **MESTRES DE UMDAAR**??

### Sobre a aventura

A aventura _A Necrópole Amaldiçoada_ foi escrita tendo em vista os materiais então disponíveis sobre o playtest de _Fate of Umdaar_. Ao longo do seu texto, ela fará muitas menções a esse livro, e é bastante importante ressaltar que ele não está em um estado finalizado ainda. Isso significa que muita coisa pode mudar no _Fate of Umdaar_, e consequentemente esta aventura pode não ser exatamente compatível com a versão final do livro. Recomenda-se cautela ao leitor.

Da mesma maneira, apesar de a aventura ter sido escrita tendo o _Fate of Umdaar_ em mente, não será nada difícil adaptá-la para qualquer outro mundo ou cenário. Sinta-se à vontade para usar entre 0% e 100% deste texto em seu jogo, lembrando-se de me dar os devidos créditos caso seu texto venha a ser publicado em algum lugar. Para maiores orientações sobre como me dar os créditos, leia em [Sobre](https://cicerone.gitlab.io/about).


## Resumo da aventura

1. Os campeões chegam a Nostromus e são recebidos pelo Prefeito Gynfreth Moscathi, que os põe à par dos acontecimentos;
    1. [OPCIONAL]: Os campeões fazem perguntas aos camponeses refugiados na cidade, e acabam ouvindo de Fashav as partes que o Prefeito omitiu;
1. Os campeões cruzam os _Altiplanos Cinzentos_ e a _Floresta de Metal_, tendo que lidar com as alterações climáticas, com os _carniçais_ e com patrulhas de Lorde Guldbaard;
1. Os campeões entram na _Necrópole_;
    1. [OPCIONAL]: Explorações nos andares superiores revelam documentos dos Doutores e pontos de acesso ao banco de dados da ruína;
1. Ao descer para os andares no subsolo, os campeões descobrem o que aconteceu na ruína: o sistema de clonagem saiu do controle e começou a liberar clones de batalha incompletos;
1. Os campeões resolvem o problema com o sistema de clonagem.

## Cenas da aventura

### Nostromus

**Aspectos**: _Importante centro médico baseado em segredos do Demiurgo_; _Crise de refugiados_; _Sitiada por carniçais, e Lorde Guldbaard é o urubu que aguarda esta carniça_.  
**Valores**: _Altruísmo_ vs. _Individualidade_ (priorizando _Individualidade_).  

Nostromus é uma pequena, porém próspera comunidade situada nos _Altiplanos Cinzentos_. Sua população total, que inclui tanto os camponeses dos assentamentos tributários a Nostromus quanto os moradores da cidade em si, historicamente oscila entre dez e doze mil pessoas, a maioria humanos, mas praticamente toda bioforma minimamente comum pode ser encontrada na cidade. Os principais negócios da cidade são os serviços médicos, e antes da crise com os _carniçais_ vinha gente de toda Umdaar atrás dos tratamentos que só poderiam ser encontrados em Nostromus.

Com a crise dos _carniçais_, a situação mudou totalmente de figura. Os camponeses dos assentamentos tributários a Nostromus vieram em massa se refugiar na cidade, e o governo simplesmente não conseguiu assentar todo mundo adequadamente. O comércio e o fluxo de pessoas em busca de tratamento médico praticamente cessou, todos com medo de cruzar os _Altiplanos_ e serem dizimados ou por _carniçais_, ou por patrulhas de Lorde Guldbaard. Os mercadores e médicos de Nostromus pressionam o governo por uma solução, e os refugiados se veem cada vez mais relegados ao segundo plano enquanto o governo prioriza os ricos e poderosos da cidade.

Para forasteiros, como os campeões, a crise é evidente e inegável. Muitos refugiados tiveram que virar moradores de rua, e não poucos passaram a recorrer à criminalidade para manter-se alimentados. O _Largo dos Doutores_, o distrito residencial dos ricos e abastados, mantém seus portões convenientemente fechados, sobrecarregando ainda mais a _Praça do Mercado_, a _Cidade Nova_ e a _Cidade Velha_, os demais distritos, com a crise. O clima geral na cidade é o _cada um por si_. A _Crise de refugiados_ é tão evidente que não seria preciso solicitar uma rolagem de dados para notar a existência do aspecto.

#### Encontro com o Prefeito Gynfreth Moscathi

Ao chegarem na cidade, os campeões são encaminhados ao Palácio do Prefeito, na Cidade Velha. Lá, serão recebidos pelo Prefeito Gynfreth Moscathi em pessoa, e serão tratados como hóspedes de honra. O Prefeito pessoalmente contará a eles o motivo do pedido de ajuda. Alguma coisa aconteceu na _Necrópole_ que infestou a região com carniçais, e homens do Barão Surocco de Jel'Thazzar, um Mestre de Umdaar, estão rondeado a região visando tomar para si tanto a ruína quanto a própria Nostromus. Caso os jogadores peçam algum teste, faça-os rolar _Empatia_ contra o _Enganar_ de Gynfreth. Em caso de sucesso, eles notarão que o prefeito parece bem nervoso, como se estivesse escondendo ou temendo algo. Um sucesso com estilo dá a certeza de que ele está escondendo algo, gerando o aspecto _Segredos do Prefeito_.

Obviamente, ele não vai contar _tudo_ que está acontecendo. Ele não mencionará que o pedido de ajuda foi mandado também para o Barão Surocco, tampouco falará sobre a crise de refugiados na cidade. Entretanto, não será possível persuadi-lo a falar mais do que ele já falou, não importa o quanto os campeões tentem – ele é um político consumado, os campeões, não. Ele evitará esses dois tópicos o quanto for possível, e dispensará os campeões dizendo que precisa tratar de outros assuntos com outras pessoas caso eles insistam demais no assunto. Será preciso descobrir de outra forma.

##### Prefeito Gynfreth Moscathi (ele/dele)

Gynfreth é um centauro, meio humano, meio cavalo, com quase sessenta anos de idade. Os cabelos na parte humana já estão totalmente brancos, enquanto os pelos na parte equina são de vários tons de marrom, como se estivessem manchados. Ele se veste finamente, buscando ao mesmo tempo enfatizar seu poder político e esconder que o vigor e o viço da juventude já lhe abandonaram faz tempo. Seu rosto é marcado por rugas, e suas mãos tremem levemente. Apenas seus olhos permanecem vívidos e poderosos, um sutil lembrete do quão astuta, sagaz e ardilosa é a mente por trás da aquela aparência de velhice.

| **Aspectos** | |
| ---: | :--- |
| ***Conceito*** | _Ardiloso político centauro_ |
| ***Motivação*** | _PODER!_ |
| ***Relacionamento*** | _Serviçal dos poderosos de Nostromus_ |
| ***Origens*** | _Nostromus: terra dos que tem... e dos que não são nada_ |

| **Perícias** | | | | |
| ---: | :--- | :--- | :--- | :--- |
| ***Ótimo (+4)*** | Enganar | | | |
| ***Bom (+3)*** | Contatos | Recursos | | |
| ***Razoável (+2)*** | Provocar | Saberes | Comunicação | |
| ***Regular (+1)*** | Vontade | Empatia | Investigação | Percepção |

| **Façanhas** | |
| ---: | :--- |
| ***Não vamos mais falar sobre isso*** | Uma vez por sessão, Gynfreth pode simplesmente sair de um conflito exclusivamente mental sem precisar conceder ou nocautear seus oponentes se ele puder usar seu poder de alguma forma para encerrar o assunto. Ele não recebe Pontos de Destino por consequências que tenha sofrido durante o conflito do qual ele saia usando esta façanha. |
| ***Sorriso de político, ouvidos de mercador*** | Uma vez por cena, Gynfreth pode gastar um Ponto de Destino para automaticamente criar uma vantagem que envolva dissimulação ou falsidade em geral. Caso alguém tente realizar uma _Ação de Superar_ para superar essa vantagem, use o valor de _Enganar_ de Gynfreth como dificuldade passiva. |

| **Estresses e consequências** | |
| ---: | :--- |
| ***Estresse físico*** | [ 1 ] [ 1 ] [ 1 ] |
| ***Estresse mental*** | [ 1 ] [ 1 ] [ 1 ] [ 1 ] |
| ***Consequência suave (2)*** | |
| ***Consequências moderada (4)*** | |
| ***Consequência severa (6)*** | |


#### OPCIONAL: encontro com Fashav Sorridente

Caso os campeões queiram descobrir o que está acontecendo, será preciso procurar quem esteja disposto a falar com eles. Essa será uma _Ação de Superar_ por _Contatos_ contra dificuldade +4, ou +2 se algum dos campeões for nativo de Nostromus. O aspecto _Segredos do Prefeito_ pode ser invocado nesta rolagem, caso ele tenha sido criado. Em qualquer resultado, eles encontrarão Fashav. Em caso de falha na rolagem dos campeões, ela será abertamente hostil, acreditando que os campeões são, na verdade, agentes do Prefeito, e os campeões terão sobre si o aspecto _Malditos agentes de Gynfreth!_, com uma invocação gratuita nesse aspecto para Fashav usar. Em caso de empate, Fashav estará desconfiada, mas ainda assim disposta a ouvir. Em um sucesso com estilo, Fashav saberá, de algum modo, que os personagens são _Campeões de Umdaar_, com uma invocação gratuita nesse aspecto para os campeões, e confiará neles automaticamente.

Em caso de empate ou falha na rolagem de _Contatos_, Fashav ainda precisará ser persuadida a falar. Determine as rolagens de acordo com os métodos escolhidos pelos campeões. Em caso de falha ou empate, Fashav ainda contará o que sabe, conforme descrito a seguir, mas os campeões não conseguirão obter nenhum benefício de seus relatos. Em caso de sucesso, o relato de Fashav criará o aspecto _Preparados contra os carniçais_, com uma invocação gratuita para os campeões, e em caso de sucesso com estilo será criado adicionalmente o aspecto _Preparado contra os homens de Lorde Guldbaard_, com uma invocação gratuita para os campeões.

Em caso de sucesso na rolagem de _Contatos_, Fashav não precisará ser persuadida a falar. Seu relato amistoso, porém, ainda precisará ser transformado em algo útil pelos personagens por meio de uma _Ação de Criar Vantagem_ por alguma perícia apropriada a critério dos campeões. Cada campeão poderá tentar até duas _Ações de Criar Vantagem_ com as perícias que preferir, e os aspectos criados deverão refletir a perícia usada na rolagem.

Em todo caso, os campeões encontrarão Fashav. Em caso de empate ou falha na rolagem de _Contatos_, eles serão conduzidos até ela por um grupo de pessoas com cara de poucos amigos. Em caso de sucesso, ela se apresentará diretamente aos campeões. De qualquer modo, Fashav (ou seus capangas) levará o grupo até uma taverna, onde sentará e conversará com eles. Ela contará aos campeões o que Gynfreth não contou. Ela falará da crise dos refugiados, sobre como os camponeses das comunidades tributárias de Nostromus foram expulsos de suas terras por causa da infestação de carniçais e dos homens de Guldbaard. Ela revelará ser uma desses camponeses, e que, assim como muitos outros, ela se juntou a um grupo de defesa mútua para evitar predação por outros poderes na cidade, já que o governo os deixou ao deus-dará. Ela enfatizará como o governo tem ajudado apenas os ricos e poderosos de Nostromus e deixado os pobres a ver navios. Ela revelará que o Prefeito enviou um pedido de ajuda ao Barão Jel'Thazzar, e é por causa desse pedido de ajuda que Lorde Guldbaard e seus homens estão espalhados pelos _Altiplanos Cinzentos_. Ela contará relatos sobre encontros com _carniçais_ e patrulhas de Guldbaard, explicando como elas parecem agir – mas sem o detalhamento e precisão com as quais os campeões estão acostumados, já que ela é uma camponesa, não uma guerreira.

##### Fashav Sorridente (ela/dela)

Fashav é uma sátira cujas partes superior é humana e inferior, gazela. Seus cabelos são verdes e os olhos, castanho-avermelhados, são maiores do que os olhos de um humano comum. Ela tem o corpo definido por anos de trabalho duro no campo, o torso um tanto compacto contrastando com as pernas suavemente curvas. Sua voz é suave, e ela está sempre sorrindo.

| **Aspectos** | |
| ---: | :--- |
| ***Conceito*** | Sátira camponesa líder de grupo de defesa mútua de refugiados |
| ***Motivação*** | Eu quero dignidade e voltar para casa |
| ***Origens*** | Devo tudo o que sou ao trabalho no campo |
| ***Relacionamento*** | Nós, camponeses, estamos todos juntos nessa |

| **Perícias** | | | | |
| ---: | :--- | :--- | :--- | :--- |
| ***Ótimo (+4)*** | Vigor | | | |
| ***Bom (+3)*** | Contatos | Comunicação | | |
| ***Razoável (+2)*** | Provocar | Ofícios | Empatia | |
| ***Regular (+1)*** | Vontade | Saberes | Investigação | Percepção |

| **Façanhas** | |
| ---: | :--- |
| ***Assustadora como um touro*** | Apesar de ser uma sátira meio gazela, Fashav sabe usar muito bem seu tamanho para se impor. Use _Vigor_ ao invés de _Provocar_ para ações relacionadas a intimidar pessoas. |
| ***Teimosa como um touro*** | Use _Vigor_ ao invés de _Vontade_ para determinar estresses e consequências adicionais. |

| **Estresses e consequências** | |
| ---: | :--- |
| ***Estresse físico*** | [ 1 ] [ 1 ] [ 1 ] [ 1 ] [ 1 ] [ 1 ] |
| ***Estresse mental*** | [ 1 ] [ 1 ] [ 1 ] [ 1 ] [ 1 ] [ 1 ] |
| ***Consequência suave (2)*** | |
| ***Consequências moderada (4)*** | |
| ***Consequência severa (6)*** | |

### Os Altiplanos Cinzentos

**Aspectos**: _Clima caótico e inclemente_; _Presença de carniçais_; _Patrulhas de Lorde Gundbaard_.

Munidos com essas informações, permita aos campeões fazer preparativos adicionais antes de partirem rumo à _Necrópole_. Porém, eles precisarão atravessar primeiro os _Altiplanos Cinzentos_.

