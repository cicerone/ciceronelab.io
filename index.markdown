---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
list_title: Postagens
---

Esta é uma lista cronológica de todas as postagens feitas na _Biblioteca do Cicerone_. Para listar as postagens separadas por tags, clique em [Postagens por tags](https://cicerone.gitlab.io/tags/).

Caso você queira contribuir com o meu trabalho me pagando um cafezinho, você pode me mandar um Pix.


<details>
  <summary markdown="span">Clique aqui para ver minha chave Pix</summary>
  
  lu.cicerone.cavalheiro@gmail.com
</details>

---

