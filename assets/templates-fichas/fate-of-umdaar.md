**Nome**: (pronomes)  
**Recarga**:

| **Aspectos** | |
| ---: | :--- |
| ***Conceito***| |
| ***Motivação*** | |
| ***Origens*** | |
| ***Relacionamento*** | |
| ***Aspecto Livre*** | |

| **Perícias** | | | | |
| ---: | :--- | :--- | :--- | :--- |
| ***Ótimo (+4)*** | | | | |
| ***Bom (+3)*** | | | | |
| ***Razoável (+2)*** | | | | |
| ***Regular (+1)*** | | | | |

| **Façanhas** | |
| ---: | :--- |
| | |

| **Estresses e Consequências** | |
| ---: | :--- |
| ***Estresse Físico*** | [ 1 ] [ 1 ] [ 1 ] |
| ***Estresse Mental*** | [ 1 ] [ 1 ] [ 1 ] |
| ***Consequência Suave (2)*** | |
| ***Consequência Moderada (4)*** | |
| ***Consequência Severa (6)*** | |

