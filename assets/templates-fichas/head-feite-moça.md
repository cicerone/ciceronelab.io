---
layout: post
title: ": um personagem para Fate Condensado"
lang: pt-BR
date: 2022-06-22 22:20:00 -0300
tags: RPG Fate Evil-Hat Fate-Condensado Personagens Exemplo CC-BY-SA-4.0-Internacional
---

Sejam bem-vindos a mais uma postagem aqui no site! Hoje eu dei uma pausa no processo de criação de jogos e aventuras para postar um personagem para Fate Condensado, a fim de que vocês possam usá-los à vontade.

Diferente das outras paradas postadas aqui no site, este personagem está licenciado sob **Licença Creative Commons Atribuição-CompartilhaIgual 4.0 Internacional**, cujo resumo em português para leigos pode ser lido em <https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR>


