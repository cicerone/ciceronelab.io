# A Biblioteca do Cicerone

## Sobre o site

A _Biblioteca do Cicerone_ é um espaço para publicação de textos, a maioria deles, provavelmente, sobre RPG, de Lu _Cicerone_ Cavalheiro. Apesar disso, na _Biblioteca_ será possível encontrar materiais menos lúdicos, por assim dizer, como política, filosofia, e reflexões sobre a vida, o universo e tudo o mais que não terão um simples número como resposta. Dessa forma, é válido dizer que a _Biblioteca_ é menos um arquivo e mais um _diário do autor_. 

## Sobre o autor

Lu _Cicerone_ Cavalheiro nasceu em São Paulo/SP, mas atualmente mora em Duque de Caxias/RJ com a mãe, esposa e filhos felinos. Aficcionado pelo sistema Fate de RPG, publicado originalmente pela [Evil Hat Productions](https://www.evilhat.com), conta com quase vinte anos de experiência em RPG, tendo jogado ou narrado incontáveis sistemas nesse meio tempo. Por questões de vida acontecendo, nunca chegou a publicar nenhuma aventura ou cenário autoral, mas pretende um dia corrigir isso.

Para a comunidade Fate, Cicerone é um dos apresentadores do _podcast_ [Fate Masters](https://fatemasters.gitlab.io), onde surgiu seu nome artístico, e um dos administradores do servidor [Movimento Fate Brasil](https://discord.gg/hyAbuThFQd) no Discord. Além disso, fez parte da equipe de tradução comunitária do Fate Condensado, uma iniciativa da [Ink Head Publishing](https://www.facebook.com/inkheadpublishing/), e é um defensor feroz do [Fate SRD Brasil](https://fatesrdbrasil.gitlab.io/).

## Ferramentas e licenças usadas na construção do site

Este site está hospedado no [GitLab](https://gitlab.com). Ele usa o [Jekyll](https://jekyllrb.com) como gerador de conteúdo estático e o tema [Minima](https://github.com/jekyll/minima)

Todos os textos publicados neste site estão sob **Licença Creative Commons Atribuição 4.0 Internacional**, salvo se explicitamente indicado o contrário. Para ler um resumo para leigos da licença, [clique aqui](https://creativecommons.org/licenses/by/4.0/deed.pt_BR).
