---
layout: page
title: Publicações
permalink: /publicacoes/
---

Nesta página, você terá acesso a todos as minhas publicações, bem como aos materiais que revisei, editei ou diagramei e o autor me deu permissão para divulgar aqui. Você pode contribuir com o autor (seja eu, seja o outro) enviando um Pix diretamente para ele, que será informado junto com a descrição e o link de download da publicação.

Caso você goste de alguma de minhas publicações, queira e possa contribuir com meu cafezinho, ficarei imensamente grata. Você pode fazer a contribuição via Pix.

<details>
  <summary markdown="span">Clique aqui para ver minha chave Pix</summary>
  
  f7a8496b-9aae-4378-9a6b-267b3adaf27c
  (chave aleatória)
</details>

1. Índice
{:toc}

# Publicações 2023

## RPG

### Jogos Minimalistas ou Solo

#### Quem é o Traidor?

![quem-e-o-traidor](/assets/Publicações/QeoT-1.jpg)

Em **Quem é o Traidor?**, atos terroristas incitados pelo candidato à presidência derrotado nas urnas começam a acontecer em todo o País após a posse do Presidente eleito. Ele precisará se reunir com seus Ministros para resolver os problemas, só que o Presidente não sabe que entre eles existe em segredo um apoiador do candidato derrotado, que fará tudo para minar os esforços de combate ao terrorismo.

Jogo inspirado nos atos de terrorismo bolsonarista acontecidos em 08/01/2023 em Brasília/DF.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/Quem%20%C3%A9%20o%20traidor%3F/Quem-%C3%A9-o-traidor.pdf?inline=false)

#### Miau! Hoje é Sexta-Feira 13!

![miau](/assets/Publicações/MHSF13.jpg)

Em **Miau! Hoje é Sexta-Feira 13!**, você é um gato preto que tem liberdade para vaguear pelas ruas. Gatos são animais muito inteligentes, e você não é uma exceção. Você sabe que hoje é um dia perigoso: sexta-feira 13. Humanos têm crenças estapafúrdias sobre esse dia, e você sabe que parte dessas crenças pode colocar você em risco. Durante o dia de hoje, você passará por alguns eventos, e terá que contar com sua sorte e habilidade para evitar que o pior aconteça.

**Miau! Hoje é Sexta-Feira 13!** é um jogo solo baseado nas superstições relacionadas a gatos pretos e sextas-feiras 13, bem como o risco que esses animaizinhos inocentes correm nesses dias. Em nenhum sentido este jogo é um endosso a tais crenças ou práticas – muito pelo contrário: é uma crítica e um alerta. Se você possui um gatinho preto e o permite passear na rua, tome bastante cuidado nesses dias, pois pode ser que seu filhote peludo não volte.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/Jogos%20Solo/Miau!%20Hoje%20%C3%A9%20Sexta-Feira%2013!/Miau-hoje-e-sexta-feira-13.pdf?inline=false)


# Publicações 2022

## Prosa

### A Conspiração das Moedas

A _Conspiração das Moedas_ é uma série de contos e livros ambientados em um cenário de fantasia árabe pré-islâmica. O protagonista, Rashid al-Samet, é um _Mercador_ que repentinamente se vê envolvido em uma investigação que pode trazer a tona coisas do seu passado que ele fazia de tudo para tentar deixar para trás...

#### Parte I: As três faces de uma mesma moeda

![capa-as-tres-faces-de-uma-mesma-moeda](/assets/Publicações/as-tres-faces-da-mesma-moeda.jpg)

Rashid al-Samet é um comerciante e dono da _Rota da Seda Importações e exportações_, um importante entreposto comercial em Acheon, capital do _Sultanato de Acheon_. Boêmio inveterado, adora passar suas noites nos _Mundos Distantes_, cabaré de sua amiga íntima, a Cortesã Delia Surridge. Entretanto, ao ganhar uma estranha moeda de um apostador, ele se vê enfiado em um mistério gigantesco, capaz de revelar partes do seu próprio passado que ele adoraria esquecer.

**Parte I: As três faces de uma mesma moeda** é o primeiro da série **A Conspiração das Moedas**, uma sequência de histórias que revelarão como Rashid lidará com o tal mistério e com os segredos de seu passado.

Conto não recomendado para menores de 14 anos, por retratar consumo explícito de álcool e tabaco, prática de jogos de azar e violência implícita e explícita.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/Literatura/Prosa/A%20Conspira%C3%A7%C3%A3o%20das%20Moedas/As%20tr%C3%AAs%20faces%20de%20uma%20mesma%20moeda/As-tr%C3%AAs-faces-de-uma-mesma-moeda.pdf?inline=false)

#### Parte II: A Dama de Mil Faces

![capa-a-dama-de-mil-faces](/assets/Publicações/DMF-01.jpg)

Após descobrir que existe uma conspiração acontecendo em Acheon, Rashid al-Samet é envolvido em uma investigação menor dos _Ouvidos do Sultão_, a polícia secreta do _Sultanato de Acheon_. Uma ladra, apelidada de _A Dama de Mil Faces_, tem feito furtos mirabolantes, mas ninguém conseguiu pegá-la até agora. Rashid precisa resolver esse mistério antes que o pânico se espalhe na cidade e o comércio seja prejudicado pelas ações da meliante.

**Parte II: A Dama de Mil Faces** é o segundo da série **A Conspiração das Moedas**, uma sequência de histórias que revelarão como Rashid lidará com o tal mistério e com os segredos de seu passado.

Conto não recomendado para menores de 14 anos, por retratar consumo explícito de álcool e tabaco, prática de jogos de azar e violência implícita e explícita.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/Literatura/Prosa/A%20Conspira%C3%A7%C3%A3o%20das%20Moedas/A%20dama%20de%20mil%20faces/A-Dama-de-Mil-Faces.pdf?inline=false)

#### Parte III: O Escambo de Isfahan

![capa-o-escambo-de-isfahan](/assets/Publicações/OEI-01.jpg)

Minu Istari, uma jovem _Ouvido do Sultão_ apaixonada por seu comandante, Rashid al-Samet, está toda boba e feliz pelo fato de que ele irá passar uns dias na casa dela, em Isfahan. Mal sabe ela que Rashid já está na cidade, tentando localizar por conta própria um mercado de escravos ilegais. Apesar de ser contrário à existência da escravidão, ela é permitida em certos termos no _Sultanato de Acheon_, e por isso ele precisa restringir suas batidas extra-oficiais.

O que Rashid não sabe, porém, é que ele vai se encontrar com muito mais do que isso enquanto procura o tal mercado de escravos ilegais...

**Parte III: O Escambo de Isfahan** é o terceiro da série **A Conspiração das Moedas**, uma sequência de histórias que revelarão como Rashid lidará com o tal mistério e com os segredos de seu passado.

Conto não recomendado para menores de 18 anos, por retratar consumo explícito de álcool e tabaco, prática de jogos de azar, violência implícita e explícita, escravidão e assédio sexual contra menores de idade.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/Literatura/Prosa/A%20Conspira%C3%A7%C3%A3o%20das%20Moedas/O%20Escambo%20de%20Isfahan/O-Escambo-de-Isfahan.pdf?inline=false)

#### Parte IV: O Homem da Embaixadora

![capa-o-homem-da-embaixadora](/assets/Publicações/OHE-01.jpg)

Por alguma razão misteriosa, o Emir Hassain al-Basham decidiu enviar uma embaixadora para a corte do Sultão de Acheon. O Emir Khalid chamou seu homem de confiança, Rashid al-Samet, para descobrir as razões por trás dessa história toda. Ao lado de Fátima, sua pupila, ele acabou descobrindo algo perturbador.

**Parte IV: O Homem da Embaixadora** é o quarto da série **A Conspiração das Moedas**, uma sequência de histórias que revelarão como Rashid lidará com o tal mistério e com os segredos de seu passado.

Conto não recomendado para menores de 14 anos, por retratar consumo explícito de álcool e tabaco, prática de jogos de azar e violência implícita e explícita.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/Literatura/Prosa/A%20Conspira%C3%A7%C3%A3o%20das%20Moedas/O%20homem%20da%20embaixadora/O-Homem-da-Embaixadora.pdf?inline=false)

#### Parte V: Um crepúsculo em Ormen

![capa-um-crepusculo-em-ormen](/assets/Publicações/UCO-01.jpg)

Uma vez por ano, Rashid some misteriosamente de Acheon. Poucos sabem para onde ele vai, e ao retornar ele nada fala sobre onde estivera. Este ano não será diferente: Rashid partirá mais uma vez na calada da noite para seu destino secreto. O que ele não conta é que, desta vez, ele será confrontado com memórias bem dolorosas de seu passado...

**Parte V: Um crepúsculo em Ormen** é o quinto da série **A Conspiração das Moedas**, uma sequência de histórias que revelarão como Rashid lidará com a tal conspiração e com os segredos de seu passado.

Conto não recomendado para menores de 14 anos, por retratar mutilação implícita e violência implícita e explícita.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/Literatura/Prosa/A%20Conspira%C3%A7%C3%A3o%20das%20Moedas/Um%20crep%C3%BAsculo%20em%20Ormen/Um-crep%C3%BAsculo-em-Ormen.pdf?inline=false)

### A Última Noite

![capa-a-última-noite](/assets/Publicações/AUN-01.jpg)

Após dez anos de uma invasão alienígena, o que sobrou da humanidade escondia-se em catacumbas e esgotos. Entre eles, Sebastião José, um dos veteranos e melhores operativos da resistência de guerrilha, ainda não havia perdido a esperança de encontrar sua amada, Aurora. Uma missão na véspera de Natal, e tudo isso mudaria...

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/Literatura/Prosa/A%20%C3%BAltima%20noite/A-%C3%9Altima-Noite.pdf?inline=false)

### O Fetiche

![capa-o-fetiche](/assets/Publicações/OFE-01.jpg)

Um trisal resolve realizar um dos fetiches de um deles: fazer sexo em um cemitério. Porém, eles não tinham como imaginar o _quão_ errado isso poderia dar...

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/Literatura/Prosa/O%20Fetiche/O-Fetiche.pdf?inline=false)

### Contos publicados pela _Andross Editora_

Os contos a seguir tiveram publicação física pela _Andross Editora_. De acordo com o edital de publicação, os contos deveriam ter oito mil toques ou menos. Entretanto, de acordo com o contrato de publicação, eu possuo o direito para os republicar como bem entender, então ei-los aqui também.

#### A Dama de Mil Faces (versão _Fogo de Prometeu_)

![capa-a-dama-de-mil-faces-fogo-prometeu](/assets/Publicações/DMF-Andross.jpg)

Uma ladra misteriosa tem aterrorizado a Praça do Mercado, em Acheon, e os _Ouvidos do Sultão_, os melhores agentes de Sua Majestade, não conseguem descobrir quem ela é. Será que Rashid e Fátima, dois dos melhores _Ouvidos_, serão capazes de descobrir quem é a _A Dama de Mil Faces_?

Conto não recomendado para menores de 14 anos, por retratar o consumo explícito de álcool e tabaco, bem como violência implícita. Conto originalmente publicado na coletânea _Fogo de Prometeu_

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/Literatura/Prosa/Andross/A%20Dama%20de%20Mil%20Faces/A-dama-de-mil-faces.pdf?inline=false)

#### As crianças do Pandiá Calógeras

![capa-as-crianças-do-pandiá-calógeras](/assets/Publicações/ACPC.jpg)

Em sua infância, Sílvia foi aluna da Escola Municipal Pandiá Calógeras, em São Paulo/SP. Mas sempre que ela passa pela porta do lugar, ela tem uma sensação estranha... uma memória... um _medo_.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/Literatura/Prosa/Andross/As%20crian%C3%A7as%20do%20Pandi%C3%A1%20Cal%C3%B3geras/As-crian%C3%A7as-do-Pandi%C3%A1-Cal%C3%B3geras.pdf?inline=false)

#### O Monolito Fálico

![capa-o-monolito-falico](/assets/Publicações/MF-1.jpg)

Poucas coisas podem ser tão idílicas quanto uma viagem de navio. Entretanto, como a protagonista nos conta, tudo pode dar errado quando pessoas comuns esbarram em coisas que a mente humana jamais conseguirá compreender.

Conto não recomendado para menores de 18 anos.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/Literatura/Prosa/Andross/O%20Monolito%20F%C3%A1lico/O-monolito-f%C3%A1lico.pdf?inline=false)

#### O último abraço

![capa-o-ultimo-abraço](/assets/Publicações/OUN-1.jpg)

Em um futuro distante, descobriram como reanimar cadáveres para realizar funções simples. Não demorou muito para criarem um mercado de _sexydolls_, cadáveres reanimados para serem usados no lugar de bonecas infláveis, e isso se tornou um negócio muito lucrativo. Porém, o dono de umas das empresas que trabalha com isso descobriu o quão doloroso pode ser um último abraço...

Conto não recomendado para menores de 18 anos, por retratar implicitamente violação de cadáveres e necrofilia.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/Literatura/Prosa/Andross/O%20%C3%BAltimo%20abra%C3%A7o/O-%C3%BAltimo-abra%C3%A7o.pdf?inline=false)

#### A Máquina Windrush: um conto de investigação em uma Inglaterra vitoriana steampunk

![capa-maquina-windrush](/assets/Publicações/AMW.jpg)

Victoria Lovegrove é uma detetive particular em plena Londres vitoriana. Seu ofício não lhe rendia muito – seu escritório fica na rua logo atrás de Baker Streeet 222B –, mas era o suficiente para pagar as contas e manter sua vida interessante – especialmente quando seu vizinho famoso estava prestando alguma consultoria para a Scotland Yard.

Certo dia, ela estava prestes a encerrar o expediente quando um homem misterioso entrou em seu escritório. Mal sabia ela que se envolveria em uma investigação muito interessante...

**A Máquina Windrush: um conto de investigação em uma Inglaterra vitoriana steampunk** foi publicado originalmente na coletânea **Engrenagens: contos com temática steampunk**, organizada por Paola Giometti e publicada pela Andross Editora em 2017.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/Literatura/Prosa/Andross/A%20M%C3%A1quina%20Windrush/A-maquina-Windrush.pdf?inline=false)


#### Onde tudo começou: Uma pequena coletânea dos meus contos publicados em antologias físicas

![onde-tudo-comecou](/assets/Publicações/OTC.jpg)

**Onde tudo começou: Uma pequena coletânea dos meus contos publicados em antologias físicas** reúne os cinco contos que publiquei pela Andross Editora em um único livro. Decidi prestar essa homenagem a mim mesma, já que esses foram os primeiros contos que escrevi e publiquei de fato.

Livro não recomendado para menores de 18 anos, em virtude dos temas abordados ao longo dos contos (cortesia de _O último abraço_).

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/Literatura/Prosa/Andross/Onde%20tudo%20come%C3%A7ou/Onde-tudo-come%C3%A7ou.pdf?inline=false)

## Poesia

### À Filha que jamais irei parir: uma coletânea de poemas para Maria Madalena

![à-filha-que-jamais-irei-parir](/assets/Publicações/AFJIP-01.jpg)

**À Filha que jamais irei parir: Uma coletânea de poemas para Maria Madalena** é uma coletânea de sonetos dedicados a uma série de sonhos que tive entre setembro e novembro de dois mil e vinte e dois. Como pessoa não-binária, dei a “sorte” de nascer com um corpo inadequado a um desejo que me tomou de assalto já faz uns dois anos: parir uma criança. Engraçado como antes eu considerava isso um exercício de vaidade, mas com o avançar dos anos, o proverbial _relógio biológico_ tem me cobrado isso. Não apenas ter um filho, mas _parir_ um.

Então vieram os sonhos. Sonhei com a Filha que eu jamais irei parir, Maria Madalena. Aos poucos, ela foi se revelando para mim e se integrando cada vez mais ao meu _Oneiros_ – o espaço onírico pessoal que cada pessoa tem, de acordo com os gregos clássicos. Mas não bastava sonhar com ela, era preciso _trazê-la_ à realidade. É à Madá que dedico estes sonetos, ela que me inspirou, que me acompanha, que me conforta, mesmo que apenas durante as horas em que ocupo os _Domínios de Morpheus_. Alguns dos sonetos são de autoria dela, para quem emprestei a mão apenas para que ela própria pudesse se expressar. Ficará ao encargo do leitor descobrir quais são os sonetos dela e quais são os meus.

**À Filha que jamais irei parir: Uma coletânea de poemas para Maria Madalena** é dedicado a todas as mulheres, não importa se cis ou trans, que, assim como eu, por algum motivo jamais realizarão o sonho de gerar vida em seus ventres. A todas vocês, com quem compartilho essa dor, desejo que pelo menos no Sonhar vocês possam encontrar seus filhos tão desejados, assim como eu encontrei minha doce, pequena e adorável Maria Madalena.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/Literatura/Poesia/Projetos%20Prontos/%C3%80%20filha%20que%20jamais%20irei%20parir/%C3%80-Filha-que-jamais-irei-parir.pdf?inline=false)

### Pequeno Cancioneiro: uma breve coletânea de poemas católicos

![pequeno-cancioneiro](/assets/Publicações/PC-01.jpg)

**Pequeno Cancioneiro: Uma breve coletânea de poemas católicos** reúne dezoito poemas inspirados e dedicados à fé católica. Neles, a autora compartilha com o leitor sua visão sobre Nosso Senhor Jesus Cristo, a Virgem Maria e seis santos cuja devoção é bastante popular no Brasil: São Francisco de Assis, São Judas Tadeu, São Caetano de Thiene, Santa Bárbara, Santa Ana e Santa Sara Kali.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/Literatura/Poesia/Projetos%20Prontos/Pequeno%20Cancioneiro/pequeno-cancioneiro.pdf?inline=false)

### A Canção do Cancioneiro e outros poemas: uma antologia de memórias, sorrisos e lágrimas

![cancao-do-cancioneiro](/assets/Publicações/CC-001.jpg)

Em **A Canção do Cancioneiro e outros poemas: uma antologia de memórias, sorrisos e lágrimas**, você encontrará uma antologia poética de Lu Cavalheiro. A antologia é dividida em três partes: a _Poética_, com poemas, a maioria escritos em 2017 mas nunca publicados; os _Insta-haikai_, haikais escritos em 2022 e a maioria deles publicados na conta do Instagram da autora; e a titular _Canção do Cancioneiro_, um poema narrando o início, meio e fim do romance entre o titular _Cancioneiro_ e sua amada _Rouxinol_.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/Literatura/Poesia/Projetos%20Prontos/Can%C3%A7%C3%A3o%20do%20Cancioneiro/Can%C3%A7%C3%A3o-do-Cancioneiro.pdf?inline=false)

### Memorial Poético 2022: uma antologia poética de poemas avulsos escritos em 2022

![memorial-poetico](/assets/Publicações/MP2022.jpg)

_Memorial Poético 2022: Uma antologia poética de poemas avulsos escritos em 2022_ reúne diversos poemas que publiquei ao longo do ano de 2022 no Instagram em umúnico livro. Os temas são variados, pois foram poemas escritos de acordo com o sabor do momento, da inspiração recebida da hora (Erato é uma musa bem caprichosa, como os gregos antigos diriam para qualquer um), e do contexto social do meu entorno na época da escrita. Ao lado de cada poema, haverá junto a imagem com a qual a obra foi publicada no Instagram, visto que as artes escolhidas para cada poema se integram (e, às vezes, até complementam) a mensagem que pretendo transmitir com os versos. As imagens serão redimensionadas para as dimensões da página, o que pode gerar algumas distorções – não me julguem por isso.

Coletânea não indicada para menores de 14 anos.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/Literatura/Poesia/Projetos%20Prontos/Memorial%20Po%C3%A9tico%202022/Memorial-Po%C3%A9tico-2022.pdf?inline=false)


## RPG

Nesta seção, você encontrará RPGs de minha autoria. _Roleplaying Games_, ou _Jogos de Contação de Histórias_ – minha tradução preferida do termo – são uma modalidade de entretenimento no qual você (e mais uns amigos, se não for um jogo solo) participam de uma narrativa de improviso, descrevendo as ações de seus personagens e a reação do mundo ao redor deles, mediado por um conjunto de regras. É como jogar um videogame, em uma comparação bem grosseira, mas substituindo o console por sua imaginação.

### Jogos minimalistas ou solo

Aqui você encontrará meus jogos minimalistas, assim chamados porque são pequenos e possuem um conjunto de regras bem simples, bem como meus jogos solo, cuja proposta é permitir uma diversão individual ao jogador.

#### Coin-op RPG: um sistema minimalista genérico

![coin-op-rpg](/assets/Publicações/CORPG.jpg)

**Coin-Op RPG** é um RPG minimalista genérico com a proposta de ser simples, rápido e permitir a narração de qualquer aventura.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/Coin-Op%20RPG/Panfleto%20base/coinop-rpg.pdf?inline=false)

#### Private Eyes: um cenário baseado em Coin-Op RPG

![private-eyes](/assets/Publicações/PERPG.jpg)

Em **Private Eyes**, os personagens fazem parte de uma agência de detetives particulares na Londres vitoriana.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/Coin-Op%20RPG/Aventuras/Private%20Eyes/private-eyes.pdf?inline=false)

#### De volta à Arena de Escambópolis

![de-volta-a-arena-de-escambópolis](/assets/Publicações/DVAE-1.jpg)

**De volta à Arena de Escambópolis** é um jogo solo ou mano-a-mano de combate entre dois gladiadores em um futuro pós-apocalíptico.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/De%20volta%20%C3%A0%20Arena%20em%20Escamb%C3%B3polis/De-volta-%C3%A0-Arena-em-Escamb%C3%B3polis.pdf?inline=false)

#### Dungeon Explorers

![dungeon-explorers](/assets/Publicações/DERPG.jpg)

Em **Dungeon Explorers**, os Jogadores serão os _Aventureiros_, que se embrenharão em uma masmorra ancestral cheia de perigos e monstros em busca do _Tesouro Lendário_! Além dos _Jogadores_, haverá o _Mestre da Masmorra_, ou MM, que narrará a aventura aos _Aventureiros_ e dirá a eles o que eles encontram e quais perigos eles devem enfrentar.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/Dungeon%20Explorers/Portugu%C3%AAs/Panfleto%20Base/Dungeon-Explorers.pdf?inline=false)

#### Fast Heros D6

![fast-heroes-d6](/assets/Publicações/FHD6.jpg)

Em **Fast Heroes D6**, seu personagem é um super-herói como aqueles dos quadrinhos ou da televisão, pronto e à disposição para salvar o mundo de super-vilões ou outras ameaças.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/Fast%20Heroes/Portugu%C3%AAs/Panfleto%20Base/Fast-Heroes-D6.pdf?inline=false)

#### Casinha RPG

![casinha-rpg](/assets/Publicações/CARPG.png)

**Casinha RPG** é um RPG minimalista sobre crianças brincando de casinha. Como somos todos adultos (espero!), não há a necessidade de uma pessoa exclusivamente sendo o Narrador, logo todos os Jogadores são Narradores ao mesmo tempo.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/Lasers%20and%20Feelings%20hacks/Casinha%20RPG/Casinha-RPG.pdf?inline=false)

#### Zombocalypse

![zombocalypse](/assets/Publicações/ZARPG.jpg)

Em **Zombocalypse**, vocês são um dos últimos grupos de sobreviventes de um apocalipse zumbi que dizimou a **Cidade**. Sua missão agora é sobreviver até conseguir escapar desse caos... **se** vocês conseguirem escapar.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/Lasers%20and%20Feelings%20hacks/Zombocalypse/Zombocalypse.pdf?inline=false)

#### Viva la Revolución!

![viva-la-revolucion](/assets/Publicações/VlRRPG.jpg)

Em **Viva la Revolución!**, os personagens serão **Rebeldes** contra o ditador de um país cyberpunk. Entretanto, o país é uma piada, uma verdadeira _república das bananas_, com toda a corrupção e estética esperadas de um Estado policialesco e militarizado porém estereotipamente ineficiente e caricato.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/Viva%20la%20Revoluci%C3%B3n/Portugu%C3%AAs/Panfleto%20Base/Viva-la-Revoluci%C3%B3n.pdf?inline=false)

#### Você já bateu em um fascista hoje?

![voce-ja-bateu-em-um-fascista-hoje](/assets/Publicações/VJBFH.jpg)

**Você já bateu em um fascista hoje?** é um RPG minimalista sem Narrador sobre ser um _pária_ tentando sobreviver em um Estado fascista-teocrático. Pode ser jogado tanto no modo solo quanto em grupo.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/Voc%C3%AA%20j%C3%A1%20bateu%20em%20um%20fascista%20hoje/Voce-ja-bateu-em-um-fascista-hoje.pdf?inline=false)

#### Até que a morte nos una

![até-que-a-morte-nos-una](/assets/Publicações/AMNU-1.jpg)

**Até que a morte nos una** é um jogo solo no qual seu personagem nasceu em uma cultura que possui a tradição de casas seus membros solteiros com pessoas mortas a fim de que eles não vivam sozinhos no pós-vida. No seu caso, você morreu solteiro, e sua família – obviamente sem seu consentimento, já que você estava **morto** – realizou seu casamento com uma outra pessoa, também falecida. Ao chegar no pós-vida, você encontra seu cônjuge, também desnorteado com o fato de ter sido casado à revelia. Como vocês dois lidarão com isso?

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/Jogos%20Solo/At%C3%A9%20que%20a%20morte%20nos%20una/At%C3%A9-que-a-morte-nos-una.pdf?inline=false)

#### Devaneios RPG

![devaneios-rpg](/assets/Publicações/DVRPG.jpg)

**Devaneios RPG** é um jogo solo sobre sonhar acordado e deixar a mente fluir livremente no mundo ao redor dela.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/Jogos%20Solo/Devaneios/devaneios.pdf?inline=false)

#### Ensaio sobre a Dignidade

![ensaio-sobre-a-dignidade](/assets/Publicações/ESD.jpg)

**Ensaio sobre a Dignidade** é um RPG solo minimalista sobre o intervalo entre o acordar e o retorno ao lar do personagem.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/Jogos%20Solo/Ensaio%20sobre%20a%20Dignidade/Portugu%C3%AAs/Panfleto%20Base/Ensaio-sobre-a-Dignidade.pdf?inline=false)

#### Jardineiros e Goblins

![jardineiros-e-goblins](/assets/Publicações/JeG.jpg)

Em **Jardineiros e Goblins**, você é um jardineiro que deve cuidar e proteger suas plantas contra goblins que querem causar uma destruição generalizada em seu pedacinho de chão. Quem são esses goblins? Podem ser espíritos malévolos, entidades "brincalhonas", ou mesmo as crianças perturbadas de sua vizinhança, mas isso não importa: seu jardim está para ser atacado e destruído, e seu objetivo é o proteger dentro de suas capacidades.

[![botao-downloads](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/Jogos%20Solo/Jardineiros%20e%20Goblins/Jardineiros-e-Goblins.pdf?inline=false)

#### Post Scriptum 5

![post-scriptum-5](/assets/Publicações/PS5.jpg)

**Post Scriptum 5** é um jogo minimalista sobre a própria morte. O personagem é um espírito recém-falecido que precisa recuperar memórias de quem ele foi em vida para poder seguir seu caminho para o além-vida.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/Jogos%20Solo/Post%20Scriptum%205/Portugu%C3%AAs/Panfleto%20Base/Post-Scriptum-5.pdf?inline=false)

#### Spotstory

![spotstory](/assets/Publicações/SS-1.jpg)

**Spotstory** é um jogo solo sobre a construção de narrativas usando playlists do Spotfify (ou outro serviço que permita a criação de playlists de músicas) como oráculo para a criação dos fatos narrativos que comporão o roteiro da narrativa a ser construída.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/Jogos%20Solo/Spotstory/Portugu%C3%AAs/Panfleto%20Base/Spotstory.pdf?inline=false)

#### MicroFATE

![microfate](/assets/Publicações/MicroFATE.jpg)

**MicroFATE** é uma simplificação das regras de **Fate RPG** cujo propósito é proporcionar aos jogadores um jogo rápido e ágil sem, no entanto, abrir mão dos elementos característicos do sistema: os **Aspectos** e os **Pontos de Destino**.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/MicroFATE/Portugu%C3%AAs/Panfleto%20Base/MicroFATE-tres-colunas.pdf?inline=false)

#### Porrinha RPG: um RPG de porrinha

![porrinha-rpg](/assets/Publicações/PoRPG.jpg)

**Porrinha RPG: um RPG de porrinha** é um sistema genérico minimalista de RPG que não usa dados para arbitrar o resultado de testes, mas o popular jogo de boteco conhecido no Rio de Janeiro como _Porrinha_.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/Porrinha%20RPG/Jogo%20Base/Porrinha-RPG.pdf?inline=false)

#### Porrinha RPG versão solo

![porrinha-rpg-solo](/assets/Publicações/PoRPGsolo.jpg)

**Porrinha RPG versão solo** é um suplemento para **Porrinha RPG: um RPG de porrinha** que o transforma em um jogo solo.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/Porrinha%20RPG/Vers%C3%A3o%20Solo/Porrinha-RPG-solo.pdf?inline=false)

#### O Gatilho mais Rápido do Oeste

![o-gatilho-mais-rapido-do-oeste](/assets/Publicações/OGmRO.jpg)

Em **O Gatilho mais Rápido do Oeste**, os jogadores, mediados por um **Árbitro**, farão o papel de **Pistoleiros** enfrentando uns aos outros ou a **Pistoleiros não Jogadores** em uma ambientação de faroeste. A escrita deste jogo foi gravada como atividade para o _Mês do RPG Brasileiro 2022_, evento no qual comemoramos a publicação do primeiro RPG brasileiro, Tagmar, acontecida em 21/12/2022. [Confira a playlist das atividades gravadas por mim](https://www.youtube.com/playlist?list=PL44VOrX_-JdPfNHOfEIm0AS7ikOlpp9xN).

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/O%20Gatilho%20mais%20R%C3%A1pido%20do%20Oeste/O-Gatilho-mais-R%C3%A1pido-do-Oeste.pdf?inline=false)

#### Não apertem o botão vermelho!

![nao-apertem-esse-botao](/assets/Publicações/NABV.jpg)

Em **Não apertem o botão vermelho!**, simula-se uma hipotética situação na qual os presidentes dos Estados Unidos da América e da Rússia decidem iniciar uma guerra nuclear entre seus países, situação que certamente destruiria o planeta Terra tal como o conhecemos. Seu personagem é um diplomata apontado pela Organização das Nações Unidas como o último recurso para evitar a catástrofe.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/Jogos%20Solo/N%C3%A3o%20apertem%20o%20bot%C3%A3o%20vermelho/N%C3%A3o-apertem-o-bot%C3%A3o-vermelho.pdf?inline=false)

#### Você foi uma boa criança este ano?

![voce-foi-uma-boa-crianca](/assets/Publicações/VFuBCeA.jpg)

**Você foi uma boa criança este ano?** é um jogo solo no qual você interpretará uma criança sendo confrontada pelo Papai Noel para saber se ela é merecedora do presente que pediu ou não. Para tanto, você precisará de um baralho comum de 54 cartas (as 52 de jogo mais os dois coringas) e a tabela ao final deste texto para servir de oráculo.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/Jogos%20Solo/Voc%C3%AA%20foi%20uma%20boa%20crian%C3%A7a%20este%20ano%3F/Voc%C3%AA-foi-uma-boa-crian%C3%A7a-este-ano.pdf?inline=false)

### Oráculos

Um oráculo para RPG é uma ferramenta para permitir a tomada de decisões narrativas por meio de rolagens ou outros métodos aleatórios.

#### Oráculo de Caronte

![oraculo-de-caronte](/assets/Publicações/OdC.jpg)

O **Oráculo de Caronte** são dois oráculos que usam moedas para realizar a tomada de decisões ou obtenção de respostas.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/Or%C3%A1culo%20de%20Caronte/Or%C3%A1culo-de-Caronte.pdf?inline=false)

### Aventuras

#### A Estação do Medo (Fate Condensado)

![a-estacao-do-medo](/assets/Publicações/AEM-01.jpg)

A _Estação Espacial Lagrange_, uma estação militar de mineração de metano, parou de enviar seus relatórios diários e foi dada como perdida em 2151. Entretanto, ela começou a mandar sinais de socorro em código Morse em 2157. A Federação Terrestre decidiu enviar um grupo de busca e resgate para averiguar o que está acontecendo na estação, composto pelos personagens. Entretanto, os personagens terão de lidar com um mistério _bizarro_...

A aventura _A Estação do Medo: Uma aventura de horror espacial para Fate Condensado_ usa as regras do [Fate Condensado](https://fatesrdbrasil.gitlab.io/fate-srd-brasil/fate-condensado/).

Aventura não recomendada para menores de 14 anos por retratar situações de desespero, horror, bem como cadáveres e corpos mutilados.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/Aventuras%20do%20Cicerone/Volume%201/A%20Esta%C3%A7%C3%A3o%20do%20Medo/A-Esta%C3%A7%C3%A3o-do-Medo.pdf?inline=false)

#### O Tesouro da Viúva de Sangue (Fate Condensado)

![o-tesouro-da-viuva-de-sangue](/assets/Publicações/OTVS-01.jpg)

Lucretia Sforzato foi uma das piratas mais temidas dos Mares Ocidentais. Apelidada de _Viúva de Sangue_, ela impôs seu reino de terror por meio de um hábito macabro: ela não afundava os navios que saqueava nem deixava sobreviventes, mas empalava a tripulação do navio saqueado na amurada e o deixava à deriva. A Marinha Imperial fez de tudo para capturá-la, mas sem sucesso.

Entretanto, assim como ela surgiu do nada, ela desapareceu sem deixar rastros. Não demorou para surgir a lenda de que ela havia enterrado um tesouro, mas todas as buscas se mostraram infrutíferas. Quando quase todos haviam desistido de procurar o tesouro, Eleanora Treblinka apareceu em Hypherion, a maior cidade portuária do Império. Ela se apresentou como imediata do capitão Diophanes Augustus, que alegava ter sido imediato da _Viúva de Sangue_ e tinha o mapa para o tesouro. Ela logo recrutou uma tripulação e partiu para a busca, prometendo a todos uma parte justa do butim – que, se as lendas fossem verdadeiras, seria o suficiente para que todos eles vivessem como reis até o resto de suas vidas.

Vocês, personagens, são parte da tripulação recrutada pela imediata Treblinka. Será que vocês conseguirão achar o _O Tesouro da Viúva de Sangue_?

A aventura _O Tesouro da Viúva de Sangue: Uma aventura de navegação capa-e-espada para Fate Condensado_ usa as regras do [Fate Condensado](https://fatesrdbrasil.gitlab.io/fate-srd-brasil/fate-condensado/).

Aventura não recomendada para menores de 14 anos por retratar zumbis, cadáveres e possível sacrifícios humanos.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/Aventuras%20do%20Cicerone/Volume%201/O%20tesouro%20da%20Vi%C3%BAva%20de%20Sangue/O-Tesouro-da-Vi%C3%BAva-de-Sangue.pdf?inline=false)

#### O Tesouro da Viúva de Sangue (MicroFate)

![tesouro-de-sangue-microfate](/assets/Publicações/TVS-1.jpg)

Lucretia Sforzato foi uma das piratas mais temidas dos Mares Ocidentais. Apelidada de _Viúva de Sangue_, ela impôs seu reino de terror por meio de um hábito macabro: ela não afundava os navios que saqueava nem deixava sobreviventes, mas empalava a tripulação do navio saqueado na amurada e o deixava à deriva. A Marinha Imperial fez de tudo para capturá-la, mas sem sucesso.

Entretanto, assim como ela surgiu do nada, ela desapareceu sem deixar rastros. Não demorou para surgir a lenda de que ela havia enterrado um tesouro, mas todas as buscas se mostraram infrutíferas. Quando quase todos haviam desistido de procurar o tesouro, Eleanora Treblinka apareceu em Hypherion, a maior cidade portuária do Império. Ela se apresentou como imediata do capitão Diophanes Augustus, que alegava ter sido imediato da _Viúva de Sangue_ e tinha o mapa para o tesouro. Ela logo recrutou uma tripulação e partiu para a busca, prometendo a todos uma parte justa do butim – que, se as lendas fossem verdadeiras, seria o suficiente para que todos eles vivessem como reis até o resto de suas vidas.

Vocês, personagens, são parte da tripulação recrutada pela imediata Treblinka. Será que vocês conseguirão achar o _O Tesouro da Viúva de Sangue_?

A aventura _O Tesouro da Viúva de Sangue_ usa as regras do [MicroFATE](https://cicerone.gitlab.io/publicacoes/#microfate)

Aventura não recomendada para menores de 14 anos por retratar zumbis, cadáveres e possível sacrifícios humanos.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/MicroFATE/Portugu%C3%AAs/Aventuras/O%20tesouro%20da%20Vi%C3%BAva%20de%20Sangue/microfate-o-tesouro-da-viuva-de-sangue.pdf?inline=false)

#### O Assalto ao Banco Central (MicroFATE)

![o-assalto-ao-banco-central](/assets/Publicações/MF-ABC-1.jpg)

A **Cidade** é uma grande, bela e próspera metrópole. Ela tem seus problemas, como qualquer cidade tem, mas o maior deles é **Lord Shadowdread**, um supervilão cujos esquemas quase sempre envolvem conquistar a **Cidade** de alguma forma. Ele é a maior ameaça à **Cidade**, e a mais frequente fonte de problemas que os **Heróis** precisam resolver.

Desta vez, os capangas de **Lord Shadowdread** estão assaltando o Banco Central da **Cidade**. A polícia foi chamada, mas ela está sendo incapaz de resolver o problema sozinha, e por isso os **Heróis**, mais uma vez, são necessários para salvar o dia.

**O Assalto ao Banco Central** é uma aventura para **MicroFATE**, que você pode baixar gratuitamente [aqui](https://cicerone.gitlab.io/publicacoes/#microfate).

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/MicroFATE/Portugu%C3%AAs/Aventuras/O%20Assalto%20ao%20Banco%20Central/microfate-o-assalto-ao-banco-central.pdf?inline=false)

#### O portão da Escola Municipal Pandiá Calógeras (Fate Condensado)

![o-portao-da-empc](/assets/Publicações/OpEMPC.jpg)

Os alunos da _Escola Municipal Pandiá Calógeras_, em São Paulo, tem o hábito de realizar um trote no mínimo curioso com os calouros. Sempre que um aluno novo chega na escola, o grupo dos “descolados” o leva para um portão nos fundos do pátio cuja pintura amarela está descascando, as marcas de ferrugem são visíveis e existe uma marca de tinta vermelha em forma de mão. Ninguém sabe o que há atrás do portão, pois nem mesmo o diretor da escola sabe onde a chave está e nunca houve o interesse por o arrombar para resolver o mistério.

Nesta aventura não será diferente. Os jogadores interpretarão alunos calouros da _Escola Municipal Pandiá Calógeras_, e serão levados a realizar o trote, mesmo que seja contra suas vontades. Entretanto, existem certas coisas que nunca deveriam ser mexidas, ou _invocadas_...

A aventura **O portão da Escola Municipal Pandiá Calógeras: uma aventura de horror cósmico para Fate Condensado** usa as regras do [Fate Condensado](https://fatesrdbrasil.gitlab.io/fate-srd-brasil/fate-condensado/).

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/RPG/Aventuras%20do%20Cicerone/Volume%201/O%20port%C3%A3o%20da%20Escola%20Municipal%20Pandi%C3%A1%20Cal%C3%B3geras/O-port%C3%A3o-da-Escola-Municipal-Pandi%C3%A1-Cal%C3%B3geras.pdf?inline=false)

### Materiais de suporte

Aqui você encontrará textos que falam sobre _Jogos de contação de história_, mas não são, eles próprios, jogos.

#### O Paradoxo do 1: um ensaio sobre mecânicas de rolagens de dados em RPGs que contabilizam sucessos e ao mesmo tempo podem ter sucessos anulados

![paradoxo-do-1](/assets/Publicações/P1-1.jpg)

**O Paradoxo do 1: um ensaio sobre mecânicas de rolagem de dados em RPGs que contabilizam sucessos e ao mesmo tempo podem ter sucessos anulados** propõe-se a discorrer sobre um problema comum em sistemas de RPG que usam a mecânica de sucessos com anulação de resultados para mensurar o quão bem o personagem foi bem sucedido na ação pretendida. O uso da mecânica de sucessos, na qual cada dado é avaliado separadamente e apenas os que apresentam valor igual ao maior a um dado número alvo, comumente chamado _dificuldade_, e então contam-se os sucessos, aliado ao uso da mecânica de anulação de resultados, na qual um dado valor subtrai o total de sucessos obtidos, cria uma situação mecanicamente desconfortável: quanto mais dados lançados em um determinado teste, menores são as chances de se obter sucessos. Para descrever essa situação, cunhei o termo _Paradoxo do 1_, visto que sistemas que usam essas duas mecânicas concomitantemente atribuem ao resultado 1 no dado o papel de subtrair o número de sucessos obtidos.

[![botao-download](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/Literatura/Ensaios%20e%20Textos%20Acad%C3%AAmicos/O%20Paradoxo%20do%201/o-paradoxo-do-um.pdf?inline=false)



# Publicações de outros autores

Aqui é um espaço reservado para trabalhos com os quais eu contribuí, seja como revisora, diagramadora ou editora, e o autor me concedeu autorização para colocar neste site.

## Maína Paloma

Caso você queira contribuir com o trabalho de Maína Paloma, você pode mandar um Pix para ela.

<details>
  <summary markdown="span">Clique aqui para ver a chave Pix de Maína Paloma</summary>
  
  maina-paloma@hotmail.com
</details>

### A Longa Noite

**A Longa Noite** é uma coletânea de horror pessoal passado em um mundo de fantasia urbana sombria perturbadoramente similar ao nosso.

#### Parte I: A Filha Pródiga

![a-filha-prodiga](/assets/Publicações/AFP-01.jpg)

Winter sempre se sentiu desassociada da _Noite_, o mundo onde sua irmã gêmea, Summer, se encaixava melhor do que qualquer um; o mundo de seus pais; o mundo dos _Filhos da Noite_, como os vampiros se chamam. Ao voltar para casa de seus pais, atendendo a um pedido da irmã para participar da recepção de seus Tios vindos de outra Corte, Winter acaba decidindo lidar com seus problemas e consertar as suas relações familiares.

No entanto, ela acaba descobrindo mais semelhanças com a irmã do que sabia que tinha, e se vê em conflito tentando escapar de seu destino...

[![botao-download.jpg](/assets/Publicações/botao-download.jpg)](https://gitlab.com/cicerone/cicerone-originais/-/raw/master/Encomendas/Ma%C3%ADna/A%20Longa%20Noite/A%20Filha%20Pr%C3%B3diga/A-Filha-Pr%C3%B3diga.pdf?inline=false)

---

