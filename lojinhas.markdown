---
layout: page
title: EPUB Kindle
permalink: /epub-kindle/
---

Minhas publicações literárias em formato EPUB Kindle estão disponíveis no [Amazon Kindle](https://www.amazon.com.br/s?i=digital-text&rh=p_27%3ALu+Cavalheiro&s=relevancerank&text=Lu+Cavalheiro&ref=dp_byline_sr_ebooks_1). Veja a lista completa:

+ **A Dama de Mil Faces (versão "Fogo de Prometeu")** (português), um conto de investigação em um mundo de fantasia árabe.

---

